using XKit.Lib.Common.Fabric;
using XKit.Lib.Host;
using XKit.Lib.Common.Client;
using XKit.Lib.Testing;

namespace SystemTests._NAMESPACE.Tests {

    public class TestBase {
        protected IDependencyConnector DependencyConnector => 
            HostEnvironmentHelper.DependencyConnector;
        protected string FabricId => 
            HostEnvironmentHelper.Host?.FabricId;

        protected static void ClassInit() {

            TestHostHelper.Initialize();

            TestHostHelper.AddService(Svc1.Service.Svc1ServiceFactory.Create());            

            TestHostHelper.StartHost();       
        }

        protected static void ClassTeardown() {
            TestHostHelper.DestroyHost();
        }

        protected T CreateClient<T>(IServiceClientFactory<T> factory, ServiceCallTypeParameters callTypeParameters = null) {
            return factory.CreateServiceClient(
                clientParameters: ServiceClientParameters.CreateForConsumer(
                    requestorFabricId: FabricId, 
                    defaultCorrelationId: XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                    dependencyConnector: DependencyConnector
                ), 
                defaultCallTypeParameters: callTypeParameters ?? ServiceCallTypeParameters.SyncResult()
            );
        }
    }
}