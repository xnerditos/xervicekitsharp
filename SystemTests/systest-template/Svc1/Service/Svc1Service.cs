using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Config;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Utility;

namespace SystemTests._NAMESPACE.Svc1.Service {

	public class Svc1Service 
        : ManagedService<Svc1Operation>, ISvc1Service {

		private static readonly IReadOnlyDescriptor descriptor = Constants.ServiceDescriptor;
		
		private SetOnceOrThrow<IConfigReader<Svc1Config>> configReader = new SetOnceOrThrow<IConfigReader<Svc1Config>>();

		private IConfigReader<Svc1Config> ConfigReader {
            get => configReader.Value;
            set => configReader.Value = value;
        }

		// =====================================================================
		// overrides
		// =====================================================================

		protected override IReadOnlyDescriptor Descriptor => descriptor;

		// =====================================================================
		// construction
		// =====================================================================

		public Svc1Service(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
		) : base(localEnvironment, dependencyConnector) { }
		// TODO:
        //     this.OnServiceStartEvent += this.OnServiceStart;
        //     this.OnServiceStopEvent += this.OnServiceStop;
		// }

		// // =====================================================================
		// // ISvc1Service
		// // =====================================================================

        // async Task<Svc1Config> ISvc1Service.GetConfig(Svc1Config defaultValue) {
        //     return await ConfigReader.GetConfig(defaultValue);
        // }
		
        // // =====================================================================
        // // Events
        // // =====================================================================
        
        // private void OnServiceStart(IRuntimeMonitor monitor) {

		// 	ConfigReader = AddFeatureConfigurable<Svc1Config>();	
        // }

        // private void OnServiceStop(IRuntimeMonitor monitor) { }
	}
}