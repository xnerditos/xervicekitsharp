using XKit.Lib.Common.Fabric;
using XKit.Lib.Host;
using XKit.Lib.Common.Client;
using XKit.Lib.Testing;

namespace SystemTests.Daemons.Tests {

    public class TestBase {
        protected IDependencyConnector DependencyConnector => 
            HostEnvironmentHelper.DependencyConnector;
        protected string FabricId => 
            HostEnvironmentHelper.Host?.FabricId;

        protected static void ClassInit() {

            TestHostHelper.Initialize(useDaemonDebugMode: false);
            TestHostHelper.AddService(
                SvcWithAutoMessaging.Service.SvcWithAutoMessagingServiceFactory.Create()
            );
            TestHostHelper.StartHost();
        }

        protected static void ClassTeardown() {

            TestHostHelper.DestroyHost();
        }

        protected T CreateClient<T>(IServiceClientFactory<T> factory, ServiceCallTypeParameters callTypeParameters = null) {
            return factory.CreateServiceClient(
                clientParameters: ServiceClientParameters.CreateForConsumer(
                    requestorFabricId: FabricId, 
                    defaultCorrelationId: XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                    dependencyConnector: DependencyConnector
                ), 
                defaultCallTypeParameters: callTypeParameters ?? ServiceCallTypeParameters.SyncResult()
            );
        }
    }
}