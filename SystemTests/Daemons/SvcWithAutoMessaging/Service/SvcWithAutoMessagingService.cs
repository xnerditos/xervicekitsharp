using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;

namespace SystemTests.Daemons.SvcWithAutoMessaging.Service {

	public class SvcWithAutoMessagingService 
        : ManagedService<SvcWithAutoMessagingOperation>, ISvcWithAutoMessagingService {

		// =====================================================================
		// overrides
		// =====================================================================

		protected override IReadOnlyDescriptor Descriptor => Constants.ServiceDescriptor;

		// =====================================================================
		// construction
		// =====================================================================

		public SvcWithAutoMessagingService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
		) : base(localEnvironment, dependencyConnector) { 
            AddDaemon(new SvcWithAutoMessagingDaemon());
		}
	}
}