using System.Threading.Tasks;
using XKit.Lib.Common.Host;

namespace SystemTests.Daemons.SvcWithAutoMessaging.Service {

    public interface ISvcWithAutoMessagingService : IManagedService, IServiceBase {
    }
}