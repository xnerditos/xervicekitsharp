
namespace SystemTests.Daemons.SvcWithAutoMessaging.Entities {
    
    public class DaemonMessage {

        public uint Ticks { get; set; }
        public string Message { get; set; }
    }
}