
namespace SystemTests.Daemons.SvcWithGenericTimer.Entities {
    public class TestValueResponse {
        public string TheIncomingValue { get; set; }
        public string RandomValue { get; set; }
    }
}