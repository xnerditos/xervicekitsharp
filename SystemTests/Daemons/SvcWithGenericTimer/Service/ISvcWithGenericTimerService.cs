using XKit.Lib.Common.Host;

namespace SystemTests.Daemons.SvcWithGenericTimer.Service {

    public interface ISvcWithGenericTimerService : IManagedService, IServiceBase {
    }
}