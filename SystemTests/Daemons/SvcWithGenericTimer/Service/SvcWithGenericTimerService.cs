using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Host.Services;

namespace SystemTests.Daemons.SvcWithGenericTimer.Service {

	public class SvcWithGenericTimerService 
        : ManagedService<SvcWithGenericTimerOperation>, ISvcWithGenericTimerService {

		private static readonly IReadOnlyDescriptor descriptor = Constants.ServiceDescriptor;
		
		// =====================================================================
		// overrides
		// =====================================================================

		protected override IReadOnlyDescriptor Descriptor => descriptor;

		// =====================================================================
		// construction
		// =====================================================================

		public SvcWithGenericTimerService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
		) : base(localEnvironment, dependencyConnector) { 
            AddDaemon(
                new GenericTimerDaemon<SvcWithGenericTimerDaemonOperation>(
                    timerDelayMilliseconds: 1000
            ));
        }	
    }
}