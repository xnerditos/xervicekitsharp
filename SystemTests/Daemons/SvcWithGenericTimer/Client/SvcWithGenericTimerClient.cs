using System.Threading.Tasks;
using SystemTests.Daemons.SvcWithGenericTimer.Entities;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;
using XKit.Lib.Connector.Service;

namespace SystemTests.Daemons.SvcWithGenericTimer.Client {

    internal class SvcWithGenericTimerClient : ServiceClientBase<ISvcWithGenericTimerApi>, ISvcWithGenericTimerApi {

        public SvcWithGenericTimerClient(
            IReadOnlyDescriptor dependency,
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters
        ) : base(
            dependency,
            clientParameters,
            defaultCallTypeParameters
        ) { }

        // =====================================================================
        // overridables
        // =====================================================================

        protected override IReadOnlyDescriptor ServiceDescriptor => Constants.ServiceDescriptor;

    }
}