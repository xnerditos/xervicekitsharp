using XKit.Lib.Common.Host;

namespace SystemTests.ServiceCalls.SvcSendsMessages.Service {

    public interface ISvcSendsMessagesService : IManagedService, IServiceBase {}
}