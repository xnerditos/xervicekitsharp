using System.Threading.Tasks;
using SystemTests.ServiceCalls.SvcSendsMessages.Entities;
using SystemTests.ServiceCalls.SvcSimple.Entities;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;
using XKit.Lib.Connector.Service;

namespace SystemTests.ServiceCalls.SvcSendsMessages.Client {

    internal class SvcSendsMessagesClient : ServiceClientBase<ISvcSendsMessagesApi>, ISvcSendsMessagesApi {

        public SvcSendsMessagesClient(
            IReadOnlyDescriptor dependency,
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters
        ) : base(
            dependency,
            clientParameters,
            defaultCallTypeParameters
        ) { }

        // =====================================================================
        // overridables
        // =====================================================================

        protected override IReadOnlyDescriptor ServiceDescriptor => Constants.ServiceDescriptor;

        // =====================================================================
        // ISvcSimple
        // =====================================================================

        Task<ServiceCallResult> ISvcSendsMessagesApi.RaisesEvent1(
            Message request
        ) => ExecuteCall<Message>(request);

        Task<ServiceCallResult> ISvcSendsMessagesApi.RaisesEvent2(
            Message request
        ) => ExecuteCall<Message>(request);

        Task<ServiceCallResult> ISvcSendsMessagesApi.IssuesCommand1(
            Message request
        ) => ExecuteCall<Message>(request);

        Task<ServiceCallResult> ISvcSendsMessagesApi.IssuesCommand2(
            Message request
        ) => ExecuteCall<Message>(request);

        Task<ServiceCallResult> ISvcSendsMessagesApi.IssuesCommand1AndWaitsForFinish(
            Message request
        ) => ExecuteCall<Message>(request);
    }
}