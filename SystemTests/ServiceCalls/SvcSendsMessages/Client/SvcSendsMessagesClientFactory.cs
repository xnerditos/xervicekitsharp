using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;

namespace SystemTests.ServiceCalls.SvcSendsMessages.Client {
    
	public interface ISvcSendsMessagesClientFactory : IServiceClientFactory<ISvcSendsMessagesApi> {	}

	public class SvcSendsMessagesClientFactory : ISvcSendsMessagesClientFactory {
		private static ISvcSendsMessagesClientFactory factory = new SvcSendsMessagesClientFactory();

		public static ISvcSendsMessagesClientFactory Factory => factory;

        // =====================================================================
        // IServiceClientFactory<IRegistryClient>
        // =====================================================================

		ISvcSendsMessagesApi IServiceClientFactory<ISvcSendsMessagesApi>.CreateServiceClient(
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters
		) => new SvcSendsMessagesClient(
				Constants.ServiceDescriptor,
				clientParameters,
                defaultCallTypeParameters
			);

		IReadOnlyDescriptor IServiceClientFactory.Descriptor => Constants.ServiceDescriptor;
		
        // =====================================================================
        // Static methods
        // =====================================================================

        public static ISvcSendsMessagesApi CreateServiceClient(
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters = null
        ) => SvcSendsMessagesClientFactory.Factory.CreateServiceClient(
				clientParameters,
                defaultCallTypeParameters
		);

        public static void InjectCustomFactory(ISvcSendsMessagesClientFactory factory) =>
            SvcSendsMessagesClientFactory.factory = factory; 
	}
}