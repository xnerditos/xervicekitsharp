using System.Threading.Tasks;
using XKit.Lib.Common.Host;

namespace SystemTests.ServiceCalls.SvcWithDependency1.Service {

    public interface ISvcWithDependency1Service : IManagedService, IServiceBase {
    }
}