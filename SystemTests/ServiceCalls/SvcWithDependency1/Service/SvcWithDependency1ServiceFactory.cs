using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;
using XKit.Lib.Testing.RegistrySvc.Service;

namespace SystemTests.ServiceCalls.SvcWithDependency1.Service {

	public interface ISvcWithDependency1ServiceFactory : ITestServiceFactory {}

	public class SvcWithDependency1ServiceFactory : ISvcWithDependency1ServiceFactory
	{
		private static ISvcWithDependency1ServiceFactory factory = new SvcWithDependency1ServiceFactory();

		public static ISvcWithDependency1ServiceFactory Factory => factory;

        // =====================================================================
        // ISvcWithDependency1ServiceFactory
        // =====================================================================

		IManagedService ITestServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new SvcWithDependency1Service(localEnvironment, dependencyConnector);
        } 

        IReadOnlyDescriptor IServiceFactory.Descriptor => Constants.ServiceDescriptor;

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => SvcWithDependency1ServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(ISvcWithDependency1ServiceFactory factory) =>
            SvcWithDependency1ServiceFactory.factory = factory; 
	}
}