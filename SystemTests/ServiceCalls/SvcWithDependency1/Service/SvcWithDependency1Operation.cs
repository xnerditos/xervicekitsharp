using System.Threading.Tasks;
using SystemTests.ServiceCalls.SvcWithDependency1.Entities;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Host.DefaultBaseClasses;
using FluentAssertions;
using XKit.Lib.Common.Client;

namespace SystemTests.ServiceCalls.SvcWithDependency1.Service {

    public partial class SvcWithDependency1Operation : ServiceOperation<ISvcWithDependency1Service>, ISvcWithDependency1 {

        public SvcWithDependency1Operation(
            ServiceOperationContext context
        ) : base(context) { }


        // =====================================================================
        // ISvcWithDependency1.GetTestValueWithDependency2Levels
        // =====================================================================
        async Task<ServiceCallResult<TestValueResponse>> ISvcWithDependency1.GetTestValueWithDependency2Levels(TestValueRequest request) {
            return await RunServiceCall(
                request,
                operationAction: async (r) => {
                    var svcWithDep2 = SvcWithDependency2.Client.SvcWithDependency2ClientFactory.CreateServiceClient(
                        clientParameters: ServiceClientParameters.CreateForHost(this.Context, this.Monitor),
                        defaultCallTypeParameters: ServiceCallTypeParameters.SyncResult()
                    );
                    var result = await svcWithDep2.GetTestValueWithDependency1Level(new SvcWithDependency2.Entities.TestValueRequest {
                        TheValue = r.TheValue
                    });
                    
                    result?.ResponseBody.Should().NotBeNull();
                    result.ImmediateSuccess.Should().BeTrue();

                    return new TestValueResponse {
                        RandomValue = result.ResponseBody.RandomValue,
                        TheIncomingValue = result.ResponseBody.TheIncomingValue
                    };
                }
            );
        }

        // =====================================================================
        // ISvcWithDependency1.ChangeStaticValueWithDependency2Levels
        // =====================================================================
        async Task<ServiceCallResult> ISvcWithDependency1.ChangeStaticValueWithDependency2Levels(TestValueRequest request) {
            return await RunServiceCall(
                request,
                operationAction: async (r) => {
                    var svcWithDep2 = SvcWithDependency2.Client.SvcWithDependency2ClientFactory.CreateServiceClient(
                        clientParameters: ServiceClientParameters.CreateForHost(this.Context, this.Monitor),
                        defaultCallTypeParameters: ServiceCallTypeParameters.FireAndForget()
                    );
                    var result = await svcWithDep2.ChangeStaticValueWithDependency1Level(new SvcWithDependency2.Entities.TestValueRequest {
                        TheValue = r.TheValue
                    });
                }
            );
        }
    }
}