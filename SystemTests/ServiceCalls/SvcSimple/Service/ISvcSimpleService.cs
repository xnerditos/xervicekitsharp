using System.Threading.Tasks;
using XKit.Lib.Common.Host;

namespace SystemTests.ServiceCalls.SvcSimple.Service {

    public interface ISvcSimpleService : IManagedService, IServiceBase {}
}