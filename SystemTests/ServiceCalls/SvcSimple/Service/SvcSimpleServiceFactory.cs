using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;
using XKit.Lib.Testing.RegistrySvc.Service;

namespace SystemTests.ServiceCalls.SvcSimple.Service {

	public interface ISvcSimpleServiceFactory : ITestServiceFactory {}

	public class SvcSimpleServiceFactory : ISvcSimpleServiceFactory
	{
		private static ISvcSimpleServiceFactory factory = new SvcSimpleServiceFactory();

		public static ISvcSimpleServiceFactory Factory => factory;

        // =====================================================================
        // ISvcSimpleServiceFactory
        // =====================================================================

		IManagedService ITestServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new SvcSimpleService(localEnvironment, dependencyConnector);
        } 

        IReadOnlyDescriptor IServiceFactory.Descriptor => Constants.ServiceDescriptor;

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => SvcSimpleServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(ISvcSimpleServiceFactory factory) =>
            SvcSimpleServiceFactory.factory = factory; 
	}
}