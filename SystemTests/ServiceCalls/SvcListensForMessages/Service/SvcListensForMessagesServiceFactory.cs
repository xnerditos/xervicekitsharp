using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;

namespace SystemTests.ServiceCalls.SvcListensForMessages.Service {

	public interface ISvcListensForMessagesServiceFactory : ITestServiceFactory {}

	public class SvcListensForMessagesServiceFactory : ISvcListensForMessagesServiceFactory {
		private static ISvcListensForMessagesServiceFactory factory = new SvcListensForMessagesServiceFactory();

		public static ISvcListensForMessagesServiceFactory Factory => factory;

        // =====================================================================
        // IMockServiceFactory
        // =====================================================================

		IManagedService ITestServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new SvcListensForMessagesService(localEnvironment, dependencyConnector);
        } 

        // =====================================================================
        // IServiceFactory
        // =====================================================================

        IReadOnlyDescriptor IServiceFactory.Descriptor => Constants.ServiceDescriptor;
        
        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => SvcListensForMessagesServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(ISvcListensForMessagesServiceFactory factory) =>
            SvcListensForMessagesServiceFactory.factory = factory; 
	}
}