using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Config;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Utility;

namespace SystemTests.ServiceCalls.SvcListensForMessages.Service {

    public interface ISvcListensForMessagesService : IManagedService, IServiceBase { 
        string TestValue { get; set; }
        string MessageName { get; set; } 
    }

	public class SvcListensForMessagesService 
        : ManagedService<SvcListensForMessagesOperation>, ISvcListensForMessagesService {

		private static readonly IReadOnlyDescriptor descriptor = Constants.ServiceDescriptor;
		
		protected override IReadOnlyDescriptor Descriptor => descriptor;

        public string TestValue { get; set; }
        public string MessageName { get; set; }

        // =====================================================================
        // construction
        // =====================================================================

        public SvcListensForMessagesService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
		) : base(localEnvironment, dependencyConnector) { 
            AddEventSubscription<SvcSendsMessages.TestEvents>(x => x.Event1(null));
            AddCommandSubscription<SvcSendsMessages.TestCommands>(x => x.Command1(null));
            AddCommandSubscription<SvcSendsMessages.TestCommands>(x => x.Command2(null));
        }
	}
}