using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;

namespace SystemTests.ServiceCalls.SvcGeneric.Client {
    
	public interface ISvcGenericClientFactory : IServiceClientFactory<ISvcGenericApi> {	}

	public class SvcGenericClientFactory : ISvcGenericClientFactory {
		private static ISvcGenericClientFactory factory = new SvcGenericClientFactory();

		public static ISvcGenericClientFactory Factory => factory;

        // =====================================================================
        // IServiceClientFactory<IRegistryClient>
        // =====================================================================

		ISvcGenericApi IServiceClientFactory<ISvcGenericApi>.CreateServiceClient(
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters
		) => new SvcGenericClient(
				Constants.ServiceDescriptor,
				clientParameters,
                defaultCallTypeParameters
			);

		IReadOnlyDescriptor IServiceClientFactory.Descriptor => Constants.ServiceDescriptor;
		
        // =====================================================================
        // Static methods
        // =====================================================================

        public static void InjectCustomFactory(ISvcGenericClientFactory factory) =>
            SvcGenericClientFactory.factory = factory; 
	}
}