using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;
using XKit.Lib.Testing.RegistrySvc.Service;

namespace SystemTests.ServiceCalls.SvcCallbackReceiver.Service {

	public interface ISvcCallbackReceiverServiceFactory : ITestServiceFactory { }

	public class SvcCallbackReceiverServiceFactory : ISvcCallbackReceiverServiceFactory
	{
		private static ISvcCallbackReceiverServiceFactory factory = new SvcCallbackReceiverServiceFactory();

		public static ISvcCallbackReceiverServiceFactory Factory => factory;

        // =====================================================================
        // ISvcCallbackReceiverServiceFactory
        // =====================================================================

		IManagedService ITestServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new SvcCallbackReceiverService(localEnvironment, dependencyConnector);
        } 

        IReadOnlyDescriptor IServiceFactory.Descriptor => Constants.ServiceDescriptor;

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => SvcCallbackReceiverServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(ISvcCallbackReceiverServiceFactory factory) =>
            SvcCallbackReceiverServiceFactory.factory = factory; 
	}
}