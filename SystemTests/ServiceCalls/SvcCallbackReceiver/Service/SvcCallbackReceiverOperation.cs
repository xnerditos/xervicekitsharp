using System.Threading.Tasks;
using SystemTests.ServiceCalls.SvcCallbackReceiver.Entities;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Host.DefaultBaseClasses;
using SystemTests.ServiceCalls.Environment;

namespace SystemTests.ServiceCalls.SvcCallbackReceiver.Service {

    public partial class SvcCallbackReceiverOperation : ServiceOperation<ISvcCallbackReceiverService>, ISvcCallbackReceiver {

        public class SavedData {
            public string OriginalCallerFabricId = null;
            public TestValueResponse TestResponse = null;
        }

        public SvcCallbackReceiverOperation(
            ServiceOperationContext context
        ) : base(context) { }


        // ---------------------------------------------------------------------
        // ISvcCallbackReceiver.ReceiveTestValueCallback
        // ---------------------------------------------------------------------
        public async Task<ServiceCallResult> ReceiveTestValueCallback(ServiceCallResult<TestValueResponse> callbackResult) {
            return await RunServiceCall(
                callbackResult,
                operationAction: (cr) => {
                    ValueHelper.SaveJsonTestData(
                        new SavedData {
                            OriginalCallerFabricId = cr.RequestorFabricId,
                            TestResponse = cr.ResponseBody
                        }
                    );
                    return Task.CompletedTask;
                }
            );
        }

        // ---------------------------------------------------------------------
        // ISvcCallbackReceiver.ReceiveTestValueCallback
        // ---------------------------------------------------------------------
        public async Task<ServiceCallResult> ReceiveTestValueCallbackUsingSlimResult(ServiceCallbackSlimResult<TestValueResponse> callbackResult) {
            return await RunServiceCall(
                callbackResult,
                operationAction: (cr) => {
                    ValueHelper.SaveJsonTestData(
                        new SavedData {
                            OriginalCallerFabricId = null,
                            TestResponse = cr.ResponseBody
                        }
                    );
                    return Task.CompletedTask;
                }
            );
        }
    }
}