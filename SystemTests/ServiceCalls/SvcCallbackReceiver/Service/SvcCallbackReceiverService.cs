using System.Collections.Generic;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Utility;

namespace SystemTests.ServiceCalls.SvcCallbackReceiver.Service {

	public class SvcCallbackReceiverService 
        : ManagedService<SvcCallbackReceiverOperation>, ISvcCallbackReceiverService {

		private static readonly IReadOnlyDescriptor descriptor = Constants.ServiceDescriptor;
		
		// =====================================================================
		// overrides
		// =====================================================================

		protected override IReadOnlyDescriptor Descriptor => descriptor;

        protected override IEnumerable<IReadOnlyDescriptor> Dependencies => new IReadOnlyDescriptor[0];

        protected override IReadOnlyServiceCallPolicy CallPolicy => DefaultPolicy;

        protected override string ConfigurationDocumentIdentifier => Identifiers.GetServiceVersionLevelKey(descriptor);

		// =====================================================================
		// construction
		// =====================================================================

		public SvcCallbackReceiverService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
		) : base(localEnvironment, dependencyConnector) {}
    }
}