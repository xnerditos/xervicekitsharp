using XKit.Lib.Common.Host;

namespace SystemTests.ServiceCalls.SvcCallbackReceiver.Service {

    public interface ISvcCallbackReceiverService : IManagedService, IServiceBase {}
}