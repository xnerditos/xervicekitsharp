
namespace SystemTests.ServiceCalls.SvcCallbackReceiver.Entities {
    public class TestValueResponse {
        public string TheIncomingValue { get; set; }
        public string RandomValue { get; set; }
    }
}