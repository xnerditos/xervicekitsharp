
namespace SystemTests.ServiceCalls.SvcWithDependency2.Entities {
    public class TestValueResponse {
        public string TheIncomingValue { get; set; }
        public string RandomValue { get; set; }
    }
}