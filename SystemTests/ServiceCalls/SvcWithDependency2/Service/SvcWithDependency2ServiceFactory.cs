using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;
using XKit.Lib.Testing.RegistrySvc.Service;

namespace SystemTests.ServiceCalls.SvcWithDependency2.Service {

	public interface ISvcWithDependency2ServiceFactory : ITestServiceFactory { }

	public class SvcWithDependency2ServiceFactory : ISvcWithDependency2ServiceFactory
	{
		private static ISvcWithDependency2ServiceFactory factory = new SvcWithDependency2ServiceFactory();

		public static ISvcWithDependency2ServiceFactory Factory => factory;

        // =====================================================================
        // ISvcWithDependency2ServiceFactory
        // =====================================================================

		IManagedService ITestServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new SvcWithDependency2Service(localEnvironment, dependencyConnector);
        } 

        IReadOnlyDescriptor IServiceFactory.Descriptor => Constants.ServiceDescriptor;
        
        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => SvcWithDependency2ServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(ISvcWithDependency2ServiceFactory factory) =>
            SvcWithDependency2ServiceFactory.factory = factory; 
	}
}