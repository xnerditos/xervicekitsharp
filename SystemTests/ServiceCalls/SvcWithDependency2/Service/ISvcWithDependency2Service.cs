using System.Threading.Tasks;
using XKit.Lib.Common.Host;

namespace SystemTests.ServiceCalls.SvcWithDependency2.Service {

    public interface ISvcWithDependency2Service : IManagedService, IServiceBase { }
}