using System;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SystemTests.ServiceCalls.Environment;
using XKit.Lib.Common.Log;

namespace SystemTests.ServiceCalls.Tests {

    [TestClass]
    public class Callback : TestBase {

        [ClassInitialize]
        public static void Initialize(TestContext context) { TestBase.ClassInit(); }

        [ClassCleanup]
        public static void Teardown() { TestBase.ClassTeardown(); }

        [TestMethod]
        public async Task ServiceCallWithCallbackHappyPath() {
            string testValue = Guid.NewGuid().ToString();
            
            var client = CreateClient(
                SvcSimple.Client.SvcSimpleClientFactory.Factory,
                ServiceCallTypeParameters.Callback(
                    SvcCallbackReceiver.Constants.ServiceDescriptor,
                    null,
                    nameof(SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.ReceiveTestValueCallback)
                ));

            ValueHelper.ClearJsonTestData();

            var result = await client.GetTestValueNoDependencies(new SvcSimple.Entities.TestValueRequest {
                TheValue = testValue
            });
            
            result.ServiceCallStatus.Should().Be(ServiceCallStatusEnum.Completed);
            result.OperationStatus.Should().Be(JobResultStatusEnum.Pending);
            Thread.Sleep(1000);
            var testData = 
                ValueHelper.GetJsonTestData<SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.SavedData>();
            
            testData.OriginalCallerFabricId
                .Should().Be(FabricId);
            testData.TestResponse.TheIncomingValue
                .Should().Be(testValue);
        }

        [TestMethod]
        public async Task ServiceCallWithCallbackErrorPath() {
            
            var client = CreateClient(
                SvcSimple.Client.SvcSimpleClientFactory.Factory,
                ServiceCallTypeParameters.Callback(
                    SvcCallbackReceiver.Constants.ServiceDescriptor,
                    null,
                    nameof(SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.ReceiveTestValueCallback)
                ));

            ValueHelper.ClearJsonTestData();

            var result = await client.Fails();
            
            result.ServiceCallStatus.Should().Be(ServiceCallStatusEnum.Completed);
            result.OperationStatus.Should().Be(JobResultStatusEnum.Pending);
            Thread.Sleep(1000);

            var testData = 
                ValueHelper.GetJsonTestData<SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.SavedData>();
            testData.OriginalCallerFabricId
                .Should().Be(FabricId);
            testData.TestResponse
                .Should().BeNull();
        }

        [TestMethod]
        public async Task InterchangeableWithSlimResult() {
            string testValue = Guid.NewGuid().ToString();

            var client = CreateClient(
                SvcSimple.Client.SvcSimpleClientFactory.Factory,
                ServiceCallTypeParameters.Callback(
                    SvcCallbackReceiver.Constants.ServiceDescriptor,
                    null,
                    nameof(SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.ReceiveTestValueCallbackUsingSlimResult)
                ));

            ValueHelper.ClearJsonTestData();

            var result = await client.GetTestValueNoDependencies(new SvcSimple.Entities.TestValueRequest {
                TheValue = testValue
            });
            
            result.ServiceCallStatus.Should().Be(ServiceCallStatusEnum.Completed);
            result.OperationStatus.Should().Be(JobResultStatusEnum.Pending);
            Thread.Sleep(1000);
            var testData = 
                ValueHelper.GetJsonTestData<SvcCallbackReceiver.Service.SvcCallbackReceiverOperation.SavedData>();
            
            testData.OriginalCallerFabricId
                .Should().BeNull();         // slim result has no host id
            testData.TestResponse.TheIncomingValue
                .Should().Be(testValue);
        }
    }
}