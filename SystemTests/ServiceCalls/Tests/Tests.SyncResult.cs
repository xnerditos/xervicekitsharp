using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SystemTests.ServiceCalls.Tests {

    [TestClass]
    public class SyncResult : TestBase {

        [ClassInitialize]
        public static void Initialize(TestContext context) { TestBase.ClassInit(); }

        [ClassCleanup]
        public static void Teardown() { TestBase.ClassTeardown(); }

        [TestMethod]
        public async Task SingleServiceCallHappyPath() {
            
            string testValue = Guid.NewGuid().ToString();
            var client = CreateClient(SvcSimple.Client.SvcSimpleClientFactory.Factory);
            var result = await client.GetTestValueNoDependencies(new SvcSimple.Entities.TestValueRequest {
                TheValue = testValue
            });
            
            result.ImmediateSuccess.Should().BeTrue();
            result.ResponseBody.TheIncomingValue.Should().Be(testValue);
            result.ResponseBody.RandomValue.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public async Task ChainedServiceCallHappyPath() {
            
            string testValue = Guid.NewGuid().ToString();
            var client = CreateClient(SvcWithDependency1.Client.SvcWithDependency1ClientFactory.Factory);
            var result = await client.GetTestValueWithDependency2Levels(new SvcWithDependency1.Entities.TestValueRequest {
                TheValue = testValue
            });
            
            result.ImmediateSuccess.Should().BeTrue();
            result.ResponseBody.TheIncomingValue.Should().Be(testValue);
            result.ResponseBody.RandomValue.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public async Task SingleServiceCallWithError() {

            var client = CreateClient(SvcSimple.Client.SvcSimpleClientFactory.Factory);
            var result = await client.Fails();
            
            result.ImmediateSuccess.Should().BeFalse();
            result.ResponseBody.Should().BeNull();
        }
    }
}