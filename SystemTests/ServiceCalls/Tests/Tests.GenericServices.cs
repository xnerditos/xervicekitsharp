using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SystemTests.ServiceCalls.Tests {

    [TestClass]
    public class GenericServices : TestBase {

        [ClassInitialize]
        public static void Initialize(TestContext context) { TestBase.ClassInit(); }

        [ClassCleanup]
        public static void Teardown() { TestBase.ClassTeardown(); }

        [TestMethod]
        public async Task SingleServiceCallHappyPath() {
            
            string testValue = Guid.NewGuid().ToString();
            var client = CreateClient(SvcGeneric.Client.SvcGenericClientFactory.Factory);
            var result = await client.GetTestValueNoDependencies(new SvcGeneric.Entities.TestValueRequest {
                TheValue = testValue
            });
            
            result.ImmediateSuccess.Should().BeTrue();
            result.ResponseBody.TheIncomingValue.Should().Be(testValue);
            result.ResponseBody.RandomValue.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public async Task SingleServiceCallWithError() {

            var client = CreateClient(SvcGeneric.Client.SvcGenericClientFactory.Factory);
            var result = await client.Fails();
            
            result.ImmediateSuccess.Should().BeFalse();
            result.ResponseBody.Should().BeNull();
        }
    }
}