using XKit.Lib.Common.Fabric;
using XKit.Lib.Host;
using XKit.Lib.Common.Client;
using XKit.Lib.Testing;
using XKit.Lib.Common.Host;
using System.Collections.Generic;
using XKit.Lib.Common.Registration;

namespace SystemTests.MetaServices.Tests {

    public class TestBase {
        protected IDependencyConnector DependencyConnector => 
            HostEnvironmentHelper.DependencyConnector;
        protected string FabricId => 
            HostEnvironmentHelper.Host?.FabricId;

        protected static void ClassInit() {

            TestHostHelper.Initialize();

            TestHostHelper.AddService(Svc1.Service.Svc1ServiceFactory.Create());     

            TestHostHelper.StartHost();       
        }

        protected static void ClassTeardown() {
            TestHostHelper.DestroyHost();
        }

        protected static void SetRuntimeConfiguration(
            HostConfigDocument hostConfig = null,
            IDictionary<IReadOnlyDescriptor, object> servicesConfig = null
        ) => TestHostHelper.SetRuntimeConfiguration(hostConfig, servicesConfig);

        protected T CreateClient<T>(IServiceClientFactory<T> factory, ServiceCallTypeParameters callTypeParameters = null) {
            return factory.CreateServiceClient(
                clientParameters: ServiceClientParameters.CreateForConsumer(
                    requestorFabricId: FabricId, 
                    defaultCorrelationId: XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                    dependencyConnector: DependencyConnector
                ), 
                defaultCallTypeParameters: callTypeParameters ?? ServiceCallTypeParameters.SyncResult()
            );
        }
    }
}