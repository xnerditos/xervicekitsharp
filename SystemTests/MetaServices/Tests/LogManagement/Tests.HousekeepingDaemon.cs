using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Registration;
using XKit.Lib.Testing;

namespace SystemTests.MetaServices.Tests {

    [TestClass]
    public class LogHousekeepingDaemon : TestBase {

        [ClassInitialize]
        public static void Initialize(TestContext context) { 
            TestBase.ClassInit(); 
        }

        [ClassCleanup]
        public static void Teardown() { TestBase.ClassTeardown(); }

        [TestMethod]
        public async Task Succeeds() {

            TestBase.ClassInit();
            SetRuntimeConfiguration(
                servicesConfig: new Dictionary<IReadOnlyDescriptor, object> {
                    {
                        MetaServiceConstants.Services.LogManagement.Descriptor,
                        new LogManagementConfig {
                            HousekeepingDaemon = new LogManagementConfig.HousekeepingDaemonType {
                                EnableArchiving = true,
                                CheckEveryXSeconds = 6,
                                ArchiveJobsOlderThanXMinutes = 0
                            }
                        }
                    }
                }
            );

            int jobCount = 0;

            try {
                var client = CreateClient(Svc1.Client.Svc1ClientFactory.Factory);

                for(int i = 0; i < 10; i++) {
                    await client.GenerateLogEntries(new Svc1.Entities.GenerateLogEntriesRequest {
                        Count = 10
                    });
                }

                await Task.Delay(10000);
                var logSession = TestHostHelper.LogManager.CreateReadOnlySession();
                var logReader = logSession.GetLogReader();
                var jobs = await logReader.QueryJobs();                
                jobCount = jobs.Count();
            } finally {
                TestBase.ClassTeardown();
            }

            jobCount.Should().BeLessOrEqualTo(3);
        }
    }
}