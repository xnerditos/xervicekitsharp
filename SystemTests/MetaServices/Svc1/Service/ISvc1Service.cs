using System.Threading.Tasks;
using XKit.Lib.Common.Host;

namespace SystemTests.MetaServices.Svc1.Service {

    public interface ISvc1Service : IManagedService, IServiceBase { }
}