using System;
using System.Threading.Tasks;
using SystemTests.MetaServices.Svc1.Entities;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Host.DefaultBaseClasses;

namespace SystemTests.MetaServices.Svc1.Service {

    public partial class Svc1Operation : ServiceOperation<ISvc1Service>, ISvc1Api {

        public Svc1Operation(
            ServiceOperationContext context
        ) : base(context) { }


        // ---------------------------------------------------------------------
        // ISvc1.GetTestValue
        // ---------------------------------------------------------------------
        async Task<ServiceCallResult> ISvc1Api.GenerateLogEntries(GenerateLogEntriesRequest request) {
            return await RunServiceCall(
                request,
                operationAction: (r) => {
                    
                    for(int i = 0; i < r.Count; i++) {
                        Log.Info("Test entry " + i.ToString());
                    }

                    return Task.CompletedTask;
                }
            );
        }
    }
}