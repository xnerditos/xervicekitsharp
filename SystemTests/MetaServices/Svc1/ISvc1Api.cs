using System.Threading.Tasks;
using SystemTests.MetaServices.Svc1.Entities;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;

namespace SystemTests.MetaServices.Svc1 {

    public interface ISvc1Api : IServiceApi {

        Task<ServiceCallResult> GenerateLogEntries(
            GenerateLogEntriesRequest request
        );
    }
}