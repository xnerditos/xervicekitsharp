using System.Threading.Tasks;
using SystemTests.MetaServices.Svc1.Entities;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;
using XKit.Lib.Connector.Service;

namespace SystemTests.MetaServices.Svc1.Client {

    internal class Svc1Client : ServiceClientBase<ISvc1Api>, ISvc1Api {

        public Svc1Client(
            IReadOnlyDescriptor dependency,
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters
        ) : base(
            dependency,
            clientParameters,
            defaultCallTypeParameters
        ) { }

        // =====================================================================
        // overridables
        // =====================================================================

        protected override IReadOnlyDescriptor ServiceDescriptor => Constants.ServiceDescriptor;

        // =====================================================================
        // ISvc1
        // =====================================================================

        async Task<ServiceCallResult> ISvc1Api.GenerateLogEntries(
            GenerateLogEntriesRequest request
        ) => await ExecuteCall(request);
    }
}