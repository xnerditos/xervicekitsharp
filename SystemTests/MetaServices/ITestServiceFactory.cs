using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Services;

namespace SystemTests.MetaServices {
    public interface ITestServiceFactory : IServiceFactory {

		IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        );
    }
}