using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Utility.Threading;

namespace XKit.Lib.LocalLog.Internal {

    internal interface ILogSessionInternal {
        JobTypeEnum JobType { get; }
        JobSystemEffect Effect { get; }
        string OriginatorName { get; }
        int OriginatorVersion { get; }
        string OperationName { get; }
        string HostFabricId { get; }
        string InstanceId { get; }
        Guid JobId { get; }
        string CorrelationId { get; }
        string RequestorInstanceId { get; }
        string RequestorFabricId { get; }
        
        IEnumerable<string> CorrelationTags { get; }
        bool Writeable { get; }
        Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> JobMonitor { get; }
        Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> EventMonitor { get; }
        Action<Exception> PanicHandler { get; }
        //SemaphoreSlim WriteSyncronizer { get; }        
        int NextTraceIndex();

        SQLiteConnection CreateConnection();

        void AddBackgroundTask(Func<Task> logAction);
    }
}