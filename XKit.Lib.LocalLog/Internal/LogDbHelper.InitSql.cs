
namespace XKit.Lib.LocalLog.Internal {

    internal static partial class LogDbHelper {
        private const string NewDbSetupSql = @"
CREATE TABLE LogJobs (
    LogJobEntryId  GUID   PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    JobType        String NOT NULL,
    OriginatorName String NULL,
    OriginatorVersion Int NULL,
    JobName String NULL,
    StartTimestamp DateTime NOT NULL,
    CompleteTimestamp DateTime NULL,
    CorrelationId String NULL,
    CorrelationTags String NULL,
    HostFabricId String NULL,
    InstanceId String NULL,
    RequestorInstanceId String NULL,
    RequestorFabricId String NULL,
    JobNote String NULL,
    Status String NULL,
    SystemEffect String NULL,
    ReplayStrategy String NULL,
    ResultCode String NULL,
    Message String NULL
);
CREATE INDEX LogJobs_StartTimestamp_idx ON LogJobs (
    StartTimestamp
);
CREATE INDEX LogJobs_CompleteTimestamp_idx ON LogJobs (
    CompleteTimestamp
);
CREATE INDEX LogJobs_CorrelationId_idx ON LogJobs (
    CorrelationId
);

CREATE TABLE LogEvents (
    LogEventEntryId GUID  PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    LogJobEntryId  GUID   NOT NULL,
    Timestamp DateTime NOT NULL,
    EventTypeName String NOT NULL,
    Code String NULL,
    Message String NULL,
    Tags String NULL,
    Attributes String NULL,
    TraceIndex Int NULL,
    IsPrivate Bool NULL
);

CREATE INDEX LogEvents_LogJobEntryId_idx ON LogEvents (
    LogJobEntryId
);
CREATE INDEX LogEvents_Timestamp_idx ON LogEvents (
    Timestamp
);
CREATE INDEX LogEvents_EventTypeName_idx ON LogEvents (
    EventTypeName
);

CREATE TABLE LogData (
    LogEntryDataId GUID  PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    LogEventEntryId GUID  NOT NULL,
    LogJobEntryId  GUID   NOT NULL,
    Name String NOT NULL,
    Data BLOB NULL
);

CREATE INDEX LogData_LogJobEntryId_idx ON LogData (
    LogJobEntryId
);
CREATE INDEX LogData_LogEventEntryId_idx ON LogData (
    LogEventEntryId
);
        ";
    }
}