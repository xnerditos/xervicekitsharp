using System.IO;
using System.Threading;
using System;
using System.Text;
using System.Data.SQLite;

namespace XKit.Lib.LocalLog.Internal {

    internal static partial class LogDbHelper {

        private static readonly object parameterLocker = new object();
        private static readonly Random random = new Random();
        private static readonly SemaphoreSlim createSynchronizer = new SemaphoreSlim(1, 1);
        private static volatile uint parameterInc = 0;

		// ===========================================================================
  		// public
  		// ===========================================================================

        public static SQLiteConnection EnsureDb(
            string logPath,
            bool shared = false
        ) {

			createSynchronizer.Wait();
			try {

                bool newFile = false;
                newFile = !File.Exists(logPath);

                if (newFile) {
                    var folder = Path.GetDirectoryName(Path.GetFullPath(logPath));
                    Directory.CreateDirectory(folder);
                }

                var database = new SQLiteConnection($"Data Source={logPath};Version=3;Pooling=True;Max Pool Size=100;");
                database.Open();

                if (newFile) {
                    InitNewLog(database);                    
                } 

                return database;
            } finally {
                createSynchronizer.Release();
            }
        }

        public static string GetHeaderSql() =>
            @"
    BEGIN;
    PRAGMA temp_store = 2;";

        public static (string header, string table, string footer) GetListTableSql() {
            string tableName = "[_Lst_" + Guid.NewGuid().ToString("N") + "]";
            string header = $"CREATE TEMP TABLE {tableName} (Val TEXT PRIMARY KEY);\n";
            string footer = $"DROP TABLE {tableName};";
            return (header, tableName, footer);
        }

        public static string GetFooterSql() => 
            @"END;";

        public static void AddListItem(
            StringBuilder query,
            string tableName, 
            SQLiteCommand cmd, 
            object item
        ) {
            string parameterName;

            lock(parameterLocker) {
                if (parameterInc == uint.MaxValue) {
                    parameterInc = 0;
                }
                parameterName = "p" + parameterInc.ToString();
                parameterInc++;
            }

            cmd.Parameters.AddWithValue(parameterName, item);
            query.AppendLine($"INSERT INTO {tableName} (Val) VALUES (@{parameterName});");
        }

		private static void InitNewLog(
            SQLiteConnection database
        ) {
            using (var cmd = database.CreateCommand()) {
                cmd.CommandText = NewDbSetupSql;
                cmd.ExecuteNonQuery();
            }
		}
   }
}