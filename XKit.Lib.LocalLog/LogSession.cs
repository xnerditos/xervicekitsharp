using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Utility.Threading;
using XKit.Lib.LocalLog.Internal;
using XKit.Lib.LocalLog.Entities;
using System.Data.SQLite;
using System.Threading;

namespace XKit.Lib.LocalLog {

    internal class LogSession : ILogSession, ILogSessionInternal {
        
        private const int BackgroundTasksQueueSize = 32;

		private enum StateEnum {
			NotStarted,
			Started,
			Ended
		}

		private StateEnum state = StateEnum.NotStarted;
        private readonly string logPath;
		private readonly string jobName;
		private readonly string originatorName;
		private readonly int originatorVersion;
        private readonly string hostId;
		private readonly string instanceId;
		private readonly Guid jobId;
		private readonly string correlationId;
		private readonly string requestorInstanceId;
        private readonly string requestorFabricId;      
        private readonly JobTypeEnum jobType;  
        private readonly JobSystemEffect jobEffect;
		private readonly IEnumerable<string> correlationTags;
		private readonly bool writeable;
		private volatile int nextTraceIndex = 0;
		private readonly LogReader reader;
		private readonly LogWriter writer;
		private LogReader ReaderAsConcrete => reader;
		private ILogReader Reader => reader;
		private ILogWriter Writer => writer;
        private readonly TaskQueue writeTaskQueue = new TaskQueue();
        private readonly Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor;
        private readonly Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor;
        private readonly Action<Exception> panicHandler;
        private readonly LogManager logManager;
        private readonly SemaphoreSlim writeSyncronizer;
        private bool includeEntryAndExitData;
        
		public LogSession(
            LogManager logManager,
            string logPath,
            JobTypeEnum jobType,
            JobSystemEffect jobEffect,
			string jobName,
			string originatorName,
			int originatorVersion,
            string hostId,
			string instanceId,
			Guid jobId,
			string correlationId,
			string requestorInstanceId,
            string requestorFabricId,
			IEnumerable<string> correlationTags,
			bool writeable,
            bool immediateFlushOnWrite,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor,
            Action<Exception> panicHandler,
            TaskQueue writeTaskQueue,
            SemaphoreSlim writeSyncronizer
		) {
            this.logManager = logManager;
            this.logPath = logPath;
			this.jobName = jobName;
            this.jobType = jobType;
            this.jobEffect = jobEffect;
			this.originatorName = originatorName;
			this.originatorVersion = originatorVersion;
            this.hostId = hostId;
			this.instanceId = instanceId;
			this.jobId = jobId;
			this.correlationId = correlationId;
			this.requestorInstanceId = requestorInstanceId;
            this.requestorFabricId = requestorFabricId;
			this.correlationTags = correlationTags != null ?
				new List<string>(correlationTags) : null;
			this.writeable = writeable;
			this.reader = new LogReader(this as ILogSessionInternal);;
			this.writer = writeable ? new LogWriter(this as ILogSessionInternal, writeSyncronizer) : null;
            this.logJobMonitor = logJobMonitor;
            this.logEventMonitor = logEventMonitor;
            this.panicHandler = panicHandler;
            this.writeTaskQueue = writeTaskQueue;
            this.writeSyncronizer = writeSyncronizer;
		}

		// =====================================================================
		// ILogSession
		// =====================================================================

		ILogReader ILogSession.GetLogReader() {
			return reader;
		}

		ILogWriter ILogSession.GetLogWriter() {
            try {
                if (!writeable) {
                    throw new Exception("Session is read only");
                }

                return writer;
            } catch (Exception ex) {
                panicHandler?.Invoke(ex);
                return null;
            }
		}

		async Task ILogSession.BeginLog(
			object workItem, 
			IDictionary<string, object> parameters,
            LoggingOptions options         
		) {
            InitLogBegin(options);
            
            try {
                await BeginLogEx(
                    workItem,
                    parameters
                );
            } catch (Exception ex) {
                panicHandler?.Invoke(ex);
            }
		}

        void ILogSession.BeginLogBackground(
			object workItem, 
			IDictionary<string, object> parameters,
            LoggingOptions options         
		) {
            InitLogBegin(options);
            try {
                EnqueueLogAction(() => BeginLogEx(
                    workItem,
                    parameters
                ));
            } catch (Exception ex) {
                panicHandler?.Invoke(ex);
            }
		}

        Task ILogSession.PendingLog(
			IDictionary<string, object> pendingLogParameters,
			string statusMessage
        ) => PendingLogEx(pendingLogParameters, statusMessage);

        void ILogSession.PendingLogBackground(
			IDictionary<string, object> pendingLogParameters,
			string statusMessage
        ) {
            EnqueueLogAction(() => PendingLogEx(pendingLogParameters, statusMessage));
        }

		async Task ILogSession.EndLog(
			JobResultStatusEnum? status,
			string statusMessage,
			object result,
            object code,
            JobReplayStrategyEnum replayStrategy,
            object replayData
		) {
            try {
                await EndLogEx(
                    status,
                    statusMessage,
                    result,
                    code,
                    replayStrategy,
                    replayData
                );
            } catch (Exception ex) {
                panicHandler?.Invoke(ex);
            }
		}

		void ILogSession.EndLogBackground(
			JobResultStatusEnum? status,
			string statusMessage,
			object result,
            object code,
            JobReplayStrategyEnum replayStrategy,
            object replayData
		) {
            try {
                EnqueueLogAction(() => EndLogEx(
                    status,
                    statusMessage,
                    result,
                    code,
                    replayStrategy,
                    replayData
                ));
            } catch (Exception ex) {
                panicHandler?.Invoke(ex);
            }
		}

        async Task<IReadOnlyList<Guid>> ILogSession.ArchiveJobs(
            IEnumerable<Guid> jobIds,
            string matchingJobName,
            JobTypeEnum? matchingJobType,
			DateTime? startOnOrBeforeTimestamp,
			DateTime? startOnOrAfterTimestamp,
            bool? isComplete,
			DateTime? completeOnOrBeforeTimestamp,
			DateTime? completeOnOrAfterTimestamp,
            string matchingCorrelationId,
            string havingCorrelationTag,
            int limitCount,
            Func<IReadOnlyLogEventEntry, bool> eventSelector, 
            string archiveDbFile,
            bool deleteArchivedJobsFromSource,
            int waitBetweenStepsMilliseconds
        ) {
            return await ArchiveJobs(
                jobIds, 
                matchingJobName,
                matchingJobType,
                startOnOrBeforeTimestamp,
                startOnOrAfterTimestamp,
                isComplete,
                completeOnOrBeforeTimestamp,
                completeOnOrAfterTimestamp,
                matchingCorrelationId,
                havingCorrelationTag,
                limitCount,
                eventSelector, 
                archiveDbFile, 
                deleteArchivedJobsFromSource,
                waitBetweenStepsMilliseconds
            );
        }

        Task ILogSession.ExecuteStartupMaintenance() => this.ExecuteStartupMaintenance();

		// =====================================================================
		// IDisposable
		// =====================================================================

		void IDisposable.Dispose() { }

		// void IDisposable.Dispose() {
        //     try {
        //         switch(state) {
        //             case StateEnum.Ended:
        //             case StateEnum.NotStarted:
        //                 break;
        //             case StateEnum.Started: 
        //                 TaskUtil.RunSyncSafely(async () => {
        //                     if (Writer != null) {
        //                         await Writer.NewEvent(
        //                             LogEventType.Erratum,
        //                             message: "Log session was not ended before disposal"
        //                         );
        //                     }
        //                     await EndLogEx(
        //                         JobResultStatusEnum.Unknown, 
        //                         message: "Erratum"
        //                     );
        //                 });
        //                 break;
        //         }
        //     } catch (Exception ex) {
        //         panicHandler?.Invoke(ex);
        //     }
		// }

		// =====================================================================
		// ILogSessionInternal
		// =====================================================================
        JobTypeEnum ILogSessionInternal.JobType => jobType;
        JobSystemEffect ILogSessionInternal.Effect => jobEffect;
		string ILogSessionInternal.OriginatorName => originatorName;
		int ILogSessionInternal.OriginatorVersion => originatorVersion;
		string ILogSessionInternal.HostFabricId => hostId;
		string ILogSessionInternal.InstanceId => instanceId;
		Guid ILogSessionInternal.JobId => jobId;
		string ILogSessionInternal.CorrelationId => correlationId;
		string ILogSessionInternal.RequestorInstanceId => requestorInstanceId;
		string ILogSessionInternal.RequestorFabricId => requestorFabricId;
		string ILogSessionInternal.OperationName => jobName;
		IEnumerable<string> ILogSessionInternal.CorrelationTags => correlationTags;
		bool ILogSessionInternal.Writeable => writeable;
        Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> ILogSessionInternal.JobMonitor => logJobMonitor;
        Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> ILogSessionInternal.EventMonitor => logEventMonitor;
        Action<Exception> ILogSessionInternal.PanicHandler => panicHandler;
        
		int ILogSessionInternal.NextTraceIndex() => nextTraceIndex++;

        void ILogSessionInternal.AddBackgroundTask(Func<Task> logAction) 
            => EnqueueLogAction(logAction);

        SQLiteConnection ILogSessionInternal.CreateConnection() => LogDbHelper.EnsureDb(logPath);

		// ===========================================================================
		// private 
  		// ===========================================================================

        private void InitLogBegin(LoggingOptions options) {
            switch(state) {
                case StateEnum.Started: 
                    throw new Exception("Log session already started");
                case StateEnum.Ended:
                    throw new Exception("Cannot use a log session after it is ended");
            }

			state = StateEnum.Started;
            includeEntryAndExitData = hasOption(LoggingOptions.IncludeEntryAndExitData);

            bool hasOption(LoggingOptions oneOption) 
                => (options & oneOption) == oneOption;
        }

        private async Task ExecuteStartupMaintenance() {
            var jobs = await ReaderAsConcrete.QueryJobs(
                reader => reader.GetGuid(0),
                selectFields: "LogJobEntryId",
                isComplete: false
            );
            
            using var connection = LogDbHelper.EnsureDb(logPath);
            using (var cmd = connection.CreateCommand()) {                
                cmd.CommandText = 
                    @"BEGIN;
                    UPDATE LogJobs SET
                        CompleteTimestamp = @CompleteTimestamp,
                        Status = 'Abandoned',
                    WHERE CompleteTimestamp IS NULL AND HostFabricId <> @HostFabricId;
                    END;";
                cmd.Parameters.AddWithValue("@CompleteTimestamp", DateTime.UtcNow);
                cmd.Parameters.AddWithValue("@HostFabricId", this.hostId ?? (object)DBNull.Value);
                await cmd.ExecuteNonQueryAsync();
            }
            await connection.CloseAsync();
        }

        private void EnqueueLogAction(Func<Task> action) {
            try {
                writeTaskQueue.Enqueue(action);
            } catch(Exception ex) {
                panicHandler?.Invoke(ex);                
            }
        }

        private async Task<IReadOnlyList<Guid>> ArchiveJobs(
            IEnumerable<Guid> jobIds,
            string matchingJobName,
            JobTypeEnum? matchingJobType,
			DateTime? startOnOrBeforeTimestamp,
			DateTime? startOnOrAfterTimestamp,
            bool? isComplete,
			DateTime? completeOnOrBeforeTimestamp,
			DateTime? completeOnOrAfterTimestamp,
            string matchingCorrelationId,
            string havingCorrelationTag,
            int limitJobCount,
            Func<IReadOnlyLogEventEntry, bool> eventSelector, 
            string archiveDbFile,
            bool deleteArchivedFromSource,
            int waitBetweenStepsMilliseconds
        ) {

            try {

                var jobs = await ReaderAsConcrete.QueryJobs(
                    reader => (id: reader.GetGuid(0), completed: !reader.IsDBNull(1)),
                    selectFields: "LogJobEntryId, CompleteTimestamp",
                    jobIds: jobIds,
                    matchingJobName: matchingJobName,
                    matchingJobType: matchingJobType,
                    startOnOrBeforeTimestamp: startOnOrBeforeTimestamp,
                    startOnOrAfterTimestamp: startOnOrAfterTimestamp,
                    isComplete: isComplete,
                    completeOnOrBeforeTimestamp: completeOnOrBeforeTimestamp,
                    completeOnOrAfterTimestamp: completeOnOrAfterTimestamp,
                    matchingCorrelationId: matchingCorrelationId,
                    havingCorrelationTag: havingCorrelationTag,
                    pageSize: limitJobCount
                );
                
                using var connection = LogDbHelper.EnsureDb(logPath);

                bool archive = archiveDbFile != null;

                if (archive) {
                    var archiveDbTmpConnection = LogDbHelper.EnsureDb(archiveDbFile);
                    await archiveDbTmpConnection.CloseAsync();
                    archiveDbTmpConnection.Dispose();

                    using (var cmd = connection.CreateCommand()) {
                        cmd.CommandText = $"ATTACH '{archiveDbFile}' AS archive;";
                        await writeSyncronizer.WaitAsync();
                        try { await cmd.ExecuteNonQueryAsync(); }
                        finally { writeSyncronizer.Release(); }
                    }

                    foreach (var job in jobs) {
                        using (var cmd = connection.CreateCommand()) {
                            cmd.CommandText = 
                                @"BEGIN;
                                INSERT INTO archive.LogJobs SELECT * FROM main.LogJobs 
                                    WHERE 
                                        LogJobEntryId = @id
                                    ON CONFLICT(LogJobEntryId) DO UPDATE SET 
                                        CompleteTimestamp = excluded.CompleteTimestamp,
                                        Status = excluded.Status,
                                        ResultCode = excluded.ResultCode,
                                        Message = excluded.Message;
                                INSERT INTO archive.LogEvents SELECT * FROM main.LogEvents 
                                    WHERE 
                                        LogJobEntryId = @id AND
                                        EventTypeName <> 'Status' AND
                                        EventTypeName <> 'Trace' AND
                                        EventTypeName <> 'LoggingEngineArchivePlaceholder' AND
                                        EventTypeName <> 'LoggingEngineArchiveSummary' AND
                                        (IsPrivate IS NULL OR IsPrivate = 0)
                                    ON CONFLICT DO NOTHING;
                                INSERT INTO archive.LogData SELECT * FROM main.LogData 
                                    WHERE 
                                        LogJobEntryId = @id AND
                                        LogEventEntryId IN 
                                            (SELECT LogEventEntryId FROM LogEvents WHERE 
                                                LogJobEntryId = @id AND 
                                                EventTypeName <> 'Status' AND
                                                EventTypeName <> 'Trace' AND
                                                EventTypeName <> 'LoggingEngineArchivePlaceholder' AND
                                                EventTypeName <> 'LoggingEngineArchiveSummary' AND
                                                (IsPrivate IS NULL OR IsPrivate = 0))
                                    ON CONFLICT DO NOTHING;
                                END;";
                            cmd.Parameters.AddWithValue("@id", job.id);
                            await writeSyncronizer.WaitAsync();
                            try { await cmd.ExecuteNonQueryAsync(); }
                            finally { writeSyncronizer.Release(); }
                        }
                    }                
                }

                int jobsFullyDeleted = 0;
                int jobsPartiallyDeleted = 0;

                if (deleteArchivedFromSource) {

                    foreach(var job in jobs) {

                        if (job.completed) {
                            using (var cmd = connection.CreateCommand()) {
                                cmd.CommandText = 
                                    @"BEGIN;
                                    DELETE FROM LogJobs WHERE LogJobEntryId = @id;
                                    DELETE FROM LogEvents WHERE LogJobEntryId = @id;
                                    DELETE FROM LogData WHERE  LogJobEntryId = @id;
                                    END";
                                cmd.Parameters.AddWithValue("@id", job.id);
                                await writeSyncronizer.WaitAsync();
                                try { await cmd.ExecuteNonQueryAsync(); }
                                finally { writeSyncronizer.Release(); }
                                await Task.Delay(waitBetweenStepsMilliseconds);
                            }
                            jobsFullyDeleted++;                        
                        } else {

                            jobsPartiallyDeleted++;
                            if (eventSelector == null) {
                                using (var cmd = connection.CreateCommand()) {
                                    DateTime beforeTimestamp = DateTime.UtcNow.AddDays(-7);
                                    cmd.CommandText = 
                                        @"BEGIN;
                                        DELETE FROM LogData WHERE 
                                            LogJobEntryId = @id AND 
                                            LogEventEntryId IN 
                                                (SELECT LogEventEntryId FROM LogEvents WHERE 
                                                    LogJobEntryId = @id AND 
                                                    Timestamp < @beforeTimestamp AND
                                                    EventTypeName <> 'Enter' AND 
                                                    EventTypeName <> 'Exit' AND 
                                                    EventTypeName <> 'Warning' AND 
                                                    EventTypeName <> 'Error' AND 
                                                    EventTypeName <> 'Erratum' AND
                                                    EventTypeName <> 'ReplayHint' AND
                                                    IsPrivate <> 1);
                                        DELETE FROM LogEvents WHERE 
                                            LogJobEntryId = @id AND 
                                            LogJobEntryId = @id AND 
                                            Timestamp < @beforeTimestamp AND
                                            EventTypeName <> 'Enter' AND 
                                            EventTypeName <> 'Exit' AND 
                                            EventTypeName <> 'Warning' AND 
                                            EventTypeName <> 'Error' AND 
                                            EventTypeName <> 'Erratum' AND
                                            EventTypeName <> 'ReplayHint' AND
                                            IsPrivate <> 1;
                                        END";
                                    cmd.Parameters.AddWithValue("@id", job.id);
                                    cmd.Parameters.AddWithValue("@beforeTimestamp", beforeTimestamp);
                                    await writeSyncronizer.WaitAsync();
                                    try { await cmd.ExecuteNonQueryAsync(); }
                                    finally { writeSyncronizer.Release(); }
                                    await Task.Delay(waitBetweenStepsMilliseconds);

                                    await Writer.NewEvent(
                                        LogEventType.LoggingEngineArchivePlaceholder,
                                        attributes: new Dictionary<string, object> {
                                            { "ArchiveCuttoff",  beforeTimestamp }
                                        },
                                        data: new {
                                            eventIds = (Guid[])null
                                        }
                                    );
                                }
                            } else {
                                var events = (await ReaderAsConcrete.QueryEvents(
                                    reader => {
                                        var obj = LogEventEntry.FromDb(reader);
                                        bool preserveIfJobIncomplete = false;
                                        switch(obj.EventTypeName) {
                                            case nameof(LogEventType.Enter):
                                            case nameof(LogEventType.Exit):
                                                preserveIfJobIncomplete = true;
                                                break;
                                        }
                                        return (obj: obj, preserveIfJobIncomplete: preserveIfJobIncomplete);
                                    },
                                    jobIds: new[] { job.id }, 
                                    pageSize: int.MaxValue
                                )).Where(e => !e.preserveIfJobIncomplete && eventSelector(e.obj))
                                .ToArray();

                                foreach(var e in events) {
                                    using (var cmd = connection.CreateCommand()) {
                                        cmd.CommandText = 
                                            @"BEGIN;
                                            DELETE FROM LogEvents WHERE LogEventEntryId = @id;
                                            DELETE FROM LogData WHERE LogEventEntryId = @id;
                                            END";
                                        cmd.Parameters.AddWithValue("@id", e.obj.LogEventEntryId);
                                        await writeSyncronizer.WaitAsync();
                                        try { await cmd.ExecuteNonQueryAsync(); }
                                        finally { writeSyncronizer.Release(); }
                                    }
                                }
                                await Task.Delay(waitBetweenStepsMilliseconds);

                                if (events.Length > 0) {
                                    await Writer.NewEvent(
                                        LogEventType.LoggingEngineArchivePlaceholder,
                                        attributes: new Dictionary<string, object> {
                                            { "ArchiveCuttoff",  null }
                                        },
                                        data: new {
                                            eventIds = events.Select(e => e.obj.LogEventEntryId).ToArray()
                                        }
                                    );
                                }
                            }
                        }
                    }
                }

                if (jobsFullyDeleted > 0 || jobsPartiallyDeleted > 0) {
                    await Writer.NewEvent(
                        LogEventType.LoggingEngineArchiveSummary,
                        attributes: new Dictionary<string, object> {
                            { "JobsDeletedCount", jobsFullyDeleted },
                            { "JobsSummarizedCount", jobsPartiallyDeleted },
                            { "ArchiveDbPath", archiveDbFile ?? "<null>" }
                        },
                        data: new {
                            TargetJobIds = jobIds
                        }
                    );
                }

                await connection.CloseAsync();
                return jobs.Select(j => j.id).ToArray();

            } catch(Exception ex) {

                try {
                    await Writer.Error(
                        "Archiving jobs generated exception: ",
                        attributes: new Dictionary<string, object> {
                            { "ExceptionMessage", ex.Message },
                            { "Stacktrace", ex.StackTrace },
                            { "Source", ex.Source }
                        },
                        code: "ArchiveFailure"
                    );
                } catch (Exception ex2) {
                    panicHandler?.Invoke(ex2);
                }
                return new Guid[0];
            }
        }

		private async Task BeginLogEx(
			object workItem = null, 
			IDictionary<string, object> parameters = null
		) {
			if (writeable) {
				Dictionary<string, object> useParams;
				if (parameters != null) {
					useParams = new Dictionary<string, object>(parameters);
				} else {
					useParams = new Dictionary<string, object>();					
				}
                await Writer.Begin(
                    (this.includeEntryAndExitData || this.jobEffect == JobSystemEffect.Persistent) ? workItem : null,
                    useParams
                );
			}			
		}

		private async Task PendingLogEx(
			IDictionary<string, object> pendingLogParameters,
			string statusMessage
		) {
			switch(state) {
				case StateEnum.Started:
                    break; 
				default:
					throw new Exception("Log session has not been started");
			}

			if (writeable) {
				Dictionary<string, object> useParams;
				if (pendingLogParameters != null) {
					useParams = new Dictionary<string, object>(pendingLogParameters);
				} else {
					useParams = new Dictionary<string, object>();					
				}
                if (Writer != null) {
                    await Writer.NewEvent(
                        LogEventType.ContextChange,
                        attributes: useParams,
                        message: statusMessage,
                        code: ContextChangeTypeEnum.TransitionToBackground
                    );
                }
			}
		}

		private async Task EndLogEx(
			JobResultStatusEnum? status,
			string message = null,
			object resultData = null,
            object resultCode = null,
            JobReplayStrategyEnum? replayStrategy = null,
            object replayData = null
		) {

            var useStatus = status.GetValueOrDefault(JobResultStatusEnum.Success);
            bool success = useStatus == JobResultStatusEnum.Success || useStatus == JobResultStatusEnum.PartialSuccess;

            switch(state) {
                case StateEnum.NotStarted: 
                    throw new Exception("Cannot end a log session that is not started");
                case StateEnum.Ended:
                    throw new Exception("Log session already ended");
            }

            if (writeable && success) {

                switch(this.jobEffect) {
                    case JobSystemEffect.None:
                    case JobSystemEffect.NotSpecified:
                        if (replayStrategy.GetValueOrDefault(JobReplayStrategyEnum.NotSpecifiedOrUnknown) != JobReplayStrategyEnum.NotSpecifiedOrUnknown) {
                            EnqueueLogAction(() => Writer.Warning("Job replay strategy was specified, but job system effect is 'None' or was not specified")); 
                        }
                        break;
                    case JobSystemEffect.Transient:
                        break;
                    case JobSystemEffect.Persistent:
                        if (replayStrategy.GetValueOrDefault(JobReplayStrategyEnum.NotSpecifiedOrUnknown) == JobReplayStrategyEnum.NotSpecifiedOrUnknown) {
                            EnqueueLogAction(() => Writer.Warning("Job replay strategy was not specified, and job system effect is 'Persistent'")); 
                        } else if (replayStrategy.GetValueOrDefault(JobReplayStrategyEnum.NotSpecifiedOrUnknown) == JobReplayStrategyEnum.ReplayModifiedRequest && replayData == null) {
                            EnqueueLogAction(() => Writer.Warning("Job replay strategy is 'ReplayModifiedRequest' but no reply data was given.")); 
                        }                    
                        break;
                }
            }
            
			if (writeable) {

                await Writer.End(
                    status: useStatus,
                    message: message,
                    resultData: includeEntryAndExitData ? resultData : null,
                    resultCode: resultCode,
                    replayStrategy: replayStrategy,
                    replayData: replayData
                );
			}

			state = StateEnum.Ended;
		}
    }
}