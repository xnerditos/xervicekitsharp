using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SQLite;
using XKit.Lib.Common.Log;
using System.Threading;

namespace XKit.Lib.LocalLog.Entities {

    public class LogJobEntry : IReadOnlyLogJobEntry {

        private DateTime startTimestamp;
        private DateTime? completeTimestamp;

        public Guid LogJobEntryId { get; set; }
        public JobTypeEnum JobType { get; set; }
        public string OriginatorName { get; set; }
        public int OriginatorVersion { get; set; }
		public string JobName { get; set; }
        public DateTime StartTimestamp { 
            get => startTimestamp; 
            set => startTimestamp = value.ToUniversalTime(); 
        }
        public DateTime? CompleteTimestamp { 
            get => completeTimestamp; 
            set => completeTimestamp = value == null ? null : (DateTime?)value.Value.ToUniversalTime(); 
        }
        public string CorrelationId { get; set; }
        public LogTag[] CorrelationTags { get; set; }
        public string HostFabricId { get; set; }
        public string InstanceId { get; set; }
        public string RequestorInstanceId { get; set; }
        public string RequestorFabricId { get; set; }
        public string JobNote { get; set; }
        public JobResultStatusEnum Status { get; set; }
        public JobSystemEffect SystemEffect { get; set; }
        public JobReplayStrategyEnum? ReplayStrategy { get; set; }
        public string ResultCode { get; set; }
        public string Message { get; set; }
        IReadOnlyList<IReadOnlyLogTag> IReadOnlyLogJobEntry.CorrelationTags => this.CorrelationTags;

        public async Task ToDb(SQLiteConnection connection, SemaphoreSlim writeSyncronizer) {
            const string sql = @"
                INSERT INTO LogJobs 
                    (LogJobEntryId, JobType, OriginatorName, OriginatorVersion, JobName, StartTimestamp,
                    CorrelationId, CorrelationTags, HostFabricId, InstanceId, RequestorInstanceId,
                    RequestorFabricId, JobNote, Status, SystemEffect, ReplayStrategy, ResultCode, Message, CompleteTimestamp)
                VALUES
                    (@LogJobEntryId, @JobType, @OriginatorName, @OriginatorVersion, @JobName, @StartTimestamp,
                    @CorrelationId, @CorrelationTags, @HostFabricId, @InstanceId, @RequestorInstanceId,
                    @RequestorFabricId, @JobNote, @Status, @SystemEffect, @ReplayStrategy, @ResultCode, @Message, @CompleteTimestamp);
            ";

            using var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@LogJobEntryId", this.LogJobEntryId);
            cmd.Parameters.AddWithValue("@JobType", this.JobType.ToString());
            cmd.Parameters.AddWithValue("@OriginatorName", this.OriginatorName);
            cmd.Parameters.AddWithValue("@OriginatorVersion", this.OriginatorVersion);
            cmd.Parameters.AddWithValue("@JobName", this.JobName ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@StartTimestamp", this.StartTimestamp);
            cmd.Parameters.AddWithValue("@CorrelationId", this.CorrelationId);
            cmd.Parameters.AddWithValue(
                "@CorrelationTags", 
                this.CorrelationTags == null ? DBNull.Value : (object)("|" + string.Join('|', this.CorrelationTags.Select(t => t.Name)) + "|")
            );
            cmd.Parameters.AddWithValue("@HostFabricId", this.HostFabricId ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@InstanceId", this.InstanceId ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@RequestorInstanceId", this.RequestorInstanceId ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@RequestorFabricId", this.RequestorFabricId ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@JobNote", this.JobNote ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@Status", this.Status.ToString());
            cmd.Parameters.AddWithValue("@SystemEffect", this.SystemEffect.ToString());
            cmd.Parameters.AddWithValue("@ReplayStrategy", this.ReplayStrategy?.ToString() ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@ResultCode", this.ResultCode ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@Message", this.Message ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@CompleteTimestamp", this.CompleteTimestamp ?? (object)DBNull.Value);
            await writeSyncronizer.WaitAsync();
            try {  await cmd.ExecuteNonQueryAsync(); }
            finally { writeSyncronizer.Release(); }
        }

        public async Task UpdateCompletion(SQLiteConnection connection, SemaphoreSlim writeSyncronizer) {
            const string sql = @"
                UPDATE LogJobs SET
                    CompleteTimestamp = @CompleteTimestamp,
                    Status = @Status,
                    ResultCode = @ResultCode,
                    Message = @Message,
                    ReplayStrategy = @ReplayStrategy
                WHERE LogJobEntryId = @LogJobEntryId;
            ";

            using var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@CompleteTimestamp", this.CompleteTimestamp);
            cmd.Parameters.AddWithValue("@Status", this.Status.ToString());
            cmd.Parameters.AddWithValue("@ResultCode", this.ResultCode ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@Message", this.Message ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@ReplayStrategy", this.ReplayStrategy?.ToString() ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@LogJobEntryId", this.LogJobEntryId);
            await writeSyncronizer.WaitAsync();
            try {  await cmd.ExecuteNonQueryAsync(); }
            finally { writeSyncronizer.Release(); }
        }

        public static LogJobEntry FromDb(IDataReader reader) {
            return new LogJobEntry {
                LogJobEntryId = reader.GetGuid(0),
                JobType = Enum.Parse<JobTypeEnum>(reader.GetString(1)),
                OriginatorName = reader.GetString(2),
                OriginatorVersion = reader.GetInt32(3),
                JobName = reader.GetString(4),
                StartTimestamp = reader.GetDateTime(5),
                CompleteTimestamp = reader.IsDBNull(6) ? null : (DateTime?)reader.GetDateTime(6),
                CorrelationId = reader.IsDBNull(7) ? null : reader.GetString(7),
                CorrelationTags = 
                    reader.IsDBNull(8) ? null : 
                    reader.GetString(8)
                    .Split('|', StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => new LogTag { Name = s })
                    .ToArray(),
                HostFabricId = reader.IsDBNull(9) ? null : reader.GetString(9),
                InstanceId = reader.IsDBNull(10) ? null : reader.GetString(10),
                RequestorInstanceId = reader.IsDBNull(11) ? null : reader.GetString(11),
                RequestorFabricId = reader.IsDBNull(12) ? null : reader.GetString(12),
                JobNote = reader.IsDBNull(13) ? null : reader.GetString(13),
                Status = Enum.Parse<JobResultStatusEnum>(reader.GetString(14)),
                SystemEffect = Enum.Parse<JobSystemEffect>(reader.GetString(15)),
                ReplayStrategy = 
                    reader.IsDBNull(16) ? null : 
                    (JobReplayStrategyEnum?)Enum.Parse<JobReplayStrategyEnum>(reader.GetString(16)),
                ResultCode = reader.IsDBNull(17) ? null : reader.GetString(17),
                Message = reader.IsDBNull(18) ? null : reader.GetString(18),
            };
        }
    }
}