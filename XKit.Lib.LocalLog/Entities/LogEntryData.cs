using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;

namespace XKit.Lib.LocalLog.Entities {

    public class LogEntryData : IReadOnlyLogEntryData {

        private static readonly Jil.Options JilOptions = new Jil.Options(
            prettyPrint: false,
            excludeNulls: true,
            jsonp: false,
            Jil.DateTimeFormat.ISO8601,
            includeInherited: true,
            unspecifiedDateTimeKindBehavior: Jil.UnspecifiedDateTimeKindBehavior.IsUTC,
            serializationNameFormat: Jil.SerializationNameFormat.CamelCase
        );

		private byte[] binaryValue { get; set; }

        public Guid LogEntryDataId { get; set; }
        public Guid? LogEventEntryId { get; set; }
        public Guid LogJobEntryId { get; set; }
        public string Name { get; set; }
		public byte[] Value => binaryValue;

        public void SetValue(byte[] value) {
            this.binaryValue = value;
        }

        public void SetValue(object value) {
            this.binaryValue = value == null ? null : Encoding.UTF8.GetBytes(Jil.JSON.SerializeDynamic(value, JilOptions));
        }

		public T GetValueAs<T>() where T : class { 
            if (this.binaryValue == null) { return null; }
            return Jil.JSON.Deserialize<T>(Encoding.UTF8.GetString(this.binaryValue), JilOptions);
        }

		public dynamic GetValueAsDynamic() { 
            if (this.binaryValue == null) { return null; }
            return Jil.JSON.DeserializeDynamic(Encoding.UTF8.GetString(this.binaryValue), JilOptions);
        }

        public async Task ToDb(SQLiteConnection connection, SemaphoreSlim writeSyncronizer) {
            const string sql = @"
                INSERT INTO LogData 
                    (LogEntryDataId, LogEventEntryId, LogJobEntryId, Name, Data)
                VALUES
                    (@LogEntryDataId, @LogEventEntryId, @LogJobEntryId, @Name, @Data);
            ";

            using var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@LogEntryDataId", this.LogEntryDataId);
            cmd.Parameters.AddWithValue("@LogEventEntryId", this.LogEventEntryId);
            cmd.Parameters.AddWithValue("@LogJobEntryId", this.LogJobEntryId);
            cmd.Parameters.AddWithValue("@Name", this.Name ?? "");
            cmd.Parameters.AddWithValue("@Data", this.Value != null && this.Value.Length > 0 ? CompressValue(this.Value) : (object)DBNull.Value);
            await writeSyncronizer.WaitAsync();
            try {  await cmd.ExecuteNonQueryAsync(); }
            finally { writeSyncronizer.Release(); }
        }

        public static LogEntryData FromDb(IDataReader reader) {
            var d = new LogEntryData {
                LogEntryDataId = reader.GetGuid(0),
                LogEventEntryId = reader.IsDBNull(1) ? null : (Guid?)reader.GetGuid(1),
                LogJobEntryId = reader.GetGuid(2),
                Name = reader.IsDBNull(3) ? "" : reader.GetString(3)
            };
            d.SetValue(reader.IsDBNull(4) ? null : DecompressValue((byte[])reader.GetValue(4)));
            return d;
        }

        private static byte[] CompressValue(byte[] json) {
            if (json == null) { return null; }
            if (json.Length == 0) { return null; }
            MemoryStream output = new MemoryStream();
            var dstream = new DeflateStream(output, CompressionLevel.Optimal);
            dstream.Write(json, 0, json.Length);
            dstream.Close();
            return output.ToArray();
        }

        private static byte[] DecompressValue(byte[] data) {
            if (data == null) { return null; }
            if (data.Length == 0) { return null; }
            MemoryStream input = new MemoryStream(data);
            MemoryStream output = new MemoryStream();
            var dstream = new DeflateStream(input, CompressionMode.Decompress);
            dstream.CopyTo(output);
            dstream.Close();
            return output.ToArray();
        }        
    }
}