using XKit.Lib.Common.Log;

namespace XKit.Lib.LocalLog.Entities {

    public class LogTag : IReadOnlyLogTag {
        public string Name { get; set; }
    }
}