using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SQLite;
using XKit.Lib.Common.Log;
using System.Threading;

namespace XKit.Lib.LocalLog.Entities {

    public class LogEventEntry : IReadOnlyLogEventEntry {
        private DateTime timestamp { get; set; }

        public Guid LogEventEntryId { get; set; }
        public Guid LogJobEntryId { get; set; }
        public DateTime Timestamp { 
            get => timestamp; 
            set => timestamp = value.ToUniversalTime();
        }
        public LogEventType GetEventType() {
            switch(EventTypeName) {
                case null:
                case "": 
                    return LogEventType.None;
                default:
                    LogEventType eventType;
                    if (Enum.TryParse<LogEventType>(EventTypeName, out eventType)) {
                        return eventType;
                    }
                    return LogEventType.ServiceDefined;
            }
        }

        public void SetEventType(LogEventType value) {
            switch(value) {
                case LogEventType.ServiceDefined:
                    return;		// ignore 
                case LogEventType.None:
                    EventTypeName = string.Empty;
                    return;
                default:
                    EventTypeName = value.ToString();
                    return;
            }
        }

        public string EventTypeName { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public LogTag[] Tags { get; set; }
        public LogAttribute[] Attributes { get; set; }
        public int? TraceIndex { get; set; }
		public bool IsPrivate { get; set; }

        IEnumerable<IReadOnlyLogTag> IReadOnlyLogEventEntry.Tags => this.Tags;
        IEnumerable<IReadOnlyLogAttribute> IReadOnlyLogEventEntry.Attributes => this.Attributes;

        public async Task ToDb(SQLiteConnection connection, SemaphoreSlim writeSyncronizer) {
            const string sql = @"
                INSERT INTO LogEvents 
                    (LogEventEntryId, LogJobEntryId, Timestamp, EventTypeName, Code, Message, Tags, 
                    Attributes, TraceIndex, IsPrivate)
                VALUES
                    (@LogEventEntryId, @LogJobEntryId, @Timestamp, @EventTypeName, @Code, @Message, @Tags, 
                    @Attributes, @TraceIndex, @IsPrivate);
            ";

            using var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@LogEventEntryId", this.LogEventEntryId);
            cmd.Parameters.AddWithValue("@LogJobEntryId", this.LogJobEntryId);
            cmd.Parameters.AddWithValue("@Timestamp", this.Timestamp);
            cmd.Parameters.AddWithValue("@EventTypeName", this.EventTypeName);
            cmd.Parameters.AddWithValue("@Code", this.Code ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue("@Message", this.Message ?? (object)DBNull.Value);
            cmd.Parameters.AddWithValue(
                "@Tags", 
                this.Tags == null ?  (object)DBNull.Value : "|" + string.Join('|', this.Tags.Select(t => t.Name)) + "|"
            );
            cmd.Parameters.AddWithValue(
                "@Attributes", 
                this.Attributes == null ?  (object)DBNull.Value : "|" + string.Join('|', this.Attributes.Select(a => $"{a.Name}={a.Value}")) + "|"
            );
            cmd.Parameters.AddWithValue("@TraceIndex", this.TraceIndex);
            cmd.Parameters.AddWithValue("@IsPrivate", this.IsPrivate);
            await writeSyncronizer.WaitAsync();
            try {  await cmd.ExecuteNonQueryAsync(); }
            finally { writeSyncronizer.Release(); }
        }

        public static LogEventEntry FromDb(IDataReader reader) {
            return new LogEventEntry {
                LogEventEntryId = reader.GetGuid(0),
                LogJobEntryId = reader.GetGuid(1),
                Timestamp = reader.GetDateTime(2),
                EventTypeName = reader.GetString(3),
                Code = reader.IsDBNull(4) ? null : reader.GetString(4), 
                Message = reader.IsDBNull(5) ? null : reader.GetString(5),
                Tags = 
                    reader.IsDBNull(6) ? null : 
                    reader.GetString(6)
                    .Split('|', StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => new LogTag { Name = s })
                    .ToArray(),
                Attributes = 
                    reader.IsDBNull(7) ? null : 
                    reader.GetString(7)
                    .Split('|', StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => {
                        int i = s.IndexOf('=');
                        return new LogAttribute { 
                            Name = s.Substring(0, i),
                            Value = s.Substring(i + 1) 
                        };
                    }).ToArray(),
                TraceIndex = reader.IsDBNull(8) ? null : (int?)reader.GetInt32(8),
                IsPrivate = reader.IsDBNull(9) ? false : reader.GetBoolean(9) 
            };
        }
    }
}