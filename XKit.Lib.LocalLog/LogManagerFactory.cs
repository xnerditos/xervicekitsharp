using System;
using System.Collections.Generic;
using XKit.Lib.Common.Log;
using XKit.Lib.LocalLog.Entities;

namespace XKit.Lib.LocalLog {

	public class LogManagerFactory : ILogManagerFactory {

		private static ILogManagerFactory factory = new LogManagerFactory();
        private static bool sharedDbMode = false;
		public static ILogManagerFactory Factory => factory;

		// =====================================================================
		// ILogManagerFactory
		// =====================================================================

		ILogManager ILogManagerFactory.Create(
            string path,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor,
            Action<Exception> panicHandler
        ) => new LogManager(path, logJobMonitor, logEventMonitor, panicHandler, sharedDbMode);

		// =====================================================================
		// Static
		// =====================================================================

        public static bool SetSharedDbMode(bool shared) => sharedDbMode = shared;

		public static ILogManager Create(
            string path,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor = null,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor = null,
            Action<Exception> panicHandler = null
        ) => Factory.Create(path, logJobMonitor, logEventMonitor, panicHandler);

		public static void InjectCustomFactory(ILogManagerFactory factory) 
			=> LogManagerFactory.factory = factory;
	}
}