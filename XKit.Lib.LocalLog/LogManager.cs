using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Utility.Threading;

namespace XKit.Lib.LocalLog {

    internal class LogManager : ILogManager {

        private readonly string logPath;
        private readonly Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor;
        private readonly Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor;
        private readonly Action<Exception> panicHandler;
        private readonly TaskQueue writeTaskQueue = new TaskQueue();
        private readonly SemaphoreSlim writeSyncronizer = new SemaphoreSlim(1, 1);

        public LogManager(
            string logPath,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor,
            Action<Exception> panicHandler,
            bool sharedDbMode
        ) {
            this.logPath = logPath;
            this.logJobMonitor = logJobMonitor;
            this.logEventMonitor = logEventMonitor;
            this.panicHandler = panicHandler;
        }

        // ===========================================================================
        // ILogManager
        // ===========================================================================

        ILogSession ILogManager.CreateWriteableSession(
            JobTypeEnum jobType,
            JobSystemEffect effect,
            string originatorName,
            int originatorVersion,
            string jobName,
            string hostId,
            string instanceId,
            Guid jobId,
            string correlationId,
            string requestorFabricId,
            string requestorInstanceId,
            IEnumerable<string> correlationTags
        ) => CreateSession(
            true,
            jobType,
            effect,
            originatorName,
            originatorVersion,
            jobName,
            hostId,
            instanceId,
            jobId,
            correlationId,
            requestorFabricId,
            requestorInstanceId,
            correlationTags
        );

        ILogSession ILogManager.CreateReadOnlySession() => CreateSession(false);

        void ILogManager.Finish() {
            var lastTask = writeTaskQueue.LastTask;
            if (lastTask != null) {
                Task.WaitAll(new[] { lastTask }, 30 * 1000);
            }
        }

        // ===========================================================================
        // private
        // ===========================================================================

        ILogSession CreateSession(
            bool writeable,
            JobTypeEnum? jobType = null,
            JobSystemEffect? effect = null,
            string originatorName = null,
            int originatorVersion = 0,
            string jobName = null,
            string hostId = null,
            string instanceId = null,
            Guid jobId = default(Guid),
            string correlationId = null,
            string requestorFabricId = null,
            string requestorInstanceId = null,
            IEnumerable<string> correlationTags = null            
        ) {

            return new LogSession(
                this,
                logPath,
                jobType.GetValueOrDefault(JobTypeEnum.OtherUnknown),
                effect.GetValueOrDefault(writeable ? JobSystemEffect.NotSpecified : JobSystemEffect.None),
                jobName,
                originatorName,
                originatorVersion,
                hostId,
                instanceId,
                jobId,
                correlationId ?? XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                requestorInstanceId,
                requestorFabricId,
                correlationTags,
                writeable,
                false,
                logJobMonitor,
                logEventMonitor,
                panicHandler,
                writeTaskQueue,
                writeSyncronizer
            );
        }
    }
}