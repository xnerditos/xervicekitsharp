using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.LocalLog.Entities;
using XKit.Lib.LocalLog.Internal;

namespace XKit.Lib.LocalLog {

    internal class LogReader : ILogReader {

        private readonly ILogSessionInternal SessionData;

        public LogReader(ILogSessionInternal sessionData) {
            this.SessionData = sessionData;
        }

        // =====================================================================
        // public
        // =====================================================================
        public Task<IEnumerable<LogJobEntry>> QueryJobs(
            IEnumerable<Guid> jobIds = null,
            string matchingOriginatorName = null,
            int? matchingOriginatorVersion = null,
            string matchingJobName = null,
            JobTypeEnum? matchingJobType = null,
			DateTime? startOnOrBeforeTimestamp = null,
			DateTime? startOnOrAfterTimestamp = null,
            bool? isComplete = null,
			DateTime? completeOnOrBeforeTimestamp = null,
			DateTime? completeOnOrAfterTimestamp = null,
            string matchingCorrelationId = null,
            string havingCorrelationTag = null,
            string matchingInstanceId = null,
            string matchingHostFabricId = null,
            string matchingRequestorInstanceId = null,
            string matchingRequestorFabricId = null,
            int pageSize = 100,
            int pageIndex = 0
        ) => QueryJobs(
            reader => LogJobEntry.FromDb(reader),
            null,
            jobIds,
            matchingOriginatorName,
            matchingOriginatorVersion,
            matchingJobName,
            matchingJobType,
            startOnOrBeforeTimestamp,
            startOnOrAfterTimestamp,
            isComplete,
            completeOnOrBeforeTimestamp,
            completeOnOrAfterTimestamp,
            matchingCorrelationId,
            havingCorrelationTag,
            matchingInstanceId,
            matchingHostFabricId,
            matchingRequestorInstanceId,
            matchingRequestorFabricId,
            pageSize,
            pageIndex
        );

        public async Task<IEnumerable<T>> QueryJobs<T>(
            Func<IDataReader, T> selector,
            string selectFields = null,
            IEnumerable<Guid> jobIds = null,
            string matchingOriginatorName = null,
            int? matchingOriginatorVersion = null,
            string matchingJobName = null,
            JobTypeEnum? matchingJobType = null,
			DateTime? startOnOrBeforeTimestamp = null,
			DateTime? startOnOrAfterTimestamp = null,
            bool? isComplete = null,
			DateTime? completeOnOrBeforeTimestamp = null,
			DateTime? completeOnOrAfterTimestamp = null,
            string matchingCorrelationId = null,
            string havingCorrelationTag = null,
            string matchingInstanceId = null,
            string matchingHostFabricId = null,
            string matchingRequestorInstanceId = null,
            string matchingRequestorFabricId = null,
            int pageSize = 100,
            int pageIndex = 0
        ) {
            selectFields = selectFields ?? "*";
            using var connection = SessionData.CreateConnection();
            using var cmd = connection.CreateCommand();

            StringBuilder query = new StringBuilder(128);
            query.AppendLine(LogDbHelper.GetHeaderSql());
            string footer = "";
            string innerJoin = "";

            // Indexed
            if (jobIds != null) {
                var lstSql = LogDbHelper.GetListTableSql();
                
                query.Append(lstSql.header);
                footer += lstSql.footer;

                jobIds.ForEach(g => LogDbHelper.AddListItem(query, lstSql.table, cmd, g));
                innerJoin += $"INNER JOIN {lstSql.table} ON LogJobs.LogJobEntryId = {lstSql.table}.Val \n";
            }

            query.AppendLine($"SELECT {selectFields} FROM LogJobs ");
            query.Append(innerJoin);
            query.AppendLine("WHERE 1=1");

            if (!string.IsNullOrEmpty(matchingCorrelationId)) {
                query.AppendLine("AND CorrelationId = @CorrelationId");
                cmd.Parameters.AddWithValue("@CorrelationId", matchingCorrelationId);
            }
            if (startOnOrBeforeTimestamp.HasValue || startOnOrAfterTimestamp.HasValue) {
                var onOrBeforeStart =
                    (startOnOrBeforeTimestamp.HasValue ? startOnOrBeforeTimestamp.Value : DateTime.MaxValue);
                var onOrAfterStart =
                    (startOnOrAfterTimestamp.HasValue ? startOnOrAfterTimestamp.Value : DateTime.MinValue);
                query.AppendLine("AND StartTimestamp <= @onOrBeforeStart AND StartTimestamp >= @onOrAfterStart");
                cmd.Parameters.AddWithValue("@onOrBeforeStart", onOrBeforeStart);
                cmd.Parameters.AddWithValue("@onOrAfterStart", onOrAfterStart);
            }
            if (isComplete.HasValue) {
                if (isComplete.Value) {
                    query.AppendLine("AND CompleteTimestamp IS NOT NULL");
                } else {
                    query.AppendLine("AND CompleteTimestamp IS NULL");
                }
            }
            if (completeOnOrBeforeTimestamp.HasValue || completeOnOrAfterTimestamp.HasValue) {
                var onOrBeforeComplete =
                    (completeOnOrBeforeTimestamp.HasValue ? completeOnOrBeforeTimestamp.Value : DateTime.MaxValue);
                var onOrAfterComplete =
                    (completeOnOrBeforeTimestamp.HasValue ? completeOnOrAfterTimestamp.Value : DateTime.MinValue);
                query.AppendLine("AND CompleteTimestamp <= @onOrBeforeComplete AND CompleteTimestamp >= @onOrAfterComplete");
                cmd.Parameters.AddWithValue("@onOrBeforeComplete", onOrBeforeComplete);
                cmd.Parameters.AddWithValue("@onOrAfterComplete", onOrAfterComplete);
            }
            if (!string.IsNullOrEmpty(matchingInstanceId)) {
                query.AppendLine("AND InstanceId = @InstanceId");
                cmd.Parameters.AddWithValue("@InstanceId", matchingInstanceId);
            }
            if (!string.IsNullOrEmpty(matchingHostFabricId)) {
                query.AppendLine("AND HostFabricId = @HostFabricId");
                cmd.Parameters.AddWithValue("@HostFabricId", matchingHostFabricId);
            }
            if (!string.IsNullOrEmpty(matchingOriginatorName)) {
                query.AppendLine("AND OriginatorName = @OriginatorName");
                cmd.Parameters.AddWithValue("@OriginatorName", matchingOriginatorName);
                if (matchingOriginatorVersion != null) {
                    query.AppendLine("AND OriginatorVersion = @OriginatorVersion");
                    cmd.Parameters.AddWithValue("@OriginatorVersion", matchingOriginatorVersion);
                }
            }
            if (!string.IsNullOrEmpty(matchingJobName)) {
                query.AppendLine("AND JobName = @JobName");
                cmd.Parameters.AddWithValue("@JobName", matchingJobName);
            }
            if (!string.IsNullOrEmpty(matchingRequestorInstanceId)) {
                query.AppendLine("AND RequestorInstanceId = @RequestorInstanceId");
                cmd.Parameters.AddWithValue("@RequestorInstanceId", matchingRequestorInstanceId);
            }
            if (!string.IsNullOrEmpty(matchingRequestorFabricId)) {
                query.AppendLine("AND RequestorFabricId = @RequestorFabricId");
                cmd.Parameters.AddWithValue("@RequestorFabricId", matchingRequestorFabricId);
            }
            if (!string.IsNullOrEmpty(havingCorrelationTag)) {
                query.AppendLine($"AND CorrelationTags LIKE '%|{havingCorrelationTag}|%'");
            }

            query.AppendLine("ORDER BY StartTimestamp");
            int skipCount = pageIndex * pageSize;
            query.AppendLine($"LIMIT {pageSize} OFFSET {skipCount};");
            query.AppendLine(footer);
            query.AppendLine(LogDbHelper.GetFooterSql());

            cmd.CommandText = query.ToString();
            using var reader = await cmd.ExecuteReaderAsync();
            var lst = new List<T>();
            while(await reader.ReadAsync()) {
                lst.Add(selector(reader));
            }
            connection.Close();
            return lst;
        }

        public Task<IEnumerable<LogEventEntry>> QueryEvents(
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
			DateTime? onOrBeforeTimestamp = null,
			DateTime? onOrAfterTimestamp = null,
			LogEventType? matchingEventType = null,
			string elseMatchingEventTypeName = null,
            string matchingCode = null,
            string havingTag = null,
            int pageSize = 100,
            int pageIndex = 0,
			bool includePrivate = false,
            bool groupByJob = false
        ) => QueryEvents(
            reader => LogEventEntry.FromDb(reader),
            null,
            jobIds,
            eventIds,
            onOrBeforeTimestamp,
            onOrAfterTimestamp,
            matchingEventType,
            elseMatchingEventTypeName,
            matchingCode,
            havingTag,
            pageSize,
            pageIndex,
            includePrivate,
            groupByJob
        );

        public async Task<IEnumerable<T>> QueryEvents<T>(
            Func<IDataReader, T> selector,
            string selectFields = null,
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
			DateTime? onOrBeforeTimestamp = null,
			DateTime? onOrAfterTimestamp = null,
			LogEventType? matchingEventType = null,
			string elseMatchingEventTypeName = null,
            string matchingCode = null,
            string havingTag = null,
            int pageSize = 100,
            int pageIndex = 0,
			bool includePrivate = false,
            bool groupByJob = false
        ) {
            selectFields = selectFields ?? "*";
            using var connection = SessionData.CreateConnection();
            using var cmd = connection.CreateCommand();

            StringBuilder query = new StringBuilder(128);
            query.AppendLine(LogDbHelper.GetHeaderSql());
            string footer = "";
            string innerJoin = "";

            if (eventIds != null) {
                var lstSql = LogDbHelper.GetListTableSql();
                
                query.Append(lstSql.header);
                footer += lstSql.footer;

                eventIds.ForEach(g => LogDbHelper.AddListItem(query, lstSql.table, cmd, g));
                innerJoin += $"INNER JOIN {lstSql.table} ON LogEvents.LogEventEntryId = {lstSql.table}.Val \n";
            }
            if (jobIds != null) {
                var lstSql = LogDbHelper.GetListTableSql();
                
                query.Append(lstSql.header);
                footer += lstSql.footer;

                jobIds.ForEach(g => LogDbHelper.AddListItem(query, lstSql.table, cmd, g));
                innerJoin += $"INNER JOIN {lstSql.table} ON LogEvents.LogJobEntryId = {lstSql.table}.Val \n";
            }

            query.AppendLine($"SELECT {selectFields} FROM LogEvents ");
            query.Append(innerJoin);
            query.AppendLine("WHERE 1=1");

            if (!string.IsNullOrEmpty(matchingCode)) {
                query.AppendLine("AND Code = @Code");
                cmd.Parameters.AddWithValue("@Code", matchingCode);
            }
            if (matchingEventType.HasValue) {
                string s = matchingEventType.ToString();
                query.AppendLine("AND EventTypeName = @EventTypeName");
                cmd.Parameters.AddWithValue("@EventTypeName", s);
            }
            if (elseMatchingEventTypeName != null) {
                query.AppendLine("AND EventTypeName = @EventTypeName");
                cmd.Parameters.AddWithValue("@EventTypeName", elseMatchingEventTypeName);
            }
            if (onOrBeforeTimestamp.HasValue || onOrAfterTimestamp.HasValue) {
                var useOnOrBeforeTimestampDateTime =
                    (onOrBeforeTimestamp.HasValue ? onOrBeforeTimestamp.Value : DateTime.MaxValue);
                var useOnOrAfterTimestampDateTime =
                    (onOrAfterTimestamp.HasValue ? onOrAfterTimestamp.Value : DateTime.MinValue);
                query.AppendLine("AND Timestamp <= @useOnOrBeforeTimestampDateTime AND Timestamp >= @useOnOrAfterTimestampDateTime");
                cmd.Parameters.AddWithValue("@useOnOrBeforeTimestampDateTime", useOnOrBeforeTimestampDateTime);
                cmd.Parameters.AddWithValue("@useOnOrAfterTimestampDateTime", useOnOrAfterTimestampDateTime);
            }
            if (!string.IsNullOrEmpty(havingTag)) {
                query.AppendLine($"AND Tags LIKE '%|{havingTag}|%'");
            }
            if (!includePrivate) {
                query.AppendLine("AND IsPrivate <> true");
            }

            if (groupByJob) {
                query.AppendLine("ORDER BY LogJobEntryId, Timestamp");
            } else {
                query.AppendLine("ORDER BY Timestamp");
            }

            int skipCount = pageIndex * pageSize;
            query.AppendLine($"LIMIT {pageSize} OFFSET {skipCount};");
            query.AppendLine(footer);
            query.AppendLine(LogDbHelper.GetFooterSql());

            cmd.CommandText = query.ToString();
            using var reader = await cmd.ExecuteReaderAsync();
            var lst = new List<T>();
            while(await reader.ReadAsync()) {
                lst.Add(selector(reader));
            }
            connection.Close();
            return lst;
        }

        public Task<IEnumerable<LogEntryData>> QueryData(
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
            int pageSize = 100,
            int pageIndex = 0
        ) => QueryData(
            reader => LogEntryData.FromDb(reader),
            null,
            jobIds,
            eventIds,
            pageSize,
            pageIndex
        );

        public async Task<IEnumerable<T>> QueryData<T>(
            Func<IDataReader, T> selector,
            string selectFields = null,
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
            int pageSize = 100,
            int pageIndex = 0
        ) {
            selectFields = selectFields ?? "*";

            using var connection = SessionData.CreateConnection();
            using var cmd = connection.CreateCommand();

            StringBuilder query = new StringBuilder(128);
            query.AppendLine(LogDbHelper.GetHeaderSql());
            string footer = "";
            string innerJoin = "";

            if (eventIds != null) {
                var lstSql = LogDbHelper.GetListTableSql();
                
                query.Append(lstSql.header);
                footer += lstSql.footer;

                eventIds.ForEach(g => LogDbHelper.AddListItem(query, lstSql.table, cmd, g));
                innerJoin += $"INNER JOIN {lstSql.table} ON LogData.LogEventEntryId = {lstSql.table}.Val \n";
            }
            if (jobIds != null) {
                var lstSql = LogDbHelper.GetListTableSql();
                
                query.Append(lstSql.header);
                footer += lstSql.footer;

                jobIds.ForEach(g => LogDbHelper.AddListItem(query, lstSql.table, cmd, g));
                innerJoin += $"INNER JOIN {lstSql.table} ON LogData.LogJobEntryId = {lstSql.table}.Val \n";
            }

            query.AppendLine($"SELECT {selectFields} FROM LogData ");
            query.AppendLine(innerJoin);

            int skipCount = pageIndex * pageSize;
            query.AppendLine($"LIMIT {pageSize} OFFSET {skipCount};");
            query.AppendLine(footer);
            query.AppendLine(LogDbHelper.GetFooterSql());

            cmd.CommandText = query.ToString();
            using var reader = await cmd.ExecuteReaderAsync();
            var lst = new List<T>();
            while(await reader.ReadAsync()) {
                lst.Add(selector(reader));
            }
            connection.Close();
            return lst;
        }

        // =====================================================================
        // ILogReader
        // =====================================================================

        async Task<IEnumerable<IReadOnlyLogJobEntry>> ILogReader.QueryJobs(
            IEnumerable<Guid> jobIds, 
            string matchingOriginatorName, 
            int? matchingOriginatorVersion, 
            string matchingJobName, 
            JobTypeEnum? matchingJobType, 
            DateTime? startOnOrBeforeTimestamp, 
            DateTime? startOnOrAfterTimestamp, 
            bool? isComplete,
            DateTime? completeOnOrBeforeTimestamp, 
            DateTime? completeOnOrAfterTimestamp, 
            string matchingCorrelationId, 
            string havingCorrelationTag, 
            string matchingInstanceId, 
            string matchingHostFabricId, 
            string matchingRequestorInstanceId, 
            string matchingRequestorFabricId, 
            int pageSize, 
            int pageIndex
        ) => await QueryJobs(
            jobIds,
            matchingOriginatorName,
            matchingOriginatorVersion,
            matchingJobName,
            matchingJobType,
            startOnOrBeforeTimestamp,
            startOnOrAfterTimestamp,
            isComplete,
            completeOnOrBeforeTimestamp,
            completeOnOrAfterTimestamp,
            matchingCorrelationId,
            havingCorrelationTag,
            matchingInstanceId,
            matchingHostFabricId,
            matchingRequestorInstanceId,
            matchingRequestorFabricId,
            pageSize,
            pageIndex
        );

        async Task<IEnumerable<IReadOnlyLogEventEntry>> ILogReader.QueryEvents(
            IEnumerable<Guid> jobIds, 
            IEnumerable<Guid> eventIds, 
            DateTime? onOrBeforeTimestamp, 
            DateTime? onOrAfterTimestamp, 
            LogEventType? matchingEventType, 
            string elseMatchingEventTypeName, 
            string matchingCode, 
            string havingTag, 
            int pageSize, 
            int pageIndex, 
            bool includePrivate, 
            bool groupByJob
        ) => await QueryEvents(
            jobIds,
            eventIds,
            onOrBeforeTimestamp,
            onOrAfterTimestamp,
            matchingEventType,
            elseMatchingEventTypeName,
            matchingCode,
            havingTag,
            pageSize,
            pageIndex,
            includePrivate,
            groupByJob
        );

        async Task<IEnumerable<IReadOnlyLogEntryData>> ILogReader.QueryData(
            IEnumerable<Guid> jobIds, 
            IEnumerable<Guid> eventIds, 
            int pageSize, 
            int pageIndex
        ) => await QueryData(
            jobIds,
            eventIds,
            pageSize,
            pageIndex
        );
    }
}