using System;
using System.Collections.Generic;
using XKit.Lib.Common.Log;
using XKit.Lib.LocalLog.Internal;
using XKit.Lib.Common.Utility.Extensions;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.LocalLog.Entities;
using System.Data.SQLite;
using System.Threading;

namespace XKit.Lib.LocalLog {

    internal class LogWriter : ILogWriter {

        private readonly ILogSessionInternal SessionData;
		private Dictionary<string, object> autologValues = new Dictionary<string, object>();
        private readonly LogJobEntry activeJobEntry;
        private readonly SemaphoreSlim WriteSyncronizer;

        private LogJobEntry ActiveJobEntry {
            get => activeJobEntry;
        } 

        public LogWriter(
            ILogSessionInternal sessionData,
            SemaphoreSlim writeSyncronizer
        ) {
            this.SessionData = sessionData;
            this.WriteSyncronizer = writeSyncronizer;
            activeJobEntry = CreateJobEntity();
        }

        // =====================================================================
        // ILogWriter
        // =====================================================================

        async Task<IReadOnlyLogEventEntry> ILogWriter.Begin(
            object workItem,
            IReadOnlyDictionary<string, object> attributes
        ) {
            try {

                using var connection = SessionData.CreateConnection();
                await ActiveJobEntry.ToDb(connection, WriteSyncronizer);

                SessionData.JobMonitor?.Invoke(ActiveJobEntry, null, true);
                var entry = await PersistNewEventEx(
					eventTypeName: LogEventType.Enter.ToString(),
                    message: null,
                    attributes: attributes,
                    data: workItem == null ? null : new[] { new KeyValuePair<string, object>(CommonAttributeNames.JobWorkItem, workItem) },
                    connection: connection
                ); 
                connection.Close();
                return entry;     

            } catch (Exception ex) {
                SessionData.PanicHandler?.Invoke(ex);
                return new LogEventEntry();
            }
        }

        async Task<IReadOnlyLogEventEntry> ILogWriter.End(
			JobResultStatusEnum status,
			string message,
			object resultData,
            object resultCode,
            JobReplayStrategyEnum? replayStrategy,
            object replayData
        ) {
            try {

                ActiveJobEntry.Status = status;
                ActiveJobEntry.ResultCode = resultCode?.ToString();
                ActiveJobEntry.Message = message;

                bool success = false;
                switch(status) {
                    case JobResultStatusEnum.PartialSuccess:
                    case JobResultStatusEnum.Success:
                        success = true;
                        break;
                }

                using var connection = SessionData.CreateConnection();

                if (success && replayStrategy.GetValueOrDefault(JobReplayStrategyEnum.NotSpecifiedOrUnknown) != JobReplayStrategyEnum.NotSpecifiedOrUnknown) {

                    await PersistNewEventEx(
                        eventTypeName: LogEventType.ReplayHint.ToString(),
                        message: null,
                        attributes: new Dictionary<string, object> {
                            { CommonAttributeNames.JobReplayStrategy, replayStrategy.Value.ToString() },
                            { CommonAttributeNames.JobReplayTargetId, SessionData.JobId.ToString() },
                        },
                        data: new[] { new KeyValuePair<string, object>(CommonAttributeNames.JobReplayData, replayData) },
                        connection: connection
                    ); 
                } 
                
                var entry = await PersistNewEventEx(
                    eventTypeName: LogEventType.Exit.ToString(),
                    message: null,
                    attributes: autologValues,
                    data: new[] { new KeyValuePair<string, object>(CommonAttributeNames.JobEndResultData, resultData) },
                    connection: connection
                ); 

                ActiveJobEntry.CompleteTimestamp = DateTime.UtcNow.FloorTime(TimeSpan.FromMilliseconds(1));
                await ActiveJobEntry.UpdateCompletion(connection, WriteSyncronizer);
                connection.Close();
                
                SessionData.JobMonitor?.Invoke(ActiveJobEntry, null, false);                    
                return entry;

            } catch (Exception ex) {
                SessionData.PanicHandler?.Invoke(ex);
            }
            return new LogEventEntry();
        }

        ILogWriter ILogWriter.AutoLog(string key, object value) {
            autologValues[key ?? string.Empty] = value;
			return this;
        }

        ILogWriter ILogWriter.AutoLog(IDictionary<string, object> values) {
            foreach(var kv in values) {
                autologValues[kv.Key] = kv.Value;
            }
			return this;
        }

        ILogWriter ILogWriter.AutoLog(object anonObjectAsKeysAndValues) {
            if (anonObjectAsKeysAndValues == null) { return this; }
            // if (anonObjectAsKeysAndValues.GetType().Namespace != null) {
            //     throw new ArgumentException("Object must be anonymous");
            // }
            
            foreach(var kv in anonObjectAsKeysAndValues.FieldsToDictionary()) {
                autologValues[kv.Key] = kv.Value;
            }
			return this;
        }

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Erratum(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Erratum),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);

        Task<IReadOnlyLogEventEntry> ILogWriter.ErratumAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, int lineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Erratum),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber,
				includeStackTrace: false
			);

        void ILogWriter.ErratumBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Erratum),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);

        void ILogWriter.ErratumAsBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, int lineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Erratum),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber,
				includeStackTrace: false
			);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Error(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Error),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);

        Task<IReadOnlyLogEventEntry> ILogWriter.ErrorAs(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Error),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber,
				includeStackTrace: false
			);

        void ILogWriter.ErrorBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Error),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);

        void ILogWriter.ErrorAsBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Error),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber,
				includeStackTrace: false
			);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Fatality(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) {
			var logEntry = PersistNewEvent(
				eventTypeName: nameof(LogEventType.Fatality),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);
			return logEntry;
		} 

        void ILogWriter.FatalityBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Fatality),
				message: message,
				attributes: attributes,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber,
				includeStackTrace: false
			);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Info(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
			object data, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Info),
				message: message,
				attributes: attributes,
				data: data,
				code: code,
				tags: tags
			);

        void ILogWriter.InfoBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
			object data, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Info),
				message: message,
				attributes: attributes,
				data: data,
				code: code,
				tags: tags
			);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.NewEvent(
			LogEventType eventType, 
			string message, 
			object data, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags,
			bool isPrivate
		) => PersistNewEvent(
				eventTypeName: eventType.ToString(),
				message: message,
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: isPrivate
		);

        Task<IReadOnlyLogEventEntry> ILogWriter.NewEvent(
			string eventTypeName, 
			string message, 
			object data, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags,
			bool isPrivate
		) => PersistNewEvent(
				eventTypeName: eventTypeName == nameof(LogEventType.None) || eventTypeName == nameof(LogEventType.ServiceDefined) 
					? "" : eventTypeName ?? "",
				message: message,
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: isPrivate
			);

        void ILogWriter.NewEventBackground(
			LogEventType eventType, 
			string message, 
			object data, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags,
			bool isPrivate
		) => PersistNewEventBackground(
				eventTypeName: eventType.ToString(),
				message: message,
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: isPrivate
		    );

        void ILogWriter.NewEventBackground(
			string eventTypeName, 
			string message, 
			object data, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags,
			bool isPrivate
		) => PersistNewEventBackground(
				eventTypeName: eventTypeName == nameof(LogEventType.None) || eventTypeName == nameof(LogEventType.ServiceDefined) 
					? "" : eventTypeName ?? "",
				message: message,
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: isPrivate
			);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Snapshot(
			IReadOnlyDictionary<string, object> attributes, 
			object data, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Snapshot),
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: true
		);

        void ILogWriter.SnapshotBackground(
			IReadOnlyDictionary<string, object> attributes, 
			object data, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Snapshot),
				attributes: attributes,
				code: code,
				data: data,
				tags: tags,
				isPrivate: true
		    );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Status(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Status),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags
		);

        void ILogWriter.StatusBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
			IEnumerable<string> tags
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Status),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags
		    );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Trace(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Trace),
				message: message,
				attributes: attributes,
				tags: tags,
				code: code,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber
		);

        Task<IReadOnlyLogEventEntry> ILogWriter.TraceAs(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Trace),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber
		);

        void ILogWriter.TraceBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Trace),
				message: message,
				attributes: attributes,
				tags: tags,
				code: code,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber
		    );

        void ILogWriter.TraceAsBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Trace),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber
		    );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Audit(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Audit),
				message: message,
				attributes: attributes,
				tags: tags,
				code: code
		);

        void ILogWriter.AuditBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Audit),
				message: message,
				attributes: attributes,
				tags: tags,
				code: code
		    );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.Warning(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Warning),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber
		);

        Task<IReadOnlyLogEventEntry> ILogWriter.WarningAs(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEvent(
				eventTypeName: nameof(LogEventType.Warning),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber
		);

        void ILogWriter.WarningBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string callerFilePath,
			string callerMemberName,
			int callerLineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Warning),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: callerFilePath,
				callerMemberName: callerMemberName,
				callerLineNumber: callerLineNumber
		);

        void ILogWriter.WarningAsBackground(
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
			string filePath,
			string memberName,
			int lineNumber
		) => PersistNewEventBackground(
				eventTypeName: nameof(LogEventType.Warning),
				message: message,
				attributes: attributes,
				code: code,
				tags: tags,
				callerFilePath: filePath,
				callerMemberName: memberName,
				callerLineNumber: lineNumber
	    	);

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ILogWriter.ReplaceReplayHint(
            Guid forJobId, 
            JobReplayStrategyEnum replayStrategy, 
            object replayData
		) => PersistNewEvent(
            eventTypeName: LogEventType.ReplayHint.ToString(),
            message: null,
            attributes: new Dictionary<string, object> {
                { CommonAttributeNames.JobReplayStrategy, replayStrategy.ToString() },
                { CommonAttributeNames.JobReplayTargetId, forJobId.ToString() },
            },
            data: new[] { new KeyValuePair<string, object>(CommonAttributeNames.JobReplayData, replayData) }
		);

        void ILogWriter.ReplaceReplayHintBackground(
            Guid forJobId, 
            JobReplayStrategyEnum replayStrategy, 
            object replayData
		) => PersistNewEventBackground(
            eventTypeName: LogEventType.ReplayHint.ToString(),
            message: null,
            attributes: new Dictionary<string, object> {
                { CommonAttributeNames.JobReplayStrategy, replayStrategy.ToString() },
                { CommonAttributeNames.JobReplayTargetId, forJobId.ToString() },
            },
            data: new[] { new KeyValuePair<string, object>(CommonAttributeNames.JobReplayData, replayData) }
		);

		// ===========================================================================
		// private 
  		// ===========================================================================

        private void PersistNewEventBackground(
			string eventTypeName, 
			string message = null, 
			IReadOnlyDictionary<string, object> attributes = null, 
			object data = null,
			object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = null,
			string callerMemberName = null,
			int callerLineNumber = 0,
			bool includeStackTrace = false,
			bool isPrivate = false
		) => PersistNewEventBackground(
            eventTypeName,
            message,
            attributes,
            data == null ? null : new[] { new KeyValuePair<string, object>("", data )},
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            includeStackTrace,
            isPrivate
        );

        private void PersistNewEventBackground(
			string eventTypeName, 
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
			KeyValuePair<string, object>[] data,
			object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = null,
			string callerMemberName = null,
			int callerLineNumber = 0,
			bool includeStackTrace = false,
			bool isPrivate = false
		) {
            SessionData.AddBackgroundTask(
                () => PersistNewEvent(
                    eventTypeName,
                    message,
                    attributes,
                    data,
                    code,
                    tags,
                    callerFilePath,
                    callerMemberName,
                    callerLineNumber,
                    includeStackTrace,
                    isPrivate
                )
            );
        }

        Task<IReadOnlyLogEventEntry> PersistNewEvent(
			string eventTypeName, 
			string message = null, 
			IReadOnlyDictionary<string, object> attributes = null, 
			object data = null,
			object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = null,
			string callerMemberName = null,
			int callerLineNumber = 0,
			bool includeStackTrace = false,
			bool isPrivate = false,
            SQLiteConnection connection = null
		) => PersistNewEventEx(
            eventTypeName,
            message,
            attributes,
            data == null ? null : new[] { new KeyValuePair<string, object>("", data )},
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            includeStackTrace,
            isPrivate,
            connection
        );

        async Task<IReadOnlyLogEventEntry> PersistNewEventEx(
			string eventTypeName, 
			string message, 
			IReadOnlyDictionary<string, object> attributes, 
			KeyValuePair<string, object>[] data,
			object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = null,
			string callerMemberName = null,
			int callerLineNumber = 0,
			bool includeStackTrace = false,
			bool isPrivate = false,
            SQLiteConnection connection = null
		) {
            var useConnection = connection ?? SessionData.CreateConnection();
            try {
                Dictionary<string, object> attributesDictionary = 
                    attributes == null ? 
                        new Dictionary<string, object>() :
                        new Dictionary<string, object>(attributes);

                if (!string.IsNullOrEmpty(callerFilePath) || 
                    !string.IsNullOrEmpty(callerMemberName) || 
                    callerLineNumber != 0) {
                    attributesDictionary[CommonAttributeNames.CallerFileName] =  GetFilenameFromPath(callerFilePath);
                    attributesDictionary[CommonAttributeNames.CallerMemberName] = callerMemberName;
                    attributesDictionary[CommonAttributeNames.CallerLineNumber] = callerLineNumber;
                    attributesDictionary[CommonAttributeNames.CallerFileHash] = callerFilePath.GetHashCode();
                }

                if (includeStackTrace) {
                    attributesDictionary[CommonAttributeNames.StackTrace] = GetStackTrace();
                }

                if (tags == null) {
                    tags = new string[0];
                }

                var entry = CreateEventEntity(
                    eventTypeName,
                    message,
                    code,
                    attributesDictionary,
                    tags,
                    isPrivate
                );

                LogEntryData[] dataEntries = null;
                if (data != null) {
                    dataEntries = new LogEntryData[data.Length];
                    int i = 0;
                    data.ForEach(d => {
                        if (d.Key != null || d.Value != null) {
                            dataEntries[i++] = CreateDataEntity(
                                entry.LogEventEntryId, 
                                d.Key ?? "", 
                                d.Value
                            );
                        }
                    });
                }
                SessionData.EventMonitor?.Invoke(this.ActiveJobEntry, entry, dataEntries);                    

                await entry.ToDb(useConnection, WriteSyncronizer);
                if (dataEntries != null) {
                    await dataEntries.Where(d => d != null).ForEach(async (d) => await d.ToDb(useConnection, WriteSyncronizer));
                }

                return entry;
                
            } catch (Exception ex) {
                SessionData.PanicHandler?.Invoke(ex);
                return new LogEventEntry();
            } finally {
                if (connection == null) {
                    try {
                        await useConnection.CloseAsync();
                        await useConnection.DisposeAsync();
                    } catch (Exception ex2) {
                        SessionData.PanicHandler?.Invoke(ex2);
                    }
                }
            }
        }
 
 		private string GetStackTrace()
		{
			var stackTrace = Environment.StackTrace;
			int firstLine = stackTrace.IndexOf(Environment.NewLine);
			if (firstLine <= 0) {
				return stackTrace;
			}
			firstLine += Environment.NewLine.Length;
			return stackTrace.Substring(firstLine + Environment.NewLine.Length);
		}

		private string GetFilenameFromPath(string path) {
			if (string.IsNullOrEmpty(path)) {
				return path;
			}
			return System.IO.Path.GetFileName(path);
		}

        public LogJobEntry CreateJobEntity() {
            return new LogJobEntry {
                OriginatorName = SessionData.OriginatorName,
                OriginatorVersion = SessionData.OriginatorVersion,
				JobName = SessionData.OperationName,
                HostFabricId = SessionData.HostFabricId,
                InstanceId = SessionData.InstanceId,
                LogJobEntryId = SessionData.JobId,
                CorrelationId = SessionData.CorrelationId,
                RequestorInstanceId = SessionData.RequestorInstanceId,
                RequestorFabricId = SessionData.RequestorFabricId,
                CorrelationTags = SessionData.CorrelationTags?.Select(s => new LogTag { Name = s }).ToArray(),
                StartTimestamp = DateTime.UtcNow,
                JobType = SessionData.JobType,
                SystemEffect = SessionData.Effect,
                Status = JobResultStatusEnum.Incomplete
            };
        } 
 
        public LogEventEntry CreateEventEntity(
            string eventTypeName, 
            string message, 
			object code,
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags,
			bool isPrivate
        ) {
            return new LogEventEntry {
                LogEventEntryId = Guid.NewGuid(),
                EventTypeName = eventTypeName ?? "",
                Message = message,
                Attributes = 
                    attributes != null ? 
                    attributes.Select(kv => new LogAttribute { Name = kv.Key, Value = kv.Value?.ToString() }).ToArray() : 
                    null,
                Tags = tags?.Select(s => new LogTag { Name = s }).ToArray(),
                Timestamp = DateTime.UtcNow,
                //LogEventEntryId = Guid.NewGuid(),
                TraceIndex = SessionData.NextTraceIndex(),
				IsPrivate = isPrivate,
				Code = code?.ToString(),
                LogJobEntryId = SessionData.JobId
            };
        } 
 
        public LogEntryData CreateDataEntity(
            Guid ownerId,
            string name, 
			object value
        ) {
            var d = new LogEntryData {
                LogEntryDataId = Guid.NewGuid(),
                Name = name,
                LogEventEntryId = ownerId,
                LogJobEntryId = SessionData.JobId
            };
            d.SetValue(value);
            return d;
        } 
    }
}