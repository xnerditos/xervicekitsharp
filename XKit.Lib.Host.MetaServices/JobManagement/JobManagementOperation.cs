using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.OperationManagement {

    internal class JobManagementOperation : ServiceOperation<IJobManagementMetaService>, IJobManagementApi {

        public JobManagementOperation(
            ServiceOperationContext context
        ) :base(context) {
        }

        // =====================================================================
        // IOperationControlOperation
        // =====================================================================

        async Task<ServiceCallResult<JobQueryResponse>> IJobManagementApi.Query(JobQueryRequest request) {
            return await RunServiceCall(
                requestBody: request,
                requestValidationAction: ValidateQueryOperation,
                operationAction: Query
            );
        }

        // =====================================================================
        // Workers
        // =====================================================================

        private bool ValidateQueryOperation(JobQueryRequest request) {
            bool validated = false;
            if (!validated) { validated = !string.IsNullOrEmpty(request.CorrelationId); }
            if (!validated) { validated = !string.IsNullOrEmpty(request.CorrelationTag); }
            if (!validated) { validated = request.JobIds != null && request.JobIds.Any(); }
            return validated;
        }

        private async Task<JobQueryResponse> Query(JobQueryRequest request) {
            
            var logQuery = await LogSession.GetLogReader().QueryJobs(
                matchingCorrelationId: request.CorrelationId,
                havingCorrelationTag: request.CorrelationTag,
                jobIds: request.JobIds
            );

            return new JobQueryResponse {
                Items = logQuery.Select(x => new JobQueryResponse.JobQueryItem {
                    JobId = x.LogJobEntryId,
                    JobType = x.JobType,
                    Status = x.Status,
                    JobName = x.JobName,
                    StartTimestamp = x.StartTimestamp
                    // JobInput = request.WantInput.GetValueOrDefault() ? x.JobInput : null,
                    // JobResult = request.WantResult.GetValueOrDefault() ? x.JobResult : null
                }).ToArray(),
            };
        }
    }
}