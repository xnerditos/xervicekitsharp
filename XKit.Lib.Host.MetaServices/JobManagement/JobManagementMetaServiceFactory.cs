using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.OperationManagement {

    public interface IJobManagementMetaServiceFactory {
        IJobManagementMetaService Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        );
    }

    public class JobManagementMetaServiceFactory : IJobManagementMetaServiceFactory {

        private static IJobManagementMetaServiceFactory factory = new JobManagementMetaServiceFactory(); 

        public static IJobManagementMetaServiceFactory Factory => factory; 

        // =====================================================================
        // IOperationControlMetaServiceFactory
        // =====================================================================
        IJobManagementMetaService IJobManagementMetaServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) { 
            return new JobManagementMetaService(localEnvironment, dependencyConnector);
        }

        // =====================================================================
        // Static 
        // ===================================================================== 

        public static IJobManagementMetaService Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        )
            => JobManagementMetaServiceFactory.Factory.Create(localEnvironment, dependencyConnector); 

        public static void InjectCustomFactory(IJobManagementMetaServiceFactory factory)
            => JobManagementMetaServiceFactory.factory = factory;            
    }
}