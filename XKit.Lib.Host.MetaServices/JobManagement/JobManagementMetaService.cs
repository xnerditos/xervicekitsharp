using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Fabric;

namespace XKit.Lib.Host.MetaServices.OperationManagement {

    public interface IJobManagementMetaService 
        : IMetaService, IServiceBase {}

    internal class JobManagementMetaService 
        : MetaService<JobManagementOperation>, IJobManagementMetaService {

        public JobManagementMetaService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) : base(
                StandardCapabilityNames.LocalOperationsManagement,
                localEnvironment,
                dependencyConnector
            ) { }

        // =====================================================================
        // base class overrides
        // =====================================================================

        protected override IReadOnlyDescriptor Descriptor => MetaServiceConstants.Services.JobManagement.Descriptor;
    }
}