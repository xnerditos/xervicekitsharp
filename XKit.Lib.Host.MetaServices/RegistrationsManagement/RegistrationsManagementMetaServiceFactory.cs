using XKit.Lib.Common.Host;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.RegistrationsManagement {

    public interface IRegistrationsManagementMetaServiceFactory {
        IRegistrationsManagementMetaService Create(
            IHostManager hostManager,
            IFabricConnector fabricConnector
        );
    }

    public class RegistrationsManagementMetaServiceFactory : IRegistrationsManagementMetaServiceFactory {

        private static IRegistrationsManagementMetaServiceFactory factory = new RegistrationsManagementMetaServiceFactory();

        public static IRegistrationsManagementMetaServiceFactory Factory => factory;

        // =====================================================================
        // IRegistrationManagementMetaServiceFactory
        // =====================================================================
        IRegistrationsManagementMetaService IRegistrationsManagementMetaServiceFactory.Create(
            IHostManager hostManager, 
            IFabricConnector fabricConnector
        ) {
            return new RegistrationManagementMetaService(hostManager, fabricConnector);
        }

        // =====================================================================
        // Static 
        // ===================================================================== 

        public static IRegistrationsManagementMetaService Create(
            IHostManager hostManager,
            IFabricConnector fabricConnector
        ) => RegistrationsManagementMetaServiceFactory.Factory.Create(hostManager, fabricConnector);

        public static void InjectCustomFactory(IRegistrationsManagementMetaServiceFactory factory)
            => RegistrationsManagementMetaServiceFactory.factory = factory;
    }
}