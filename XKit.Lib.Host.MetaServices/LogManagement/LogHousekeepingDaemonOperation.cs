using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Host.DefaultBaseClasses;

namespace XKit.Lib.Host.MetaServices.LogManagement {

    public partial class LogHousekeepingDaemonOperation
        : ServiceDaemonOperation<object, ILogManagementMetaService> {

        private const int DefaultArchiveJobsOlderThanXMinutes = 60 * 24 * 7;    // one week

        new private IGenericTimerDaemon Daemon => (IGenericTimerDaemon)base.Daemon;

        public LogHousekeepingDaemonOperation(
            ServiceDaemonOperationContext context
        ) : base(context) { }

        protected override JobSystemEffect OperationEffect => JobSystemEffect.Transient;

        protected async override Task DoOperationLogic(object message) {
            const int defaultLimitCount = 100;

            var config = Service.GetConfig();
            Daemon.SetTimerDelay((config?.HousekeepingDaemon?.CheckEveryXSeconds ?? LogManagementMetaService.DefaultTimerIntervalSeconds) * 1000);
            Daemon.SetNextEventReadyDelay(config.HousekeepingDaemon?.WaitBetweenBatchesMilliseconds);

            if ((config.HousekeepingDaemon.EnableArchiving).GetValueOrDefault()) {
    
                var archiveOlderThanMinutes = (config?.HousekeepingDaemon?.ArchiveJobsOlderThanXMinutes).GetValueOrDefault(DefaultArchiveJobsOlderThanXMinutes);
                var archiveCuttoff = DateTime.UtcNow.AddMinutes(archiveOlderThanMinutes * -1);
                IReadOnlyList<Guid> jobIdsArchived = null;
    
                try {
                    jobIdsArchived = await LogSession.ArchiveJobs(
                        startOnOrBeforeTimestamp: archiveCuttoff,
                        limitCount: config.HousekeepingDaemon?.BatchLimitSize ?? defaultLimitCount,
                        isComplete: true,
                        deleteArchivedJobsFromSource: true,
                        archiveDbFile: config?.HousekeepingDaemon?.ArchiveDbPath,
                        waitBetweenStepsMilliseconds: (config.HousekeepingDaemon?.WaitBetweenBatchStepsMilliseconds).GetValueOrDefault(0)
                    ); 
                    if (jobIdsArchived.Count == defaultLimitCount) {
                        Daemon.ManuallyPostNewTimerEvent();
                    } else {
                        // Only run the archive on  incomplete jobs once we finish with any backlog of
                        // complete jobs.  Otherwise, we'll do this each time we iterate through.
                        await LogSession.ArchiveJobs(
                            startOnOrBeforeTimestamp: archiveCuttoff,
                            isComplete: false,
                            deleteArchivedJobsFromSource: true,
                            archiveDbFile: config?.HousekeepingDaemon?.ArchiveDbPath
                        ); 
                    }
                } catch (Exception ex) {
                    Error(
                        "Error doing log housekeeping: " + ex.Message,
                        new {
                            ex.StackTrace,
                            ex.TargetSite,
                            ex.Source
                        });
                }
            }
        }
    }
}