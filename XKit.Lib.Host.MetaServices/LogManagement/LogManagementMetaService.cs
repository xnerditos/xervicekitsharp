using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Utility;
using XKit.Lib.Common.Config;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Host.Services;

namespace XKit.Lib.Host.MetaServices.LogManagement {

    public interface ILogManagementMetaService 
        : IMetaService, IServiceBase { 

        LogManagementConfig GetConfig(LogManagementConfig defaultValue = null);
    }

    internal class LogManagementMetaService 
        : MetaService<LogManagementOperation>, ILogManagementMetaService {

        public const int DefaultTimerIntervalSeconds = 3600;  // 1 hour

		private SetOnceOrThrow<IConfigReader<LogManagementConfig>> configReader = new SetOnceOrThrow<IConfigReader<LogManagementConfig>>();

		private IConfigReader<LogManagementConfig> ConfigReader {
            get => configReader.Value;
            set => configReader.Value = value;
        }

        public LogManagementMetaService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector,
            bool enableHousekeepingDaemon
        ) : base(
            StandardCapabilityNames.LocalLogManagement,
            localEnvironment,
            dependencyConnector
        ) { 
            ConfigReader = AddFeatureConfigurable<LogManagementConfig>();
            var initialConfig = ConfigReader.GetConfig();

            if (enableHousekeepingDaemon) {
                AddDaemon(
                    new GenericTimerDaemon<LogHousekeepingDaemonOperation>(
                        timerDelayMilliseconds: (initialConfig?.HousekeepingDaemon?.CheckEveryXSeconds ?? DefaultTimerIntervalSeconds) * 1000,
                        name: "LogHousekeepingDaemon",
                        onEnvironmentChangeHandler: (monitor, daemon) => {
                            var cfg = ConfigReader.GetConfig();
                            daemon.SetTimerDelay(cfg?.HousekeepingDaemon?.CheckEveryXSeconds);
                            //daemon.SetTimerEnabled(cfg?.HousekeepingDaemon.EnableArchiving);
                        }
                    )
                );
            }
        }

        // =====================================================================
        // base class overrides
        // =====================================================================

        protected override IReadOnlyDescriptor Descriptor => MetaServiceConstants.Services.LogManagement.Descriptor;

        // =====================================================================
        // ILogManagementMetaService
        // =====================================================================

        LogManagementConfig ILogManagementMetaService.GetConfig(LogManagementConfig defaultValue) {
            return ConfigReader.GetConfig(defaultValue);
        }
    }
}