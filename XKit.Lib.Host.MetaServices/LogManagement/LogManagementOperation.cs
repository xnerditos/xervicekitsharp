using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Host.DefaultBaseClasses;

namespace XKit.Lib.Host.MetaServices.LogManagement {

    internal class LogManagementOperation : ServiceOperation<ILogManagementMetaService>, ILogManagementApi {

        public LogManagementOperation(
            ServiceOperationContext context
        ) : base(context) { }

        // =====================================================================
        // ILogOperation
        // =====================================================================

        async Task<ServiceCallResult<LogQueryResponse>> ILogManagementApi.Query(LogQueryRequest request) {
            return await RunServiceCall(
                request,
                operationAction: async(r) => {
                    int pageSize = r.PageSize.GetValueOrDefault() <= 0 ? 100 : r.PageSize.Value;
                    int pageIndex = r.PageIndex.GetValueOrDefault() < 0 ? 0 : r.PageIndex.Value;

                    List<IReadOnlyLogJobEntry> jobs = null;
                    List<IReadOnlyLogEventEntry> events = null;
                    List<IReadOnlyLogEntryData> dataEntries = null;

                    List<Guid> filterJobIds = request.JobIds;
                    List<Guid> filterEventIds = request.EventIds;

                    if (request.WantJobs.GetValueOrDefault(true)) {
                        jobs = (await LogSession.GetLogReader().QueryJobs(
                            jobIds: filterJobIds,
                            startOnOrBeforeTimestamp: r.JobStartOnOrBefore,
                            startOnOrAfterTimestamp: r.JobStartOnOrAfter,
                            completeOnOrBeforeTimestamp: r.JobCompleteOnOrBefore,
                            completeOnOrAfterTimestamp: r.JobCompletetOnOrAfter,
                            pageSize: pageSize,
                            pageIndex: pageIndex
                        )).ToList();

                        filterJobIds = jobs
                            .Select(e => e.LogJobEntryId)
                            .ToList();
                        pageSize = int.MaxValue;
                        pageIndex = 0;
                    }

                    if (request.WantEvents.GetValueOrDefault(true)) {
                        events = (await LogSession.GetLogReader().QueryEvents(
                            jobIds: filterJobIds,
                            eventIds: request.EventIds,
                            onOrBeforeTimestamp: r.EventOnOrBefore,
                            onOrAfterTimestamp: r.EventOnOrAfter,
                            elseMatchingEventTypeName: r.EventType,
                            havingTag: r.HasTag,
                            pageSize: pageSize,
                            pageIndex: pageIndex
                        )).ToList();

                        filterEventIds = events
                            .Select(e => e.LogEventEntryId)
                            .ToList();

                        pageSize = int.MaxValue;
                        pageIndex = 0;
                    }

                    if (request.WantData.GetValueOrDefault(false)) {
                        dataEntries = (await LogSession.GetLogReader().QueryData(
                            jobIds: filterEventIds != null ? null : filterJobIds,
                            eventIds : filterEventIds,
                            pageSize : pageSize,
                            pageIndex : pageIndex
                        )).ToList();
                    }

                    var response = new LogQueryResponse {
                        Events = events,
                        Jobs = jobs,
                        EventData = dataEntries,
                        EventCount = events == null ? 0 : events.Count,
                        JobCount = jobs == null ? 0 : jobs.Count,
                        DataCount = dataEntries == null ? 0 : dataEntries.Count
                    };

                    return ResultSuccess(
                        resultData: response,
                        logMessage: "Individual log items excluded",
                        logData : new LogQueryResponse {
                            EventCount = events == null ? 0 : events.Count,
                                JobCount = jobs == null ? 0 : jobs.Count,
                                DataCount = dataEntries == null ? 0 : dataEntries.Count
                        }
                    );
                }
            );
        }
    }
}