using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;

namespace XKit.Lib.Host.MetaServices.LogManagement
{

    public interface ILogManagementMetaServiceFactory {
        ILogManagementMetaService Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector,
            bool enableHousekeepingDaemon = true
        );
    }

    public class LogManagementMetaServiceFactory : ILogManagementMetaServiceFactory {

        private static ILogManagementMetaServiceFactory factory = new LogManagementMetaServiceFactory(); 

        public static ILogManagementMetaServiceFactory Factory => factory; 

        // =====================================================================
        // ILogMetaServiceFactory
        // =====================================================================
        ILogManagementMetaService ILogManagementMetaServiceFactory.Create(
            ILocalHostEnvironment localeEnvironment,
            IDependencyConnector dependencyConnector,
            bool enableHousekeepingDaemon
        ) { 
            return new LogManagementMetaService(localeEnvironment, dependencyConnector, enableHousekeepingDaemon);
        }

        // =====================================================================
        // Static 
        // ===================================================================== 

        public static ILogManagementMetaService Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector,
            bool enableHousekeepingDaemon = true
        )
            => LogManagementMetaServiceFactory.Factory.Create(
                localEnvironment, 
                dependencyConnector,
                enableHousekeepingDaemon
            ); 

        public static void InjectCustomFactory(ILogManagementMetaServiceFactory factory)
            => LogManagementMetaServiceFactory.factory = factory;            
    }
}