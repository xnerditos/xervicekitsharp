using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.ConfigManagement
{

    public interface IConfigManagementMetaServiceFactory {
        IConfigManagementMetaService Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        );
    }

    public class ConfigManagementMetaServiceFactory : IConfigManagementMetaServiceFactory {

        private static IConfigManagementMetaServiceFactory factory = new ConfigManagementMetaServiceFactory(); 

        public static IConfigManagementMetaServiceFactory Factory => factory; 

        // =====================================================================
        // IHostManagementMetaServiceFactory
        // =====================================================================
        IConfigManagementMetaService IConfigManagementMetaServiceFactory.Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) => new ConfigManagementMetaService(hostManager, dependencyConnector);

        // =====================================================================
        // Static 
        // ===================================================================== 

        public static IConfigManagementMetaService Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) => Factory.Create(hostManager, dependencyConnector);

        public static void InjectCustomFactory(IConfigManagementMetaServiceFactory factory)
            => ConfigManagementMetaServiceFactory.factory = factory;            
    }
}