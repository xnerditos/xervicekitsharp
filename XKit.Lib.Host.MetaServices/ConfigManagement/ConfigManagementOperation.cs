using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.MetaServices.Entities;
using System.Linq;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.ConfigManagement {

    internal class ConfigManagementOperation : ServiceOperation<IConfigManagementMetaService>, IConfigManagementApi {

        private readonly IHostManager HostManager;

        public ConfigManagementOperation(
            ServiceOperationContext context,
            IHostManager hostManager
        ) : base(context) {
            this.HostManager = hostManager;
        }

        // =====================================================================
        // IConfigOperation
        // =====================================================================

        async Task<ServiceCallResult<object>> IConfigManagementApi.Retrieve(ConfigMetaServiceRetrieveRequest requestBody)
            => await RunServiceCall(
                requestBody: requestBody,
                operationAction: async (r) => {
                    var localConfigSession = LocalHostEnvironment.LocalConfigSessionFactory.Create(r.DocumentIdentifier);
                    return await localConfigSession.GetConfig();
                }
            );

        async Task<ServiceCallResult> IConfigManagementApi.Update(ConfigMetaServiceUpdateRequest request)
            => await RunServiceCall(
                requestBody: request,
                operationAction: async (r) => {

                    if (HostManager.ConfigurationDocumentIdentifier == r.DocumentIdentifier) {
                        await updateConfig();
                        HostManager.SignalLocalEnvironmentChange(Monitor);
                    } else { 

                        var service = HostManager.GetManagedServices()
                            .Where(svc => svc.ConfigurationDocumentIdentifier == r.DocumentIdentifier)
                            .FirstOrDefault();

                        if (service == null) {
                            Error(
                                message: "Service appears not to be registered with this host",
                                new { configDocumentIdentifier = r.DocumentIdentifier }
                            );
                            return;
                        }

                        await updateConfig();

                        service.SignalEnvironmentChange(Monitor);
                    }

                    // -----------------------------------------------------------------
                    async Task updateConfig() {
                        var localConfigSession = LocalHostEnvironment.LocalConfigSessionFactory.Create(r.DocumentIdentifier); 
                        await localConfigSession.UpdateConfig(r.ConfigInfo);
                    }
                }
            );
    }
}