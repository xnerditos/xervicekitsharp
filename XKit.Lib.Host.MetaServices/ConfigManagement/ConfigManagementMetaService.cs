using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Fabric;

namespace XKit.Lib.Host.MetaServices.ConfigManagement {

    public interface IConfigManagementMetaService 
        : IMetaService, IServiceBase { }

    internal class ConfigManagementMetaService 
        : MetaService<ConfigManagementOperation>, IConfigManagementMetaService {

        private readonly IHostManager HostManager; 

        public ConfigManagementMetaService(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) :base(
            StandardCapabilityNames.LocalHostManagement,
            hostManager,
            dependencyConnector
        ) {
            this.HostManager = hostManager;
        }

        // =====================================================================
        // base class overrides
        // =====================================================================

        protected override IReadOnlyDescriptor Descriptor => MetaServiceConstants.Services.ConfigManagement.Descriptor;
    }
}