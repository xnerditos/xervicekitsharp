// using XKit.Lib.Host.Management;
// using Microsoft.AspNetCore.Mvc;

// namespace XKit.Lib.Host.MetaServices.Services {

//     public interface IServicesMetaServiceFactory {
//         IServicesMetaService Create(IHostManager hostManager);
//         IServicesMetaService Create();
//     }

//     public class ServicesMetaServiceFactory : IServicesMetaServiceFactory {

//         private static IServicesMetaServiceFactory factory = new ServicesMetaServiceFactory(); 

//         public static IServicesMetaServiceFactory Factory => factory; 

//         // =====================================================================
//         // IServiceContextFactory
//         // =====================================================================
//         IServicesMetaService IServicesMetaServiceFactory.Create(
//             IHostManager hostManager
            
//         ) { 
//             return new ServicesMetaService(hostManager);
//         }

//         IServicesMetaService IServicesMetaServiceFactory.Create() { 
//             return Create(HostManagerFactory.CreateSingleton());
//         }

//         // =====================================================================
//         // Static 
//         // ===================================================================== 

//         public static IServicesMetaService Create(
//             IHostManager hostManager
//         ) => ServicesMetaServiceFactory.Factory.Create(hostManager); 

//         public static IServicesMetaService Create()
//             => ServicesMetaServiceFactory.Factory.Create(); 

//         public static void InjectCustomFactory(IServicesMetaServiceFactory factory)
//             => ServicesMetaServiceFactory.factory = factory;            
//     }
// }