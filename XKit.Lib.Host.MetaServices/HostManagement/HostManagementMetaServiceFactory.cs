using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.HostManagement
{

    public interface IHostManagementMetaServiceFactory {
        IHostManagementMetaService Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        );
    }

    public class HostManagementMetaServiceFactory : IHostManagementMetaServiceFactory {

        private static IHostManagementMetaServiceFactory factory = new HostManagementMetaServiceFactory(); 

        public static IHostManagementMetaServiceFactory Factory => factory; 

        // =====================================================================
        // IHostManagementMetaServiceFactory
        // =====================================================================
        IHostManagementMetaService IHostManagementMetaServiceFactory.Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) => new HostManagementMetaService(hostManager, dependencyConnector);

        // =====================================================================
        // Static 
        // ===================================================================== 

        public static IHostManagementMetaService Create(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) => HostManagementMetaServiceFactory.Factory.Create(hostManager, dependencyConnector);

        public static void InjectCustomFactory(IHostManagementMetaServiceFactory factory)
            => HostManagementMetaServiceFactory.factory = factory;            
    }
}