using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Host.DefaultBaseClasses;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.MetaServices;

namespace XKit.Lib.Host.MetaServices.HostManagement {

    public interface IHostManagementMetaService 
        : IMetaService, IServiceBase { }

    internal class HostManagementOperation : ServiceOperation<IHostManagementMetaService>, IHostManagementApi {

        private readonly IHostManager HostManager;

        public HostManagementOperation(
            ServiceOperationContext context,
            IHostManager hostManager
        ) :base(context) { 
            this.HostManager = hostManager;
        }

        // =====================================================================
        // IHostManagementOperation
        // =====================================================================

        async Task<ServiceCallResult> IHostManagementApi.Kill(HostMetaServiceControlRequest request) 
            => await RunServiceCall(request, (r) => {
                HostManager.KillHost(Monitor);
                return Task.CompletedTask;
            });

        async Task<ServiceCallResult> IHostManagementApi.Pause(HostMetaServiceControlRequest request)  
            => await RunServiceCall(request, (r) => { 
                HostManager.PauseHost(Monitor); 
                return Task.CompletedTask;
            });

        async Task<ServiceCallResult> IHostManagementApi.Resume(HostMetaServiceControlRequest request) 
            => await RunServiceCall(request, (r) => { 
                HostManager.ResumeHost(Monitor);
                return Task.CompletedTask;
            });

        async Task<ServiceCallResult> IHostManagementApi.Stop(HostMetaServiceControlRequest request)  
            => await RunServiceCall(request, (r) => {
                HostManager.StopHost(Monitor);
                return Task.CompletedTask;
            });
    }
}