using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Host.DefaultBaseClasses;

namespace XKit.Lib.Host.MetaServices.HostManagement {

    internal class HostManagementMetaService 
        : MetaService<HostManagementOperation>, IHostManagementMetaService {

        private readonly IHostManager HostManager;

        public HostManagementMetaService(
            IHostManager hostManager,
            IDependencyConnector dependencyConnector
        ) :base(
            StandardCapabilityNames.LocalHostManagement,
            hostManager,
            dependencyConnector
        ) { 
            this.HostManager = hostManager;
        }

        // =====================================================================
        // base class overrides
        // =====================================================================

        protected override IReadOnlyDescriptor Descriptor => MetaServiceConstants.Services.HostManagement.Descriptor;
    }
}