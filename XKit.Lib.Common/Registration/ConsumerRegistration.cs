using System.Collections.Generic;
using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;
using System.Linq;

namespace XKit.Lib.Common.Registration {

	public partial class ConsumerRegistration : IReadOnlyConsumerRegistration {

		public string FabricId { get; set; }

		public List<Descriptor> Dependencies { get; set; }

        public ConsumerRegistration Clone() 
            => new ConsumerRegistration {
                Dependencies = Dependencies.Select(x => x.Clone()).ToList(),
                FabricId = FabricId
            };

        [JsonIgnore]
		IEnumerable<IReadOnlyDescriptor> IReadOnlyConsumerRegistration.Dependencies => this.Dependencies;
    }

	public partial class ConsumerRegistration {
		public static ConsumerRegistration FromJson(string json) => JsonConvert.DeserializeObject<ConsumerRegistration>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this ConsumerRegistration self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}