
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {
	public interface IReadOnlyFabricStatus {
		string FabricId { get; }
        
        [JsonConverter(typeof(StringEnumConverter))]
		HealthEnum? Health { get; }

        [JsonConverter(typeof(StringEnumConverter))]
		RunStateEnum? RunState { get; }

        FabricStatus Clone();
	}
}