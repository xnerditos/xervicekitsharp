using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {

	public partial class FabricStatus : IReadOnlyFabricStatus {

		public string FabricId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
		public HealthEnum? Health { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
		public RunStateEnum? RunState { get; set; }

        public FabricStatus Clone() 
            => new FabricStatus {
                Health = Health,
                FabricId = FabricId,
                RunState = RunState
            };
            
		public void TryUpdate(HealthEnum? health, RunStateEnum? state = null) {
			if (health.HasValue) {
				this.Health = health.Value;
			}
			if (state.HasValue) {
				this.RunState = state.Value;
			}
		}
	}

	public partial class FabricStatus {
		public static FabricStatus FromJson(string json) => JsonConvert.DeserializeObject<FabricStatus>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this FabricStatus self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}