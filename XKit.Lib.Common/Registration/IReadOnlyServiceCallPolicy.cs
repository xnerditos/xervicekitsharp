using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {

	public interface IReadOnlyServiceCallPolicy  {

		int TimeoutMs { get; }

        [JsonConverter(typeof(StringEnumConverter))]
		ServiceCallPatternEnum? CallPattern { get; }

        ServiceCallPolicy Clone();
	}
}