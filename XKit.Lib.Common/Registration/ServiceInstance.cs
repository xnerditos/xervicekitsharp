using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {

	public partial class ServiceInstance : IReadOnlyServiceInstance {

        public Descriptor Descriptor { get; set; }

		public string InstanceId { get; set; }

		public string HostFabricId { get; set; }

        public string HostAddress { get; set; }

		public string RegistrationKey { get; set; }

		public ServiceInstanceStatus Status { get; set; }

        public ServiceInstance Clone() 
            => new ServiceInstance {
                HostFabricId = HostFabricId,
                InstanceId = InstanceId,
                RegistrationKey = RegistrationKey,
                HostAddress = HostAddress,
                Status = Status?.Clone(),
                Descriptor = Descriptor?.Clone()
            };

        [JsonIgnore]
		IReadOnlyServiceInstanceStatus IReadOnlyServiceInstance.Status => this.Status;
        [JsonIgnore]
        IReadOnlyDescriptor IReadOnlyServiceInstance.Descriptor => this.Descriptor;
	}

	public partial class ServiceInstance {
		public static ServiceInstance FromJson(string json) => JsonConvert.DeserializeObject<ServiceInstance>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this ServiceInstance self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}