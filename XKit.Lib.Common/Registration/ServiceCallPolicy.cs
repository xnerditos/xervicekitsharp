using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {

	public partial class ServiceCallPolicy : IReadOnlyServiceCallPolicy {
		
		public int TimeoutMs { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
		public ServiceCallPatternEnum? CallPattern { get; set; }

        public ServiceCallPolicy Clone() 
            => new ServiceCallPolicy {
                TimeoutMs = TimeoutMs,
                CallPattern = CallPattern
            };
	}
}