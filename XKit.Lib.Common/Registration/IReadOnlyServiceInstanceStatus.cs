using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {
	public interface IReadOnlyServiceInstanceStatus {
		string InstanceId { get; }

        [JsonConverter(typeof(StringEnumConverter))]
		AvailabilityEnum Availability { get; }

        [JsonConverter(typeof(StringEnumConverter))]
		HealthEnum Health { get; }

        [JsonConverter(typeof(StringEnumConverter))]
		RunStateEnum RunState { get; }

        ServiceInstanceStatus Clone();
	}
}