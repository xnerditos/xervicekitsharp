using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;

namespace XKit.Lib.Common.Registration {

	public partial class ServiceTopologyMap : IReadOnlyServiceTopologyMap {

		public List<ServiceRegistration> Services { get; set; }

		public DateTime? CacheExpiration { get; set; }
        
        public ServiceTopologyMap Clone() 
            => new ServiceTopologyMap {
                CacheExpiration = CacheExpiration,
                Services = Services?.Select(x => x.Clone()).ToList()
            };

        [JsonIgnore]
		IEnumerable<IReadOnlyServiceRegistration> IReadOnlyServiceTopologyMap.Services => this.Services;
	}

	public partial class ServiceTopologyMap {
		public static ServiceTopologyMap FromJson(string json) => JsonConvert.DeserializeObject<ServiceTopologyMap>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this ServiceTopologyMap self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}