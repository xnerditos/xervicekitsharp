using System.Collections.Generic;
using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;
using System.Linq;

namespace XKit.Lib.Common.Registration {

	public partial class FabricRegistration : IReadOnlyFabricRegistration {

		public string FabricId { get; set; }

        public FabricStatus Status { get; set; }

		public List<ServiceRegistration> HostedServices { get; set; }

		public List<Descriptor> Dependencies { get; set; }

		public string Address { get; set; }

		public List<string> Capabilities { get; set; }

        public FabricRegistration Clone() 
            => new FabricRegistration {
                Dependencies = Dependencies?.Select(x => x.Clone()).ToList(),
                Capabilities = new List<string>(Capabilities),
                Address = Address,
                FabricId = FabricId,
                Status = Status?.Clone(),
                HostedServices = HostedServices?.Select(x => x.Clone()).ToList()
            };

        [JsonIgnore]
		IEnumerable<IReadOnlyDescriptor> IReadOnlyFabricRegistration.Dependencies => this.Dependencies;

        [JsonIgnore]
        IEnumerable<IReadOnlyServiceRegistration> IReadOnlyFabricRegistration.HostedServices => this.HostedServices;

        [JsonIgnore]
        IReadOnlyFabricStatus IReadOnlyFabricRegistration.Status => Status;

        [JsonIgnore]
        IEnumerable<string> IReadOnlyFabricRegistration.Capabilities => Capabilities;
    }

	public partial class FabricRegistration {
		public static FabricRegistration FromJson(string json) => JsonConvert.DeserializeObject<FabricRegistration>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this FabricRegistration self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}