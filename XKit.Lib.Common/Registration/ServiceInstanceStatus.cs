using XKit.Lib.Common.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XKit.Lib.Common.Registration {

	public partial class ServiceInstanceStatus : IReadOnlyServiceInstanceStatus {

		public string InstanceId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
		public AvailabilityEnum Availability { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
		public HealthEnum Health { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
		public RunStateEnum RunState { get; set; }

        public ServiceInstanceStatus Clone() 
            => new ServiceInstanceStatus {
                Availability = Availability,
                Health = Health,
                InstanceId = InstanceId,
                RunState = RunState
            };

		public void TryUpdate(IReadOnlyServiceInstanceStatus update) 
			=> TryUpdate(
				update.Availability,
				update.Health,
				update.RunState
			);

		public void TryUpdate(AvailabilityEnum? availability, HealthEnum? health, RunStateEnum? state = null) {
			if (availability.HasValue) {
				this.Availability = availability.Value;
			}
			if (health.HasValue) {
				this.Health = health.Value;
			}
			if (state.HasValue) {
				this.RunState = state.Value;
			}
		}
	}

	public partial class ServiceInstanceStatus {
		public static ServiceInstanceStatus FromJson(string json) => JsonConvert.DeserializeObject<ServiceInstanceStatus>(json, Converter.Settings);
	}

	public static partial class Serialize {
		public static string ToJson(this ServiceInstanceStatus self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}
}