using XKit.Lib.Common.Registration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using XKit.Lib.Common.Host;
using System.Linq.Expressions;
using System;
using System.Threading.Tasks;

namespace XKit.Lib.Common.Fabric {
    
    public class ServiceCallTypeParameters {

        [JsonConverter(typeof(StringEnumConverter))]
        public ServiceCallTypeEnum CallType { get; set; } = ServiceCallTypeEnum.SyncResult;
        public Descriptor CallbackService { get; set; }
        public string CallbackOperationName { get; set; }
        public string CallbackHostId { get; set; }
        public static ServiceCallTypeParameters SyncResult() 
            => new ServiceCallTypeParameters { CallType = ServiceCallTypeEnum.SyncResult };
        public static ServiceCallTypeParameters CallbackReturn() 
            => new ServiceCallTypeParameters { CallType = ServiceCallTypeEnum.CallbackReturn };
        public static ServiceCallTypeParameters FireAndForget() 
            => new ServiceCallTypeParameters { CallType = ServiceCallTypeEnum.FireAndForget };
        public static ServiceCallTypeParameters Callback(
            IReadOnlyDescriptor callbackService, 
            string callbackInterface,
            string callbackMethod, 
            string callbackHostId = null
        ) => new ServiceCallTypeParameters { 
                CallType = ServiceCallTypeEnum.CallbackOperationWithResult, 
                CallbackService = callbackService.Clone(), 
                CallbackOperationName = callbackInterface == null ? callbackMethod : $"{callbackInterface}.{callbackMethod}",
                CallbackHostId = callbackHostId
            };
        public static ServiceCallTypeParameters Callback<TCallInterface>(
            IReadOnlyDescriptor callbackService, 
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> method,
            string callbackHostId = null
        ) where TCallInterface : IServiceCallable => Callback(
            callbackService, 
            $"{typeof(TCallInterface).Name}.{((MethodCallExpression)method.Body).Method.Name}",
            callbackHostId
        );
        
        public static ServiceCallTypeParameters CallbackSlimResult(
            IReadOnlyDescriptor callbackService, 
            string callbackInterface,
            string callbackMethod, 
            string callbackHostId = null
        ) => new ServiceCallTypeParameters { 
                CallType = ServiceCallTypeEnum.CallbackOperationWithSlimResult, 
                CallbackService = callbackService.Clone(), 
                CallbackOperationName = callbackInterface == null ? callbackMethod : $"{callbackInterface}.{callbackMethod}",
                CallbackHostId = callbackHostId
            };

        public static ServiceCallTypeParameters CallbackSlimResult<TCallInterface>(
            IReadOnlyDescriptor callbackService, 
             Expression<Func<TCallInterface, Task<ServiceCallResult>>> method,
           string callbackHostId = null
        ) where TCallInterface : IServiceCallable => CallbackSlimResult(
            callbackService, 
            $"{typeof(TCallInterface).Name}.{((MethodCallExpression)method.Body).Method.Name}",
            callbackHostId
        );
    }
}