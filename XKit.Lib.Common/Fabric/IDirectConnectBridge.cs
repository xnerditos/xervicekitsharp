using System.Threading.Tasks;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Common.Fabric {

    /// <summary>
    /// Used to make service calls directly to local service instances
    /// </summary>
    public interface IDirectConnectBridge {
        IReadOnlyDescriptor Descriptor { get; }
        Task<ServiceCallResult<TResponseBody>> ExecuteCall<TRequestBody, TResponseBody>(
            ServiceCallRequest<TRequestBody> request) 
            where TResponseBody : class 
            where TRequestBody : class;
    }
}