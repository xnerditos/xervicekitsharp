using System.Collections.Generic;
using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Common.Fabric {

    /// <summary>
    /// Provides the ability to connect with the service fabric.
    /// </summary>
    public interface IFabricConnector : IDependencyConnector {

        /// <summary>
        /// Called to initialize the client 
        /// </summary>
        /// <returns>The fabric id assigned</returns>
        string Initialize();

        /// <summary>
        /// Registers a host with the registry.   
        /// </summary>
        /// <param name="registration">entity describing the host</param>
        /// <param name="initialRegistryHostAddresses">Hosts that have the Registry service</param>
        /// <param name="localEnvironment">The host environment object that gives access to the current host</param>
        /// <param name="monitor">runtime monitor for the call</param>
        /// <returns>true if success</returns>
        Task<bool> RegisterAsHost(
            IEnumerable<string> initialRegistryHostAddresses,
            ILocalHostEnvironment localEnvironment,
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister = false
        );

        /// <summary>
        /// Registers a consumer with the registry.   
        /// </summary>
        /// <param name="registration">entity describing the host</param>
        /// <param name="initialRegistryHostAddresses">Hosts that have the Registry service</param>
        /// <param name="localEnvironment">The host environment object that gives access to the current host</param>
        /// <param name="monitor">runtime monitor for the call</param>
        /// <returns>true if success</returns>
        Task<bool> RegisterAsConsumer(
            IEnumerable<string> initialRegistryHostAddresses,
            ILocalFabricEnvironment localEnvironment,
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister = false
        );

        /// <summary>
        /// Updates the connector with the fabric.  If this is a host, then 
        /// this method updates the status of the host as well as getting
        /// refreshed dependency information.  If a host is not registered, then
        /// only dependency information is refreshed.
        /// </summary>
        /// <returns>true if succeeded</returns>
        Task<bool> Refresh(
            IRuntimeMonitor monitor = null,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null
        );

        /// <summary>
        /// Allows the consumer to forcibly set the dependency info 
        /// </summary>
        Task ForceResetTopologyMap(
            IReadOnlyServiceTopologyMap map
        );

        Task<bool> Unregister(
            IRuntimeMonitor monitor
        );

        string FabricId { get; }

        bool IsHost { get; }
    }
}