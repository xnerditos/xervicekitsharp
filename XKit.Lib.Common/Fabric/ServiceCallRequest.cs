using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Common.Fabric {

    public class ServiceCallRequest {

        public ServiceCallTypeParameters CallTypeParameters { get; set; }
        public string OperationName { get; set; }
        public string RequestorInstanceId { get; set; }
        public string RequestorFabricId { get; set; }
        public string CorrelationId { get; set; }
        public string[] CorrelationTags { get; set; }
        public string Payload { get; set; }

        public T GetBody<T>() where T : class {
            return Json.From<T>(this.Payload);
        }

        public object GetBody(System.Type type) {
            return Json.From(this.Payload, type);
        }

        public object GetBody() {
            return Json.FromDynamic(this.Payload);
        }

        public void SetBody(object body) {
            this.Payload = Json.ToDynamic(body);
        }

        public void SetBody<T>(T body) {
            this.Payload = Json.To<T>(body);
        }

        public ServiceCallRequest() { }

        protected ServiceCallRequest(
            ServiceClientParameters clientParameters, 
            ServiceCallTypeParameters callTypeParameters,
            string payload,
            string correlationId,
            IEnumerable<string> correlationTags,
            string operationName,
            string requestorFabricId,
            string requestorInstanceId
        ) {
            this.OperationName = operationName;
            this.CallTypeParameters = callTypeParameters ?? ServiceCallTypeParameters.SyncResult();;
            this.Payload = payload;
            this.RequestorFabricId = requestorFabricId ?? clientParameters?.RequestorFabricId ?? throw new ArgumentNullException(nameof(requestorFabricId));
            this.RequestorInstanceId = requestorInstanceId ?? clientParameters?.RequestorInstanceId;
            this.CorrelationId = correlationId ?? clientParameters?.DefaultCorrelationId ?? Utility.Identifiers.GenerateIdentifier();
            var combinedCorrelationTags = new List<string>();
            combinedCorrelationTags.AddRange(clientParameters?.CommonCorrelationTags ?? new string[0]);
            combinedCorrelationTags.AddRange(correlationTags ?? new string[0]);
            this.CorrelationTags = combinedCorrelationTags.Distinct().ToArray();
        }

        public ServiceCallRequest<T> ConvertTo<T>() where T : class
            => new ServiceCallRequest<T> {
                CallTypeParameters = this.CallTypeParameters,
                CorrelationId = this.CorrelationId,
                CorrelationTags = this.CorrelationTags,
                OperationName = this.OperationName,
                Payload = this.Payload,
                RequestorFabricId = this.RequestorFabricId,
                RequestorInstanceId = this.RequestorInstanceId
            };
        
        public ServiceCallRequest Clone()             
            => new ServiceCallRequest {
                CallTypeParameters = this.CallTypeParameters,
                CorrelationId = this.CorrelationId,
                CorrelationTags = this.CorrelationTags,
                OperationName = this.OperationName,
                Payload = this.Payload,
                RequestorFabricId = this.RequestorFabricId,
                RequestorInstanceId = this.RequestorInstanceId
            };

        public static ServiceCallRequest Create(
            ServiceClientParameters clientParameters, 
            string operationName,
            string payload,
            string requestorFabricId = null,
            string correlationId = null,
            IEnumerable<string> correlationTags = null,
            string requestorInstanceId = null,
            ServiceCallTypeParameters callTypeParameters = null
        ) => new ServiceCallRequest(
            clientParameters ?? throw new ArgumentNullException(nameof(clientParameters)),
            callTypeParameters,
            payload,
            correlationId,
            correlationTags,
            operationName,
            requestorFabricId,
            requestorInstanceId
        );

        public static ServiceCallRequest Create(
            string operationName,
            string payload,
            string requestorFabricId,
            string correlationId = null,
            IEnumerable<string> correlationTags = null,
            string requestorInstanceId = null,
            ServiceCallTypeParameters callTypeParameters = null
        ) => new ServiceCallRequest(
            null,
            callTypeParameters,
            payload,
            correlationId,
            correlationTags,
            operationName,
            requestorFabricId,
            requestorInstanceId
        );
    }

    public class ServiceCallRequest<T> : ServiceCallRequest where T : class {

        private T requestBody = null; 

        public T RequestBody { 
            get {
                if (requestBody != null) { return requestBody; }
                if (this.Payload == null || this.Payload.Length == 0) { return null; }
                requestBody = GetBody<T>();
                return requestBody;
            } 
            set {
                requestBody = value;
                SetBody(value);
            }
        } 

        new public ServiceCallRequest<T> Clone()             
            => new ServiceCallRequest<T> {
                CallTypeParameters = this.CallTypeParameters,
                CorrelationId = this.CorrelationId,
                CorrelationTags = this.CorrelationTags,
                OperationName = this.OperationName,
                Payload = this.Payload,
                RequestorFabricId = this.RequestorFabricId,
                RequestorInstanceId = this.RequestorInstanceId
            };

        public ServiceCallRequest() {}

        protected ServiceCallRequest(
            ServiceClientParameters clientParameters, 
            string correlationId,
            IEnumerable<string> correlationTags,
            string operationName,
            ServiceCallTypeParameters callTypeParameters,
            string requestorFabricId,
            string requestorInstanceId,
            T requestBody,
            string payload
        ) : base(
            clientParameters, 
            callTypeParameters,
            null,
            correlationId,
            correlationTags, 
            operationName, 
            requestorFabricId, 
            requestorInstanceId
        ) { 
            if (payload != null) {
                this.Payload = payload;
            } else {
                this.RequestBody = requestBody;
            }
        }

        public static ServiceCallRequest<T> Create(
            string operationName,
            T requestBody,
            string requestorFabricId = null,
            string correlationId = null,
            IEnumerable<string> correlationTags = null,
            string requestorInstanceId = null,
            ServiceCallTypeParameters callTypeParameters = null,
            ServiceClientParameters clientParameters = null,
            string payload = null
        ) => new ServiceCallRequest<T>(
            clientParameters,
            correlationId,
            correlationTags,
            operationName,
            callTypeParameters,
            requestorFabricId,
            requestorInstanceId,
            requestBody,
            payload
        );
    }
}