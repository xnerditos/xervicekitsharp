using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Common.Fabric {

    public class OperationResult {

        [JsonConverter(typeof(StringEnumConverter))]
        public JobResultStatusEnum? OperationStatus { get; set; }
        public string Message { get; set; }
        public string LogMessage { get; set; }
        public object Code { get; set; }

        [JsonIgnore]
        public bool HasError => 
            !(this.OperationStatus == JobResultStatusEnum.Success || 
            this.OperationStatus == JobResultStatusEnum.PartialSuccess ||
            this.OperationStatus == JobResultStatusEnum.Pending);

        [JsonIgnore]
        public bool ImmediateSuccess => 
            this.OperationStatus == JobResultStatusEnum.Success || 
            this.OperationStatus == JobResultStatusEnum.PartialSuccess;

        [JsonIgnore]
        public bool IsPending => 
            this.OperationStatus == JobResultStatusEnum.Pending;
    }

    public class OperationResult<T> : OperationResult where T : class { 

        public T ResultData { get; set; }

        public T LogData { get; set; }
    }
}