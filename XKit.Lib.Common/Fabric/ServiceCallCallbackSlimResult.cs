
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Common.Fabric {

    public class ServiceCallbackSlimResult {
        [JsonConverter(typeof(StringEnumConverter))]
        public JobResultStatusEnum? OperationStatus { get; set; }
        public Guid OperationId { get; set; }
        public bool Success => this.OperationStatus == JobResultStatusEnum.Success || this.OperationStatus == JobResultStatusEnum.PartialSuccess;
    }

    public class ServiceCallbackSlimResult<TResponseBody> : ServiceCallbackSlimResult where TResponseBody : class {
        public TResponseBody ResponseBody { get; set; }
    }
}