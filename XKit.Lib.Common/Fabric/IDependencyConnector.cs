using System.Collections.Generic;
using System.Threading.Tasks;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Common.Fabric {

    /// <summary>
    /// Handles communication with service dependencies
    /// </summary>
    public interface IDependencyConnector {

        /// <summary>
        /// Gets an IServiceCallRouter capable of communicating with the target dependency
        /// </summary>
        /// <param name="requestorInstanceId"></param>
        /// <param name="target"></param>
        /// <param name="fatalIfNotAvailable"></param>
        /// <returns></returns>
        Task<IServiceCallRouter> CreateCallRouter(
            IReadOnlyDescriptor target, 
            IRuntimeMonitor monitor, 
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            bool fatalIfNotAvailable = true,
            bool allowRegistryRefreshIfRequested = true
        );
    }
}
