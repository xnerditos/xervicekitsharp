
using System;

namespace XKit.Lib.Common.Utility.Extensions {

    public static class Json {

        private static readonly Jil.Options JilOptionsUgly = new Jil.Options(
            prettyPrint: false,
            excludeNulls: true,
            jsonp: false,
            Jil.DateTimeFormat.ISO8601,
            includeInherited: true,
            unspecifiedDateTimeKindBehavior: Jil.UnspecifiedDateTimeKindBehavior.IsUTC,
            serializationNameFormat: Jil.SerializationNameFormat.CamelCase
        );
        private static readonly Jil.Options JilOptionsPretty = new Jil.Options(
            prettyPrint: false,
            excludeNulls: true,
            jsonp: false,
            Jil.DateTimeFormat.ISO8601,
            includeInherited: true,
            unspecifiedDateTimeKindBehavior: Jil.UnspecifiedDateTimeKindBehavior.IsUTC,
            serializationNameFormat: Jil.SerializationNameFormat.CamelCase
        );

        public static string To<T>(T obj, bool pretty = false) {
            if (obj == null) { return null; }
            return Jil.JSON.Serialize<T>(obj, pretty ? JilOptionsPretty : JilOptionsUgly);
        }

        public static string To(object obj, bool pretty = false) {
            if (obj == null) { return null; }
            return Jil.JSON.SerializeDynamic(obj, pretty ? JilOptionsPretty : JilOptionsUgly);
        }

        public static string ToDynamic(object obj, bool pretty = false) {
            if (obj == null) { return null; }
            return Jil.JSON.SerializeDynamic(obj, pretty ? JilOptionsPretty : JilOptionsUgly);
        }

        public static T From<T>(string json) {
            if (json == null) { return default(T); }
            return Jil.JSON.Deserialize<T>(json, JilOptionsUgly);
        }

        public static object From(string json, Type type) {
            if (json == null) { return null; }
            return Jil.JSON.Deserialize(json, type, JilOptionsUgly);
        }

        public static object FromDynamic(string json) {
            return Jil.JSON.DeserializeDynamic(json, JilOptionsUgly);
        }
    }
}