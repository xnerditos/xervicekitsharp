namespace XKit.Lib.Common.ObjectInstantiation {

    public interface IInprocessGlobalObjectRepositoryFactory{
        IInprocessGlobalObjectRepository CreateSingleton();
        
    }

    public class InprocessGlobalObjectRepositoryFactory : IInprocessGlobalObjectRepositoryFactory {

        private static IInprocessGlobalObjectRepositoryFactory factory = new InprocessGlobalObjectRepositoryFactory();

        private static IInprocessGlobalObjectRepository singleton;

        public static IInprocessGlobalObjectRepositoryFactory Factory => factory;

        // =====================================================================
        // IInjectableGlobalObjectRepositoryFactory
        // =====================================================================

        IInprocessGlobalObjectRepository IInprocessGlobalObjectRepositoryFactory.CreateSingleton() {
            if (singleton == null) {
                singleton = new InprocessGlobalObjectRepository();
            }
            return singleton;
        }

        // =====================================================================
        // static
        // =====================================================================

        public static IInprocessGlobalObjectRepository CreateSingleton() 
            => factory.CreateSingleton();

        public static void InjectCustomFactory(IInprocessGlobalObjectRepositoryFactory factory) 
            => InprocessGlobalObjectRepositoryFactory.factory = factory;
    }
}