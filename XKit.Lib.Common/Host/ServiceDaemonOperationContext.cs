using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Host {

    /// <summary>
    /// Provides the context of a service operation, with information
    /// particular to the lifecyle of the operation.
    /// </summary>
    public class ServiceDaemonOperationContext : OperationContext {

        public ServiceDaemonOperationContext(
            IServiceDaemonOperationOwner daemon,
            IServiceBase service,
            ILocalHostEnvironment localHostEnvironment,
            Guid messageProcessingId,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null
        ) : base(
            localHostEnvironment,
            correlationId, 
            correlationTags
        ) {
            this.Service = service ?? throw new ArgumentNullException(nameof(service));
            this.Daemon = daemon ?? throw new ArgumentNullException(nameof(daemon));
            this.MessageProcessingId = messageProcessingId;
        }

        // =====================================================================
        // public
        // =====================================================================

        public IServiceBase Service { get; }
        public IServiceDaemonOperationOwner Daemon { get; }
        public Guid MessageProcessingId { get; }
    }
}