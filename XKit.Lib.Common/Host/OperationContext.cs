using System;
using System.Collections.Generic;
using XKit.Lib.Common.Fabric;

namespace XKit.Lib.Common.Host {

    /// <summary>
    /// Provides the context of a service operation, with information
    /// particular to the lifecyle of the operation.
    /// </summary>
    public class OperationContext {

        public OperationContext(
            ILocalHostEnvironment localHostEnvironment,
            string correlationId,
            IReadOnlyList<string> correlationTags
        ) {
            this.LocalHostEnvironment = localHostEnvironment ?? throw new ArgumentNullException(nameof(localHostEnvironment));
            this.CorrelationId = correlationId ?? Common.Utility.Identifiers.GenerateIdentifier();
            this.CorrelationTags = correlationTags;
            this.OperationId = Guid.NewGuid();
        }

        // =====================================================================
        // public
        // =====================================================================

        public Guid OperationId { get; }
        public string CorrelationId { get; }
        public IReadOnlyList<string> CorrelationTags { get; }
        public ILocalHostEnvironment LocalHostEnvironment { get; }
        public IDependencyConnector DependencyConnector => LocalHostEnvironment.DependencyConnector;
    }
}