using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Common.Client {

    public interface IServiceClientFactory {

        IReadOnlyDescriptor Descriptor { get; }
    }

    public interface IServiceClientFactory<TServiceClientInterface> : IServiceClientFactory {

        TServiceClientInterface CreateServiceClient(
            ServiceClientParameters clientParameters,
            ServiceCallTypeParameters defaultCallTypeParameters = null
        );
    }
}