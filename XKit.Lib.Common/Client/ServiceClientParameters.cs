using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.ObjectInstantiation;

namespace XKit.Lib.Common.Client {
    public class ServiceClientParameters {
        
        private List<string> commonCorrelationTags;
        
        public IDependencyConnector DependencyConnector { get; }
        public IRuntimeMonitor Monitor { get; }
        public string RequestorInstanceId { get; }
        public string RequestorFabricId { get; }
        public string DefaultCorrelationId { get; private set; }
        public IReadOnlyList<string> CommonCorrelationTags => commonCorrelationTags;

        private ServiceClientParameters(
            IDependencyConnector dependencyConnector,
            IRuntimeMonitor monitor,
            string requestorFabricId,
            string requestorInstanceId, 
            string defaultCorrelationId, 
            IReadOnlyList<string> commonCorrelationTags
        ) {
            this.DependencyConnector = dependencyConnector;
            this.Monitor = monitor;
            this.RequestorFabricId = requestorFabricId;
            this.RequestorInstanceId = requestorInstanceId;
            this.DefaultCorrelationId = defaultCorrelationId;
            this.commonCorrelationTags = commonCorrelationTags?.Distinct().ToList() ?? new List<string>();
        }

        public ServiceClientParameters SetDefaultCorrelationId(string defaultCorrelationId) {
            this.DefaultCorrelationId = defaultCorrelationId;
            return this;
        }
        
        public ServiceClientParameters NewForRequestor(string requestorFabricId, string requestorInstanceId) =>
            new ServiceClientParameters(DependencyConnector, Monitor, requestorFabricId, requestorInstanceId, this.DefaultCorrelationId, this.CommonCorrelationTags);

        public ServiceClientParameters Clone() =>
            new ServiceClientParameters(DependencyConnector, this.Monitor, this.RequestorFabricId, this.RequestorInstanceId, this.DefaultCorrelationId, this.CommonCorrelationTags);
        
        public ServiceClientParameters AddCommonCorrelationTags(
            params string[] tags
        ) {
            if (CommonCorrelationTags == null) {
                this.commonCorrelationTags = new List<string>();
            }
            this.commonCorrelationTags.AddRange(tags);
            this.commonCorrelationTags = commonCorrelationTags.Distinct().ToList();
            return this;
        }

        public static ServiceClientParameters CreateForConsumer(            
            string requestorFabricId, 
            string defaultCorrelationId = null,
            IRuntimeMonitor monitor = null,
            IReadOnlyList<string> commonCorrelationTags = null,
            IDependencyConnector dependencyConnector = null
        ) => new ServiceClientParameters(
            dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>(), 
            monitor, 
            requestorFabricId, 
            null, 
            defaultCorrelationId, 
            commonCorrelationTags
        );
        
        public static ServiceClientParameters CreateForHost(
            ServiceOperationContext context,
            IRuntimeMonitor monitor
        ) => new ServiceClientParameters(
            context.DependencyConnector, 
            monitor, 
            context.RequestorFabricId, 
            context.RequestorInstanceId, 
            context.CorrelationId, 
            context.CorrelationTags?.ToArray()
        );

        public static ServiceClientParameters CreateForHost(
            ServiceDaemonOperationContext context,
            IRuntimeMonitor monitor
        ) => new ServiceClientParameters(
            context.DependencyConnector, 
            monitor, 
            context.LocalHostEnvironment.FabricId, 
            context.Service.InstanceId, 
            context.CorrelationId, 
            context.CorrelationTags?.ToArray()
        );
    }
}