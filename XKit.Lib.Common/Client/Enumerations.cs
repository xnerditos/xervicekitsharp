namespace XKit.Lib.Common.Client {
    public enum ServiceClientErrorHandling {
        DoNothing,
        LogInfo,
        LogWarning,
        LogErrorRecoverable,
        LogErrorNonRecoverable,
        ThrowException
    }
}