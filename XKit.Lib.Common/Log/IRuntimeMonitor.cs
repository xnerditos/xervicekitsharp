using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace XKit.Lib.Common.Log {

    public interface IRuntimeMonitor {

        bool IsInErrorState { get; }
        MonitorErrorState ErrorState { get; }
        string ErrorMessage { get; }
        bool ClearErrorState();

        void ErratumAs(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
			IEnumerable<string> tags = null,
            string callerFilePath = "",
			string callerMemberName = "",
			int callerLineNumber = 0
        );

        void ErrorAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            bool recoverable = false,
			IEnumerable<string> tags = null,
			string callerFilePath = "",
			string callerMemberName = "",
			int callerLineNumber = 0
        );

        void WarningAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = "",
			string callerMemberName = "",
			int callerLineNumber = 0
        );

        void TraceAs(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
			IEnumerable<string> tags = null,
			string callerFilePath = "",
			string callerMemberName = "",
			int callerLineNumber = 0
        );

        void Erratum(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Error(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            bool recoverable = false,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Warning(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Status(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
			IEnumerable<string> tags = null
        );
        
        void Audit(
            string message, 
            IReadOnlyDictionary<string, object> attributes = null, 
            object code = null, 
            IEnumerable<string> tags = null
        );

        void Trace(
            string message,
            IReadOnlyDictionary<string, object> attributes,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
                
        void Info(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object data = null,
            object code = null, 
            IEnumerable<string> tags = null
        );

        void Erratum(
            string message,
            object attributes,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Error(
            string message,
            object attributes,
            object code = null,
            bool recoverable = false,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Warning(
            string message,
            object attributes,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Status(
            string message,
            object attributes,
            object code = null,
			IEnumerable<string> tags = null
        );
        
        void Audit(
            string message, 
            object attributes, 
            object code = null, 
            IEnumerable<string> tags = null
        );

        void Trace(
            string message,
            object attributes,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Trace(
            string message,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Trace(
            object attributes,
            object code = null,
			IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void Info(
            string message,
            object attributes,
            object data = null,
            object code = null, 
            IEnumerable<string> tags = null
        );
    }
}