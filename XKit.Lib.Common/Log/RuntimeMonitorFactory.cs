using System.Collections.Generic;

namespace XKit.Lib.Common.Log {

    public interface IRuntimeMonitorFactory {
        IRuntimeMonitor Create(
            ILogWriter log = null,
            RuntimeMonitorEvent erratumHandler = null,
            RuntimeMonitorEvent errorHandler = null,
            RuntimeMonitorEvent warningHandler = null,
            RuntimeMonitorEvent infoHandler = null,
            RuntimeMonitorEvent snapshotHandler = null,
            RuntimeMonitorEvent statusHandler = null,
            RuntimeMonitorEvent traceHandler = null
        );
    }

    public delegate bool RuntimeMonitorEvent(
            string message, 
            object code, 
            IReadOnlyDictionary<string, object> attributes, 
            string callerFilePath, 
            string callerMemberName, 
            int? callerLineNumber,
            object data,
            IEnumerable<string> tags
        );

    public class RuntimeMonitorFactory : IRuntimeMonitorFactory {

        private static IRuntimeMonitorFactory factory = new RuntimeMonitorFactory();

        public static IRuntimeMonitorFactory Factory => factory;

        // =====================================================================
        // IServiceManagerFactory
        // =====================================================================

        IRuntimeMonitor IRuntimeMonitorFactory.Create(
            ILogWriter log,
            RuntimeMonitorEvent erratumHandler,
            RuntimeMonitorEvent errorHandler,
            RuntimeMonitorEvent warningHandler,
            RuntimeMonitorEvent infoHandler,
            RuntimeMonitorEvent snapshotHandler,
            RuntimeMonitorEvent statusHandler,
            RuntimeMonitorEvent traceHandler
        ) => new RuntimeMonitor(
                log,
                erratumHandler,
                errorHandler,
                warningHandler,
                infoHandler,
                snapshotHandler,
                statusHandler,
                traceHandler
            );

        // =====================================================================
        // Static
        // =====================================================================

        public static IRuntimeMonitor Create(
            ILogWriter log = null,
            RuntimeMonitorEvent erratumHandler = null,
            RuntimeMonitorEvent errorHandler = null,
            RuntimeMonitorEvent warningHandler = null,
            RuntimeMonitorEvent infoHandler = null,
            RuntimeMonitorEvent snapshotHandler = null,
            RuntimeMonitorEvent statusHandler = null,
            RuntimeMonitorEvent traceHandler = null
        ) => Factory.Create(
            log,
            errorHandler,
            warningHandler,
            infoHandler,
            snapshotHandler,
            statusHandler,
            traceHandler
        );
        
        public static void InjectCustomFactory(IRuntimeMonitorFactory factory)
            => RuntimeMonitorFactory.factory = factory;
    }
}