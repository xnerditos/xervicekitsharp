
namespace XKit.Lib.Common.Log {

    public interface IReadOnlyLogTag {
        string Name { get; }
    }
}