using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Log {

    public interface IReadOnlyLogEventEntry {

        Guid LogJobEntryId { get; }
        Guid LogEventEntryId { get; }
        DateTime Timestamp { get; }
        LogEventType GetEventType();
        //void SetEventType(LogEventType value);
        string EventTypeName { get; }
        string Code { get; }
        string Message { get; }
        IEnumerable<IReadOnlyLogTag> Tags { get; }
        IEnumerable<IReadOnlyLogAttribute> Attributes { get; }
        int? TraceIndex { get; }
		bool IsPrivate { get; }
    }
}