using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Log {

    public interface IReadOnlyLogEntryData {
        Guid LogEntryDataId { get; }
        Guid? LogEventEntryId { get; }
        Guid LogJobEntryId { get; }
        string Name { get; }
		byte[] Value { get; }
		T GetValueAs<T>() where T : class;
    }
}