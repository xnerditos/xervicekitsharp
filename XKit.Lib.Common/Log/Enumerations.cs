namespace XKit.Lib.Common.Log {

    public enum LogEventType {

        // =====================================================================
        // "Public" log types
        // =====================================================================

        /// <summary>
        /// Log event type is unknown
        /// </summary>
        None = 0,

        /// <summary>
        /// Entering a series of events associated with a service operation.  The code
        /// indicates the type of operation (OperationTypeEnum)
        /// </summary>
        Enter = 1,

        /// <summary>
        /// The operation is transitioning to a different running context.  The code
        /// will indicate the nature of the transition (ContextChangeTypeEnum)
        /// </summary>
        ContextChange = 2,

        /// <summary>
        /// Unexpected condition that resulted in termination of the operation
        /// </summary>
        Fatality = 3,       
        
        /// <summary>
        /// A likely bug.  This can be followed by a warning or error
        /// </summary>
        Erratum = 4,

        /// <summary>
        /// Unexpected error for the operation, usually unrecoverable
        /// </summary>
        Error = 5,          
        
        /// <summary>
        /// Unexpected condition that is recoverable or should be noted
        /// </summary>
        Warning = 6,       
        
        /// <summary>
        /// Status message intended to communicate the progress of the operation
        /// </summary>
        Status = 7,         
        
        /// <summary>
        /// Saves a set of data that is for information
        /// </summary>
        Info = 8,
        
        /// <summary>
        /// Event that is intended to trace the execution path 
        /// </summary>
        Trace = 9,

        /// <summary>
        /// Event that records auditable activity 
        /// </summary>
        Audit = 10,

        /// <summary>
        /// Information for replaying  
        /// </summary>
        ReplayHint = 98,

        /// <summary>
        /// Exiting a series of events associated with an operation
        /// </summary>
        Exit = 99,

        // =====================================================================
        // "Private" log types
        // These follow log message types are private to the service
        // =====================================================================
        
        /// <summary>
        /// Save information about the operation at a given point, intended
        /// to be available as data for the future
        /// </summary>
        Snapshot = 100,

        // =====================================================================
        // "Maintenance" log types
        // =====================================================================
        
        /// <summary>
        /// used by the logging engine to stand in for deleted entries
        /// </summary>
        LoggingEngineArchivePlaceholder = 200,

        /// <summary>
        /// used by the logging engine to stand in for deleted entries
        /// </summary>
        LoggingEngineArchiveSummary = 201,

        /// <summary>
        /// used by the logging engine to indicate a problem when doing log maintenance
        /// </summary>
        LoggingEngineWarning = 299,

        // =====================================================================
        // Service defined log type names use this LogEventType enumeration
        // =====================================================================
        
        ServiceDefined = 1000
    }

    public enum LoggingOptions : uint { 
        None = 0x0000,
        IncludeEntryAndExitData = 0x0001,
        Default = IncludeEntryAndExitData,
        All = 0xffff
    }

    public enum ContextChangeTypeEnum {
        TransitionToBackground = 1
    }

    public enum JobSystemEffect { 
        /// <summary>
        /// The operation may or may not have effects
        /// </summary>
        NotSpecified = 0,

        /// <summary>
        /// The operation has no direct effects on the system
        /// </summary>
        None = 1,

        /// <summary>
        /// the operation has only transient direct effects on the system.  It can reasonably
        /// be skipped if we were rebuilding the system from the log.
        /// </summary>
        Transient = 2,

        /// <summary>
        /// the operation has persistent effecs on the system and shoul be included if we were
        /// rebuilding the system by replaying from the log.
        /// </summary>
        Persistent = 3
    }

    /// <summary>
    /// Specifies how a job can be replayed to rebuild the system if necessary.
    /// </summary>
    public enum JobReplayStrategyEnum { 
        /// <summary>
        /// No replay strategy has been given
        /// </summary>
        NotSpecifiedOrUnknown = 0,
        /// <summary>
        /// This job is not replayable
        /// </summary>
        NotReplayable = 1,
        /// <summary>
        /// This job can be replayed by rerunning the original request
        /// </summary>
        ReplayOriginalRequest = 2,
        /// <summary>
        /// This job can be replayed by running a modified request that was given at termination
        /// </summary>
        ReplayModifiedRequest = 3,
        /// <summary>
        /// This job does not need to be replayed (usually because of a later event that deprecates replay)
        /// </summary>
        ReplayNotNeeded = 4
    }

    public enum JobResultStatusEnum {
        /// <summary>
        /// No status determined
        /// </summary>
        Incomplete = 0,

        /// <summary>
        /// Operation completed successfully
        /// </summary>
        Success = 1,

        /// <summary>
        /// Operation completed with partial success
        /// </summary>
        PartialSuccess = 2,

        /// <summary>
        /// Operation completion is still pending
        /// </summary>
        Pending = 3,

        /// <summary>
        /// Operation work item has incorrect data
        /// </summary>
        NoAction_BadRequest = 10,

        /// <summary>
        /// The job code could not be run becuase it was considered unavailable
        /// </summary>
        NoAction_JobUnavailable = 11,

        /// <summary>
        /// A callback was requested with the operation (operation), which is unavailable
        /// </summary>
        NoAction_CallbackUnavailable = 12,

        /// <summary>
        /// The operation was attempted, but did not respond or complete in time 
        /// </summary>
        NoAction_Timeout = 13,

        /// <summary>
        /// The attempt to connect failed (operation)
        /// </summary>
        NoAction_ConnectionFailed = 14,

        /// <summary>
        /// General error which is not expected to be recoverable.  Look at code and data for details.
        /// If it is unknown whether or not an error is recovable, the default chocie should be 
        /// this one, non-recoverable.
        /// </summary>
        NonRecoverableError = 100,

        /// <summary>
        /// An error occurred that might be ok with another attempt
        /// </summary>
        RecoverableError = 101,

        /// <summary>
        /// An exception occurred at the task level, indicating a possible problem in the toolkit
        /// itself since all such cases should be handled.
        /// </summary>
        Fault = 200,

        /// <summary>
        /// Unknown status.  Look at code and data for details.
        /// </summary>
        Unknown = 9999
    }

    public enum JobTypeEnum {
        OtherUnknown = 0,
        ServiceOperation = 1,
        ServiceDaemonMain = 2,
        ServiceDaemonOperation = 3,
        HostAction = 10,
        ClientAction = 20,
        DevelopmentTest = 100,
    }

    public enum MonitorErrorState { 
        NoErrors,
        RecoverableError,
        NonRecoverableError
    }
}