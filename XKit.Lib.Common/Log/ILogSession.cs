using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XKit.Lib.Common.Log {

    public interface ILogSession : IDisposable {

		Task BeginLog(
			object workItem = null, 
			IDictionary<string, object> parameters = null,
            LoggingOptions options = LoggingOptions.Default          
		);
        
		void BeginLogBackground(
			object workItem = null, 
			IDictionary<string, object> parameters = null,        
            LoggingOptions options = LoggingOptions.Default          
		);

        ILogWriter GetLogWriter();
        
        ILogReader GetLogReader();

        Task ExecuteStartupMaintenance();

        Task<IReadOnlyList<Guid>> ArchiveJobs(
            IEnumerable<Guid> jobIds = null,
            string matchingJobName = null,
            JobTypeEnum? matchingJobType = null,
			DateTime? startOnOrBeforeTimestamp = null,
			DateTime? startOnOrAfterTimestamp = null,
            bool? isComplete = null,
			DateTime? completeOnOrBeforeTimestamp = null,
			DateTime? completeOnOrAfterTimestamp = null,
            string matchingCorrelationId = null,
            string havingCorrelationTag = null,
            int limitCount = 1000,
            Func<IReadOnlyLogEventEntry, bool> eventSelector = null, 
            string archiveDbFile = null,
            bool deleteArchivedJobsFromSource = false,
            int waitBetweenStepsMilliseconds = 0
        );

        // Task MakeReplayArchive(
        //     IEnumerable<Guid> jobIds = null,
		// 	DateTime? startOnOrBeforeTimestamp = null,
		// 	DateTime? startOnOrAfterTimestamp = null,
        //     string matchingCorrelationId = null,
        //     string havingCorrelationTag = null,
        //     int limitCount = 1000,
        //     string replayArchiveDbFile = null
        // );

		Task PendingLog(
			IDictionary<string, object> pendingLogParameters = null,
			string statusMessage = null
		);

		void PendingLogBackground(
			IDictionary<string, object> pendingLogParameters = null,
			string statusMessage = null
		);

		Task EndLog(
			JobResultStatusEnum? status = JobResultStatusEnum.Incomplete,
			string statusMessage = null,
			object operationResult = null,
            object operationCode = null,
            JobReplayStrategyEnum replayStrategy = JobReplayStrategyEnum.NotSpecifiedOrUnknown,
            object replayData = null
		);		

		void EndLogBackground(
			JobResultStatusEnum? status = JobResultStatusEnum.Incomplete,
			string statusMessage = null,
			object operationResult = null,
            object operationCode = null,
            JobReplayStrategyEnum replayStrategy = JobReplayStrategyEnum.NotSpecifiedOrUnknown,
            object replayData = null
		);		
	}
}