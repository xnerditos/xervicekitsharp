using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Log
{
    public interface ILogManagerFactory {
        ILogManager Create(
            string logPath,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor = null,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor = null,
            Action<Exception> panicHandler = null
        );
    }
}