using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XKit.Lib.Common.Log {
	
    public interface ILogReader {

        Task<IEnumerable<IReadOnlyLogJobEntry>> QueryJobs(
            IEnumerable<Guid> jobIds = null,
            string matchingOriginatorName = null,
            int? matchingOriginatorVersion = null,
            string matchingJobName = null,
            JobTypeEnum? matchingJobType = null,
			DateTime? startOnOrBeforeTimestamp = null,
			DateTime? startOnOrAfterTimestamp = null,
            bool? isComplete = null,
			DateTime? completeOnOrBeforeTimestamp = null,
			DateTime? completeOnOrAfterTimestamp = null,
            string matchingCorrelationId = null,
            string havingCorrelationTag = null,
            string matchingInstanceId = null,
            string matchingHostFabricId = null,
            string matchingRequestorInstanceId = null,
            string matchingRequestorFabricId = null,
            int pageSize = 100,
            int pageIndex = 0
        );

        Task<IEnumerable<IReadOnlyLogEventEntry>> QueryEvents(
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
			DateTime? onOrBeforeTimestamp = null,
			DateTime? onOrAfterTimestamp = null,
			LogEventType? matchingEventType = null,
			string elseMatchingEventTypeName = null,
            string matchingCode = null,
            string havingTag = null,
            int pageSize = 100,
            int pageIndex = 0,
			bool includePrivate = false,
            bool groupByJob = false
        );

        Task<IEnumerable<IReadOnlyLogEntryData>> QueryData(
            IEnumerable<Guid> jobIds = null,
            IEnumerable<Guid> eventIds = null,
            int pageSize = 100,
            int pageIndex = 0
        );
	}
}