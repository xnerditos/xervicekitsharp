
namespace XKit.Lib.Common.Log {

    public interface IReadOnlyLogAttribute { 
        string Name { get; }
        string Value { get; }        
    }
}