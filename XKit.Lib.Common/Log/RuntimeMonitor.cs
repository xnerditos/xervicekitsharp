using System.Collections.Generic;
using System.Threading.Tasks;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Common.Log {

    internal class RuntimeMonitor : IRuntimeMonitor {

        private readonly ILogWriter log;
        private readonly RuntimeMonitorEvent erratumHandler;
        private readonly RuntimeMonitorEvent errorHandler;
        private readonly RuntimeMonitorEvent warningHandler;
        private readonly RuntimeMonitorEvent infoHandler;
        private readonly RuntimeMonitorEvent snapshotHandler;
        private readonly RuntimeMonitorEvent statusHandler;
        private readonly RuntimeMonitorEvent traceHandler;

        private MonitorErrorState errorState = MonitorErrorState.NoErrors;
        private string errorMessage = null;

        public RuntimeMonitor(
            ILogWriter log,
            RuntimeMonitorEvent erratumHandler,
            RuntimeMonitorEvent errorHandler,
            RuntimeMonitorEvent warningHandler,
            RuntimeMonitorEvent infoHandler,
            RuntimeMonitorEvent snapshotHandler,
            RuntimeMonitorEvent statusHandler,
            RuntimeMonitorEvent traceHandler
        ) {
            this.log = log;
            this.erratumHandler = erratumHandler;
            this.errorHandler = errorHandler;
            this.warningHandler = warningHandler;
            this.infoHandler = infoHandler;
            this.snapshotHandler = snapshotHandler;
            this.statusHandler = statusHandler;
            this.traceHandler = traceHandler;
        }

        // =====================================================================
        // IRuntimeMonitor
        // =====================================================================

        void IRuntimeMonitor.ErratumAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Erratum(
            message,
            attributes,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.ErrorAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            bool recoverable,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Error(
            message,
            attributes,
            code,
            recoverable,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.WarningAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Warning(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        void IRuntimeMonitor.TraceAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Trace(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Erratum(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Erratum(
            message,
            attributes,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.Error(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            bool recoverable,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Error(
            message,
            attributes,
            code,
            recoverable,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.Info(
            string message, 
            IReadOnlyDictionary<string, object> attributes,
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => Info(
            message,
            attributes,
            data,
            code,
            tags            
        );

        void IRuntimeMonitor.Audit(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) => Audit(
            message,
            attributes,
            code,
            tags
        );

        void IRuntimeMonitor.Status(
            string message, 
            IReadOnlyDictionary<string, object> attributes,
            object code,
            IEnumerable<string> tags
        ) => Status(
            message,
            attributes,
            code,
            tags        
        );

        void IRuntimeMonitor.Trace(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Trace(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Warning(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Warning(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Erratum(
            string message, 
            object attributes, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Erratum(
            message,
            attributes?.FieldsToDictionary(),
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.Error(
            string message, 
            object attributes, 
            object code, 
            bool recoverable,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Error(
            message,
            attributes?.FieldsToDictionary(),
            code,
            recoverable,
            callerFilePath,
            callerMemberName,
            callerLineNumber,
            tags
        );

        void IRuntimeMonitor.Info(
            string message, 
            object attributes,
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => Info(
            message,
            attributes?.FieldsToDictionary(),
            data,
            code,
            tags            
        );

        void IRuntimeMonitor.Audit(
            string message, 
            object attributes, 
            object code, 
            IEnumerable<string> tags
        ) => Audit(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags
        );

        void IRuntimeMonitor.Status(
            string message, 
            object attributes,
            object code,
            IEnumerable<string> tags
        ) => Status(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags   
        );

        void IRuntimeMonitor.Trace(
            string message, 
            object attributes, 
            object code,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Trace(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Trace(
            string message, 
            object code,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Trace(
            message,
            (Dictionary<string,object>)null,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Trace(
            object attributes, 
            object code,
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Trace(
            "",
            attributes?.FieldsToDictionary(),
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        void IRuntimeMonitor.Warning(
            string message, 
            object attributes, 
            object code, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => Warning(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber            
        );

        bool IRuntimeMonitor.ClearErrorState() {
            var b = errorState != MonitorErrorState.NoErrors;
            errorState = MonitorErrorState.NoErrors;
            errorMessage = null;
            return b;
        }

        bool IRuntimeMonitor.IsInErrorState => this.errorState != MonitorErrorState.NoErrors;

        string IRuntimeMonitor.ErrorMessage => this.errorMessage;

        MonitorErrorState IRuntimeMonitor.ErrorState => this.errorState;

        // =====================================================================
        // Workers
        // =====================================================================

        private void Erratum(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber,
            IEnumerable<string> tags
        ) {
            bool logEvent = InvokeHandler(
                handler: erratumHandler,
                message: message,
                attributes: attributes,
                filePath: callerFilePath,
                memberName: callerMemberName,
                lineNumber: callerLineNumber,
                tags: tags
            );
            if (logEvent && log != null) {
                log.ErratumAsBackground(
                    message: message,
                    attributes: attributes,
                    filePath: callerFilePath,
                    memberName: callerMemberName,
                    lineNumber: callerLineNumber,
                    tags: tags
                );
            }

            // default to an error state.  A subsequent calle to one fo the Error methods
            // will override this.
            this.errorState = MonitorErrorState.NonRecoverableError;
        }

        private void Error(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            bool recoverable,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber,
            IEnumerable<string> tags
        ) {
            bool logEvent = InvokeHandler(
                handler: errorHandler,
                message: message,
                code: code,
                attributes: attributes,
                filePath: callerFilePath,
                memberName: callerMemberName,
                lineNumber: callerLineNumber,
                tags: tags
            );
            if (logEvent && log != null) {
                log.ErrorAsBackground(
                    message: message,
                    attributes: attributes,
                    code: code,
                    filePath: callerFilePath,
                    memberName: callerMemberName,
                    lineNumber: callerLineNumber,
                    tags: tags
                );
            }
            this.errorState = recoverable ? MonitorErrorState.RecoverableError : MonitorErrorState.NonRecoverableError;
            this.errorMessage = message;
        }

        private void Info(
            string message, 
            IReadOnlyDictionary<string, object> attributes,
            object data, 
            object code, 
            IEnumerable<string> tags
        ) {
            bool logEvent = InvokeHandler(
                handler: infoHandler,
                message: message,
                attributes: attributes,
                code: code,
                data: data,
                tags: tags
            );
            if (logEvent && log != null) {
                log.InfoBackground(
                    message: message,
                    attributes: attributes,
                    code: code,
                    data: data,
                    tags: tags
                );
            }
        }

        private void Status(
            string message, 
            IReadOnlyDictionary<string, object> attributes,
            object code, 
            IEnumerable<string> tags
        ) {
            bool logEvent = InvokeHandler(
                handler: statusHandler,
                message: message,
                code: code,
                attributes: attributes,
                tags: tags
            );
            if (logEvent && log != null) {
                log.StatusBackground(
                    message: message,
                    code: code,
                    attributes: attributes,
                    tags: tags
                );
            }
        }

        private void Trace(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags,
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) {
            bool logEvent = InvokeHandler(
                handler: traceHandler,
                message: message,
                attributes: attributes,
                code: code,
                filePath: callerFilePath,
                memberName: callerMemberName,
                lineNumber: callerLineNumber,
                tags: tags
            );
            if (logEvent && log != null) {
                log.TraceAsBackground(
                    message: message,
                    attributes: attributes,
                    code: code,
                    filePath: callerFilePath,
                    memberName: callerMemberName,
                    lineNumber: callerLineNumber,
                    tags: tags
                );
            }
        }

        private void Audit(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) {
            bool logEvent = InvokeHandler(
                handler: traceHandler,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );
            if (logEvent && log != null) {
                log.AuditBackground(
                    message: message,
                    attributes: attributes,
                    code: code,
                    tags: tags
                );
            }
        }

        private void Warning(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) {
            bool logEvent = InvokeHandler(
                handler: warningHandler,
                message: message,
                code: code,
                attributes: attributes,
                filePath: callerFilePath,
                memberName: callerMemberName,
                lineNumber: callerLineNumber,
                tags
            );
            if (logEvent && log != null) {
                log.WarningAsBackground(
                    message: message,
                    attributes: attributes,
                    code: code,
                    filePath: callerFilePath,
                    memberName: callerMemberName,
                    lineNumber: callerLineNumber,
                    tags: tags
                );
            }
        }

        // =====================================================================
        // private
        // =====================================================================

        bool InvokeHandler(
            RuntimeMonitorEvent handler,
            string message = null, 
            object code = null, 
            IReadOnlyDictionary<string, object> attributes = null, 
            string filePath = null, 
            string memberName = null, 
            int? lineNumber = null,
            object data = null,
            IEnumerable<string> tags = null
        ) => handler == null ? true : handler(
            message,
            code,
            attributes,
            filePath,
            memberName,
            lineNumber,
            data,
            tags
        );
    }
}