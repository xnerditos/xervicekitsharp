using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace XKit.Lib.Common.Log {
	
    public interface ILogWriter {

        // ---------------------------------------------------------------------
        
        Task<IReadOnlyLogEventEntry> Begin(
            object workItem,
            IReadOnlyDictionary<string, object> parameters
        );

        Task<IReadOnlyLogEventEntry> End(
			JobResultStatusEnum status,
			string message,
			object resultData,
            object resultCode,
            JobReplayStrategyEnum? replayStrategy,
            object replayData
        );

        // ---------------------------------------------------------------------
        
        Task<IReadOnlyLogEventEntry> NewEvent(
            LogEventType eventType,
            string message = null,
            object data = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			bool isPrivate = false
        );

        Task<IReadOnlyLogEventEntry> NewEvent(
            string eventTypeName,
            string message = null,
            object data = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			bool isPrivate = false
        );

        void NewEventBackground(
            LogEventType eventType,
            string message = null,
            object data = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			bool isPrivate = false
        );

        void NewEventBackground(
            string eventTypeName,
            string message = null,
            object data = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			bool isPrivate = false
        );

        // ---------------------------------------------------------------------
        
        Task<IReadOnlyLogEventEntry> Fatality(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        void FatalityBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Erratum(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        Task<IReadOnlyLogEventEntry> ErratumAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        void ErratumBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        void ErratumAsBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Error(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        Task<IReadOnlyLogEventEntry> ErrorAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );
        
        void ErrorBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        void ErrorAsBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Warning(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        Task<IReadOnlyLogEventEntry> WarningAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );
        
        void WarningBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
        
        void WarningAsBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Status(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        void StatusBackground(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Trace(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
                
        Task<IReadOnlyLogEventEntry> TraceAs(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        void TraceBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        );
                
        void TraceAsBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			string filePath = "",
			string memberName = "",
			int lineNumber = 0
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Audit(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        void AuditBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Info(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        );
        
        void InfoBackground(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> Snapshot(
            IReadOnlyDictionary<string, object> attributes,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        void SnapshotBackground(
            IReadOnlyDictionary<string, object> attributes,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        );

        // ---------------------------------------------------------------------

        Task<IReadOnlyLogEventEntry> ReplaceReplayHint(
            Guid forJobId,
            JobReplayStrategyEnum replayStrategy,
            object replayData = null
        );

        void ReplaceReplayHintBackground(
            Guid forJobId,
            JobReplayStrategyEnum replayStrategy,
            object replayData = null
        );

        // ---------------------------------------------------------------------
        
        ILogWriter AutoLog(IDictionary<string, object> values);
        ILogWriter AutoLog(string key, object value);
        ILogWriter AutoLog(object anonObjectAsKeysAndValues);
	}
}