
namespace XKit.Lib.Common.Log {

    public class LogAttribute : IReadOnlyLogAttribute { 
        public string Name { get; set; }
        public string Value { get; set; }        
    }
}