using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Log {

    public interface IReadOnlyLogJobEntry {

        Guid LogJobEntryId { get; }
        JobTypeEnum JobType { get; }
        string OriginatorName { get; }
        int OriginatorVersion { get; }
		string JobName { get; }
        DateTime StartTimestamp { get; }
        DateTime? CompleteTimestamp { get; }
        string CorrelationId { get; }
        IReadOnlyList<IReadOnlyLogTag> CorrelationTags { get; }
        string HostFabricId { get; }
        string InstanceId { get; }
        string RequestorInstanceId { get; }
        string RequestorFabricId { get; }
        string JobNote { get; }
        string ResultCode { get; }
        JobResultStatusEnum Status { get; set; }
    }
}