using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.Log {
	
    public interface ILogManager {

		ILogSession CreateWriteableSession(
            JobTypeEnum jobType,
            JobSystemEffect effect,
            string originatorName,
            int originatorVersion,
            string jobName,
            string hostId,
            string instanceId = null,
            Guid jobId = default(Guid),
            string correlationId = null,
            string requestorFabricId = null,
            string requestorInstanceId = null,
            IEnumerable<string> correlationTags = null
        );

		ILogSession CreateReadOnlySession();

        void Finish();
	}
}