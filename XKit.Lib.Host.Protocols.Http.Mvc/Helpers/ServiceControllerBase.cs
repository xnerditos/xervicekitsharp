using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using Microsoft.AspNetCore.Mvc;
using XKit.Lib.Common.Host;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using XKit.Lib.Common.Log;
using System.Collections.Concurrent;
using XKit.Lib.Common.Utility;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Host.Protocols.Http.Mvc.Helpers {

    [Produces("application/json")]
    [ApiController]
    public abstract class ServiceControllerBase : ControllerBase, IDisposable {

        public class SlimLogEntry {
            public Guid EventId { get; set; }
            public DateTime Timestamp { get; set; }
            public string Type { get; set; }
            public int? TraceIndex { get; set; }
            public string Code { get; set; }
            public string Message { get; set; }
            public IEnumerable<IReadOnlyLogTag> Tags { get; set; }
            public IEnumerable<IReadOnlyLogAttribute> Attributes { get; set; }
        }

        protected ILocalHostEnvironment LocalEnvironment { get; private set; }
        protected ServiceOperationContext Context { get; private set; }
        protected IServiceBase ServiceCore { get; private set; } 
        protected IServiceOperation Operation { get; private set; }

        protected ServiceControllerBase(
            ILocalHostEnvironment localEnvironment,
            IServiceBase service
        ) {
            LocalEnvironment = localEnvironment ?? throw new ArgumentNullException(nameof(localEnvironment));
            ServiceCore = service ?? throw new ArgumentNullException(nameof(service));
        }

        protected ServiceControllerBase(
            ILocalHostEnvironment localEnvironment
        ) {
            LocalEnvironment = localEnvironment ?? throw new ArgumentNullException(nameof(localEnvironment));
        }

        protected void SetService(
            IServiceBase service
        ) {
            ServiceCore = service ?? throw new ArgumentNullException(nameof(service));
        }

        protected async Task<ActionResult> RunServiceOperationAndHttpResponse(
            IServiceBase service
        ) {
            if (service == null) {
                return BadRequest("Service not found");
            }
            SetService(service);

            const string debugQueryKey = "debug";
            var t = Request.Query[debugQueryKey];
            var debug = SelectQueryValue(debugQueryKey)?.Equals("true", StringComparison.InvariantCultureIgnoreCase);

            using var reader = new StreamReader(Request.Body);
            string content = await reader.ReadToEndAsync();
            ServiceCallRequest request = Json.From<ServiceCallRequest>(content);                        

            ServiceCallResult result = null;
            Exception operationException = null;
            try { 
                result = await service.ExecuteCall(request);
            } catch(Exception ex) { 
                operationException = ex; 
            }
            
            var serviceStatus = ServiceCore.GetServiceStatus();

            if (debug.GetValueOrDefault()) {
                return Ok(new {
                    result = result,
                    log = GetOperationLog()
                });
            } else {
                return Ok(result);
            }
        }

        // =====================================================================
        // IDispoable
        // =====================================================================
        public void Dispose() {
            // operation should not be disposed, it will care for it's own termination
            Operation = null;
        }

        // =====================================================================
        // private 
        // ===================================================================== 
    
        private async Task<IEnumerable<SlimLogEntry>> GetOperationLog() {
            using (var logSession = LocalEnvironment.LogManager.CreateReadOnlySession()) {
                var reader = logSession.GetLogReader();
                var log = await reader.QueryEvents(jobIds: new[] { Context.OperationId }, pageSize: 1000);
                return log.Select(entry => new SlimLogEntry {
                    EventId = entry.LogEventEntryId,
                    Timestamp = entry.Timestamp,
                    Type = entry.EventTypeName ?? entry.EventTypeName,
                    TraceIndex = entry.TraceIndex,
                    Code = entry.Code,
                    Message = entry.Message,
                    Tags = entry.Tags != null && entry.Tags.Any() ? entry.Tags : null,
                    Attributes = entry.Attributes != null && entry.Attributes.Any() ? entry.Attributes : null
                });
            }
        }

        private string SelectQueryValue(string keyName)
            => Request.Query.ContainsKey(keyName) ? Request.Query[keyName].FirstOrDefault() : null; 
    }
}