using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common;
using XKit.Lib.Connector.Fabric;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Utility.Threading;
using System.Threading.Tasks;

namespace XKit.Lib.Consumer {

    public static class ConsumerHelper {

        private static List<IReadOnlyDescriptor> Dependencies = new List<IReadOnlyDescriptor>();
        
        private class SimpleFabricEnviroment : ILocalFabricEnvironment {
            private string fabricId;
            Func<IEnumerable<IReadOnlyDescriptor>> getDependenciesSource;

            string ILocalFabricEnvironment.FabricId => fabricId;

            IEnumerable<IReadOnlyDescriptor> ILocalFabricEnvironment.GetDependencies() 
                => this.getDependenciesSource?.Invoke()?.Select(d => d.Clone()).ToArray() ?? new Descriptor[0];

            public SimpleFabricEnviroment(
                string fabricId, 
                Func<IList<IReadOnlyDescriptor>> getDependenciesSource
            ) {
                this.fabricId = fabricId;
                this.getDependenciesSource = getDependenciesSource;
            }
        }

        private static IFabricConnector fabricConnector;
        private static Lazy<IInprocessGlobalObjectRepository> igorLazy = new Lazy<IInprocessGlobalObjectRepository>(() => InprocessGlobalObjectRepositoryFactory.CreateSingleton());
        public static IInprocessGlobalObjectRepository InjectableGlobalObjectRepository => igorLazy.Value;
        public static IDependencyConnector DependencyConnector => fabricConnector;
        public static IFabricConnector FabricConnector => fabricConnector;
        public static string FabricId => fabricConnector.FabricId;

        public static IFabricConnector CreateInitConsumer(
            IEnumerable<IInstanceClientFactory> instanceClientFactories,
            IEnumerable<IServiceClientFactory> dependencyFactories,
            IEnumerable<string> initialRegistryAddresses = null,
            bool fatalIfCannotRegister = false,
            IRuntimeMonitor monitor = null
        ) {
            return CreateInitConsumer(
                instanceClientFactories, 
                dependencyFactories.Select(df => df.Descriptor),
                initialRegistryAddresses,
                fatalIfCannotRegister,
                monitor
            );
        }

        public static IFabricConnector CreateInitConsumer(
            IEnumerable<IInstanceClientFactory> instanceClientFactories = null,
            IEnumerable<IReadOnlyDescriptor> dependencies = null,
            IEnumerable<string> initialRegistryAddresses = null,
            bool fatalIfCannotRegister = true,
            IRuntimeMonitor monitor = null
        ) {
            if (ConsumerHelper.fabricConnector == null) {
                if (initialRegistryAddresses == null) {
                    string registryAddresses = Environment.GetEnvironmentVariable(EnvironmentHelperConstants.EnvironmentVariables.InitialRegistryAddresses);
                    initialRegistryAddresses = registryAddresses?.Split(';', StringSplitOptions.RemoveEmptyEntries);
                }

                if (instanceClientFactories == null) {
                    instanceClientFactories = new[] {
                        XKit.Lib.Connector.Protocols.Http.HttpClientFactory.Factory
                    };
                }
                ConsumerHelper.fabricConnector = FabricConnectorFactory.Create(instanceClientFactories.ToList());

                if (dependencies != null) {
                    Dependencies.AddRange(dependencies);
                }

                fabricConnector.Initialize();

                InjectableGlobalObjectRepository.RegisterSingleton(
                    fabricConnector,
                    typeof(IFabricConnector),
                    typeof(IDependencyConnector)
                );

                XKit.Lib.Common.Utility.Threading.TaskUtil.RunSyncSafely(
                    () => fabricConnector.RegisterAsConsumer(
                        initialRegistryAddresses,
                        new SimpleFabricEnviroment(fabricConnector.FabricId, () => Dependencies),
                        monitor,
                        fatalIfCannotRegister
                    )
                );
            }
            return fabricConnector;
        }

        public static void ResetDependencies(IEnumerable<IReadOnlyDescriptor> dependencies) {
            Dependencies.Clear();
            Dependencies.AddRange(dependencies);
        }

        public static TServiceClientInterface CreateServiceClient<TServiceClientInterface>(
            IServiceClientFactory<TServiceClientInterface> factory,
            IRuntimeMonitor monitor = null,
            string defaultCorrelationId = null, 
            IReadOnlyList<string> commonCorrelationTags = null,
            ServiceCallTypeParameters defaultCallParameters = null
        ) {
            return factory.CreateServiceClient(
                ServiceClientParameters.CreateForConsumer(
                    fabricConnector.FabricId, 
                    defaultCorrelationId, 
                    monitor,
                    commonCorrelationTags,
                    DependencyConnector
                ), 
                defaultCallTypeParameters: defaultCallParameters ?? ServiceCallTypeParameters.SyncResult()
            );
        }

        public static void Refresh(IRuntimeMonitor monitor = null) 
            => TaskUtil.RunSyncSafely(() => fabricConnector.Refresh(monitor));

        public static void Unregister(IRuntimeMonitor monitor = null) 
            => TaskUtil.RunSyncSafely(() => fabricConnector.Unregister(monitor));

        public static Task RefreshAsync(IRuntimeMonitor monitor = null) 
            => fabricConnector.Refresh(monitor);

        public static Task UnregisterAsync(IRuntimeMonitor monitor = null) 
            => fabricConnector.Unregister(monitor);
    }
}