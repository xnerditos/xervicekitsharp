using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;
using XKit.Lib.LocalLog;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XKit.Lib.Common.Utility.Threading;

namespace UnitTests.LocalLog {

    public static class Extensions {
        public static async Task<IReadOnlyLogEventEntry> GetLogEntry(this ILogReader reader, Guid id) {
           return (await reader.QueryEvents(eventIds: new[] { id })).FirstOrDefault();
        }
        public static async Task<IReadOnlyLogEntryData> GetLogData(this ILogReader reader, Guid id) {
           return (await reader.QueryData(eventIds: new[] { id })).FirstOrDefault();
        }
        public static async Task<IReadOnlyLogJobEntry> GetLogJob(this ILogReader reader, Guid id) {
           return (await reader.QueryJobs(jobIds: new[] { id })).FirstOrDefault();
        }
    }

	[TestClass]
	public partial class LogTests {

		// ===========================================================================
		// Test types
		// ===========================================================================
		public class TestDataEntity {
			public string SomeTestString { get; set; }
			public int SomeTestInt { get; set; }
		}
        public class TestLogAttribute {
            public string Name { get; set; }
            public object Value { get; set; }
        }

		// ===========================================================================
		// Test data
		// ===========================================================================

        private const string OriginatorName = nameof(OriginatorName);
        private const int OriginatorVersion = 1;
		private const string OperationName = nameof(OperationName);
        private const string InstanceId1 = nameof(InstanceId1);
        private readonly Guid OperationId1 = Guid.NewGuid();
        private const string HostFabricId1 = nameof(HostFabricId1);
        private const string CorrelationId1 = nameof(CorrelationId1);
        private const string RequestorInstanceId1 = nameof(RequestorInstanceId1);
        private const string RequestorFabricId1 = nameof(RequestorFabricId1);
		private const string Code = nameof(Code);
		private const string Tag1 = nameof(Tag1);
		private const string Tag2 = nameof(Tag2);
		private readonly List<string> Tags = new List<string> { Tag1, Tag2 };
		private const string Key1 = nameof(Key1);
		private const string Key2 = nameof(Key2);
		private const string Value1 = nameof(Value1);
		private const int Value2 = 1;
		private const string LogMessage = nameof(LogMessage);
		private readonly TestLogAttribute[] Attributes = new TestLogAttribute[] {
			new TestLogAttribute { Name = Key1, Value = Value1 },
			new TestLogAttribute { Name = Key2, Value = Value2.ToString() }
		};
		private const string TestString = nameof(TestString);
		private const int TestInt = 1;

		// ===========================================================================
		// Test common
		// ===========================================================================

		private string logPath;
		private ILogManager logManager;

		[TestInitialize]
		public void TestInit() {
		}

		[TestCleanup]
		public void TestCleanup() { 
			Cleanup();
		}
		
		private void Cleanup() {
			if (logPath != null && File.Exists(logPath)) {
				File.Delete(logPath);
			}
		}

		private ILogSession CreateSession(
            bool writeable = true,
            string originatorName = OriginatorName,
            int originatorVersion = OriginatorVersion,
			string operationName = OperationName,
            string instanceId = InstanceId1,
            string hostId = HostFabricId1,
            Guid jobId = default(Guid),
            string correlationId = CorrelationId1,
            string requestorInstanceId = RequestorInstanceId1,
            string requestorFabricId = RequestorFabricId1,
            IEnumerable<string> correlationTags = null
		) {

            if (jobId == default(Guid)) {
                jobId = OperationId1;
            }
			if (logManager == null) {
				logPath = $"./test.{Guid.NewGuid().ToString()}.log.db";
				Cleanup();
				logManager = LogManagerFactory.Create(logPath);
			}

            if (writeable) {
                return logManager.CreateWriteableSession(
                    JobTypeEnum.DevelopmentTest,
                    JobSystemEffect.NotSpecified,
                    originatorName,
                    originatorVersion,
                    operationName,
                    hostId,
                    instanceId,
                    jobId,
                    correlationId,
                    requestorFabricId,
                    requestorInstanceId,
                    correlationTags
                );
            } else {
                return logManager.CreateReadOnlySession();
            }
		}

		// ===========================================================================
		// Assertions
		// ===========================================================================

		[TestMethod]
		public async Task CreateLogEntry() {

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();
				var logReader = logSession.GetLogReader();

				var entry1 = await logWriter.NewEvent(
					LogEventType.Info,
					LogMessage,
					null,
					Attributes.ToDictionary(at => at.Name, at => at.Value),
					Code,
					Tags
				);
				
				var count = (await logReader.QueryEvents()).Count();
				var entry2 = await logReader.GetLogEntry(entry1.LogEventEntryId);

				await logSession.EndLog(JobResultStatusEnum.Success);

				entry1.LogEventEntryId.Should().NotBeEmpty();
				entry1.Should().BeEquivalentTo(
                    entry2,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                entry1.Timestamp.Should().BeCloseTo(entry2.Timestamp);
				count.Should().Be(2); // NOTE: The count does not include the Exit log entry
			}
		}

		[TestMethod]
		public async Task CreateLogEntryWithData() {

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();
				var logReader = logSession.GetLogReader();
				var data = new TestDataEntity {
					SomeTestString = TestString,
					SomeTestInt = TestInt
				};

				var entry1 = await logWriter.NewEvent(
					LogEventType.Info,
					LogMessage,
					data, 
					Attributes.ToDictionary(at => at.Name, at => at.Value),
					Code,
					Tags
					);
				
				var entry2 = await logReader.GetLogEntry(entry1.LogEventEntryId);
				var entry2Data = await logReader.GetLogData(entry1.LogEventEntryId);

				await logSession.EndLog(JobResultStatusEnum.Success);

				entry1.Should().BeEquivalentTo(
                    entry2,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                entry1.Timestamp.Should().BeCloseTo(entry2.Timestamp);
				entry2.Should().NotBeNull();
				entry2Data.Should().NotBeNull();
				entry2Data.GetValueAs<TestDataEntity>().Should().NotBeNull();
				entry2Data.GetValueAs<TestDataEntity>().Should().BeEquivalentTo(data);
			}
		}

		[TestMethod]
		public async Task LogEntryIsRetrievableFromNewSession() {

			IReadOnlyLogEventEntry entry1;

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();

				entry1 = await logWriter.NewEvent(
					eventType: LogEventType.Info,
					message: LogMessage,
					attributes: Attributes.ToDictionary(at => at.Name, at => at.Value),
					code: Code,
					tags: Tags
					);

				await logSession.EndLog(JobResultStatusEnum.Success);
			}

			using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();

				var logReader = logSession.GetLogReader();
				var entry2 = await logReader.GetLogEntry(entry1.LogEventEntryId);

				var count = (await logReader.QueryEvents()).Count();

				await logSession.EndLog(JobResultStatusEnum.Success);

				entry1.LogEventEntryId.Should().NotBeEmpty();
				entry1.Should().BeEquivalentTo(
                    entry2,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                entry1.Timestamp.Should().BeCloseTo(entry2.Timestamp);
				count.Should().Be(3);
			}
		}

		[TestMethod]
		public async Task PageSizeAndIndexRespected() {

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();
				AddRandomMessages(logWriter, 13);
				await logWriter.Info();
				await logWriter.Info();
				await logWriter.Trace();
				await logWriter.Trace();
				await logWriter.Trace();
				await logWriter.Trace();
				await logWriter.Trace();
				await logWriter.Trace();
				await logSession.EndLog(JobResultStatusEnum.Success);
			}

			using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();

				var logReader = logSession.GetLogReader();

				var count1stPagePlain = (await logReader.QueryEvents(
					pageSize:5, 
					pageIndex:0)
				).Count();
				var count1stPageInfoOnly = (await logReader.QueryEvents(
					matchingEventType: LogEventType.Info, 
					pageSize:5, 
					pageIndex:0)
				).Count();

				var count1stPageTraceOnly = (await logReader.QueryEvents(
					matchingEventType: LogEventType.Trace, 
					pageSize:5, 
					pageIndex:0)
				).Count();
				var count2ndPageTraceOnly = (await logReader.QueryEvents(
					matchingEventType: LogEventType.Trace, 
					pageSize:5, 
					pageIndex:1)
				).Count();
				var count3rdPageTraceOnly = (await logReader.QueryEvents(
					matchingEventType: LogEventType.Trace, 
					pageSize:5, 
					pageIndex:2)
				).Count();

				await logSession.EndLog(JobResultStatusEnum.Success);

				count1stPagePlain.Should().Be(5);
				count1stPageInfoOnly.Should().Be(2);
				count1stPageTraceOnly.Should().Be(5);
				count2ndPageTraceOnly.Should().Be(1);
				count3rdPageTraceOnly.Should().Be(0);
			}
		}

		[TestMethod]
		public async Task PrivateIsRespected() {

			IReadOnlyLogEventEntry entry1;

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();
				AddRandomMessages(logWriter, 2);

				entry1 = await logWriter.NewEvent(
					LogEventType.Info,
					LogMessage,
					Attributes,
					null,
					Code,
					Tags,
					isPrivate: true
					);

				AddRandomMessages(logWriter, 2);
				await logSession.EndLog(JobResultStatusEnum.Success);
			}

			using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
				var logReader = logSession.GetLogReader();

				var count = (await logReader.QueryEvents(includePrivate: false)).Count();

				await logSession.EndLog(JobResultStatusEnum.Success);

				count.Should().Be(6);  // Enter + Exit + 4 random messages
			}
		}

		[TestMethod]
		public async Task QueryOnMatchingDates() {

			IReadOnlyLogEventEntry entry1;
			IReadOnlyLogEventEntry entry2;
			DateTime timestamp1;

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();

				entry1 = await logWriter.NewEvent(
					LogEventType.Info,
					LogMessage + "1",
					attributes: Attributes.ToDictionary(at => at.Name, at => at.Value),
					code: Code,
					tags: Tags
					);
				
				Thread.Sleep(100);
				timestamp1 = DateTime.UtcNow;
				Thread.Sleep(100);
				
				entry2 = await logWriter.NewEvent(
					LogEventType.Info,
					LogMessage + "2",
					attributes: Attributes.ToDictionary(at => at.Name, at => at.Value),
					code: Code,
					tags: Tags
					);

				await logSession.EndLog(JobResultStatusEnum.Success);
			}

			using (var logSession = CreateSession(writeable: false)) {

				IReadOnlyLogEventEntry testEntry1;
				IReadOnlyLogEventEntry testEntry2;
				int count1;
				int count2;

				await logSession.BeginLog();

				var logReader = logSession.GetLogReader();

				var query1 = await logReader.QueryEvents(
					matchingEventType: LogEventType.Info,
					onOrBeforeTimestamp: timestamp1,
					onOrAfterTimestamp: null
				);
				count1 = query1.Count();
				testEntry1 = query1.FirstOrDefault();

				var query2 = await logReader.QueryEvents(
					matchingEventType: LogEventType.Info,
					onOrBeforeTimestamp: null,
					onOrAfterTimestamp: timestamp1
				);
				count2 = query2.Count();
				testEntry2 = query2.FirstOrDefault();

				await logSession.EndLog(JobResultStatusEnum.Success);

				count1.Should().Be(1);
				testEntry1.Should().BeEquivalentTo(
                    entry1,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                testEntry1.Timestamp.Should().BeCloseTo(testEntry1.Timestamp);

				count2.Should().Be(1);
				testEntry2.Should().BeEquivalentTo(
                    entry2,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                testEntry2.Timestamp.Should().BeCloseTo(entry2.Timestamp);
			}
		}

		[TestMethod]
		public async Task QueryOnOneOfEventTypes() 
			=> await TestQueryOnProperty(matchingEventType: LogEventType.Info);

		[TestMethod]
		public async Task QueryOnOperationId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingOperationId: OperationId1
			);

		[TestMethod]
		public async Task QueryOnOperationName() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingOperationName: OperationName
			);

		// [TestMethod]
		// public async Task QueryOnMatchingAttribute() 
		// 	=> await TestQueryOnProperty(matchingAttribute: Attributes.First());

		// [TestMethod]
		// public async Task QueryOnHavingAttribute() 
		// 	=> await TestQueryOnProperty(havingAttribute: Attributes.First().Key);

		[TestMethod]
		public async Task QueryOnTag() 
			=> await TestQueryOnProperty(havingTag: Tags.First());

		[TestMethod]
		public async Task QueryOnCorrelationTag()
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				havingCorrelationTag: Tag1
			);

		[TestMethod]
		public async Task QueryOnCorrelationId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingCorrelationId: CorrelationId1
			);

		[TestMethod]
		public async Task QueryOnInstanceId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingInstanceId: InstanceId1
			);

		[TestMethod]
		public async Task QueryOnCode() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingCode: Code
			);

		[TestMethod]
		public async Task QueryOnHostFabricId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingHostFabricId: HostFabricId1
			);

		[TestMethod]
		public async Task QueryOnOriginator() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 
				matchingOriginatorName: OriginatorName,
				matchingOriginatorVersion: OriginatorVersion
			);

		[TestMethod]
		public async Task QueryOnRequestorInstanceId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 				
				matchingRequestorInstanceId: RequestorInstanceId1
			);

		[TestMethod]
		public async Task QueryOnRequestorHostFabricId() 
			=> await TestQueryOnProperty(
				matchingEventType: LogEventType.Info, 				
				matchingRequestorFabricId: RequestorFabricId1
			);

		[TestMethod]
		public async Task QueryOnEventTypeName() 
			=> await TestQueryOnProperty(elseMatchingEventTypeName: nameof(LogEventType.Info));

		[TestMethod]
		public async Task QueryOnEventType() 
			=> await TestQueryOnProperty(matchingEventType: LogEventType.Info);

		[TestMethod]
		public async Task ExitHasCorrectInfo() {

			var entity = new TestDataEntity {
				SomeTestString = TestString,
				SomeTestInt = TestInt
			};

			using (var logSession = CreateSession()) {

				await logSession.BeginLog();

				var logWriter = logSession.GetLogWriter();
				logWriter.AutoLog(Key1, Value1);
				logWriter.AutoLog(Key2, Value2);
				AddRandomMessages(logWriter, 5);

				await logSession.EndLog(
					JobResultStatusEnum.PartialSuccess,
					LogMessage,
					entity
				);
			}

			using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();

				var entry = (await logReader.QueryEvents(matchingEventType: LogEventType.Exit)).FirstOrDefault();
                var entryData = await logReader.GetLogData(entry.LogEventEntryId);
				var expectedAttributes = Attributes.ToList();

				entry.Should().NotBeNull();
				entry.LogEventEntryId.Should().NotBeEmpty();
				entry.Attributes.Should().BeEquivalentTo(expectedAttributes);
				entryData.GetValueAs<TestDataEntity>().Should().BeEquivalentTo(entity);
			}
		}

		[TestMethod]
		public async Task EntryHasCorrectInfo() {

			var entity = new TestDataEntity {
				SomeTestString = TestString,
				SomeTestInt = TestInt
			};

			using (var logSession = CreateSession()) {

				await logSession.BeginLog(entity, Attributes.ToDictionary(at => at.Name, at => at.Value));

				var logWriter = logSession.GetLogWriter();
				AddRandomMessages(logWriter, 5);

				await logSession.EndLog(JobResultStatusEnum.Success);
			}

			using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();

				var entry = (await logReader.QueryEvents(elseMatchingEventTypeName: nameof(LogEventType.Enter))).FirstOrDefault();
                var entryData = await logReader.GetLogData(entry.LogEventEntryId);

				entry.Should().NotBeNull();
				entry.LogEventEntryId.Should().NotBeEmpty();
				entry.Attributes.Should().BeEquivalentTo(Attributes);
				entryData.GetValueAs<TestDataEntity>().Should().BeEquivalentTo(entity);
			}
		}

		[TestMethod]
        public async Task ArchivingMovesEntries() {
			
            var job1 = await CreateLogJob();
            int job1EventCount = 0;
            int job1DataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                job1EventCount = (await logReader.QueryEvents()).Count();
                job1DataCount = (await logReader.QueryData()).Count();
                await logSession.EndLog();
			}

            var job2 = await CreateLogJob();

            int job2EventCount = 0;
            int job2DataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                job2EventCount = (await logReader.QueryEvents()).Count() - job1EventCount;
                job2DataCount = (await logReader.QueryData()).Count() - job1DataCount;
                await logSession.EndLog();
			}

            var archiveLogfile = $"./test.{Guid.NewGuid().ToString()}.archive.log.db";
            var archivingOperationName = "Archiving";
            using (var logSession = CreateSession(operationName: archivingOperationName)) {

				await logSession.BeginLog();
                await logSession.ArchiveJobs(
                    jobIds: new[] { job1 }, 
                    archiveDbFile: archiveLogfile, 
                    deleteArchivedJobsFromSource: true
                );
                await logSession.EndLog();
			}

			var archiveLogManager = LogManagerFactory.Create(archiveLogfile);
			using (var logSession = archiveLogManager.CreateReadOnlySession()) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(1);
                events.Count.Should().Be(6);
                data.Count.Should().Be(job1DataCount);
			}

            using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(2);
                jobs.Any(j => j.JobName == archivingOperationName).Should().BeTrue();
                jobs.Any(j => j.JobName != archivingOperationName).Should().BeTrue();
                events.Count.Should().Be(job2EventCount + 3);
                events.Where(e => e.EventTypeName == "LoggingEngineArchiveSummary").Count().Should().Be(1);
                data.Count.Should().Be(job2DataCount + 2);
			}
        }

		[TestMethod]
        public async Task ArchivingSummarizesForIncompleteJobs() {

            var job = Guid.NewGuid();

            using var logSessionForJob = CreateSession(jobId: job); 
            await logSessionForJob.BeginLog();
            var testData = new TestDataEntity {
                SomeTestInt = 1,
                SomeTestString = job.ToString()
            };
            var logWriter = logSessionForJob.GetLogWriter();
            AddRandomMessages(logWriter, 10);
            await logWriter.Info(code: "a");
            await logWriter.Info(code: "b", data: testData);
            await Task.Delay(1000);
            await logWriter.Info(code: "c");
            await logWriter.Info(code: "d", data: testData);
            // job is incomplete!
                        
            int jobEventCount = 0;
            int jobDataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                jobEventCount = (await logReader.QueryEvents()).Count();
                jobDataCount = (await logReader.QueryData()).Count();
                await logSession.EndLog();
			}

            var archiveLogfile = $"./test.{Guid.NewGuid().ToString()}.archive.log.db";
            var archivingOperationName = "Archiving";
            using (var logSession = CreateSession(operationName: archivingOperationName)) {

				await logSession.BeginLog();
                await logSession.ArchiveJobs(
                    jobIds: new[] { job }, 
                    archiveDbFile: archiveLogfile, 
                    deleteArchivedJobsFromSource: true
                );
                await logSession.EndLog();
			}

			var archiveLogManager = LogManagerFactory.Create(archiveLogfile);
			using (var logSession = archiveLogManager.CreateReadOnlySession()) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(1);
                events.Count.Should().Be(5);
                data.Count.Should().Be(jobDataCount);
			}

            using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(2);
                jobs.Any(j => j.JobName == archivingOperationName).Should().BeTrue();
                jobs.Any(j => j.JobName != archivingOperationName).Should().BeTrue();
                events.Where(e => e.EventTypeName == "LoggingEngineArchivePlaceholder").Count().Should().Be(1);
			}
        }

		[TestMethod]
        public async Task ArchivingRespectsNoDeleteOption() {

            var job1 = await CreateLogJob();
            int job1EventCount = 0;
            int job1DataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                job1EventCount = (await logReader.QueryEvents()).Count();
                job1DataCount = (await logReader.QueryData()).Count();
                await logSession.EndLog();
			}

            var archiveLogfile = $"./test.{Guid.NewGuid().ToString()}.archive.log.db";
            var archivingOperationName = "Archiving";
            using (var logSession = CreateSession(operationName: archivingOperationName)) {

				await logSession.BeginLog();
                await logSession.ArchiveJobs(
                    jobIds: new[] { job1 }, 
                    archiveDbFile: archiveLogfile, 
                    deleteArchivedJobsFromSource: false
                );
                await logSession.EndLog();
			}

			var archiveLogManager = LogManagerFactory.Create(archiveLogfile);
			using (var logSession = archiveLogManager.CreateReadOnlySession()) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(1);
                events.Count.Should().Be(6);
                data.Count.Should().Be(job1DataCount);
			}

            using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(2);
                jobs.Any(j => j.JobName == archivingOperationName).Should().BeTrue();
                jobs.Any(j => j.JobName != archivingOperationName).Should().BeTrue();
                events.Count.Should().Be(job1EventCount + 2);
                data.Count.Should().Be(job1DataCount + 1);
			}
        }

		[TestMethod]
        public async Task ArchivingWorksWithoutArchiveDb() {

            var job1 = await CreateLogJob();
            int job1EventCount = 0;
            int job1DataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                job1EventCount = (await logReader.QueryEvents()).Count();
                job1DataCount = (await logReader.QueryData()).Count();
                await logSession.EndLog();
			}

            var job2 = await CreateLogJob();

            int job2EventCount = 0;
            int job2DataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                job2EventCount = (await logReader.QueryEvents()).Count() - job1EventCount;
                job2DataCount = (await logReader.QueryData()).Count() - job1DataCount;
                await logSession.EndLog();
			}

            var archivingOperationName = "Archiving";
            using (var logSession = CreateSession(operationName: archivingOperationName)) {

				await logSession.BeginLog();
                await logSession.ArchiveJobs(
                    jobIds: new[] { job1 }, 
                    archiveDbFile: null, 
                    deleteArchivedJobsFromSource: true
                );
                await logSession.EndLog();
			}

            using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(2);
                jobs.Any(j => j.JobName == archivingOperationName).Should().BeTrue();
                jobs.Any(j => j.JobName != archivingOperationName).Should().BeTrue();
                events.Count.Should().Be(job2EventCount + 3);
                events.Where(e => e.EventTypeName == "LoggingEngineArchiveSummary").Count().Should().Be(1);
                data.Count.Should().Be(job2DataCount + 2);
			}
        }

		[TestMethod]
        public async Task ArchivingRespectsEventSelectorForIncompleteJobs() {

            var job = Guid.NewGuid();

            using var logSessionForJob = CreateSession(jobId: job); 
            await logSessionForJob.BeginLog();
            var testData = new TestDataEntity {
                SomeTestInt = 1,
                SomeTestString = job.ToString()
            };
            var logWriter = logSessionForJob.GetLogWriter();
            AddRandomMessages(logWriter, 10);
            await logWriter.Info(code: "a");
            await logWriter.Info(code: "b", data: testData);
            await Task.Delay(1000);
            await logWriter.Info(code: "c");
            await logWriter.Info(code: "d", data: testData);
            // job is incomplete!
                        
            int jobEventCount = 0;
            int jobDataCount = 0;
            using (var logSession = CreateSession(writeable: false)) {

				await logSession.BeginLog();
                var logReader = logSession.GetLogReader();
                jobEventCount = (await logReader.QueryEvents()).Count();
                jobDataCount = (await logReader.QueryData()).Count();
                await logSession.EndLog();
			}

            var archiveLogfile = $"./test.{Guid.NewGuid().ToString()}.archive.log.db";
            var archivingOperationName = "Archiving";
            using (var logSession = CreateSession(operationName: archivingOperationName)) {

				await logSession.BeginLog();
                await logSession.ArchiveJobs(
                    jobIds: new[] { job }, 
                    archiveDbFile: archiveLogfile, 
                    deleteArchivedJobsFromSource: true,
                    eventSelector: (e) => e.Code == "a"
                );
                await logSession.EndLog();
			}

			var archiveLogManager = LogManagerFactory.Create(archiveLogfile);
			using (var logSession = archiveLogManager.CreateReadOnlySession()) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(1);
                events.Count.Should().Be(5);
                data.Count.Should().Be(jobDataCount);
			}

            using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();
                var jobs = (await logReader.QueryJobs()).ToList();
                var events = (await logReader.QueryEvents()).ToList();
                var data = (await logReader.QueryData()).ToList();

                jobs.Count.Should().Be(2);
                events.Where(e => e.Code == "a").Count().Should().Be(0);
                events.Where(e => e.Code == "b").Count().Should().Be(1);
                events.Where(e => e.Code == "c").Count().Should().Be(1);
                events.Where(e => e.Code == "d").Count().Should().Be(1);
			}
        }

		// ===========================================================================
		// private
		// ===========================================================================

        private async Task<Guid> CreateLogJob() {
            
            var job = Guid.NewGuid();
            using (var logSession = CreateSession(jobId: job)) {

				await logSession.BeginLog();

                var testData = new TestDataEntity {
                    SomeTestInt = 1,
                    SomeTestString = job.ToString()
                };

				var logWriter = logSession.GetLogWriter();
				AddRandomMessages(logWriter, 10);
				await logWriter.Info(code: "a");
				await logWriter.Info(code: "b", data: testData);
                await Task.Delay(400);
				await logWriter.Info(code: "c");
				await logWriter.Info(code: "d", data: testData);
				await logSession.EndLog(JobResultStatusEnum.Success);
			}

            return job;
        }

		private async Task TestQueryOnProperty(
            string matchingOriginatorName = null,
            int? matchingOriginatorVersion = null,
            string matchingOperationName = null,
			DateTime? onOrBeforeTimestamp = null,
			DateTime? onOrAfterTimestamp = null,
            LogEventType? matchingEventType = null,
			string elseMatchingEventTypeName = null,
            string matchingCorrelationId = null,
			string havingCorrelationTag = null,
			Guid matchingOperationId = default(Guid),
			string matchingInstanceId = null,
            string matchingHostFabricId = null,
			string matchingRequestorInstanceId = null,
            string matchingRequestorFabricId = null,
			string matchingCode = null,
            string havingTag = null
		) {
			
			IReadOnlyLogEventEntry entry1 = await CreateTestEntries(
					LogEventType.Info,
					LogMessage,
					Attributes.ToDictionary(at => at.Name, at => at.Value),
					Tags
			);

			using (var logSession = CreateSession(writeable: false)) {

				var logReader = logSession.GetLogReader();

				var jobs = await logReader.QueryJobs(
                    jobIds: matchingOperationId == default(Guid) ? null : new[] { matchingOperationId },
                    matchingOriginatorName: matchingOriginatorName,
                    matchingOriginatorVersion: matchingOriginatorVersion,
                    matchingJobName: null,
                    matchingJobType: null,
                    startOnOrBeforeTimestamp: null,
                    startOnOrAfterTimestamp: null,
                    completeOnOrBeforeTimestamp: null,
                    completeOnOrAfterTimestamp: null,
                    matchingCorrelationId: matchingCorrelationId,
                    havingCorrelationTag: havingCorrelationTag,
                    matchingInstanceId: matchingInstanceId,
                    matchingHostFabricId: matchingHostFabricId,
                    matchingRequestorInstanceId: matchingRequestorInstanceId,
                    matchingRequestorFabricId: matchingRequestorFabricId
				);

				var entry2 = (await logReader.QueryEvents(
                    jobIds: jobs.Select(j => j.LogJobEntryId),
                    eventIds: null,
					onOrBeforeTimestamp,
					onOrAfterTimestamp,
					matchingEventType,
					elseMatchingEventTypeName,
					matchingCode,
					havingTag
				)).FirstOrDefault();

				entry2.Should().BeEquivalentTo(
                    entry1,
                    opt => opt.Excluding(x => x.Timestamp)
                );
                entry2.Timestamp.Should().BeCloseTo(entry1.Timestamp);
			}
		}

		private async Task<IReadOnlyLogEventEntry> CreateTestEntries(
			LogEventType eventType = LogEventType.Info,
			string message = LogMessage,
			Dictionary<string, object> attributes = null,
			IList<string> tags = null
		) {
			using (var logSession = CreateSession(correlationTags: Tags, requestorFabricId: RequestorFabricId1)) {
				var logWriter = logSession.GetLogWriter();
                await logSession.BeginLog();
                AddRandomMessages(logWriter);
                var entry = await logWriter.NewEvent(
                    LogEventType.Info,
                    message,
                    null,
                    attributes,
                    Code,
                    tags
                    );
                AddRandomMessages(logWriter);
                await logSession.EndLog();
                return entry;
			}
			// return CreateTestEntries(
			// 	(logWriter, logSession) => {
    		// 		await logSession.BeginLog();
			// 		AddRandomMessages(logWriter);
			// 		var entry = await logWriter.NewEvent(
			// 			LogEventType.Info,
			// 			message,
			// 			null,
			// 			attributes,
			// 			Code,
			// 			tags
			// 			);
			// 		AddRandomMessages(logWriter);
            //         await logSession.EndLog();
			// 		return entry;
			// 	}
			// );
		}

		// private IReadOnlyLogEventEntry CreateTestEntries(
		// 	Func<ILogWriter, ILogSession, IReadOnlyLogEventEntry> createAction
		// ) {
		// 	using (var logSession = CreateSession(correlationTags: Tags, requestorFabricId: RequestorFabricId1)) {
		// 		var logWriter = logSession.GetLogWriter();
		// 		return createAction(logWriter, logSession);
		// 	}
		// }

		private void AddRandomMessages(ILogWriter writer, int count = 2) {
			for(int i = 0; i < count; i++) {
				TaskUtil.RunSyncSafely(() => writer.Status(Guid.NewGuid().ToString("N")));
			}
		}
	}
}