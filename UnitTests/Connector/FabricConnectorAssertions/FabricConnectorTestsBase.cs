using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;
using UnitTests.MockWrapper;
using System.Threading.Tasks;
using XKit.Lib.Common.Utility;
using RegistryOperationNames = XKit.Lib.Common.Services.StandardConstants.Managed.StandardServices.Registry.Operations;
using ServiceConstants = XKit.Lib.Common.Services.StandardConstants;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Log;

namespace UnitTests.Connector.FabricConnectorAssertions {

    public partial class FabricConnectorTestsBase : TestBase {


        // =====================================================================
        // Common mocks
        // =====================================================================
        protected ServiceCallFactoryMockWrapper ServiceCallRouterFactory { get; private set; } 
        protected InstanceClientFactoryMockWrapper InstanceClientFactory { get; private set; }
        protected HostEnvironmentMockWrapper HostEnvironment { get; private set; }
        protected RegistryClientMock RegistryClient { get; private set; }

        // =====================================================================
        // Init and cleanup
        // =====================================================================

        public FabricConnectorTestsBase() { 
            this.ServiceCallRouterFactory = Mocks.CreateWrapper<ServiceCallFactoryMockWrapper>();
            this.InstanceClientFactory = Mocks.CreateWrapper<InstanceClientFactoryMockWrapper>();
            this.HostEnvironment = Mocks.CreateWrapper<HostEnvironmentMockWrapper>();
            this.RegistryClient = new RegistryClientMock();
        }

        // =====================================================================
        // object creation
        // =====================================================================

        protected IFabricConnector CreateTarget(
        ) => new XKit.Lib.Connector.Fabric.FabricConnector(
                ServiceCallRouterFactory.Object,
                new[] { InstanceClientFactory.Object },
                RegistryClient
            );
        protected ServiceClientParameters CreateClientParameters() 
            => ServiceClientParameters.CreateForConsumer(
                requestorFabricId: TestConstants.RequestorFabricId,
                defaultCorrelationId: TestConstants.CorrelationId
            );

        protected FabricRegistration CreateHostRegistration(
            string hostId,
            IEnumerable<ServiceRegistration> serviceRegistrations = null
        ) => new FabricRegistration {
            Dependencies = new List<Descriptor> { TestConstants.Dependency1 },
            FabricId = hostId,
            Capabilities = null,
            Address = TestConstants.FakeLocalHostAddress,
            Status = new FabricStatus { 
                FabricId = TestConstants.HostFabricId, 
                Health = HealthEnum.Healthy, 
                RunState = RunStateEnum.Active 
            },
            HostedServices = (serviceRegistrations ?? new[] { 
                CreateServiceRegistration(
                    TestConstants.LocalServiceName1,
                    TestConstants.HostFabricId,
                    TestConstants.FakeLocalHostAddress
                ),  
                CreateServiceRegistration(
                    TestConstants.LocalServiceName2,
                    TestConstants.HostFabricId,
                    TestConstants.FakeLocalHostAddress
                ),  
            }).ToList()
        };

        public ServiceRegistration CreateServiceRegistration(
            string serviceName,
            string hostFabricId,
            params string[] hostAddresses
        ) {
            var descriptor = new Descriptor {
                    Collection = TestConstants.CollectionName,
                    Name = serviceName,
                    Version = 1
            };
            return CreateServiceRegistration(
                hostFabricId,
                descriptor,
                hostAddresses
            );
        }

        protected ServiceRegistration CreateServiceRegistration(
            string hostFabricId,
            Descriptor descriptor,
            params string[] hostAddresses
        ) {
            var registrationKey = 
                XKit.Lib.Common.Utility.Identifiers.GetServiceFullRegistrationKey(descriptor);

            var instances = new List<ServiceInstance>();

            instances.AddRange(hostAddresses.Select(address => 
                new ServiceInstance {
                    Descriptor = descriptor,
                    HostFabricId = hostFabricId,
                    HostAddress = address,
                    InstanceId = $"{descriptor.Name}-instance-for-{address}",
                    RegistrationKey = registrationKey
                }
            ));

            return new ServiceRegistration {
                Descriptor = descriptor,
                Instances = instances,
                CallPolicy = new ServiceCallPolicy(),
                RegistrationKey = registrationKey
            };
        }

        // =====================================================================
        // Setup and prepare
        // =====================================================================

        /// <summary>
        /// Sets up for the Initialize and Register calls to be made
        /// </summary>
        /// <returns></returns>
        protected ServiceCallRouterMockWrapper Setup_Registry(
            IEnumerable<ServiceRegistration> intitialServiceRegistrationsForDependencies = null,
            IEnumerable<ServiceRegistration> hostedServices = null,
            DateTime? cacheExpiration = null
        ) {

            var registryInstanceClient = Mocks.CreateWrapper<InstanceClientMockWrapper>();
            if (intitialServiceRegistrationsForDependencies == null) { 
                var dependency1Registration = CreateServiceRegistration(
                    serviceName: TestConstants.DependencyName1,
                    hostFabricId: TestConstants.FakeServiceHostId1,
                    TestConstants.FakeServiceHostAddress1
                );

                var metaRegistration = CreateServiceRegistration(
                    hostFabricId: TestConstants.FakeServiceHostId1,
                    descriptor: new Descriptor {
                        Collection = "Meta",
                        Name = TestConstants.MetaDependencyName,
                        Version = 1,
                        IsMetaService = true
                    },
                    TestConstants.FakeServiceHostAddress1
                );
                
                intitialServiceRegistrationsForDependencies = new[] { 
                    dependency1Registration,
                    metaRegistration
                };
            }
            HostEnvironment.SetupAll(
                FabricConnectorAssertions.TestConstants.HostFabricId,
                FabricConnectorAssertions.TestConstants.FakeLocalHostAddress,
                dependencies: intitialServiceRegistrationsForDependencies.Select(sv => sv.Descriptor),
                hostedServices: hostedServices
            );

            var registrationInstanceId = CreateRandomString();
            var registryServiceDependencyRegistration = new ServiceRegistration {
                Descriptor = ServiceConstants.Managed.StandardServices.Registry.Descriptor.Clone(),
                Instances = new List<ServiceInstance> { new ServiceInstance { 
                    Descriptor = ServiceConstants.Managed.StandardServices.Registry.Descriptor.Clone(),
                    HostFabricId = TestConstants.FakeServiceHostId1,
                    HostAddress = TestConstants.FakeServiceHostAddress1,
                    InstanceId = registrationInstanceId,
                    RegistrationKey = Identifiers.GetServiceFullRegistrationKey(ServiceConstants.Managed.StandardServices.Registry.Descriptor),
                    Status = new ServiceInstanceStatus {
                        Availability = AvailabilityEnum.Serving5,
                        Health = HealthEnum.Healthy,
                        InstanceId = registrationInstanceId,
                        RunState = RunStateEnum.Active
                    }
                }}
            };

            InstanceClientFactory.Setup_InitializeFactory(TestConstants.FakeLocalHostAddress);
            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                ServiceConstants.Managed.StandardServices.Registry.Descriptor,
                registryInstanceClient
            );

            var registrationCallRouter = ServiceCallRouterFactory.Setup_Create(
                ServiceConstants.Managed.StandardServices.Registry.Descriptor,
                new[] { registryInstanceClient }
            );

            RegistryClient.Setup_Register(
                new ServiceTopologyMap {
                    CacheExpiration = cacheExpiration,
                    Services = 
                        intitialServiceRegistrationsForDependencies
                        .Union(new[] { registryServiceDependencyRegistration })
                        .ToList()
                }
            );
            // registrationCallRouter.Setup_ExecuteCall<FabricRegistration, ServiceTopologyMap>(
            //     req => req.OperationName == RegistryOperationNames.Register,
            //     new XKit.Lib.Common.Fabric.ServiceCallResult<ServiceTopologyMap> {
            //         ServiceCallStatus = ServiceCallStatusEnum.Completed,
            //         OperationStatus = JobResultStatusEnum.Success,
            //         ResponseBody = new ServiceTopologyMap {
            //             CacheExpiration = cacheExpiration,
            //             Services = 
            //                 intitialServiceRegistrationsForDependencies
            //                 .Union(new[] { registryServiceDependencyRegistration })
            //                 .ToList()
            //         }
            //     }
            // );

            return registrationCallRouter;
        }

        /// <summary>
        /// Does the Initialize and Register calls on the target to prepare it
        /// </summary>
        protected async Task PrepareTarget_InitializeAndRegister(IFabricConnector target) {
            var hostId = target.Initialize();
            HostEnvironment.Setup_FabricId(hostId);
            await target.RegisterAsHost(
                new[] { TestConstants.FakeServiceHostAddress1 },
                HostEnvironment.Object,
                null
            );
        }
    }
}
