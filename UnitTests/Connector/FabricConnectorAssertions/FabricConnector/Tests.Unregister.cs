using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Connector.FabricConnectorAssertions.FabricConnector {

    [TestClass]
    public partial class Unregister : FabricConnectorTestsCommon {
        
        [TestMethod]
        public async Task UnregistersWithRegistryService() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();

            RegistryClient.Setup_Unregister_Succeeds();

            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            bool result = await target.Unregister(default);

            // -----------------------------------------------------------------
            // Assert
            result.Should().BeTrue();
            Mocks.VerifyAll();
        }

        [TestMethod]
        public async Task ReturnsFalseIfCannotUnregister() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();

            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            bool result = await target.Unregister(default);

            // -----------------------------------------------------------------
            // Assert
            result.Should().BeFalse();
            Mocks.VerifyAll();
        }
    }
}
