using System;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Registration;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnitTests.MockWrapper;

namespace UnitTests.Connector.FabricConnectorAssertions.DependencyConnectorAssertions {

    [TestClass]
    public class CreateRouter : DependencyConnectorTestsBase {
        
        [TestMethod]
        public async Task ObtainsRouterToManagedService() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();
            var callRouter = new ServiceCallRouterMockWrapper();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.Dependency1,
                new InstanceClientMockWrapper()
            );
            ServiceCallRouterFactory.Setup_Create(
                TestConstants.Dependency1,
                serviceCallRouter: callRouter
            );
            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.Dependency1,
                monitor: null,
                correlationId: TestConstants.CorrelationId
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().Be(callRouter.Object);
        }

        [TestMethod]
        public async Task ObtainsRouterToMetaService() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();
            var callRouter = new ServiceCallRouterMockWrapper();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.MetaserviceDependency,
                new InstanceClientMockWrapper()
            );
            ServiceCallRouterFactory.Setup_Create(
                TestConstants.MetaserviceDependency,
                serviceCallRouter: callRouter
            );
            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.MetaserviceDependency,
                monitor: null,
                correlationId: TestConstants.CorrelationId
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().Be(callRouter.Object);
            Mocks.VerifyAll();
        }

        // NOTE:  Broken test 
        // [TestMethod]
        // public async Task ThrowsWhenFatalIfNotAvailableTrue() {

        //     // -----------------------------------------------------------------
        //     // Arrange

        //     var target = CreateTarget();
        //     var registrationCallRouter = Setup_Registry();

        //     InstanceClientFactory.Setup_TryCreateClient(
        //         TestConstants.FakeServiceHostAddress1,
        //         TestConstants.Dependency1,
        //         null
        //     );
        //     await PrepareTarget_InitializeAndRegister(target);

        //     // -----------------------------------------------------------------
        //     // Act

        //     await target.Awaiting(async t => await t.CreateRouter(
        //         CreateCallContext(),
        //         TestConstants.Dependency1,
        //         fatalIfNotAvailable: true
        //     )).Should().ThrowAsync<Exception>();

        //     // -----------------------------------------------------------------
        //     // Assert

        //     Mocks.VerifyAll();
        // }

        [TestMethod]
        public async Task DoesNotThrowWhenFatalIfNotAvailableFalse() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.Dependency1,
                null
            );
            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.Dependency1,
                monitor: null,
                correlationId: TestConstants.CorrelationId,
                fatalIfNotAvailable: false
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().BeNull();
        }

        [TestMethod]
        public async Task RefreshesOnCacheExpired() {
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry(cacheExpiration: DateTime.Now.AddSeconds(3));
            var callRouter = new ServiceCallRouterMockWrapper();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.Dependency1,
                new InstanceClientMockWrapper()
            );

            ServiceCallRouterFactory.Setup_Create(
                TestConstants.Dependency1,
                serviceCallRouter: callRouter
            );
            
            var newDependencyRegistration = CreateServiceRegistration(
                serviceName: TestConstants.DependencyName1,
                hostFabricId: TestConstants.FakeServiceHostId1,
                TestConstants.FakeServiceHostAddress1
            );

            // registrationCallRouter.Setup_ExecuteCall<RefreshRegistrationRequest, ServiceTopologyMap>(
            //     req => req.OperationName == RegistryOperationNames.Refresh,
            //     new XKit.Lib.Common.Fabric.ServiceCallResult<ServiceTopologyMap> {
            //         ServiceCallStatus = ServiceCallStatusEnum.Completed,
            //         OperationStatus = JobResultStatusEnum.Success,
            //         ResponseBody = new ServiceTopologyMap {
            //             CacheExpiration = null,
            //             Services = new System.Collections.Generic.List<ServiceRegistration> { newDependencyRegistration }
            //         }
            //     }
            // );
            RegistryClient.Setup_Refresh(
                new ServiceTopologyMap {
                    CacheExpiration = null,
                    Services = new System.Collections.Generic.List<ServiceRegistration> { newDependencyRegistration }
                }            
            );

            HostEnvironment.Setup_GetHealth();
            HostEnvironment.Setup_GetHostedServiceStatuses(null);
            await PrepareTarget_InitializeAndRegister(target);

            Thread.Sleep(3500);    // let cache time out

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.Dependency1,
                monitor: null,
                correlationId: TestConstants.CorrelationId,
                fatalIfNotAvailable: false
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().Be(callRouter.Object);
        }

        [TestMethod]
        public async Task ResolvesForNewDependency() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();
            var callRouter = new ServiceCallRouterMockWrapper();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.Dependency2,
                new InstanceClientMockWrapper()
            );
            ServiceCallRouterFactory.Setup_Create(
                TestConstants.Dependency2,
                serviceCallRouter: callRouter
            );

            var newDependencyRegistration = CreateServiceRegistration(
                serviceName: TestConstants.DependencyName2,
                hostFabricId: TestConstants.FakeServiceHostId1,
                TestConstants.FakeServiceHostAddress1
            );
            // registrationCallRouter.Setup_ExecuteCall<RefreshRegistrationRequest, ServiceTopologyMap>(
            //     req => req.OperationName == RegistryOperationNames.Refresh,
            //     new XKit.Lib.Common.Fabric.ServiceCallResult<ServiceTopologyMap> {
            //         ServiceCallStatus = ServiceCallStatusEnum.Completed,
            //         OperationStatus = JobResultStatusEnum.Success,
            //         ResponseBody = new ServiceTopologyMap {
            //             CacheExpiration = null,
            //             Services = new System.Collections.Generic.List<ServiceRegistration> { newDependencyRegistration }
            //         }
            //     }
            // );
            RegistryClient.Setup_Refresh(
                new ServiceTopologyMap {
                    CacheExpiration = null,
                    Services = new System.Collections.Generic.List<ServiceRegistration> { newDependencyRegistration }
                }            
            );

            HostEnvironment.Setup_GetHealth();
            HostEnvironment.Setup_GetHostedServiceStatuses(null);

            await PrepareTarget_InitializeAndRegister(target);

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.Dependency2,
                monitor: null,
                correlationId: TestConstants.CorrelationId,
                fatalIfNotAvailable: false
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().Be(callRouter.Object);
        }

        [TestMethod]
        public async Task CachesRouter() {
            
            // -----------------------------------------------------------------
            // Arrange

            var target = CreateTarget();
            var registrationCallRouter = Setup_Registry();
            var callRouter = new ServiceCallRouterMockWrapper();

            InstanceClientFactory.Setup_TryCreateClient(
                TestConstants.FakeServiceHostAddress1,
                TestConstants.Dependency1,
                new InstanceClientMockWrapper()
            );
            ServiceCallRouterFactory.Setup_Create(
                TestConstants.Dependency1,
                serviceCallRouter: callRouter
            );
            await PrepareTarget_InitializeAndRegister(target);
            
            // First call gets everything, next one should be cached
            await target.CreateCallRouter(
                target: TestConstants.Dependency1,
                monitor: null,
                correlationId: TestConstants.CorrelationId,
                fatalIfNotAvailable: false
            );

            // -----------------------------------------------------------------
            // Act

            var result = await target.CreateCallRouter(
                target: TestConstants.Dependency1,
                monitor: null,
                correlationId: TestConstants.CorrelationId,
                fatalIfNotAvailable: false
            );

            // -----------------------------------------------------------------
            // Assert

            result.Should().Be(callRouter.Object);
            ServiceCallRouterFactory.Verify_Create(TestConstants.Dependency1, Times.Once);
        }
    }
}
