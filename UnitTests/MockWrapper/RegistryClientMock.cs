using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services.Registry;
using XKit.Lib.Connector.Fabric;

namespace UnitTests.MockWrapper {

    public class RegistryClientMock : IRegistryClient {

        private ServiceTopologyMap registerResponse;
        private ServiceTopologyMap refreshResponse;
        private bool unregisterSucceeds;

        // =====================================================================
        // Setup
        // =====================================================================

        public void Setup_Register(ServiceTopologyMap response) {
            this.registerResponse = response;
        }

        public void Setup_Refresh(ServiceTopologyMap response) {
            this.refreshResponse = response;
        }

        public void Setup_Unregister_Succeeds() {
            unregisterSucceeds = true;
        }

        Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> IServiceClient<IRegistryApi>.ExecuteWith<TRequestBody, TResponseBody>(Expression<Func<IRegistryApi, Task<ServiceCallResult<TResponseBody>>>> expression, TRequestBody requestBody, ServiceCallTypeParameters callTypeParameters, string correlationId, IReadOnlyList<string> correlationTags, IRuntimeMonitor monitor, IServiceCallRouter callRouter, ServiceClientErrorHandling? errorHandling, string payload) {
            if (typeof(TRequestBody) == typeof(FabricRegistration)) {
                return Task.FromResult(
                    (IReadOnlyList<ServiceCallResult<TResponseBody>>) new[] { 
                        new ServiceCallResult<TResponseBody> {
                            ServiceCallStatus = ServiceCallStatusEnum.Completed,
                            OperationStatus = this.registerResponse == null ? JobResultStatusEnum.NonRecoverableError : JobResultStatusEnum.Success,
                            ResponseBody = this.registerResponse as TResponseBody
                        }
                    } 
                );
            }
            if (typeof(TRequestBody) == typeof(RefreshRegistrationRequest)) {
                return Task.FromResult(
                    (IReadOnlyList<ServiceCallResult<TResponseBody>>) new[] { 
                        new ServiceCallResult<TResponseBody> {
                            ServiceCallStatus = ServiceCallStatusEnum.Completed,
                            OperationStatus = JobResultStatusEnum.Success,
                            ResponseBody = this.refreshResponse as TResponseBody
                        }
                    } 
                );
            }
            throw new Exception("Mock no matched");
        }

        Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> IServiceClient<IRegistryApi>.ExecuteWith<TResponseBody>(Expression<Func<IRegistryApi, Task<ServiceCallResult<TResponseBody>>>> expression, ServiceCallTypeParameters callTypeParameters, string correlationId, IReadOnlyList<string> correlationTags, IRuntimeMonitor monitor, IServiceCallRouter callRouter, ServiceClientErrorHandling? errorHandling) {
            throw new NotImplementedException();
        }

        Task<IReadOnlyList<ServiceCallResult>> IServiceClient<IRegistryApi>.ExecuteWith<TRequestBody>(Expression<Func<IRegistryApi, Task<ServiceCallResult>>> expression, TRequestBody requestBody, ServiceCallTypeParameters callTypeParameters, string correlationId, IReadOnlyList<string> correlationTags, IRuntimeMonitor monitor, IServiceCallRouter callRouter, ServiceClientErrorHandling? errorHandling, string payload) {
            return Task.FromResult(
                (IReadOnlyList<ServiceCallResult>) new[] { 
                    new ServiceCallResult {
                        ServiceCallStatus = ServiceCallStatusEnum.Completed,
                        OperationStatus = unregisterSucceeds ? JobResultStatusEnum.Success : JobResultStatusEnum.NonRecoverableError
                    }
                } 
            );
        }

        Task<IReadOnlyList<ServiceCallResult>> IServiceClient<IRegistryApi>.ExecuteWith(Expression<Func<IRegistryApi, Task<ServiceCallResult>>> expression, ServiceCallTypeParameters callTypeParameters, string correlationId, IReadOnlyList<string> correlationTags, IRuntimeMonitor monitor, IServiceCallRouter callRouter, ServiceClientErrorHandling? errorHandling) {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose() {
            throw new NotImplementedException();
        }

        Task<ServiceCallResult<ServiceTopologyMap>> IRegistryApi.Register(FabricRegistration request) {
            throw new NotImplementedException();
        }

        Task<ServiceCallResult<ServiceTopologyMap>> IRegistryApi.Refresh(RefreshRegistrationRequest request) {
            throw new NotImplementedException();
        }

        Task<ServiceCallResult> IRegistryApi.Unregister(UnregisterRequest request) {
            throw new NotImplementedException();
        }
    }
}