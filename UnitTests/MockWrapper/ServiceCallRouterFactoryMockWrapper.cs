using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Registration;
using XKit.Lib.Connector.Dependency;
using FluentAssertions;
using Moq;

namespace UnitTests.MockWrapper {

    public class ServiceCallFactoryMockWrapper : MockWrapperBase<IServiceCallRouterFactory> {

        // =====================================================================
        // Setup
        // =====================================================================

        public ServiceCallRouterMockWrapper Setup_Create(
            IReadOnlyDescriptor expectedDescriptor,
            IEnumerable<InstanceClientMockWrapper> mockClients
        ) => Setup_Create(
            expectedDescriptor, 
            mockClients, 
            MockWrappersManager.CreateWrapper<ServiceCallRouterMockWrapper>()
        );

        public ServiceCallRouterMockWrapper Setup_Create(
            IReadOnlyDescriptor expectedDescriptor,
            IEnumerable<InstanceClientMockWrapper> mockClients = null,
            ServiceCallRouterMockWrapper serviceCallRouter = null
        ) {
            Mock.Setup(
                x => x.Create(
                    It.Is<IReadOnlyServiceRegistration>(
                        p => p.Descriptor.IsSameService(expectedDescriptor)
                    ),
                    It.IsAny<DateTime>(),
                    It.IsAny<IEnumerable<IInstanceClient>>()
                )
            ).Callback((IReadOnlyServiceRegistration r, DateTime d, IEnumerable<IInstanceClient> clients) => {
                if (mockClients != null) {
                    clients.Should().BeEquivalentTo(mockClients.Select(c => c.Object));
                }
            }).Returns(serviceCallRouter?.Object);
            
            serviceCallRouter?.SetTargetService(expectedDescriptor);
            return serviceCallRouter;
        }

        // =====================================================================
        // Verify
        // =====================================================================

        public void Verify_Create(
            IReadOnlyDescriptor expectedDescriptor,
            Func<Times> times
        ) {
            Mock.Verify(
                x => x.Create(
                    It.Is<IReadOnlyServiceRegistration>(
                        p => p.Descriptor.IsSameService(expectedDescriptor)
                    ),
                    It.IsAny<DateTime>(),
                    It.IsAny<IEnumerable<IInstanceClient>>()
                ),
                times
            );
        }
    }
}