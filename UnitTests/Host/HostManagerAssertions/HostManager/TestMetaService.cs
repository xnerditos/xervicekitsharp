using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services.MessageBroker;

namespace UnitTests.Host.HostManagerAssertions.HostManager {

    public interface ITestMetaService : IMetaService { }

    public class TestMetaService : ITestMetaService, IServiceBase, IServiceOperationOwner {

        public AvailabilityEnum Availability { get; set; } = AvailabilityEnum.Serving9;
        public HealthEnum Health { get; set; } = HealthEnum.Healthy;

        // =====================================================================
        // IMetaService
        // =====================================================================

        public string CapabilityKeyName { get; set; } = "Capability";

        public string InstanceId { get; set; } = XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier();

        public IReadOnlyDescriptor Descriptor { get; set; } = new Descriptor {
            Collection = MetaServiceConstants.CollectionName,
            Name = nameof(TestMetaService),
            Version = 1,
            UpdateLevel = 0,
            PatchLevel = 0
        };

        public RunStateEnum ServiceState { get; set; } = RunStateEnum.Active;

        public string ServiceAddress { get; set; } = $"100.100.100.100";

        public ITestOperation CreateOperation(ServiceOperationContext context) => new TestOperation();

        public ServiceInstanceStatus GetServiceStatus() => new ServiceInstanceStatus {
            InstanceId = this.InstanceId,
            RunState = this.ServiceState,
            Availability = this.Availability,
            Health = this.Health
        };

        public bool CanStartNewOperation() => this.ServiceState == RunStateEnum.Active;

        public void StartService(IRuntimeMonitor monitor) => this.ServiceState = RunStateEnum.Active;

        public void StopService(IRuntimeMonitor monitor) => this.ServiceState = RunStateEnum.Inactive;

        public bool HasFeature(string featureName) => false;

        public void SignalEnvironmentChange(IRuntimeMonitor monitor) { }

        public System.Type ApiInterfaceType => typeof(ITestOperation);
        public System.Type ServiceInterfaceType => typeof(ITestMetaService);
        string IServiceBase.ConfigurationDocumentIdentifier => "TestMetaService";

        public IReadOnlyServiceCallPolicy CallPolicy { get; set; } = new ServiceCallPolicy {
            TimeoutMs = 5000, CallPattern = ServiceCallPatternEnum.FirstChance
        };

        public IEnumerable<IReadOnlyDescriptor> Dependencies { get; set; } = new IReadOnlyDescriptor[0];
        public IEnumerable<IReadOnlySubscription> EventSubscriptions =>
            throw new System.NotImplementedException();
        public IEnumerable<IReadOnlySubscription> CommandSubscriptions =>
            throw new System.NotImplementedException();

        IReadOnlyDescriptor IServiceBase.Descriptor =>
            throw new NotImplementedException();

        IReadOnlyServiceCallPolicy IServiceBase.CallPolicy =>
            throw new NotImplementedException();

        IEnumerable<IReadOnlyDescriptor> IServiceBase.Dependencies =>
            throw new NotImplementedException();

        IEnumerable<IReadOnlySubscription> IServiceBase.EventSubscriptions =>
            throw new NotImplementedException();

        IEnumerable<IReadOnlySubscription> IServiceBase.CommandSubscriptions =>
            throw new NotImplementedException();

        string IServiceBase.InstanceId =>
            throw new NotImplementedException();

        RunStateEnum IServiceBase.ServiceState =>
            throw new NotImplementedException();

        public MethodInfo GetServiceCallTargetMethodInfo(string methodName) => typeof(TestOperation).GetMethod(methodName);

        Task<ServiceCallResult> IServiceBase.ExecuteCall(
            ServiceCallRequest request
        ) {
            throw new NotImplementedException();
        }

        public Func<object, object[], object> GetInvoker(Type onType, string name) {
            throw new NotImplementedException();
        }

        ServiceInstanceStatus IServiceBase.GetServiceStatus() {
            throw new NotImplementedException();
        }

        bool IServiceBase.CanStartNewOperation() {
            throw new NotImplementedException();
        }

        bool IServiceBase.HasFeature(string featureName) {
            throw new NotImplementedException();
        }

        void IServiceBase.SignalEnvironmentChange(IRuntimeMonitor monitor) {
            throw new NotImplementedException();
        }

        IServiceDaemon IServiceBase.GetDaemon(string daemonName) {
            throw new NotImplementedException();
        }

        IServiceDaemon[] IServiceBase.GetDaemons() {
            throw new NotImplementedException();
        }
    }
}