using System;
using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.MetaServices;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host.Management;
using XKit.Lib.Host.MetaServices.HostManagement;
using XKit.Lib.Host.MetaServices.LogManagement;
using XKit.Lib.Host.Config;
using XKit.Lib.Host.MetaServices.ConfigManagement;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Config;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Fabric;
using System.IO;
using static System.Environment;
using XKit.Lib.Host.MetaServices.RegistrationsManagement;
using XKit.Lib.Connector.Fabric;
using XKit.Lib.Common.Utility;
using XKit.Lib.Common.Utility.Threading;
using XKit.Lib.Common;
using XKit.Lib.Common.Client;
using System.Threading.Tasks;
using XKit.Lib.Connector.Protocols.Direct;
using XKit.Lib.Connector.Protocols.Http;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Host {

    public static class HostEnvironmentHelper {
        
        private static Lazy<IInprocessGlobalObjectRepository> igorLazy = new Lazy<IInprocessGlobalObjectRepository>(() => InprocessGlobalObjectRepositoryFactory.CreateSingleton());
        private static IHostManager hostManager;
        private static IDependencyConnector dependencyConnector;
        private static ILogManager logManager;
        public static IInprocessGlobalObjectRepository InjectableGlobalObjectRepository => igorLazy.Value;
        public static IHostManager Host => hostManager; 
        public static IDependencyConnector DependencyConnector => dependencyConnector;
        public static ILogManager LogManager => logManager;

        public static IHostManager CreateInitHost(
            IList<IInstanceClientFactory> instanceClientFactories = null,
            ILogManagerFactory logManagerFactory = null,
            ILocalConfigSessionFactory localConfigSessionFactory = null,
            string hostAddress = null, 
            string logPath = null,
            string localMetadataDbPath = null,
            string localDataStorageFolderPath = null,
            string localConfigFolderPath = null,
            Func<HealthEnum> healthChecker = null,
            string[] capabilitiesToRegister = null,
            bool flushLogsImmediately = false,
            Action<IReadOnlyLogJobEntry, IEnumerable<IReadOnlyLogEntryData>, bool> logJobMonitor = null,
            Action<IReadOnlyLogJobEntry, IReadOnlyLogEventEntry, IEnumerable<IReadOnlyLogEntryData>> logEventMonitor = null,
            Action<Exception> panicHandler = null
        ) {
            instanceClientFactories = instanceClientFactories ?? new[] { 
                DirectLocalClientFactory.Factory,
                HttpClientFactory.Factory
            };

            logManagerFactory = logManagerFactory ?? XKit.Lib.LocalLog.LogManagerFactory.Factory;

            localDataStorageFolderPath = EnsureLocalDataStoragePath(localDataStorageFolderPath);
            logPath = EnsurePath(
                path: logPath,
                defaultParentFolderPath: localDataStorageFolderPath,
                defaultName: "_log.db",
                environmentVariable: EnvironmentHelperConstants.EnvironmentVariables.LogDbPath,
                isFolder: false
            );

            Action<Exception> defaultPanicHandler = (ex) => DefaultPanicHandler(ex, logPath);

            var logManager = logManagerFactory.Create(
                logPath,
                logJobMonitor ?? WriteJobToConsole,
                logEventMonitor ?? WriteEntryToConsole,
                panicHandler ?? defaultPanicHandler
            );
            InjectableGlobalObjectRepository.RegisterSingleton(
                logManagerFactory, 
                typeof(ILogManagerFactory)
            );

            return CreateInitHost(
                instanceClientFactories,
                logManager,
                localConfigSessionFactory,
                hostAddress,
                localMetadataDbPath,
                localDataStorageFolderPath,
                localConfigFolderPath,
                healthChecker,
                capabilitiesToRegister
            );
        }

        public static IHostManager CreateInitHost(
            IList<IInstanceClientFactory> instanceClientFactories,
            ILogManager logManager,
            ILocalConfigSessionFactory localConfigSessionFactory = null,
            string hostAddress = null, 
            string localMetadataDbPath = null,
            string localDataStorageFolderPath = null,
            string localConfigFolderPath = null,
            Func<HealthEnum> healthChecker = null,
            string[] capabilitiesToRegister = null
        ) {
            if (InjectableGlobalObjectRepository.HasObject(typeof(IHostManager))) {
                throw new Exception("Host already created");
            }
            if (logManager == null) {
                throw new ArgumentException("Cannot be null", nameof(logManager));
            }
            if (string.IsNullOrEmpty(hostAddress)) {
                hostAddress = Environment.GetEnvironmentVariable(EnvironmentHelperConstants.EnvironmentVariables.HostBaseAddress);
                if (string.IsNullOrEmpty(hostAddress)) {
                    throw new ArgumentException($"hostAddress must be passed or else {EnvironmentHelperConstants.EnvironmentVariables.HostBaseAddress} env variable must be defined.");
                }
            }

            HostEnvironmentHelper.logManager = logManager;
            
            localDataStorageFolderPath = EnsureLocalDataStoragePath(localDataStorageFolderPath);
            localMetadataDbPath = EnsurePath(
                path: localMetadataDbPath,
                defaultParentFolderPath: localDataStorageFolderPath,
                defaultName: "_metadata.db",
                environmentVariable: EnvironmentHelperConstants.EnvironmentVariables.MetaDbPath,
                isFolder: false
            );
            localConfigFolderPath = EnsurePath(
                path: localConfigFolderPath,
                defaultParentFolderPath: localDataStorageFolderPath,
                defaultName: "config",
                environmentVariable: EnvironmentHelperConstants.EnvironmentVariables.ConfigFolderPath,
                isFolder: true
            );

            if (localConfigSessionFactory == null) {
                localConfigSessionFactory = LocalConfigSessionFactory.Factory;
            }
            localConfigSessionFactory.SetPath(
                localConfigFolderPath
            );

            var fabricConnector = FabricConnectorFactory.Create(instanceClientFactories);
            HostEnvironmentHelper.dependencyConnector = fabricConnector;

            HostManagerFactory.SetHealthChecker(healthChecker);

            var hostManager = HostManagerFactory.Create(
                hostAddress,
                localMetadataDbPath,
                localDataStorageFolderPath,
                logManager,
                localConfigSessionFactory,
                fabricConnector
            );

            fabricConnector.Initialize();

            InjectableGlobalObjectRepository.RegisterSingleton(
                logManager, 
                typeof(ILogManager)
            );            
            InjectableGlobalObjectRepository.RegisterSingleton(
                fabricConnector,
                typeof(IFabricConnector),
                typeof(IDependencyConnector)
            );
            InjectableGlobalObjectRepository.RegisterSingleton(
                hostManager, 
                typeof(IHostManager),
                typeof(ILocalHostEnvironment),
                typeof(ILocalFabricEnvironment)
            );
            
            bool doAll = capabilitiesToRegister == null;

            if (doAll || capabilitiesToRegister.Contains(StandardCapabilityNames.LocalHostManagement)) {
                hostManager.AddMetaService(
                    HostManagementMetaServiceFactory.Create(hostManager, fabricConnector)
                );
            }

            if (doAll || capabilitiesToRegister.Contains(StandardCapabilityNames.LocalLogManagement)) {
                hostManager.AddMetaService(
                    LogManagementMetaServiceFactory.Create(hostManager, fabricConnector)
                );
            }

            if (doAll || capabilitiesToRegister.Contains(StandardCapabilityNames.LocalConfigManagement)) {
                hostManager.AddMetaService(
                    ConfigManagementMetaServiceFactory.Create(hostManager, fabricConnector)
                );
            }

            if (doAll || capabilitiesToRegister.Contains(StandardCapabilityNames.LocalRegistrationsManagement)) {
                hostManager.AddMetaService(
                    RegistrationsManagementMetaServiceFactory.Create(hostManager, fabricConnector)
                );
            }

            HostEnvironmentHelper.hostManager = hostManager;
            return hostManager;
        }

        public static void StartHost(
            IEnumerable<string> initialRegistryAddresses = null,
            IDictionary<string, object> startupParameters = null, 
            bool failIfCannotRegister = false,
            bool executeLogStartupMaintenance = true
        ) {
            if (hostManager != null) {

                if (initialRegistryAddresses == null) {
                    string registryAddresses = Environment.GetEnvironmentVariable(EnvironmentHelperConstants.EnvironmentVariables.InitialRegistryAddresses);
                    initialRegistryAddresses = registryAddresses?.Split(';', StringSplitOptions.RemoveEmptyEntries);
                }

                if (executeLogStartupMaintenance) {
                    RunWithDefaultMonitor(
                        (monitor, logSession) => {
                            if (logSession != null) {
                                return logSession.ExecuteStartupMaintenance();
                            }
                            return Task.CompletedTask;
                        });
                }
                RunWithDefaultMonitor(
                    (monitor) => hostManager.StartHost(
                        initialRegistryAddresses ?? new string[0],
                        monitor,
                        startupParameters,
                        failIfCannotRegister
                        ));
            }
        }        

        public static void PauseHost() {
            if (hostManager != null) {
                RunWithDefaultMonitor((monitor) => hostManager.PauseHost(monitor));
            }
        }        

        public static void ResumeHost() {
            if (hostManager != null) {
                RunWithDefaultMonitor((monitor) => hostManager.ResumeHost(monitor));
            }
        }        

        public static void StopAndDestroyHost() {
            if (hostManager != null) {
                RunWithDefaultMonitor((monitor) => hostManager.StopHost(monitor));
                InjectableGlobalObjectRepository.Clear();
                logManager.Finish();
                hostManager = null;
                logManager = null;
                dependencyConnector = null;
            }
        }        

        // =====================================================================
        // private 
        // =====================================================================

        private static void RunWithDefaultMonitor(Action<IRuntimeMonitor> action) 
            => RunWithDefaultMonitor((monitor, logSession) => {
                action(monitor);
                return Task.CompletedTask;
            });

        private static void RunWithDefaultMonitor(Action<IRuntimeMonitor, ILogSession> action) 
            => RunWithDefaultMonitor((monitor, logSession) => {
                action(monitor, logSession);
                return Task.CompletedTask;
            });

        private static void RunWithDefaultMonitor(Func<IRuntimeMonitor, Task> action) 
            => RunWithDefaultMonitor((monitor, logSession) => action(monitor));
        
        private static void RunWithDefaultMonitor(Func<IRuntimeMonitor, ILogSession, Task> action) {

            TaskUtil.RunSyncSafely(async () => {

                if (logManager == null) { 
                    await action(RuntimeMonitorFactory.Create(null), null);
                } else {
                    using var logSession = logManager.CreateWriteableSession(
                        JobTypeEnum.HostAction,
                        JobSystemEffect.Transient,
                        Identifiers.NameOrigintatorAsHost,
                        hostManager.VersionLevel.GetValueOrDefault(),
                        null,
                        hostManager.FabricId
                    );

                    await logSession.BeginLog();
                    var monitor = RuntimeMonitorFactory.Create(logSession.GetLogWriter());
                    await action(monitor, logSession);
                    await logSession.EndLog();
                }
            });
        }
        
        private static string EnsureLocalDataStoragePath(string localDataStorageFolderPath) {
            return EnsurePath(
                localDataStorageFolderPath,
                Environment.GetFolderPath(SpecialFolder.ApplicationData),
                "xkit-host",
                EnvironmentHelperConstants.EnvironmentVariables.LocalDataFolderPath,
                isFolder: true
            );
        }

        private static string EnsurePath(
            string path, 
            string defaultParentFolderPath, 
            string defaultName, 
            string environmentVariable,
            bool isFolder
        ) {
            if (string.IsNullOrEmpty(path)) {
                path = System.Environment.GetEnvironmentVariable(environmentVariable);
                if (string.IsNullOrEmpty(path)) {
                    path = $"{defaultParentFolderPath}/{defaultName}";
                }
            }
            if (isFolder) {
                if (!Directory.Exists(path)) {
                    Directory.CreateDirectory(path);
                }
            }
            return path;
        }

        private static void WriteEntryToConsole(
            IReadOnlyLogJobEntry job, 
            IReadOnlyLogEventEntry entry,
            IEnumerable<IReadOnlyLogEntryData> data
        ) {
            lock (logManager) {
                Console.Write($"<> {entry.Timestamp.ToString("yyyy/MM/dd HH:mm:ss:FFF")} LOG:{entry.EventTypeName} |jobid={entry.LogJobEntryId} |id={entry.LogEventEntryId}");
                if (entry.Code != null) { Console.Write($" |code={entry.Code}"); }
                if (entry.Message != null) { Console.Write($" |message={entry.Message}"); }
                entry.Attributes?.ForEach(a => {
                    if (a != null) {
                        var s = a.Value == null ? "" : a.Value.ToString();
                        var len = s.Length < 128 ? s.Length : 128;
                        Console.Write($" |{a.Name}={s.Substring(0, len)}");
                    }
                });
                if (job.CorrelationId != null) { Console.Write(" |correlationid: " + job.CorrelationId); }
                if (job.CorrelationTags != null && job.CorrelationTags.Count() > 0) { Console.Write(" |correlationtags: " + string.Join(',', job.CorrelationTags.Select(t => t.Name))); }
                Console.WriteLine();
            }
        }

        private static void WriteJobToConsole(
            IReadOnlyLogJobEntry job,
            IEnumerable<IReadOnlyLogEntryData> data, 
            bool isEntry
        ) {
            lock(logManager) {
                var timestamp = isEntry ? job.StartTimestamp : job.CompleteTimestamp;
                var phase = isEntry ? "START" : "END";
                Console.Write($"<> {timestamp?.ToString("yyyy/MM/dd HH:mm:ss:FFF")} {phase}:{job.JobType}:{job.JobName} jobid={job.LogJobEntryId}");
                Console.Write($" |originator={job.OriginatorName}-v{job.OriginatorVersion}");
                if (job.CorrelationId != null) { Console.Write(" |correlationid: " + job.CorrelationId); }
                if (job.CorrelationTags != null && job.CorrelationTags.Count() > 0) { Console.Write(" |correlationtags: " + string.Join(',', job.CorrelationTags.Select(t => t.Name))); }
                if (job.ResultCode != null) { Console.Write(" |resultcode: " + job.ResultCode); }
                if (!isEntry) { 
                    Console.Write(" |status: " + job.Status); 
                    if (job.JobNote != null) {
                        Console.Write(" |note: " + job.JobNote);
                    }
                }
                Console.WriteLine();
            }
        }

        private static void DefaultPanicHandler(Exception ex, string logPath) {
            try {
                var msg = $"<> {DateTime.UtcNow} UTC: PANIC!!!! ==> {ex.Message}\r\n{ex.Source}\r\n{ex.StackTrace}\r\n-------------------------------\r\n";
                Console.Write(msg);
                File.AppendAllText(logPath + $".PANIC.{DateTime.Now}.txt", msg);
            } catch {
                // swallow the sucker
            }
        }
    }
}