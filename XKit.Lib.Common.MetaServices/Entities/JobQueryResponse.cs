
using System;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Common.MetaServices.Entities {
    public class JobQueryResponse {

        public class JobQueryItem {

            public Guid JobId { get; set; }
            public DateTime StartTimestamp { get; set; }
            public JobTypeEnum JobType { get; set; }
            public string JobName { get; set; }
            public JobResultStatusEnum Status { get; set; }
            // FUTURE:
            // public object JobInput { get; set; }
            // public object JobResult { get; set; }
        }

        public JobQueryItem[] Items { get; set; } 
    }
}