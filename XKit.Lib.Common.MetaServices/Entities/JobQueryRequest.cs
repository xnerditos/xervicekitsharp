using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.MetaServices.Entities {
    public class JobQueryRequest {
        public string CorrelationId { get; set; }
        public string CorrelationTag { get; set; }
        public List<Guid> JobIds { get; set; }
        // public bool? WantInput { get; set; }
        // public bool? WantResult { get; set; }
    }
}