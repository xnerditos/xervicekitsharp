
namespace XKit.Lib.Common.MetaServices.Entities {
    public class ConfigMetaServiceUpdateRequest {
        public string DocumentIdentifier { get; set; }
        public object ConfigInfo { get; set; }
    }
}