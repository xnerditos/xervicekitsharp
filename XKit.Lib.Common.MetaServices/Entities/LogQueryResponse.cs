using System.Collections.Generic;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Common.MetaServices.Entities {
    public class LogQueryResponse {

        public List<IReadOnlyLogJobEntry> Jobs { get; set; }
        public List<IReadOnlyLogEventEntry> Events { get; set; }
        public List<IReadOnlyLogEntryData> EventData { get; set; }

        public int JobCount { get; set; }
        public int EventCount { get; set; }
        public int DataCount { get; set; }
    }
}