
namespace XKit.Lib.Common.MetaServices.Entities {
    public class ConfigMetaServiceRetrieveRequest {
        public string DocumentIdentifier { get; set; }
    }
}