using System;
using System.Collections.Generic;

namespace XKit.Lib.Common.MetaServices.Entities {
    public class LogQueryRequest {
        
        public List<Guid> JobIds { get; set; }
        public List<Guid> EventIds { get; set; }
        public DateTime? EventOnOrBefore { get; set; }
        public DateTime? EventOnOrAfter { get; set; }
        public DateTime? JobStartOnOrBefore { get; set; }
        public DateTime? JobStartOnOrAfter { get; set; }
        public DateTime? JobCompleteOnOrBefore { get; set; }
        public DateTime? JobCompletetOnOrAfter { get; set; }
        public string EventType { get; set; }
        public string HasTag { get; set; }
        public bool? WantJobs { get; set; }
        public bool? WantEvents { get; set; }
        public bool? WantData { get; set; }
        public int? PageSize { get; set; }
        public int? PageIndex { get; set; }

        // FUTURE:
        // public string OriginatorName { get; set; }
        // public int? OriginatorVersion { get; set; }
        // public string OperationName { get; set; }
        // public DateTime? StartOnOrBefore { get; set; }
        // public DateTime? StartOnOrAfter { get; set; }
        // public DateTime? CompleteOnOrBefore { get; set; }
        // public DateTime? CompleteOnOrAfter { get; set; }
        // public bool? IsComplete { get; set; }
        // public JobTypeEnum JobType { get; set; }
        // public string CorrelationId { get; set; }
        // public string CorrelationTag { get; set; }
        // public string RequestorHostId { get; set; }
        // public string RequestorInstanceId { get; set; }
         
    }
}