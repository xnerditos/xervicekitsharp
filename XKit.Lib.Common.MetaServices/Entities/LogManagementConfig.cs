namespace XKit.Lib.Common.MetaServices.Entities {
    
    public class LogManagementConfig {
        public class HousekeepingDaemonType { 
            public bool? EnableArchiving { get; set; } = true;
            public int? CheckEveryXSeconds { get; set; } = 3600 * 4;     // 4 hours
            public int? BatchLimitSize { get; set; } = 100;
            public int? WaitBetweenBatchesMilliseconds { get; set; } = 1000;
            public int? WaitBetweenBatchStepsMilliseconds { get; set; } = 10;
            public int? ArchiveJobsOlderThanXMinutes { get; set; } = (60 * 24 * 7) + (12 * 60); // one week + 12 hours
            public string ArchiveDbPath { get; set; }
        }

        public HousekeepingDaemonType HousekeepingDaemon { get; set; } = new HousekeepingDaemonType();
    }
}