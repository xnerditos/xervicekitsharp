namespace XKit.Lib.Common.MetaServices.Entities {
    
    public class HostMetaServiceControlRequest {

        public string RequestorName { get; set; }
        public string RequestorId { get; set; }
        public string Reason { get; set; }
    }
}