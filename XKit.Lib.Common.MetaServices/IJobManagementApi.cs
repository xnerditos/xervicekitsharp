using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Fabric;

namespace XKit.Lib.Common.MetaServices {

    public interface IJobManagementApi : IServiceApi {
        Task<ServiceCallResult<JobQueryResponse>> Query(JobQueryRequest request);
    }
}