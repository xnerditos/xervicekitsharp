using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Fabric;
using ServiceConstants = XKit.Lib.Common.Services.StandardConstants;

namespace XKit.Lib.Common.MetaServices {

    public interface IHostManagementApi : IServiceApi {
        Task<ServiceCallResult> Pause(HostMetaServiceControlRequest request);
        Task<ServiceCallResult> Resume(HostMetaServiceControlRequest request);
        Task<ServiceCallResult> Stop(HostMetaServiceControlRequest request);
        Task<ServiceCallResult> Kill(HostMetaServiceControlRequest request);
    }
}