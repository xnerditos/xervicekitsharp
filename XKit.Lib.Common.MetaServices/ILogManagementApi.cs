using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Host;

namespace XKit.Lib.Common.MetaServices {

    public interface ILogManagementApi : IServiceApi {
        Task<ServiceCallResult<LogQueryResponse>> Query(LogQueryRequest request);
    }
}