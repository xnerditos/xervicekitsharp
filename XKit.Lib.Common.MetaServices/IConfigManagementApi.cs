using System.Threading.Tasks;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.MetaServices.Entities;
using XKit.Lib.Common.Fabric;

namespace XKit.Lib.Common.MetaServices {

    public interface IConfigManagementApi : IServiceApi {
        Task<ServiceCallResult<object>> Retrieve(ConfigMetaServiceRetrieveRequest request);
        Task<ServiceCallResult> Update(ConfigMetaServiceUpdateRequest request);
    }
}