using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Connector.Service {

    /// <summary>
    /// This class provides the base for service clients.  A service client presents
    /// an interface that routes to a service operation, abstracting all the logic
    /// necessary to do the call.  The client is really a proxy.  It prepares the call, 
    /// and the actually routing of the call is handled by an ICallRouter
    /// </summary>

    public class ServiceClientBase : IDisposable {

        private static readonly string[] BlankCorrelationTags = new string[0];
        
        public enum LogCodes {
            ServiceCallFailed,
            OperationFailed
        }

        // =====================================================================
        // private
        // =====================================================================
        private IReadOnlyDescriptor Descriptor { get; }
        private IDependencyConnector DependencyConnector => ClientParameters.DependencyConnector;
        private readonly ServiceCallTypeParameters defaultCallTypeParameters;
        private readonly ServiceClientErrorHandling DefaultErrorHandling;
        private ServiceCallTypeParameters currentCallTypeParameters;
        private string currentCorrelationId;
        private IReadOnlyList<string> currentCorrelationTags;
        private IRuntimeMonitor currentMonitor; 
        private ServiceClientErrorHandling? currentErrorHandling;
        private ServiceCallPolicy currentPolicy;

        private readonly string TargetHostId;
        private readonly SemaphoreSlim callSynchronizer = new SemaphoreSlim(1, 1);
        private readonly string callInterfaceName;
        
        // =====================================================================
        // construction
        // =====================================================================

        public ServiceClientBase(
            IReadOnlyDescriptor descriptor,
            string callInterfaceName,
            ServiceClientParameters clientParameters = null,
            ServiceCallTypeParameters defaultCallTypeParameters = null,
            ServiceClientErrorHandling errorHandling = ServiceClientErrorHandling.LogWarning,
            string targetHostId = null
        ) {
            this.Descriptor = descriptor;
            this.callInterfaceName = callInterfaceName;
            this.DefaultErrorHandling = errorHandling;
            IInprocessGlobalObjectRepository igor = null;
            if (clientParameters?.RequestorFabricId == null || clientParameters?.DependencyConnector == null) {
                igor = InprocessGlobalObjectRepositoryFactory.CreateSingleton();
            }
            this.ClientParameters = ServiceClientParameters.CreateForConsumer(
                requestorFabricId: 
                    clientParameters?.RequestorFabricId ??
                    (igor.HasObject(typeof(ILocalFabricEnvironment)) ? igor.GetObject<ILocalFabricEnvironment>().FabricId : ""),
                defaultCorrelationId: 
                    clientParameters?.DefaultCorrelationId ??
                    XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                monitor: 
                    clientParameters?.Monitor ??
                    RuntimeMonitorFactory.Create(),
                commonCorrelationTags: 
                    clientParameters?.CommonCorrelationTags ??
                    (IReadOnlyList<string>)new string[0],
                dependencyConnector: 
                    clientParameters?.DependencyConnector ??
                    InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>()
            );
            this.defaultCallTypeParameters = defaultCallTypeParameters ?? ServiceCallTypeParameters.SyncResult();
            this.TargetHostId = targetHostId;
        }

        // =====================================================================
        // protected
        // =====================================================================

        protected virtual IReadOnlyDescriptor ServiceDescriptor => this.Descriptor; 
        protected ServiceClientParameters ClientParameters { get; }
        protected ServiceClientErrorHandling ErrorHandling => currentErrorHandling ?? DefaultErrorHandling; 
        protected IRuntimeMonitor Monitor => currentMonitor ?? ClientParameters.Monitor;
        protected string CorrelationId => currentCorrelationId ?? ClientParameters.DefaultCorrelationId;
        protected IReadOnlyList<string> CorrelationTags => currentCorrelationTags ?? BlankCorrelationTags;
        protected ServiceCallTypeParameters CallTypeParameters => currentCallTypeParameters ?? defaultCallTypeParameters;
        protected ServiceCallPolicy Policy => currentPolicy;

        protected async Task<IServiceCallRouter> BeginCall(
            ServiceCallTypeParameters callTypeParameters,
            IRuntimeMonitor monitor = null,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            ServiceClientErrorHandling? errorHandling = null,
            IServiceCallRouter callRouter = null,
            ServiceCallPolicy policy = null
        ) {
            await this.callSynchronizer.WaitAsync();

            this.currentCallTypeParameters = callTypeParameters;
            this.currentCorrelationId = correlationId;
            this.currentCorrelationTags = correlationTags;
            this.currentErrorHandling = errorHandling;
            this.currentMonitor = monitor;
            this.currentPolicy = policy;

            if (callRouter != null) {
                return callRouter;
            }
            return await DependencyConnector.CreateCallRouter(
                Descriptor,
                Monitor,
                CorrelationId,
                CorrelationTags,
                ErrorHandling == ServiceClientErrorHandling.ThrowException, 
                allowRegistryRefreshIfRequested: true
            );
        }

        protected async Task<IReadOnlyList<ServiceCallResult>> EndCall(
            Task<IReadOnlyList<ServiceCallResult>> resultTask
        ) {

            ServiceClientErrorHandling errorHandling = ErrorHandling;
            var monitor = Monitor;

            this.currentCallTypeParameters = null;
            this.currentCorrelationId = null;
            this.currentCorrelationTags = null;
            this.currentErrorHandling = null;
            this.currentMonitor = null;
            this.currentPolicy = null;

            callSynchronizer.Release();

            var resultList = await resultTask;

            if (!resultList.Any(r => !r.HasError)) {
                var firstResult = resultList.First();

                var attributes = new {
                    Target = Descriptor,
                    Operation = firstResult.OperationName,
                    Status = firstResult.ServiceCallStatus
                };
                var code = !firstResult.Completed ? LogCodes.ServiceCallFailed : LogCodes.OperationFailed;
                switch(errorHandling) {
                case ServiceClientErrorHandling.DoNothing:
                    break;
                case ServiceClientErrorHandling.LogInfo:
                    monitor.Info(
                        firstResult.Message,
                        attributes: attributes,
                        code : code
                    );
                    break;
                case ServiceClientErrorHandling.LogWarning:
                    monitor.Warning(
                        firstResult.Message,
                        attributes: attributes,
                        code : code
                    );
                    break;
                case ServiceClientErrorHandling.LogErrorRecoverable:
                case ServiceClientErrorHandling.LogErrorNonRecoverable:
                case ServiceClientErrorHandling.ThrowException:
                    monitor.Error(
                        firstResult.Message,
                        attributes: attributes,
                        code : code,
                        recoverable: errorHandling == ServiceClientErrorHandling.LogErrorRecoverable
                    );
                    if (errorHandling == ServiceClientErrorHandling.ThrowException) {
                        throw new Exception("Service call failed: " + firstResult.Message);
                    }
                    break;
                }

                return resultList;
            }

            return 
                resultList
                .OrderBy(r => !r.HasError)
                .ThenBy(r => r.Timestamp)
                .ToArray();
        }

        // protected async Task<IReadOnlyList<ServiceCallResult<T>>> EndCallWithResultBody<T>(
        //     ServiceCallResult<T> result
        // ) where T : class => (await EndCall(Task.FromResult((IReadOnlyList<ServiceCallResult<T>>) new[] { result }))).Select(r => r.ConvertTo<T>()).ToArray();

        // --------------------------------------------------------------------------

        protected async Task<ServiceCallResult<TResponseBody>> ExecuteCall<TRequestBody, TResponseBody>(
            TRequestBody requestBody,
            ServiceCallTypeParameters callTypeParameters = null,
            [CallerMemberName] string callerMemberNameAsOperationName = ""
        ) where TRequestBody : class where TResponseBody : class 
            => (await ExecuteCallEx<TRequestBody, TResponseBody>(
                callerMemberNameAsOperationName,
                requestBody,
                callTypeParameters
            )).First();

        protected async Task<ServiceCallResult<TResponseBody>> ExecuteCall<TResponseBody>(
            ServiceCallTypeParameters callTypeParameters = null,
            [CallerMemberName] string callerMemberNameAsOperationName = ""
        ) where TResponseBody : class 
            => (await ExecuteCallEx<TResponseBody>(
                callerMemberNameAsOperationName,
                callTypeParameters
            )).First();

        protected async Task<ServiceCallResult> ExecuteCall<TRequestBody>(
            TRequestBody requestBody,
            ServiceCallTypeParameters callTypeParameters = null,
            [CallerMemberName] string callerMemberNameAsOperationName = ""
        ) where TRequestBody : class  
            => (await ExecuteCallEx<TRequestBody>(
                callerMemberNameAsOperationName,
                requestBody,
                callTypeParameters
            )).First();

        protected async Task<ServiceCallResult> ExecuteCall(
            ServiceCallTypeParameters callTypeParameters = null,
            [CallerMemberName] string callerMemberNameAsOperationName = ""
        ) => (await ExecuteCallEx(
                callerMemberNameAsOperationName,
                callTypeParameters
            )).First();

        // --------------------------------------------------------------------------

        protected async Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> ExecuteCallEx<TRequestBody, TResponseBody>(
            string operationName,
            TRequestBody requestBody,
            ServiceCallTypeParameters callTypeParameters,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            IRuntimeMonitor monitor = null,
            IServiceCallRouter callRouter = null,
            ServiceClientErrorHandling? errorHandling = null,
            ServiceCallPolicy policy = null,
            string requestJsonPayload = null
        ) where TRequestBody : class where TResponseBody : class {

            var router = await BeginCall(
                callTypeParameters,
                monitor,
                correlationId,
                correlationTags,
                errorHandling,
                callRouter
            );

            if (router == null) {
                return (await EndCall(
                    Task.FromResult(
                        (IReadOnlyList<ServiceCallResult>) new[] { 
                            HydrateResult(
                                new ServiceCallResult(),
                                ServiceCallStatusEnum.NotFound,
                                null,
                                JobResultStatusEnum.Incomplete
                            )
                        }
                    )
                )).Select(r => r.ConvertTo<TResponseBody>()).ToArray();
            }

            var request = CreateRequest(
                operationName: operationName,
                requestBody: requestBody, 
                correlationId: CorrelationId,
                correlationTags: CorrelationTags,
                jsonPayload: requestJsonPayload
            );
            return (await EndCall(
                router.ExecuteCall(request, Monitor, Policy, TargetHostId)
            )).Select(r => r.ConvertTo<TResponseBody>()).ToArray();
        }

        protected async Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> ExecuteCallEx<TResponseBody>(
            string operationName,
            ServiceCallTypeParameters callTypeParameters,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            IRuntimeMonitor monitor = null,
            IServiceCallRouter callRouter = null,
            ServiceClientErrorHandling? errorHandling = null,
            ServiceCallPolicy policy = null,
            string requestJsonPayload = null
        ) where TResponseBody : class {

            var router = await BeginCall(
                callTypeParameters,
                monitor,
                correlationId,
                correlationTags,
                errorHandling,
                callRouter
            );

            if (router == null) {
                return (await EndCall(
                    Task.FromResult(
                        (IReadOnlyList<ServiceCallResult>) new[] { 
                            HydrateResult(
                                new ServiceCallResult(),
                                ServiceCallStatusEnum.NotFound,
                                null,
                                JobResultStatusEnum.Incomplete
                            )
                        }
                    )
                )).Select(r => r.ConvertTo<TResponseBody>()).ToArray();
            }

            var request = CreateRequest(
                operationName: operationName, 
                jsonPayload: requestJsonPayload,
                correlationId: correlationId,
                correlationTags: correlationTags
            );
            return (await EndCall(
                router.ExecuteCall(request, Monitor, Policy, TargetHostId)
            )).Select(r => r.ConvertTo<TResponseBody>()).ToArray();
        }

        protected async Task<IReadOnlyList<ServiceCallResult>> ExecuteCallEx<TRequestBody>(
            string operationName,
            TRequestBody requestBody,
            ServiceCallTypeParameters callTypeParameters,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            IRuntimeMonitor monitor = null,
            IServiceCallRouter callRouter = null,
            ServiceClientErrorHandling? errorHandling = null,
            ServiceCallPolicy policy = null,
            string requestJsonPayload = null
        ) where TRequestBody : class {

            var router = await BeginCall(
                callTypeParameters,
                monitor,
                correlationId,
                correlationTags,
                errorHandling,
                callRouter
            );

            if (router == null) {
                return await EndCall(
                    Task.FromResult(
                        (IReadOnlyList<ServiceCallResult>)new[] { 
                            HydrateResult(
                                new ServiceCallResult(),
                                ServiceCallStatusEnum.NotFound,
                                null,
                                JobResultStatusEnum.Incomplete
                            )
                        }
                    )
                );
            }

            var request = CreateRequest(
                operationName: operationName,
                requestBody: requestBody, 
                correlationId: CorrelationId,
                correlationTags: CorrelationTags,
                jsonPayload: requestJsonPayload
            );
            return await EndCall(
                router.ExecuteCall(request, Monitor, Policy, TargetHostId)
            );
        }

        protected async Task<IReadOnlyList<ServiceCallResult>> ExecuteCallEx(
            string operationName,
            ServiceCallTypeParameters callTypeParameters,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            IRuntimeMonitor monitor = null,
            IServiceCallRouter callRouter = null,
            ServiceClientErrorHandling? errorHandling = null,
            ServiceCallPolicy policy = null,
            string requestJsonPayload = null
        ) {

            var router = await BeginCall(
                callTypeParameters,
                monitor,
                correlationId,
                correlationTags,
                errorHandling,
                callRouter,
                policy
            );

            if (router == null) {
                return await EndCall(
                    Task.FromResult(
                        (IReadOnlyList<ServiceCallResult>)new[] { 
                            HydrateResult(
                                new ServiceCallResult(),
                                ServiceCallStatusEnum.NotFound,
                                null,
                                JobResultStatusEnum.Incomplete
                            )
                        }
                    )
                );
            }

            var request = CreateRequest(
                operationName: operationName, 
                jsonPayload: requestJsonPayload,
                correlationId: correlationId,
                correlationTags: correlationTags
            );
            return await EndCall(
                router.ExecuteCall(request, Monitor, Policy, TargetHostId)
            );
        }

        // =====================================================================
        // IDisposable
        // =====================================================================
        void IDisposable.Dispose() {
            DisposeAction();
        }

        // =====================================================================
        // IServiceClient
        // =====================================================================

        // =====================================================================
        // virtual
        // =====================================================================

        protected virtual void DisposeAction() { }

        // =====================================================================
        // private
        // =====================================================================

        private ServiceCallRequest<TRequestBody> CreateRequest<TRequestBody>(
            string operationName,
            TRequestBody requestBody,
            string jsonPayload,
            string correlationId,
            IEnumerable<string> correlationTags
        ) where TRequestBody : class => ServiceCallRequest<TRequestBody>.Create(
            clientParameters: ClientParameters, 
            callTypeParameters: CallTypeParameters, 
            payload: jsonPayload,
            correlationId: correlationId ?? ClientParameters.DefaultCorrelationId, 
            correlationTags: ClientParameters.CommonCorrelationTags.Union(correlationTags ?? new string[0]), 
            operationName: this.callInterfaceName == null || operationName.Contains('.') ? operationName : $"{this.callInterfaceName}.{operationName}", 
            requestBody: requestBody
        );

        private ServiceCallRequest CreateRequest(
            string operationName,
            string jsonPayload,
            string correlationId,
            IEnumerable<string> correlationTags
        ) => ServiceCallRequest.Create(
            clientParameters: ClientParameters, 
            payload: jsonPayload,
            callTypeParameters: CallTypeParameters, 
            correlationId: correlationId ?? ClientParameters.DefaultCorrelationId, 
            correlationTags: ClientParameters.CommonCorrelationTags.Union(correlationTags ?? new string[0]), 
            operationName: this.callInterfaceName == null || operationName.Contains('.') ? operationName : $"{this.callInterfaceName}.{operationName}" 
        );

        private ServiceCallResult HydrateResult(
            ServiceCallResult resultBase,
            ServiceCallStatusEnum callStatus,
            ServiceCallRequest request,
            JobResultStatusEnum operationStatus,
            string message = null
        ) {
            resultBase.ServiceCallStatus = callStatus;
            resultBase.CorrelationId = request?.CorrelationId;
            resultBase.CorrelationTags = request?.CorrelationTags?.Select(t => t).ToArray();
            resultBase.Message = message;
            resultBase.OperationName = request?.OperationName;
            resultBase.OperationStatus = operationStatus;
            resultBase.Code = resultBase?.Code;
            resultBase.RequestorFabricId = request?.RequestorFabricId;
            resultBase.RequestorInstanceId = request?.RequestorInstanceId;
            resultBase.Service = Descriptor.Clone();
            resultBase.ServiceStatus = null;
            resultBase.ResponderInstanceId = null;
            resultBase.ResponderFabricId = null;
            resultBase.OperationId = Guid.Empty;
            resultBase.Timestamp = DateTime.UtcNow;
            return resultBase;
        }
    }

    public class ServiceClientBase<TCallInterface> : ServiceClientBase, IServiceClient<TCallInterface>
        where TCallInterface : IServiceApi {

        // =====================================================================
        // construction
        // =====================================================================

        public ServiceClientBase(
            IReadOnlyDescriptor descriptor,
            ServiceClientParameters clientParameters = null,
            ServiceCallTypeParameters defaultCallTypeParameters = null,
            ServiceClientErrorHandling errorHandling = ServiceClientErrorHandling.LogWarning,
            string targetHostId = null
        ) : base(
            descriptor,
            typeof(TCallInterface).Name,
            clientParameters,
            defaultCallTypeParameters,
            errorHandling,
            targetHostId
        ) { }

        // --------------------------------------------------------------------------


        // =====================================================================
        // IServiceClient
        // =====================================================================

        Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> IServiceClient<TCallInterface>.ExecuteWith<TRequestBody, TResponseBody>(
            Expression<Func<TCallInterface, Task<ServiceCallResult<TResponseBody>>>> expression,
            TRequestBody requestBody, 
            ServiceCallTypeParameters callTypeParameters, 
            string correlationId, 
            IReadOnlyList<string> correlationTags, 
            IRuntimeMonitor monitor, 
            IServiceCallRouter callRouter,
            ServiceClientErrorHandling? errorHandling,
            string requestJsonPayload
        ) => ExecuteCallEx<TRequestBody, TResponseBody>(
                operationName: ((MethodCallExpression)expression.Body).Method.Name,
                requestBody: requestBody,
                callTypeParameters: callTypeParameters,
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor,
                callRouter: callRouter,
                errorHandling: errorHandling
            );

        Task<IReadOnlyList<ServiceCallResult>> IServiceClient<TCallInterface>.ExecuteWith<TRequestBody>(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression,
            TRequestBody requestBody, 
            ServiceCallTypeParameters callTypeParameters, 
            string correlationId, 
            IReadOnlyList<string> correlationTags, 
            IRuntimeMonitor monitor, 
            IServiceCallRouter callRouter,
            ServiceClientErrorHandling? errorHandling,
            string requestJsonPayload
        ) => ExecuteCallEx<TRequestBody>(
                operationName: ((MethodCallExpression)expression.Body).Method.Name,
                requestBody: requestBody,
                callTypeParameters: callTypeParameters,
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor,
                callRouter: callRouter,
                errorHandling: errorHandling
            );

        Task<IReadOnlyList<ServiceCallResult<TResponseBody>>> IServiceClient<TCallInterface>.ExecuteWith<TResponseBody>(
            Expression<Func<TCallInterface, Task<ServiceCallResult<TResponseBody>>>> expression,
            ServiceCallTypeParameters callTypeParameters, 
            string correlationId, 
            IReadOnlyList<string> correlationTags, 
            IRuntimeMonitor monitor, 
            IServiceCallRouter callRouter,
            ServiceClientErrorHandling? errorHandling
        ) => ExecuteCallEx<TResponseBody>(
                operationName: ((MethodCallExpression)expression.Body).Method.Name,
                callTypeParameters: callTypeParameters,
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor,
                callRouter: callRouter,
                errorHandling: errorHandling
            );

        Task<IReadOnlyList<ServiceCallResult>> IServiceClient<TCallInterface>.ExecuteWith(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression,
            ServiceCallTypeParameters callTypeParameters, 
            string correlationId, 
            IReadOnlyList<string> correlationTags, 
            IRuntimeMonitor monitor, 
            IServiceCallRouter callRouter,
            ServiceClientErrorHandling? errorHandling
        ) => ExecuteCallEx(
                operationName: ((MethodCallExpression)expression.Body).Method.Name,
                callTypeParameters: callTypeParameters,
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor,
                callRouter: callRouter,
                errorHandling: errorHandling
            );
    }
}