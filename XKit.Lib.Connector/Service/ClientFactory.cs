using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Registration;

namespace XKit.Lib.Connector.Service {

    public interface IClientFactory {
        IGenericServiceClient CreateGenericServiceClient(
            IReadOnlyDescriptor descriptor,
            string operationInterfaceName,
            ServiceClientParameters clientParameters = null,
            ServiceCallTypeParameters defaultCallTypeParameters = null,
            ServiceClientErrorHandling errorHandling = ServiceClientErrorHandling.LogWarning,
            string targetHostId = null
        );
        IGenericServiceClient CreateGenericServiceClient<TCallInterface>(
            IReadOnlyDescriptor descriptor,
            ServiceClientParameters clientParameters = null,
            ServiceCallTypeParameters defaultCallTypeParameters = null,
            ServiceClientErrorHandling errorHandling = ServiceClientErrorHandling.LogWarning,
            string targetHostId = null
        ) where TCallInterface : IServiceCallable;

        ICommandMessenger<TCommandInterface> CreateCommandMessengerClient<TCommandInterface>(
            ServiceClientParameters clientParameters = null
        ) where TCommandInterface : IServiceCommands;

        IEventMessenger<TEventInterface> CreateEventMessengerClient<TEventInterface>(
            ServiceClientParameters clientParameters = null
        ) where TEventInterface : IServiceEvents;
    }

    public class ClientFactory : IClientFactory {

        private static IClientFactory factory = new ClientFactory();

        public static IClientFactory Factory => factory;

        // ===========================================================================
        // IClientFactory default implementations 
        // ===========================================================================

        ICommandMessenger<TCommandInterface> IClientFactory.CreateCommandMessengerClient<TCommandInterface>(ServiceClientParameters clientParameters) 
            => new CommandMessenger<TCommandInterface>(clientParameters);

        IEventMessenger<TEventInterface> IClientFactory.CreateEventMessengerClient<TEventInterface>(ServiceClientParameters clientParameters) 
            => new EventMessenger<TEventInterface>(clientParameters);

        IGenericServiceClient IClientFactory.CreateGenericServiceClient(
            IReadOnlyDescriptor descriptor, 
            string operationInterfaceName, 
            ServiceClientParameters clientParameters, 
            ServiceCallTypeParameters defaultCallTypeParameters, 
            ServiceClientErrorHandling errorHandling, 
            string targetHostId
        ) => new ServiceClient(
            descriptor: descriptor,
            operationInterfaceName: operationInterfaceName,
            clientParameters: clientParameters,
            defaultCallTypeParameters: defaultCallTypeParameters,
            errorHandling: errorHandling
        );

        IGenericServiceClient IClientFactory.CreateGenericServiceClient<TCallInterface>(
            IReadOnlyDescriptor descriptor, 
            ServiceClientParameters clientParameters, 
            ServiceCallTypeParameters defaultCallTypeParameters, 
            ServiceClientErrorHandling errorHandling, 
            string targetHostId
        ) => new ServiceClient(
            descriptor: descriptor,
            operationInterfaceName: typeof(TCallInterface).Name,
            clientParameters: clientParameters,
            defaultCallTypeParameters: defaultCallTypeParameters,
            errorHandling: errorHandling
        );
        
        // =====================================================================
        // Static methods
        // =====================================================================

        public static void InjectCustomFactory(
            IClientFactory factory
        ) => ClientFactory.factory = factory;
    }
}