using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Services.MessageBroker;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Connector.Fabric;

namespace XKit.Lib.Connector.Service {

    public class CommandMessenger<TCallInterface> : ICommandMessenger<TCallInterface>
        where TCallInterface : IServiceCommands {

        private readonly IMessageBrokerClient Broker;
        private readonly ServiceClientParameters ClientParameters;

        // =====================================================================
        // construction
        // =====================================================================

        public CommandMessenger(
            ServiceClientParameters clientParameters = null,
            IMessageBrokerClient broker = null
        ) {
            IInprocessGlobalObjectRepository igor = null;
            if (broker == null || clientParameters == null) {
                igor = InprocessGlobalObjectRepositoryFactory.CreateSingleton();
            }

            ClientParameters = clientParameters ?? ServiceClientParameters.CreateForConsumer(
                requestorFabricId:
                    clientParameters?.RequestorFabricId ??
                    (igor.HasObject(typeof(ILocalFabricEnvironment)) ? igor.GetObject<ILocalFabricEnvironment>().FabricId : ""),
                defaultCorrelationId:
                    clientParameters?.DefaultCorrelationId ??
                    XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                monitor : RuntimeMonitorFactory.Create(),
                commonCorrelationTags:
                    clientParameters?.CommonCorrelationTags ??
                    new string[0],
                dependencyConnector:
                    clientParameters?.DependencyConnector ??
                    InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>()
                );

            this.Broker = broker ?? new InternalMessageBrokerClient(
                clientParameters: ClientParameters
            );
        }

        async Task<IReadOnlyList<ServiceCallResult>> ICommandMessenger<TCallInterface>.GetResults(
            Guid messageId, 
            float? waitSeconds
        ) {
            var waitResult = await Broker.WaitOnMessage(new WaitOnMessageRequest {
                MessageId = messageId,
                WaitTimeoutSeconds = waitSeconds
            });
            if (waitResult.HasError) {
                return null;
            }
            return waitResult.ResponseBody.Results;
        }

        async Task<Guid?> ICommandMessenger<TCallInterface>.IssueCommand(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression
        ) {
            var id = Guid.NewGuid();
            return (await Broker.IssueCommand(
                new FabricMessage {
                    MessageId = id,
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{((MethodCallExpression)expression.Body).Method.Name}"
                }
            )).HasError ? (Guid?)null : id;
        } 

        async Task<Guid?> ICommandMessenger<TCallInterface>.IssueCommand(
            string command,
            string payloadJson
        ) {
            var id = Guid.NewGuid();
            return (await Broker.IssueCommand(
                new FabricMessage {
                    MessageId = id,
                    JsonPayload = payloadJson,
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{command}",
                }
            )).HasError ? (Guid?)null : id;
        } 

        async Task<Guid?> ICommandMessenger<TCallInterface>.IssueCommand<TPayload>(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression,
            TPayload payload
        ) {
            var id = Guid.NewGuid();
            return (await Broker.IssueCommand(
                new FabricMessage {
                    MessageId = id,
                    JsonPayload = Json.To<TPayload>(payload),
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{((MethodCallExpression)expression.Body).Method.Name}"
                }
            )).HasError ? (Guid?)null : id;
        }        
    }
}