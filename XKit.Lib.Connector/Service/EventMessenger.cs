using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using XKit.Lib.Common.Client;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Services.MessageBroker;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Connector.Fabric;

namespace XKit.Lib.Connector.Service {

    public class EventMessenger<TCallInterface> : IEventMessenger<TCallInterface>
        where TCallInterface : IServiceEvents {

        private readonly ServiceClientParameters ClientParameters;
        private readonly IMessageBrokerApi Broker;

        // =====================================================================
        // construction
        // =====================================================================

        public EventMessenger(
            ServiceClientParameters clientParameters = null,
            IMessageBrokerApi broker = null
        ) {
            IInprocessGlobalObjectRepository igor = null;
            if (broker == null || clientParameters == null) {
                igor = InprocessGlobalObjectRepositoryFactory.CreateSingleton();
            }

            ClientParameters = clientParameters ?? ServiceClientParameters.CreateForConsumer(
                requestorFabricId:
                    clientParameters?.RequestorFabricId ??
                    (igor.HasObject(typeof(ILocalFabricEnvironment)) ? igor.GetObject<ILocalFabricEnvironment>().FabricId : ""),
                defaultCorrelationId:
                    clientParameters?.DefaultCorrelationId ??
                    XKit.Lib.Common.Utility.Identifiers.GenerateIdentifier(),
                monitor : RuntimeMonitorFactory.Create(),
                commonCorrelationTags:
                    clientParameters?.CommonCorrelationTags ??
                    new string[0],
                dependencyConnector:
                    clientParameters?.DependencyConnector ??
                    InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>()
                );

            this.Broker = broker ?? new InternalMessageBrokerClient(
                clientParameters: ClientParameters
            );
        }

        async Task<Guid?> IEventMessenger<TCallInterface>.RaiseEvent(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression
        ) {
            var id = Guid.NewGuid();
            return (await Broker.RaiseEvent(
                new FabricMessage {
                    MessageId = id,
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{((MethodCallExpression)expression.Body).Method.Name}"
                }
            )).HasError ? (Guid?)null : id;
        } 

        async Task<Guid?> IEventMessenger<TCallInterface>.RaiseEvent(
            string eventName,
            string payloadJson
        ) {
            var id = Guid.NewGuid();
            return (await Broker.RaiseEvent(
                new FabricMessage {
                    MessageId = id,
                    JsonPayload = payloadJson,
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{eventName}"
                }
            )).HasError ? (Guid?)null : id;
        } 

        async Task<Guid?> IEventMessenger<TCallInterface>.RaiseEvent<TPayload>(
            Expression<Func<TCallInterface, Task<ServiceCallResult>>> expression,
            TPayload payload
        ) {
            var id = Guid.NewGuid();
            return (await Broker.RaiseEvent(
                new FabricMessage {
                    MessageId = id,
                    JsonPayload = Json.To<TPayload>(payload),
                    MessageTypeName = $"{typeof(TCallInterface).Name}.{((MethodCallExpression)expression.Body).Method.Name}"
                }
            )).HasError ? (Guid?)null : id;
        } 
    }
}