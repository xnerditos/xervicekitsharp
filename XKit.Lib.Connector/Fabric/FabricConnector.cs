using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Utility;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Connector.Dependency;
using XKit.Lib.Common.Client;
using ServiceConstants = XKit.Lib.Common.Services.StandardConstants;
using System.Collections.Concurrent;
using XKit.Lib.Common.Services.Registry;

namespace XKit.Lib.Connector.Fabric {

    internal class FabricConnector : IFabricConnector {
        public enum MonitorCodes { 

            NoInstanceAvailable,
            ServiceNotAvailableFailure,
            ServiceNotAvailable,
            NewDependencyRequested,
            RefreshingWithRegistry,
            RegistrationRefreshFailed,
            InitialRegistrationFailed,
            NoRegistryServiceFound,
            ResolvingForServiceDependencyFailed,
            ResolvingForRegistryServiceFailed,
            RegistrationRefreshRegistryCallRouterNotAvailable
        }

        private const int ReAttemptDependencyResolutionTimeoutSeconds = 120;
        private readonly SemaphoreSlim synchronizer = new SemaphoreSlim(1, 1);
        private readonly ConcurrentDictionary<string, IReadOnlyServiceRegistration> depdencyRegistrations = new ConcurrentDictionary<string, IReadOnlyServiceRegistration>();
        private readonly IServiceCallRouterFactory routerFactory;
        private readonly IRegistryClient registry;
        
        private ConcurrentDictionary<string, IServiceCallRouter> callRouters = new ConcurrentDictionary<string, IServiceCallRouter>();
        private IReadOnlyList<IReadOnlyDescriptor> dependencies;
        private DateTime? cacheExpiration;
        private string localFabricId;
        private IReadOnlyList<IInstanceClientFactory> instanceClientFactories;
        private ILocalFabricEnvironment localFabricEnvironment;
        private ILocalHostEnvironment LocalHostEnvironment => localFabricEnvironment as ILocalHostEnvironment;
        private bool isRegisteredWithFabric = false;

        public FabricConnector(
            IServiceCallRouterFactory dependencyClientFactory,
            IList<IInstanceClientFactory> instanceClientFactories,
            IRegistryClient registry = null
        ) {
            this.routerFactory = dependencyClientFactory ??
                throw new ArgumentNullException(nameof(dependencyClientFactory));
            instanceClientFactories = instanceClientFactories ?? 
                throw new ArgumentNullException(nameof(instanceClientFactories));
            if (!instanceClientFactories.Any()) {
                throw new ArgumentException("list cannot be empty", nameof(instanceClientFactories));
            }
            this.instanceClientFactories = instanceClientFactories.ToList();
            this.registry = registry ?? new InternalRegistryClient(
                ServiceClientParameters.CreateForConsumer(
                    this.localFabricId,
                    dependencyConnector: this
                )
            );
        }

        // =====================================================================
        // Internal Testing
        // =====================================================================
        internal IEnumerable<IReadOnlyServiceRegistration> GetDependencyRegistrations() 
            => depdencyRegistrations.Values.ToArray();

        // =============================================================================
        // IfabricConnector
        // =============================================================================

        string IFabricConnector.Initialize() {
            if (localFabricId != null) {
                throw new Exception("Host connector already initialized");
            }
            synchronizer.Wait();
            try {
                return Initialize();
            } finally {
                synchronizer.Release();
            }
        }

        async Task<bool> IFabricConnector.RegisterAsConsumer(
            IEnumerable<string> initialRegistryHostAddresses,
            ILocalFabricEnvironment localEnvironment,
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister
        ) => await Register(initialRegistryHostAddresses, localEnvironment, monitor ?? RuntimeMonitorFactory.Create(), fatalIfUnableToRegister);

        async Task<bool> IFabricConnector.RegisterAsHost(
            IEnumerable<string> initialRegistryHostAddresses,
            ILocalHostEnvironment localEnvironment,
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister
        ) => await Register(initialRegistryHostAddresses, localEnvironment, monitor ?? RuntimeMonitorFactory.Create(), fatalIfUnableToRegister);

        async Task<bool> IFabricConnector.Refresh(
            IRuntimeMonitor monitor,
            string correlationId,
            IReadOnlyList<string> correlationTags
        ) {
            monitor = monitor ?? RuntimeMonitorFactory.Create();
            await synchronizer.WaitAsync();
            try {
                if (!isRegisteredWithFabric) {
                    return await RegisterInFabric(monitor, fatalIfUnableToRegister: false);
                } else {
                    return await RefreshFabricRegistration(
                        correlationId, 
                        correlationTags, 
                        monitor
                    );
                }
            } finally {
                synchronizer.Release();
            }
        }        

        async Task IFabricConnector.ForceResetTopologyMap(
            IReadOnlyServiceTopologyMap map
        ) {
            await synchronizer.WaitAsync();
            try {
                ResetTopologyMapForDependencies(map);
            } finally {
                synchronizer.Release();
            }
        } 

        async Task<bool> IFabricConnector.Unregister(IRuntimeMonitor monitor) {
            await synchronizer.WaitAsync();
            try {
                return await Unregister(monitor ?? RuntimeMonitorFactory.Create());
            } finally {
                synchronizer.Release();
            }
        }

        string IFabricConnector.FabricId => this.localFabricId;
        
        bool IFabricConnector.IsHost => this.LocalHostEnvironment != null;

        // =====================================================================
        // IDependencyConnector
        // =====================================================================

        async Task<IServiceCallRouter> IDependencyConnector.CreateCallRouter(
            IReadOnlyDescriptor target, 
            IRuntimeMonitor monitor, 
            string correlationId,
            IReadOnlyList<string> correlationTags,
            bool fatalIfNotAvailable,
            bool allowRegistryRefreshIfRequested
        ) {
            target = target ?? throw new ArgumentNullException(nameof(target));

            await synchronizer.WaitAsync();
            try {
                var router = await CreateOrGetServiceCallRouterEx(
                    target: target, 
                    monitor: monitor, 
                    correlationId: correlationId,
                    correlationTags: correlationTags,
                    addNewTargetsToDependencyListAndRefresh: allowRegistryRefreshIfRequested
                );

                if (router == null) {
                    if (fatalIfNotAvailable) {
                        monitor?.Error(
                            "Service not available", 
                            new { target }, 
                            code: MonitorCodes.ServiceNotAvailableFailure
                        );
                        throw new Exception($"Service not available: {Common.Utility.Identifiers.GetServiceFullRegistrationKey(target)}");
                    } else {
                        monitor?.Warning(
                            "Service not available", 
                            new { target }, 
                            code: MonitorCodes.ServiceNotAvailable
                        );
                    }
                    return null;
                }

                return router;
            } finally {
                synchronizer.Release();
            }
        }

        // =============================================================================
        // Workers
        // =============================================================================

        private async Task<bool> Register(
            IEnumerable<string> initialRegistryHostAddresses,
            ILocalFabricEnvironment localEnvironment,
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister
        ) {
            localEnvironment = localEnvironment ?? throw new ArgumentNullException(nameof(localEnvironment));
            if (localEnvironment.FabricId != this.localFabricId) {
                throw new ArgumentException("Local fabric environment does not match this fabric connector id");
            }
            initialRegistryHostAddresses = initialRegistryHostAddresses ?? new string[0];

            await synchronizer.WaitAsync();
            try {
                
                this.localFabricEnvironment = localEnvironment;
                InitializeAccess(initialRegistryHostAddresses);

                return await RegisterInFabric(monitor, fatalIfUnableToRegister);
            } finally {
                synchronizer.Release();
            }
        }

        private async Task<bool> Unregister(IRuntimeMonitor monitor) {

            var registryCallRouter = await ObtainRegistryServiceRouter(monitor);
            if (registryCallRouter == null) {
                monitor.Warning("Could not get registry call router for Unregister call");
                return false;
            }

            var result = await registry.ExecuteWith(
                x => x.Unregister(null),
                new UnregisterRequest { FabricId = this.localFabricId },
                callTypeParameters: ServiceCallTypeParameters.FireAndForget(),
                monitor: monitor,
                callRouter: registryCallRouter
            );

            return !result.First().HasError;
        }

        private string Initialize() {
            this.localFabricId = Identifiers.GenerateIdentifier();
            return this.localFabricId;
        }

        private void InitializeAccess(
            IEnumerable<string> initialRegistryHostAddresses
        ) {

            foreach(var instanceClientFactory in this.instanceClientFactories) {
                instanceClientFactory.InitializeFactory(LocalHostEnvironment);
            }

            // set up initial access to the registry service. 

            if (initialRegistryHostAddresses.Any()) {
                lock(this.depdencyRegistrations) {
                    var registryServiceDescriptor = ServiceConstants.Managed.StandardServices.Registry.Descriptor.Clone();

                    // At this point, the only service available is the registry service
                    AddCachedDependencyServiceRegistration(new ServiceRegistration {
                        Descriptor = registryServiceDescriptor,
                        RegistrationKey = Identifiers.GetServiceFullRegistrationKey(registryServiceDescriptor),
                        Instances = initialRegistryHostAddresses.Select(
                            registryHostAddress => new ServiceInstance {
                                // If the address is the "local host flag", it is local... use this host's address
                                HostAddress = registryHostAddress.Equals(HostConstants.LocalHostAddressFlag) 
                                    ? LocalHostEnvironment?.Address : registryHostAddress,
                                // If the address is the "local host flag" or matches the local host address , use the local host ID
                                HostFabricId = 
                                    LocalHostEnvironment?.Address == null ? null : 
                                        registryHostAddress.Equals(LocalHostEnvironment?.Address, StringComparison.CurrentCultureIgnoreCase) ||
                                        registryHostAddress.Equals(HostConstants.LocalHostAddressFlag, StringComparison.InvariantCultureIgnoreCase) 
                                    ? this.localFabricId : null,
                                Descriptor = registryServiceDescriptor,
                                Status = new ServiceInstanceStatus {
                                    Availability = AvailabilityEnum.Serving5,
                                    Health = HealthEnum.Unknown,
                                    RunState = RunStateEnum.Active
                            }
                        }).ToList()
                    });
                }
            }
        }

        private async Task<bool> RegisterInFabric(
            IRuntimeMonitor monitor,
            bool fatalIfUnableToRegister
        ) {
            
            var registryCallRouter = await ObtainRegistryServiceRouter(monitor);
            dependencies = localFabricEnvironment
                .GetDependencies()
                ?.Select(d => (IReadOnlyDescriptor) d.Clone())
                .ToArray();

            var registration = new FabricRegistration {
                FabricId = LocalHostEnvironment.FabricId,
                Capabilities = LocalHostEnvironment?.GetCapabilities()?.ToList(),
                Address = LocalHostEnvironment?.Address,
                Dependencies = dependencies?.Select(d => d.Clone()).ToList(),
                HostedServices = LocalHostEnvironment?.GetHostedServices().Select(s => s.Clone()).ToList(),
                Status = new FabricStatus {
                    FabricId = LocalHostEnvironment.FabricId,
                    Health = LocalHostEnvironment.GetHealth(),
                    RunState = LocalHostEnvironment.HostRunState
                }
            };

            if (registryCallRouter != null) {
                
                monitor.Trace("Have access to the registry, using it to register in the fabric");

                var result = (await registry.ExecuteWith<FabricRegistration, ServiceTopologyMap>(
                    x => x.Register(null),
                    registration,
                    monitor: monitor,
                    errorHandling: fatalIfUnableToRegister ? ServiceClientErrorHandling.ThrowException : ServiceClientErrorHandling.LogWarning,
                    callRouter: registryCallRouter
                )).First();

                if (result.Completed && result.ImmediateSuccess) {
                    isRegisteredWithFabric = true;
                    ResetTopologyMapForDependencies(result.ResponseBody);
                    return true;
                }

                monitor.Warning("Unable to register with the service fabric.  Temporarily falling back to local services only.", code: MonitorCodes.InitialRegistrationFailed);
                
                // fall back on getting as much as we can from the local environment
                // This is only temporary.  The cache expiration will cause a re-attempt in the
                // future.
                ResetTopologyMapForDependencies(
                    CreateLocalTopologyMap(
                        registration,
                        temporary: true
                    ));

                return true;
            } else {

                monitor.Warning("Could not find registry client.  Falling back on local services only.", code: MonitorCodes.NoRegistryServiceFound);

                // fall back on getting as much as we can from the local host environment
                // Because we could not obtain a registry client, this is permenat
                ResetTopologyMapForDependencies(
                    CreateLocalTopologyMap(
                        registration,
                        temporary: false
                    ));
            }

            // if we get here, we were unable to register

            if (fatalIfUnableToRegister) {
                throw new Exception("Unable to register");
            }

            return false;
        }

        // =============================================================================
        // Private utility
        // =============================================================================

        // -----------------------------------------------------------------------------
        // Stuff for instantiating ServiceCallRouter's

        private async Task<IServiceCallRouter> CreateOrGetServiceCallRouterEx(
            IReadOnlyDescriptor target, 
            IRuntimeMonitor monitor, 
            string correlationId,
            IReadOnlyList<string> correlationTags,
            bool addNewTargetsToDependencyListAndRefresh
        ) {

            IServiceCallRouter callRouter = null;

            var now = DateTime.UtcNow;
            var cacheExpired = now > this.cacheExpiration.GetValueOrDefault(DateTime.MaxValue);

            if (!cacheExpired) {
                callRouter = TryGetCachedServiceCallRouter(target);
                if (callRouter != null) {
                    return callRouter;
                }
            }

            // 
            // create the call router and cache it
            //

            IReadOnlyServiceRegistration registration;

            registration = TryGetDependencyServiceRegistration(target);
            List<IReadOnlyDescriptor> newDependencies = null;
            bool forceRefresh = false;
            lock(dependencies) {
                if (registration == null && !dependencies.Any(svc => svc.IsSameService(target))) {

                    monitor?.Trace(
                        message: "Target is new dependency",
                        code: MonitorCodes.NewDependencyRequested,
                        attributes: new { target }
                    );

                    if (addNewTargetsToDependencyListAndRefresh) {
                        newDependencies = new List<IReadOnlyDescriptor>();
                        newDependencies.Add(target);
                        // the new dependency will be added when the Registry call returns
                        //dependencies.Add(target.Clone()); 
                        forceRefresh = true;
                    }
                } 
            }

            if (cacheExpired || forceRefresh) {
                // if the cache is expired or it's a new dependency, do a full refresh of our
                // host registration (which will get any new dependencies added)

                monitor?.Trace(
                    message: "Forcing refresh with Registry service",
                    code: MonitorCodes.RefreshingWithRegistry,
                    attributes: new { cachedExpired = cacheExpired, forceRefresh = forceRefresh }
                );

                await RefreshFabricRegistration(
                    correlationId, 
                    correlationTags, 
                    monitor, 
                    newDependencies
                );
                registration = TryGetDependencyServiceRegistration(target);
            } 

            if (registration == null) {
                monitor?.Warning(
                    $"Could not obtain service registration for {Identifiers.GetServiceFullRegistrationKey(target)}", 
                    code: MonitorCodes.ResolvingForServiceDependencyFailed
                );
                return null;
            }

            var instanceClients = CreateInstanceClients(registration);
            if (!instanceClients.Any()) {
                // no instance clients could reach this target
                return null;
            }

            callRouter = routerFactory.Create(
                registration,
                cacheExpiration.GetValueOrDefault(DateTime.MaxValue),
                instanceClients
            );

            AddCachedCallRouter(target, callRouter);
            return callRouter;
        }

        private Task<IServiceCallRouter> ObtainRegistryServiceRouter(
            IRuntimeMonitor monitor
        ) {
            IServiceCallRouter callRouter = TryGetCachedServiceCallRouter(ServiceConstants.Managed.StandardServices.Registry.Descriptor);
            if (callRouter != null) {
                return Task.FromResult(callRouter);
            }

            IReadOnlyServiceRegistration registry = TryGetDependencyServiceRegistration(ServiceConstants.Managed.StandardServices.Registry.Descriptor);

            if (registry == null) {
                if (monitor != null) {
                    monitor.WarningAs(
                        $"Could not obtain dependency for Registry service", 
                        code: MonitorCodes.ResolvingForRegistryServiceFailed
                    );
                }
                return Task.FromResult((IServiceCallRouter)null);
            }

            var instanceClients = CreateInstanceClients(registry);
            if (!instanceClients.Any()) {
                if (monitor != null) {
                    monitor.Erratum(
                        "Could not find create any instance clients for Registry service",
                        attributes: new { serviceTarget = registry }.FieldsToDictionary() 
                    );
                }
                return Task.FromResult((IServiceCallRouter)null);
            }
            
            callRouter = routerFactory.Create(
                registry,
                DateTime.MaxValue,
                instanceClients
            );

            AddCachedCallRouter(
                ServiceConstants.Managed.StandardServices.Registry.Descriptor, 
                callRouter
            );
            return Task.FromResult(callRouter);
        }

        private IEnumerable<IInstanceClient> CreateInstanceClients(
            IReadOnlyServiceRegistration serviceRegistration
        ) {
            
            var instanceClients = new List<IInstanceClient>();
            
            foreach (var instance in serviceRegistration.Instances) {
                IInstanceClient instanceClient = TryCreateInstanceClientForService(instance);
                if (instanceClient != null) {
                    instanceClients.Add(instanceClient);
                }
            }

            return instanceClients;
        }

        private IInstanceClient TryCreateInstanceClientForService(
            IReadOnlyServiceInstance target
        ) {
            foreach(var factory in this.instanceClientFactories) {
                var client = factory.TryCreateClient(target);
                if (client != null) {
                    return client;
                }
            }            
            return null;
        }

        // -----------------------------------------------------------------------------
        // Registration 

        private async Task<bool> RefreshFabricRegistration(
            string correlationId,
            IReadOnlyList<string> correlationTags,
            IRuntimeMonitor monitor, 
            IEnumerable<IReadOnlyDescriptor> addDependencies = null
        ) { 
            
            var registryCallRouter = await ObtainRegistryServiceRouter(monitor);
            
            if (registryCallRouter == null) {
                monitor.WarningAs(
                    "Could not obtain registry call router for refresh", 
                    code: MonitorCodes.RegistrationRefreshRegistryCallRouterNotAvailable
                );
                return false;
            }

            var response = (await registry.ExecuteWith<RefreshRegistrationRequest, ServiceTopologyMap>(
                x => x.Refresh(null),
                new RefreshRegistrationRequest {
                    FabricId = this.localFabricId,
                    UpdateStatus = new FabricStatus {
                        FabricId = this.localFabricId,
                        Health = LocalHostEnvironment?.GetHealth(),
                        RunState = LocalHostEnvironment?.HostRunState
                    },
                    UpdateServiceStatuses = LocalHostEnvironment?.GetHostedServiceStatuses().ToList(),
                    NewDependencies = addDependencies?.Select(d => d.Clone()).ToList()
                },
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor,
                callRouter: registryCallRouter
            )).First();

            if (!response.ImmediateSuccess) {
                monitor.WarningAs("Could not complete registration refresh", code: MonitorCodes.RegistrationRefreshFailed);
                return false;
            }

            ResetTopologyMapForDependencies(response.ResponseBody);

            return true;
        }

        private void ResetTopologyMapForDependencies(IReadOnlyServiceTopologyMap map) {
            if (map?.Services != null) {
                IReadOnlyList<IReadOnlyDescriptor> refreshedDependecies = null;
                lock (this.dependencies) {
                    // the new list of dependencies will be the union of what we had already 
                    // combined with what we receive.  This essentially means that the registry
                    // service can "push" new dependencies simply by returning them.
                    // If we somehow get more than one listed for the same service, take the highest
                    // version.
                    refreshedDependecies = 
                        (from d in this.dependencies.Union(map.Services.Select(s => s.Descriptor))
                        group d by Identifiers.GetServiceVersionLevelKey(d) into g
                        select 
                            (from dep in g 
                            orderby dep.UpdateLevel, dep.PatchLevel descending 
                            select dep)
                            .First()
                        ).ToArray();
                }
                this.dependencies = refreshedDependecies;

                lock (this.depdencyRegistrations) {
                    this.depdencyRegistrations.Clear();

                    foreach (var r in map.Services) {
                        AddCachedDependencyServiceRegistration(r);
                    }
                    this.cacheExpiration = map.CacheExpiration;
                }

                this.callRouters = new ConcurrentDictionary<string, IServiceCallRouter>();
            }
        }

        private void AddCachedDependencyServiceRegistration(IReadOnlyServiceRegistration serviceRegistration) {
            this.depdencyRegistrations[Identifiers.GetServiceRegistrationKey(serviceRegistration)] = serviceRegistration;
        }

        private void AddCachedCallRouter(IReadOnlyDescriptor target, IServiceCallRouter router) {
            if (router != null) {
                this.callRouters[Identifiers.GetServiceFullRegistrationKey(target)] = router;
            }
        }

        private IReadOnlyServiceRegistration TryGetDependencyServiceRegistration(IReadOnlyDescriptor descriptor) {
            // Note that the registry service will determine what instances meet the requirement
            // for a dependency and return _all_ of them.  So we don't have to worry about it. 
            IReadOnlyServiceRegistration registration;
            this.depdencyRegistrations.TryGetValue(
                Identifiers.GetServiceFullRegistrationKey(descriptor),
                out registration
            );
            return registration;
        }

        private IServiceCallRouter TryGetCachedServiceCallRouter(
            IReadOnlyDescriptor target
        ) {
            IServiceCallRouter cached;
            this.callRouters.TryGetValue(
                Identifiers.GetServiceFullRegistrationKey(target),
                out cached
            );
            return cached;
        }

        private IReadOnlyServiceTopologyMap CreateLocalTopologyMap(
            IReadOnlyFabricRegistration registration,
            bool temporary
        ) {
            IReadOnlyServiceRegistration originalRegistryServiceRegistration = TryGetDependencyServiceRegistration(ServiceConstants.Managed.StandardServices.Registry.Descriptor);

            // Use the locally hosted services 
            var localDependencyRegistrations = 
                (registration?.HostedServices ?? new IReadOnlyServiceRegistration[0])
                .Where(sr => !sr.Descriptor.IsSameService(ServiceConstants.Managed.StandardServices.Registry.Descriptor))
                .Select(sr => sr.Clone())
                .ToList();

            if (originalRegistryServiceRegistration != null) {
                localDependencyRegistrations.Add(originalRegistryServiceRegistration.Clone());                
            }

            return new ServiceTopologyMap {
                CacheExpiration = temporary ? DateTime.UtcNow.AddSeconds(ReAttemptDependencyResolutionTimeoutSeconds) : DateTime.MaxValue,
                Services = localDependencyRegistrations
            };
        }
    }
}