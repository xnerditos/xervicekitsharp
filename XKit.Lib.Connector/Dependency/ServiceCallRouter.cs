using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Connector.Dependency {

    internal class ServiceCallRouter : IServiceCallRouter {

        private readonly IReadOnlyServiceRegistration TargetService;
        private readonly List<IInstanceClient> InstanceClients = new List<IInstanceClient>();
        private readonly Random Random = new Random();
        private DateTime validUntilTime;
        
        public ServiceCallRouter(
            IReadOnlyServiceRegistration targetService,
            DateTime validUntilTime,
            IEnumerable<IInstanceClient> instanceClients
        ) {
            this.TargetService = targetService;
            this.validUntilTime = validUntilTime;
            this.InstanceClients.AddRange(instanceClients);
        }

        // =====================================================================
        // IServiceCallRouter
        // =====================================================================

        string IServiceCallRouter.ServiceRegistrationKey => TargetService.RegistrationKey;
        IReadOnlyDescriptor IServiceCallRouter.ServiceDescriptor => TargetService.Descriptor;
        bool IServiceCallRouter.IsValid => DateTime.UtcNow <= this.validUntilTime;

        void IServiceCallRouter.Invalidate() =>
            this.validUntilTime = DateTime.MinValue;

        async Task<IReadOnlyList<ServiceCallResult>> IServiceCallRouter.ExecuteCall(
            ServiceCallRequest request,
            IRuntimeMonitor monitor,
            ServiceCallPolicy policy,
            string targetHostId
        ) {

            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }
            if (string.IsNullOrEmpty(request.OperationName)) {
                throw new ArgumentNullException(nameof(request.OperationName));
            }

            var useRequest = request.Clone();
            useRequest.CallTypeParameters = useRequest.CallTypeParameters ?? ServiceCallTypeParameters.SyncResult();

            ServiceCallPatternEnum useCallPattern = 
                policy?.CallPattern ??
                TargetService.CallPolicy?.CallPattern ?? 
                ServiceCallPatternEnum.FirstChance;

            if (
                useCallPattern == ServiceCallPatternEnum.SpecificHost && 
                string.IsNullOrEmpty(targetHostId)
            ) {
                var m = $"Inconsistent service call type.  {nameof(targetHostId)} must be specified if call pattern is {ServiceCallPatternEnum.SpecificHost.ToString()}";
                monitor.Erratum(m, new { useRequest });
                throw new ArgumentException(m);
            }
            if (
                useCallPattern != ServiceCallPatternEnum.SpecificHost && 
                !string.IsNullOrEmpty(targetHostId)
            ) {
                var m = $"Inconsistent service call type.  {nameof(targetHostId)} can only be specified if call pattern is {ServiceCallPatternEnum.SpecificHost.ToString()}";
                monitor.Erratum(m, new { useRequest });
                throw new ArgumentException(m);
            }
            switch(useRequest.CallTypeParameters.CallType) {
                case ServiceCallTypeEnum.SyncResult:
                case ServiceCallTypeEnum.FireAndForget:
                case ServiceCallTypeEnum.CallbackReturn:
                    if (
                        useRequest.CallTypeParameters.CallbackOperationName != null || 
                        useRequest.CallTypeParameters.CallbackService != null
                    ) {
                        string m = "Inconsistent service call type.  Callback must be null.";
                        monitor.Erratum(m, new { useRequest });
                        throw new ArgumentException();
                    }
                    break;
                case ServiceCallTypeEnum.CallbackOperationWithSlimResult:
                case ServiceCallTypeEnum.CallbackOperationWithResult:
                    if (
                        useRequest.CallTypeParameters.CallbackOperationName == null || 
                        useRequest.CallTypeParameters.CallbackService == null
                    ) {
                        string m = "Inconsistent service call type.  Callback service and operaiton must be specified.";
                        monitor.Erratum(m, new { request });
                        throw new ArgumentException();
                    }
                    break;
            }

            var results = new List<ServiceCallResult>();

            var callQueue = CreateClientCallQueue(request.RequestorFabricId, useCallPattern, targetHostId);
            while (callQueue.Count > 0) {
                IInstanceClient instClient;
                if (callQueue.TryDequeue(out instClient)) {
                    var instResult = await instClient.ExecuteCall(useRequest);
                    results.Add(HydrateResult(request, instResult));
                    if (instResult.Completed) {
                        switch (instResult.OperationStatus) {
                        case JobResultStatusEnum.NoAction_BadRequest:
                        case JobResultStatusEnum.NoAction_CallbackUnavailable:
                        case JobResultStatusEnum.NonRecoverableError:
                        case JobResultStatusEnum.Success:
                        case JobResultStatusEnum.PartialSuccess:
                        case JobResultStatusEnum.Pending:
                            switch (useCallPattern) {
                            case ServiceCallPatternEnum.FirstChance:
                            case ServiceCallPatternEnum.SpecificHost:
                                // these cases are non recoverable per instances.  no point
                                // in doing the rest.
                                return results;
                            case ServiceCallPatternEnum.AllInstances:
                                // in this case, just keep chugging
                                break;
                            }
                            break;
                        case JobResultStatusEnum.RecoverableError:
                        case JobResultStatusEnum.NoAction_ConnectionFailed:
                        case JobResultStatusEnum.NoAction_JobUnavailable:
                        case JobResultStatusEnum.NoAction_Timeout:
                            // in these cases, let the loop attempt the next in the queue
                            break;
                        }
                    }
                }
            }

            if (results.Count == 0) {
                var result = HydrateResult(
                    request,
                    null
                );
                result.ServiceCallStatus = ServiceCallStatusEnum.NoConnection;
                result.Message = "No connection to service";
                result.OperationStatus = JobResultStatusEnum.Incomplete;
                results.Add(result);
            }

            return results;
        }

        // =====================================================================
        // Private 
        // =====================================================================

        private ServiceCallResult HydrateResult(
            ServiceCallRequest request,
            ServiceCallResult result = null
        ) { 
            if (result == null) {
                result = new ServiceCallResult();
                result.ServiceCallStatus = ServiceCallStatusEnum.NoConnection;
            }
            result.CorrelationId = result?.CorrelationId ?? request.CorrelationId;
            result.CorrelationTags = result?.CorrelationTags?.DeepCopy() ?? request.CorrelationTags?.DeepCopy();
            result.OperationName = request.OperationName;
            result.RequestorFabricId = request.RequestorFabricId;
            result.RequestorInstanceId = request.RequestorInstanceId;
            return result;
        }

        private Queue<IInstanceClient> CreateClientCallQueue(string requestingFabricId, ServiceCallPatternEnum callPattern, string targetHostId) {

            // FUTURE:  Make these configurable

            // Note:  These are set up in an attempt to balance out the factors in relation
            // to each other and ensure "reachability" for any given service in the list.
            const int availabilityFactorWeight = 100;
            const int healthFactorWeight = ((int)AvailabilityEnum.Serving5 * availabilityFactorWeight);
            const int sameHostFactorWeight = ((int)AvailabilityEnum.Serving5 * availabilityFactorWeight);
            const int randomFactorRange = ((int)AvailabilityEnum.Serving5 * availabilityFactorWeight);

            HashSet<string> instanceIdsAdded = new HashSet<string>(this.InstanceClients.Count);

            int getInstancePreference(IReadOnlyServiceInstance inst) =>
                // calculate a weight based on availability
                (int)inst.Status.Availability * availabilityFactorWeight +
                // add one factor if it's on the same host
                (inst.HostFabricId != requestingFabricId ? 0 : sameHostFactorWeight) +
                // add one factor if the health is above moderate or unknown
                (inst.Status.Health >= HealthEnum.Moderate || inst.Status.Health == HealthEnum.Unknown ? healthFactorWeight : 0) +
                // add a degree of randomeness
                (Random.Next(randomFactorRange));

            bool canAddClientToQueue(IReadOnlyServiceInstance inst) => 
                    (targetHostId == null || inst.HostFabricId == targetHostId) &&
                    (inst.Status.Health >= HealthEnum.UnhealthyRecovering || inst.Status.Health == HealthEnum.Unknown) &&
                    inst.Status.Availability >= AvailabilityEnum.Serving5 &&
                    !instanceIdsAdded.Contains(inst.InstanceId);

            IInstanceClient selectInstanceClientAndMarkAsAdded(IInstanceClient client) {
                instanceIdsAdded.Add(client.Instance.InstanceId);
                return client;
            }

            return new Queue<IInstanceClient>(
                from 
                    c in this.InstanceClients
                where 
                    canAddClientToQueue(c.Instance) 
                orderby 
                    getInstancePreference(c.Instance) descending
                select 
                    selectInstanceClientAndMarkAsAdded(c)
            );
        }
    }
}
