using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Config;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Common.Services;
using System.Reflection;
using XKit.Lib.Common.Utility;
using XKit.Lib.Host.Config;
using XKit.Lib.Common.Services.Config;
using XKit.Lib.Common.Utility.Collections;
using System.Collections.Concurrent;
using XKit.Lib.Common.Utility.Threading;
using XKit.Lib.Host.Services;
using XKit.Lib.Host.Helpers;
using XKit.Lib.Common.Client;
using System.Threading;
using XKit.Lib.Common.Services.MessageBroker;
using XKit.Lib.Connector.Fabric;

namespace XKit.Lib.Host.Management {
    internal class HostManager : IHostManager, ILocalHostEnvironment {
        private readonly int HostVersionLevel;
        private readonly int HostUpdateLevel;
        private readonly int HostPatchLevel;
        private readonly IDictionary<System.Type, IServiceBase> ServicesByApiOperationType = new ConcurrentDictionary<System.Type, IServiceBase>();
        private readonly IList<IManagedService> ManagedServices = new SynchronizedList<IManagedService>();
        private readonly IList<IMetaService> MetaServices = new SynchronizedList<IMetaService>();
        private readonly string HostAddress;
        private readonly Func<HealthEnum> HostHealthGetter;
        private readonly IFabricConnector FabricConnector;
        private IDependencyConnector DependencyConnector => FabricConnector;
        private readonly ILogManager LogManager;
        private readonly string LocalMetaDataDbPath;
        private readonly string LocalDataFolderPath;
        private readonly ILocalConfigSessionFactory LocalConfigSessionFactory;
        private IDictionary<string,object> startupParameters;
        private RunStateEnum state = RunStateEnum.Inactive;
        private IConfigReader<HostConfigDocument> hostConfigReader;
        private readonly string configDocumentIdentifier; 
        private readonly IConfigClient ConfigClient;
        private readonly IMessageBrokerClient MessagingClient;
        private readonly SemaphoreSlim hostActionSync = new SemaphoreSlim(1, 1);

        public HostManager(
            string hostAddress,
            IFabricConnector hostFabricConnector,
            ILogManager logManager,
            ILocalConfigSessionFactory localConfigSessionFactory,
            string localMetaDataDbPath,
            string localDataFolderPath,
            Func<HealthEnum> hostHealthGetter,
            IConfigClient configClient,
            IMessageBrokerClient messagingClient
        ) { 
            this.HostAddress = hostAddress;
            this.FabricConnector = hostFabricConnector;
            this.LogManager = logManager;
            this.LocalConfigSessionFactory = localConfigSessionFactory;
            this.LocalMetaDataDbPath = localMetaDataDbPath;
            this.LocalDataFolderPath = localDataFolderPath;
            this.HostHealthGetter = hostHealthGetter;
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            this.HostVersionLevel = version.Major;
            this.HostUpdateLevel = version.Minor;
            this.HostPatchLevel = version.Build;
            this.configDocumentIdentifier = Identifiers.GetHostVersionLevelKey(version.Major);
            var clientParameters = ServiceClientParameters.CreateForConsumer(
                    hostFabricConnector.FabricId,
                    null,
                    null,
                    null,
                    this.DependencyConnector
                );
            this.ConfigClient = 
                configClient ?? 
                new InternalConfigClient(clientParameters);
            this.MessagingClient = 
                messagingClient ??
                new InternalMessageBrokerClient(clientParameters);
        }


        // =====================================================================
        // IHostManager
        // =====================================================================

        RunStateEnum IHostManager.HostState => state;

        void IHostManager.KillHost(IRuntimeMonitor monitor) {
            //TaskUtil.RunSyncSafely(() => monitor.Info("Host being killed", new { FabricId = this.FabricConnector.FabricId }));
            System.Diagnostics.Process.GetCurrentProcess().Kill(); // kerblam!  Ouch!
        }

        void IHostManager.ResumeHost(IRuntimeMonitor monitor) {
            hostActionSync.Wait();
            try {
                if (this.state != RunStateEnum.Paused) {
                    return;
                } 
                monitor.Info("Host being resumed", new { FabricId = this.FabricConnector.FabricId });
                foreach(var svc in this.ManagedServices) {
                    svc.ResumeService(monitor);
                }            
                this.state = RunStateEnum.Active;
            } finally {
                hostActionSync.Release();
            }
        }

        void IHostManager.PauseHost(IRuntimeMonitor monitor) {
            hostActionSync.Wait();
            try { 
                if (this.state != RunStateEnum.Active) {
                    return;
                } 
                monitor.Info("Host being paused", new { FabricId = this.FabricConnector.FabricId });
                foreach(var svc in this.ManagedServices) {
                    svc.PauseService(monitor);
                }
                this.state = RunStateEnum.Paused;
            } finally {
                hostActionSync.Release();
            }
        }

        async Task IHostManager.StopHostAsync(IRuntimeMonitor monitor) {
            await hostActionSync.WaitAsync();
            try { 
                if (this.state == RunStateEnum.ShuttingDown || this.state == RunStateEnum.Inactive) {
                    return;
                }
                await StopHost(monitor);
            } finally {
                hostActionSync.Release();
            }
        }

        void IHostManager.StopHost(IRuntimeMonitor monitor) {
            hostActionSync.Wait();
            try { 
                if (this.state == RunStateEnum.ShuttingDown || this.state == RunStateEnum.Inactive) {
                    return;
                }
                TaskUtil.RunSyncSafely(() => StopHost(monitor));
            } finally {
                hostActionSync.Release();
            }
        }

        IGenericManagedService IHostManager.AddCreateManagedService(
            IReadOnlyDescriptor serviceDescriptor,
            System.Type operationType
        ) {
            if (operationType != null) {
                if (!typeof(IServiceOperation).IsAssignableFrom(operationType)) {
                    throw new ArgumentException("Type must implement IServiceApiOperation", nameof(operationType));
                }
                if (operationType.IsInterface) {
                    throw new ArgumentException("Type must be an concrete class", nameof(operationType));
                }
            }

            var genericServiceType = typeof(GenericManagedService<>).MakeGenericType(
                new[] { 
                    operationType ?? typeof(BlankServiceOperation)
                });

            var serviceInstance = (IGenericManagedService)Activator.CreateInstance(
                genericServiceType, 
                serviceDescriptor.Clone(),
                (ILocalHostEnvironment)this,
                DependencyConnector
            );

            ManagedServices.Add(serviceInstance);
            return serviceInstance;
        } 

        void IHostManager.AddManagedService(
            IManagedService service
        ) {
            ManagedServices.Add(service);
        } 

        void IHostManager.AddMetaService(
            IMetaService service
        ) {
            MetaServices.Add(service);
        }

        void IHostManager.AddBuiltinService(
            BuiltinServices serviceType
        ) {
            IManagedService service;
            switch(serviceType) {
            case BuiltinServices.MessageBrokerLocalOnly:
                service = new BuiltinMessageBrokerService(this, this.DependencyConnector);
                break;
            default: 
                throw new Exception("Build in service not found");
            }
            ManagedServices.Add(service);
        } 

        async Task IHostManager.StartHostAsync(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            object startupParameters, 
            bool failIfCannotRegister
        ) {
            await hostActionSync.WaitAsync();
            try { 
                if (this.state != RunStateEnum.Inactive) {
                    return;
                }
                this.state = RunStateEnum.StartingUp;
                monitor = monitor ?? RuntimeMonitorFactory.Create();
                monitor.Info("Host being started", new { FabricId = this.FabricConnector.FabricId });
                await StartHost(
                    initialRegistryHostAddresses,
                    monitor, 
                    startupParameters?.FieldsToDictionary(), 
                    failIfCannotRegister
                );
            } finally {
                hostActionSync.Release();
            }
        }

        async Task IHostManager.StartHostAsync(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            IDictionary<string, object> startupParameters, 
            bool failIfCannotRegister
        ) {
            await hostActionSync.WaitAsync();
            try { 
                if (this.state != RunStateEnum.Inactive) {
                    return;
                }
                this.state = RunStateEnum.StartingUp;
                monitor = monitor ?? RuntimeMonitorFactory.Create();
                monitor.Info("Host being started", new { FabricId = this.FabricConnector.FabricId });
                await StartHost(
                    initialRegistryHostAddresses,
                    monitor, 
                    startupParameters, 
                    failIfCannotRegister
                );
            } finally {
                hostActionSync.Release();
            }
        }

        void IHostManager.StartHost(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            object startupParameters, 
            bool failIfCannotRegister
        ) {
            hostActionSync.Wait();
            try { 
                if (this.state != RunStateEnum.Inactive) {
                    return;
                }
                this.state = RunStateEnum.StartingUp;
                monitor = monitor ?? RuntimeMonitorFactory.Create();
                TaskUtil.RunSyncSafely(async () => {
                    monitor.Info("Host being started", new { FabricId = this.FabricConnector.FabricId });
                    await StartHost(
                        initialRegistryHostAddresses,
                        monitor, 
                        startupParameters?.FieldsToDictionary(), 
                        failIfCannotRegister
                    );
                });
            } finally {
                hostActionSync.Release();
            }
        }

        void IHostManager.StartHost(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            IDictionary<string, object> startupParameters, 
            bool failIfCannotRegister
        ) {
            hostActionSync.Wait();
            try { 
                if (this.state != RunStateEnum.Inactive) {
                    return;
                }
                this.state = RunStateEnum.StartingUp;
                monitor = monitor ?? RuntimeMonitorFactory.Create();
                TaskUtil.RunSyncSafely(async () => {
                    monitor.Info("Host being started", new { FabricId = this.FabricConnector.FabricId });
                    await StartHost(
                        initialRegistryHostAddresses,
                        monitor, 
                        startupParameters, 
                        failIfCannotRegister
                    );
                });
            } finally {
                hostActionSync.Release();
            }
        }

        async Task IHostManager.RefreshConfigurationFromSource(
            IRuntimeMonitor monitor,
            string correlationId,
            IReadOnlyList<string> correlationTags
        ) {
            hostActionSync.Wait();
            try { 
                if (this.state == RunStateEnum.Inactive || this.state == RunStateEnum.ShuttingDown) {
                    return;
                }
                monitor = monitor ?? RuntimeMonitorFactory.Create();

                var updatedConfigIdentifiers = await CallConfigurationAndRefreshLocalConfig(
                    monitor, 
                    correlationId, 
                    correlationTags, 
                    fatalIfUnavailable: false
                );
                if (updatedConfigIdentifiers.Contains(this.configDocumentIdentifier)) {
                    ActOnLocalEnvironmentChange();
                }

                foreach(var svc in this.MetaServices) {
                    if (updatedConfigIdentifiers.Contains(svc.ConfigurationDocumentIdentifier)) {
                        svc.SignalEnvironmentChange(monitor);
                    }
                }
                foreach(var svc in this.ManagedServices) {
                    if (updatedConfigIdentifiers.Contains(svc.ConfigurationDocumentIdentifier)) {
                        svc.SignalEnvironmentChange(monitor);
                    }
                }
            } finally {
                hostActionSync.Release();
            }
        }
 
        string IHostManager.LocalDataFolderPath => LocalDataFolderPath;

        string IHostManager.MetaDataDbPath => LocalMetaDataDbPath;

        void IHostManager.SignalLocalEnvironmentChange(IRuntimeMonitor monitor)
            => ActOnLocalEnvironmentChange();
            
        // =====================================================================
        // ILocalFabricEnvironment
        // =====================================================================

        string ILocalFabricEnvironment.FabricId => this.FabricConnector.FabricId;

        IEnumerable<IReadOnlyDescriptor> ILocalFabricEnvironment.GetDependencies()
            => GetDependencies();

        IDependencyConnector ILocalHostEnvironment.DependencyConnector => DependencyConnector;
            
        // =====================================================================
        // ILocalHostEnvironment
        // =====================================================================

        ILogManager ILocalHostEnvironment.LogManager => LogManager;

        bool ILocalHostEnvironment.HasHostedServices 
            => ManagedServices.Any();            

        ILocalConfigSessionFactory ILocalHostEnvironment.LocalConfigSessionFactory 
            => LocalConfigSessionFactory;
            

        int? ILocalHostEnvironment.VersionLevel => HostVersionLevel;

        string ILocalHostEnvironment.ConfigurationDocumentIdentifier => this.configDocumentIdentifier;
        
        T ILocalHostEnvironment.GetStartupParameter<T>(string key, T defaultValue) 
            => GetStartupParameter<T>(key, defaultValue);

        string ILocalHostEnvironment.Address => this.HostAddress;

        string ILocalHostEnvironment.DataRootFolderPath => this.LocalDataFolderPath;

        RunStateEnum ILocalHostEnvironment.HostRunState => this.state;

        HealthEnum ILocalHostEnvironment.GetHealth() 
            => GetHealth();

        IEnumerable<ServiceInstanceStatus> ILocalHostEnvironment.GetHostedServiceStatuses() 
            => GetLocalInstanceStatuses();

        IEnumerable<IReadOnlyServiceRegistration> ILocalHostEnvironment.GetHostedServices() 
            => GetLocalServiceRegistrations();

        IEnumerable<string> ILocalHostEnvironment.GetCapabilities() 
            => MetaServices.Select(svc => svc.CapabilityKeyName);

        IEnumerable<IManagedService> ILocalHostEnvironment.GetManagedServices(
            string collectionName,
            string serviceName,
            int? serviceVersion
        ) => ManagedServices
            .Where(
                s => (collectionName == null || s.Descriptor.Collection == collectionName) &&
                     (serviceName == null || s.Descriptor.Name == serviceName) &&
                     (serviceVersion == null || s.Descriptor.Version == serviceVersion) 
            ).ToArray();

        IManagedService ILocalHostEnvironment.GetManagedService(IReadOnlyDescriptor descriptor)
            => ManagedServices.Where(s => s.Descriptor.IsSameService(descriptor)).FirstOrDefault();
            
        IEnumerable<IMetaService> ILocalHostEnvironment.GetMetaServices(
            string serviceName
        ) => MetaServices
            .Where(
                s => s.Descriptor.Name == serviceName 
            );
        
        // =====================================================================
        // Internal for testing support
        // =====================================================================

        // =====================================================================
        // private
        // =====================================================================

        private T GetStartupParameter<T>(string key, T defaultValue = default(T)) {
            if (!startupParameters.TryGetValue(key, out object val)) {
                return defaultValue;
            }
            return (T)val;
        }

        private async Task StartHost(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            IDictionary<string, object> startupParameters, 
            bool failIfCannotRegister
        ) {
            this.startupParameters = startupParameters ?? new Dictionary<string, object>();
            var platformServices = this.ManagedServices.Where(isPlatformService).ToArray();
            var nonPlatformServices = this.ManagedServices.Where(svc => !isPlatformService(svc)).ToArray();
            var correlationId = Identifiers.GenerateIdentifier();
            
            foreach(var svc in this.MetaServices) {
                svc.StartService(monitor);
            }

            foreach(var svc in platformServices) {
                svc.StartService(monitor);
            }

            await RegisterHostWithFabric(
                initialRegistryHostAddresses,
                monitor, 
                failIfCannotRegister
            );

            if (GetStartupParameter<bool>(HostStartupParameterKeys.REFRESH_CONFIG_ON_STARTUP, true)) {
                var fatalIfUnavailable = GetStartupParameter<bool>(HostStartupParameterKeys.FAIL_IF_CONFIG_UNAVAILABLE_ON_STARTUP, false);
                var updatedConfigIdentifiers = await CallConfigurationAndRefreshLocalConfig(
                    monitor: monitor,
                    correlationId: correlationId,
                    correlationTags: null,
                    fatalIfUnavailable: fatalIfUnavailable
                );

                // signal env change because the meta services and the platform services were already started.

                foreach(var svc in this.MetaServices) {
                    if (updatedConfigIdentifiers.Contains(svc.ConfigurationDocumentIdentifier)) {
                        svc.SignalEnvironmentChange(monitor);
                    }
                }
                foreach(var svc in platformServices) {
                    if (updatedConfigIdentifiers.Contains(svc.ConfigurationDocumentIdentifier)) {
                        svc.SignalEnvironmentChange(monitor);
                    }
                }
            }

            this.hostConfigReader = ConfigReaderFactory.CreateForHost<HostConfigDocument>(this.HostVersionLevel, LocalConfigSessionFactory);

            foreach(var svc in nonPlatformServices) {
                svc.StartService(monitor);
            }

            this.state = RunStateEnum.Active;

            foreach(var svc in platformServices) {
                svc.SignalHostStartupComplete(monitor);
            }
            foreach(var svc in nonPlatformServices) {
                svc.SignalHostStartupComplete(monitor);
            }

            if (GetStartupParameter<bool>(HostStartupParameterKeys.REGISTER_SUBCRIPTIONS_ON_STARTUP, true)) {
                var fatalIfUnavailable = GetStartupParameter<bool>(HostStartupParameterKeys.FAIL_IF_MESSAGEBROKER_UNAVAILABLE_ON_STARTUP, true);

                List<Subscription> subscriptionsToSubmit = new List<Subscription>();
                foreach(var svc in this.MetaServices) {
                    subscriptionsToSubmit.AddRange(
                        svc.CommandSubscriptions.Select(s => new Subscription {
                            ErrorHandling = s.ErrorHandling,
                            FailureDelaysToRetryMs = s.FailureDelaysToRetryMs?.ToArray(),
                            MaxConsecutiveFailures = s.MaxConsecutiveFailures,
                            MaxDeliveryRetries = s.MaxDeliveryRetries,
                            MessageTypeName = s.MessageTypeName,
                            Policy = s.Policy?.Clone(),
                            Recipient = svc.Descriptor.Clone(),
                            RecipientHostId = s.RecipientHostId
                        })
                    );
                    subscriptionsToSubmit.AddRange(
                        svc.EventSubscriptions.Select(s => new Subscription {
                            ErrorHandling = s.ErrorHandling,
                            FailureDelaysToRetryMs = s.FailureDelaysToRetryMs?.ToArray(),
                            MaxConsecutiveFailures = s.MaxConsecutiveFailures,
                            MaxDeliveryRetries = s.MaxDeliveryRetries,
                            MessageTypeName = s.MessageTypeName,
                            Policy = s.Policy?.Clone(),
                            Recipient = svc.Descriptor.Clone(),
                            RecipientHostId = s.RecipientHostId
                        })
                    );
                }

                foreach(var svc in this.ManagedServices) {
                    subscriptionsToSubmit.AddRange(
                        svc.CommandSubscriptions.Select(s => new Subscription {
                            ErrorHandling = s.ErrorHandling,
                            FailureDelaysToRetryMs = s.FailureDelaysToRetryMs?.ToArray(),
                            MaxConsecutiveFailures = s.MaxConsecutiveFailures,
                            MaxDeliveryRetries = s.MaxDeliveryRetries,
                            MessageTypeName = s.MessageTypeName,
                            Policy = s.Policy?.Clone(),
                            Recipient = svc.Descriptor.Clone(),
                            RecipientHostId = s.RecipientHostId
                        })
                    );
                    subscriptionsToSubmit.AddRange(
                        svc.EventSubscriptions.Select(s => new Subscription {
                            ErrorHandling = s.ErrorHandling,
                            FailureDelaysToRetryMs = s.FailureDelaysToRetryMs?.ToArray(),
                            MaxConsecutiveFailures = s.MaxConsecutiveFailures,
                            MaxDeliveryRetries = s.MaxDeliveryRetries,
                            MessageTypeName = s.MessageTypeName,
                            Policy = s.Policy?.Clone(),
                            Recipient = svc.Descriptor.Clone(),
                            RecipientHostId = s.RecipientHostId
                        })
                    );
                }

                if (subscriptionsToSubmit.Count > 0) {
                    await CallMessageBrokerAndRegisterSubscriptions(
                        subscriptionsToSubmit,
                        monitor: monitor,
                        correlationId: correlationId,
                        correlationTags: null,
                        fatalIfUnavailable: fatalIfUnavailable
                    );
                }
            }

            // ------------------------------------------------------------------------
            bool isPlatformService(IManagedService svc) 
                => svc.Descriptor.Collection == StandardConstants.Managed.Collections.Platform;
        }

        private async Task StopHost(
            IRuntimeMonitor monitor
        ) {
            this.state = RunStateEnum.ShuttingDown;
            monitor.Info("Host being stopped", new { FabricId = this.FabricConnector.FabricId });

            var platformServices = this.ManagedServices.Where(isPlatformService).ToArray();
            var nonPlatformServices = this.ManagedServices.Where(svc => !isPlatformService(svc)).ToArray();
            
            foreach(var svc in nonPlatformServices) {
                svc.StopService(monitor);
            }

            foreach(var svc in platformServices) {
                svc.StopService(monitor);
            }

            foreach(var svc in this.MetaServices) {
                svc.StopService(monitor);
            }

            bool unRegisterSuccessful = await FabricConnector.Unregister(
                monitor
            );

            if (!unRegisterSuccessful) {
                monitor.Warning("Unregistering host failed");
            }

            foreach(var svc in nonPlatformServices) {
                svc.SignalHostShutdownComplete(monitor);
            }
            foreach(var svc in platformServices) {
                svc.SignalHostShutdownComplete(monitor);
            }

            this.state = RunStateEnum.Inactive;

            // ------------------------------------------------------------------------
            bool isPlatformService(IManagedService svc) 
                => svc.Descriptor.Collection == StandardConstants.Managed.Collections.Platform;
        }

        private async Task RegisterHostWithFabric(
            IEnumerable<string> initialRegistryHostAddresses,
            IRuntimeMonitor monitor,
            bool failIfCannotRegister
        ) {
            monitor = monitor ?? RuntimeMonitorFactory.Create();
            bool registrationSuccessful = await FabricConnector.RegisterAsHost(
                initialRegistryHostAddresses,
                this,
                monitor
            );
            if (!registrationSuccessful && failIfCannotRegister) {
                throw new Exception("Host registration failed");
            }
        }

        private async Task CallMessageBrokerAndRegisterSubscriptions(
            IReadOnlyList<Subscription> subscriptions,
            IRuntimeMonitor monitor = null,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            bool fatalIfUnavailable = false
        ) {
            var result = (await MessagingClient.ExecuteWith<SubscribeRequest>(
                x => x.Subscribe(null),
                new SubscribeRequest {
                    Subscriptions = subscriptions.ToArray()
                },
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor
            )).First();

            if (!result.Completed) {
                const string message = "Message Broker service is not available";
                monitor.Warning(message);
                if (fatalIfUnavailable) {
                    throw new Exception(message);
                }
                return;
            }

            if (result.HasError) {
                const string message = "Subscribe call failed";
                monitor.Warning(message);
                if (fatalIfUnavailable) {
                    throw new Exception(message);
                }
                return;
            }
        }

        private async Task<HashSet<string>> CallConfigurationAndRefreshLocalConfig(
            IRuntimeMonitor monitor = null,
            string correlationId = null,
            IReadOnlyList<string> correlationTags = null,
            bool fatalIfUnavailable = false
        ) {
            HashSet<string> updatedConfigIdentifiers = new HashSet<string>();
            
            var result = (await ConfigClient.ExecuteWith<ConfigServiceQueryRequest, ConfigServiceQueryResponse>(
                x => x.QueryConfig(null),
                new ConfigServiceQueryRequest {
                    HostVersionLevel = this.HostVersionLevel,
                    ServiceKeys = 
                        ManagedServices
                        .Select(svc => svc.ConfigurationDocumentIdentifier)
                        .Union(MetaServices.Select(svc => svc.ConfigurationDocumentIdentifier))
                        .ToArray()
                },
                correlationId: correlationId,
                correlationTags: correlationTags,
                monitor: monitor
            )).First();

            if (!result.Completed) {
                const string message = "Configuration service is not available";
                monitor.Warning(message);
                if (fatalIfUnavailable) {
                    throw new Exception(message);
                }
                return updatedConfigIdentifiers;
            }

            if (result.HasError) {
                const string message = "Configuration service call failed";
                monitor.Warning(message);
                if (fatalIfUnavailable) {
                    throw new Exception(message);
                }
                return updatedConfigIdentifiers;
            }

            var response = result.ResponseBody;
            await updateLocalConfig(this.configDocumentIdentifier, response.HostConfig.ToJson());

            if (response?.ServiceConfigJson?.Count > 0) {
                foreach(var key in response.ServiceConfigJson.Keys) {
                    var json = response.ServiceConfigJson[key];
                    await updateLocalConfig(key, json);
                }
            }

            return updatedConfigIdentifiers;

            // -----------------------------------------------------------------

            async Task updateLocalConfig(string documentIdentifier, string configJson) {
                if (!string.IsNullOrEmpty(configJson)) {
                    updatedConfigIdentifiers.Add(documentIdentifier);
                    var localConfigSession = LocalConfigSessionFactory.Create(documentIdentifier); 
                    await localConfigSession.UpdateConfigJson(configJson);
                }
            }
        }

        private HostConfigDocument GetConfigDocument() 
            => this.hostConfigReader.GetConfig();

        private IEnumerable<Descriptor> GetDependencies() {
            var dependencies = new Dictionary<string,IReadOnlyDescriptor>();

            foreach(var svc in MetaServices) {
                foreach(var dep in svc.Dependencies) {
                    string key = Common.Utility.Identifiers.GetServiceFullRegistrationKey(dep);
                    dependencies[key] = dep;
                }
            }

            foreach(var svc in ManagedServices) {
                foreach(var dep in svc.Dependencies) {                    
                    string key = Common.Utility.Identifiers.GetServiceFullRegistrationKey(dep);
                    dependencies[key] = dep;
                }
            }

            return dependencies.Values.Select(d => d.Clone());
        }

        private IEnumerable<ServiceRegistration> GetLocalServiceRegistrations() 
            =>  (from svc in ManagedServices
                let descriptor = svc.Descriptor.Clone()
                select new ServiceRegistration {
                    RegistrationKey = Common.Utility.Identifiers.GetServiceFullRegistrationKey(svc.Descriptor),
                    Descriptor = descriptor,
                    CallPolicy = svc.CallPolicy?.Clone(),
                    Instances = new List<ServiceInstance> { 
                        new ServiceInstance {
                            HostFabricId = this.FabricConnector.FabricId,
                            InstanceId = svc.InstanceId,
                            RegistrationKey = Common.Utility.Identifiers.GetServiceFullRegistrationKey(svc.Descriptor),
                            Status = svc.GetServiceStatus(),
                            HostAddress = HostAddress,
                            Descriptor = descriptor
                        }
                    }
                }).Union(
                from svc in MetaServices
                let descriptor = svc.Descriptor.Clone()
                select new ServiceRegistration {
                    RegistrationKey = Common.Utility.Identifiers.GetServiceFullRegistrationKey(svc.Descriptor),
                    Descriptor = descriptor,
                    CallPolicy = svc.CallPolicy?.Clone(),
                    Instances = new List<ServiceInstance> { 
                        new ServiceInstance {
                            HostFabricId = this.FabricConnector.FabricId,
                            InstanceId = svc.InstanceId,
                            RegistrationKey = Common.Utility.Identifiers.GetServiceFullRegistrationKey(svc.Descriptor),
                            Status = svc.GetServiceStatus(),
                            HostAddress = HostAddress,
                            Descriptor = descriptor
                        }
                    }
                });

        private IEnumerable<ServiceInstanceStatus> GetLocalInstanceStatuses() 
            =>  (from svc in ManagedServices
                select svc.GetServiceStatus())
                .Union(
                from svc in MetaServices
                select svc.GetServiceStatus());
                
         private HealthEnum GetHealth()
            => HostHealthGetter != null ? HostHealthGetter() : HealthEnum.Unknown;

        private void ActOnLocalEnvironmentChange()
            => this.hostConfigReader.InvalidateCache();
    }
}