using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Services.MessageBroker;
using System.Collections.Generic;
using System.Reflection;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Host.DefaultBaseClasses {

    public abstract class ManagedService<TOperation> 
        : ServiceBase<TOperation>, IManagedService
        where TOperation : IServiceOperation {

        // =====================================================================
        // Protected members
        // =====================================================================
                        
        protected string LocalDataFolderPath { get; private set; }

        protected override void StartService(IRuntimeMonitor monitor) {
            LocalDataFolderPath = $"{LocalHostEnvironment.DataRootFolderPath}/{Descriptor.Collection}/{Descriptor.Name}/{Descriptor.Version}";
            base.StartService(monitor);
        }
        
        protected void PauseService(IRuntimeMonitor monitor) {
            SetRunStatePaused();
            OnServicePauseEvent?.Invoke(monitor);
        }

        protected void ResumeService(IRuntimeMonitor monitor) {
            OnServiceResumeEvent?.Invoke(monitor);
            SetRunStateActive();
        }

        protected void SignalHostStartupComplete(IRuntimeMonitor monitor) {
            OnHostStartCompleteEvent?.Invoke(monitor);
        }
        
        protected void SignalHostShutdownComplete(IRuntimeMonitor monitor) {
            OnHostStartCompleteEvent?.Invoke(monitor);
        }
        
        // =====================================================================
        // abstract and virtual members
        // =====================================================================
        
        protected virtual AvailabilityEnum GetAvailability() 
            => AvailabilityEnum.Serving5;
        
        protected override bool CanStartNewOperation() {
            switch(this.LocalHostEnvironment.HostRunState)
            {
                case RunStateEnum.Inactive:
                case RunStateEnum.Paused:
                case RunStateEnum.ShuttingDown:
                case RunStateEnum.Unknown:
                return false;
            }
            return this.RunState == RunStateEnum.Active;
        }

        // =====================================================================
        // events
        // =====================================================================

        protected event OnHostStartCompleteDelegate OnHostStartCompleteEvent;
        protected event OnServicePauseDelegate OnServicePauseEvent;
        protected event OnServiceResumeDelegate OnServiceResumeEvent;

        // =====================================================================
        // Construction
        // =====================================================================

        protected ManagedService(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) : base(localEnvironment, dependencyConnector) { }

        // =====================================================================
        // IManagedService
        // =====================================================================

        void IManagedService.StartService(IRuntimeMonitor monitor) 
            => StartService(monitor);

        void IManagedService.PauseService(IRuntimeMonitor monitor) 
            => PauseService(monitor);

        void IManagedService.ResumeService(IRuntimeMonitor monitor) 
            => ResumeService(monitor);

        void IManagedService.StopService(IRuntimeMonitor monitor)
            => StopService(monitor);
        
        void IManagedService.SignalHostStartupComplete(IRuntimeMonitor monitor) 
            => SignalHostStartupComplete(monitor);

        void IManagedService.SignalHostShutdownComplete(IRuntimeMonitor monitor) 
            => SignalHostShutdownComplete(monitor);

    }
}