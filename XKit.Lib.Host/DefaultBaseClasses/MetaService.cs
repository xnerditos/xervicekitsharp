using XKit.Lib.Common.Host;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Fabric;
using System;
using XKit.Lib.Common.Services.MessageBroker;
using System.Collections.Generic;
using System.Reflection;
using XKit.Lib.Common.Utility.Extensions;

namespace XKit.Lib.Host.DefaultBaseClasses {

    public abstract class MetaService<TOperation> 
        : ServiceBase<TOperation>, IMetaService
        where TOperation : IServiceOperation {

        // =====================================================================
        // protected
        // =====================================================================
        
        protected string CapabilityKeyName { get; }
        
        // =====================================================================
        // abstract and virtual members
        // =====================================================================
        protected override bool CanStartNewOperation() 
            => (this.LocalHostEnvironment.HostRunState == RunStateEnum.Active || 
               this.LocalHostEnvironment.HostRunState == RunStateEnum.Paused) &&
               this.RunState == RunStateEnum.Active;

        // =====================================================================
        // Construction
        // =====================================================================

        public MetaService(
            string capabilityKeyName,
            ILocalHostEnvironment localHostEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) : base(localHostEnvironment, dependencyConnector) {
            this.CapabilityKeyName = capabilityKeyName ?? throw new ArgumentNullException(capabilityKeyName);
        }

        // =====================================================================
        // IMetaService
        // =====================================================================

        string IMetaService.CapabilityKeyName => this.CapabilityKeyName;

        void IMetaService.StartService(IRuntimeMonitor monitor)
            => this.StartService(monitor);
        void IMetaService.StopService(IRuntimeMonitor monitor)
            => this.StopService(monitor);

        // =====================================================================
        // Other private
        // =====================================================================
    }
}