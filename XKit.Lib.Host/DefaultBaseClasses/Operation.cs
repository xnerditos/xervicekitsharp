using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Common.Host;

namespace XKit.Lib.Host.DefaultBaseClasses {

    public abstract partial class Operation : IOperation {

        private bool isActive;
        private JobReplayStrategyEnum replayStrategy = JobReplayStrategyEnum.NotSpecifiedOrUnknown;
        private object replayData = null;
        protected ILocalHostEnvironment LocalHostEnvironment => Context.LocalHostEnvironment;
        protected IDependencyConnector DependencyConnector => Context.DependencyConnector;
        protected OperationContext Context { get; }
        protected ILogSession LogSession { get; private set; }
        protected ILogWriter Log { get; private set; }
        protected IRuntimeMonitor Monitor { get; private set; }
        protected string OperationName { get; private set; }
        
        public Operation(
            OperationContext context
        ) {
            this.Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        // =====================================================================
        // abstract and virtual
        // =====================================================================

        protected abstract JobTypeEnum JobType { get; }
        protected abstract string OriginatorName { get; }
        protected abstract int OriginatorVersion { get; }
        protected abstract string OriginatorInstanceId { get; }
        protected virtual bool CanStartNewOperation() => true;
        protected virtual bool IsLongRunningDefault => false;

        // =====================================================================
        // IOperationImplementor
        // =====================================================================

        bool IOperation.IsActive => this.isActive;

        // =====================================================================
        // Begin / End / Status 
        // =====================================================================

        /// <summary>
        /// Performs logic to start processing an operation
        /// </summary>
        /// <typeparam name="bool">true if operation was started successfully</typeparam>
        protected Task<bool> BeginOperation<TWorkItem>(
            TWorkItem workItem,
            string operationName,
            JobSystemEffect effectFlag,
            object additionalLogAttributes = null,
            string requestorInstanceId = null,
            string requestorFabricId = null,
            IRuntimeMonitor monitor = null,
            LoggingOptions loggingOptions = LoggingOptions.Default
        ) where TWorkItem : class => BeginOperation<TWorkItem>(
            workItem,
            operationName,
            effectFlag,
            additionalLogAttributes.FieldsToDictionary(), 
            requestorInstanceId,
            requestorFabricId,
            monitor,
            loggingOptions
        );

        /// <summary>
        /// Performs logic to start processing an operation
        /// </summary>
        /// <typeparam name="bool">true if operation was started successfully</typeparam>
        protected Task<bool> BeginOperation<TWorkItem>(
            TWorkItem requestBody,
            string operationName,
            JobSystemEffect effectFlag,
            IDictionary<string, object> additionalLogAttributes,
            string requestorInstanceId = null,
            string requestorFabricId = null,
            IRuntimeMonitor monitor = null,
            LoggingOptions loggingOptions = LoggingOptions.Default
        ) where TWorkItem : class {

            try {
                additionalLogAttributes = additionalLogAttributes ?? new Dictionary<string, object>();
                OperationName = operationName;
                LogSession = LocalHostEnvironment.LogManager.CreateWriteableSession(
                    this.JobType,
                    effectFlag,
                    this.OriginatorName,
                    this.OriginatorVersion,
                    operationName,
                    LocalHostEnvironment.FabricId,
                    this.OriginatorInstanceId,
                    (Context?.OperationId).GetValueOrDefault(),
                    Context?.CorrelationId,
                    requestorInstanceId,
                    requestorFabricId,
                    Context?.CorrelationTags
                );
                LogSession.BeginLogBackground(
                    requestBody,
                    additionalLogAttributes,
                    loggingOptions
                );
                Log = LogSession.GetLogWriter();

                if (LocalHostEnvironment.HostRunState == RunStateEnum.Inactive || !CanStartNewOperation()) {
                    Log.WarningBackground(
                        "Instance unavailable.",
                        new Dictionary<string, object> {
                            { "HostState", "LocalHostEnvironment.HostRunState" }
                        }
                    );
                    return Task.FromResult(false);
                }

                Monitor = monitor ?? RuntimeMonitorFactory.Create(Log);
                isActive = true;
                return Task.FromResult(true);
            } catch {
                isActive = false;
                return Task.FromResult(false);
            }
        }

        /// <summary>
        /// Creates an operation result with NoAction_ServiceUnavailable for return
        /// </summary>
        /// <param name="message">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult ResultCallInvalidServiceUnavailable(
            string message = null
        ) => Result(
                JobResultStatusEnum.NoAction_JobUnavailable,
                message: message
            );

        /// <summary>
        /// Creates an operation result with NoAction_ServiceUnavailable for return
        /// </summary>
        /// <param name="message">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult<TResponseBody> ResultCallInvalidServiceUnavailable<TResponseBody>(
            string message = null
        ) where TResponseBody : class => Result<TResponseBody>(
                JobResultStatusEnum.NoAction_JobUnavailable,
                null,
                message
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Pending for return and adds log entry
        /// </summary>
        /// <param name="resultMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult ResultAndLogCallPending(
            string resultMessage = null,
            string logMessage = null,
            object pendingLogParameters = null
        ) {
            if (isActive) {

                if (LogSession != null) {
                    LogSession.PendingLogBackground(
                        pendingLogParameters?.FieldsToDictionary(),
                        logMessage ?? resultMessage
                    );
                }

                return Result(
                    JobResultStatusEnum.Pending,
                    message: resultMessage
                );
            }

            var msg = "Cannot finish a call as Pending if it is not active";
            if (LogSession != null) {
                Log.ErratumAsBackground(msg);
            }
            throw new Exception(msg);
        }

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Pending for return and adds log entry
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected async Task<OperationResult<TResponseBody>> ResultAndLogCallPending<TResponseBody>(
            string operationMessage = null,
            string logMessage = null,
            object pendingLogParameters = null
        ) where TResponseBody : class {

            if (isActive) {

                if (LogSession != null) {
                    await LogSession.PendingLog(
                        pendingLogParameters?.FieldsToDictionary(),
                        logMessage ?? operationMessage
                    );
                }

                return Result<TResponseBody>(
                    JobResultStatusEnum.Pending,
                    null,
                    operationMessage
                );
            }

            var msg = "Cannot finish a call as Pending if it is not active";
            Monitor.Erratum(msg);
            throw new Exception(msg);
        }

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult ResultSuccess(
            string operationMessage = null,
            string logMessage = null
        ) => Result(
                JobResultStatusEnum.Success,
                message: operationMessage,
                logMessage: logMessage
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult<TResultData> ResultSuccess<TResultData>(
            TResultData resultData,
            string operationMessage = null,
            string logMessage = null,
            TResultData logData = null
        ) where TResultData : class => Result<TResultData>(
                JobResultStatusEnum.Success,
                resultData: resultData,
                message: operationMessage,
                logMessage: logMessage,
                logData: logData 
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult PartialResultSuccess(
            string operationMessage = null,
            string logMessage = null
        ) => Result(
                JobResultStatusEnum.PartialSuccess,
                message: operationMessage,
                logMessage: logMessage 
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult<TResultData> PartialResultSuccess<TResultData>(
            TResultData resultData,
            string operationMessage = null,
            string logMessage = null,
            TResultData logData = null
        ) where TResultData : class => Result<TResultData>(
                JobResultStatusEnum.PartialSuccess,
                resultData: resultData,
                message: operationMessage,
                logMessage: logMessage,
                logData: logData 
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult ResultError(
            string operationMessage = null,
            string logMessage = null,
            bool recoverable = false
        ) => Result(
                operationStatus: recoverable ? JobResultStatusEnum.RecoverableError : JobResultStatusEnum.NonRecoverableError,
                message: operationMessage,
                logMessage: logMessage 
            );

        /// <summary>
        /// Creates an operation result with OperationResultStatusEnum.Success for return
        /// </summary>
        /// <param name="operationMessage">accompanying result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult<TResultData> ResultError<TResultData>(
            string operationMessage,
            string logMessage = null,
            bool recoverable = false,
            TResultData resultData = null,
            TResultData logData = null
        ) where TResultData : class => Result<TResultData>(
                operationStatus: recoverable ? JobResultStatusEnum.RecoverableError : JobResultStatusEnum.NonRecoverableError,
                resultData: resultData,
                message: operationMessage,
                logMessage: logMessage,
                logData: logData 
            );
        
        /// <summary>
        /// Performs logic to end processing an operation.  The OperationResult from EndOperation is ready
        /// to be immediately returned.
        /// </summary>
        /// <param name="fromResult">A result from the operationAction code that is used as a 
        /// basis for the operation ending logic. </param>
        /// <param name="operationStatus">A status for the operation to be used if fromResult is null 
        /// </param>
        /// <param name="operationMessage">A message for the operation to be used if fromResult is null
        /// </param>
        /// <param name="resultData">Result data for the operation to be used if fromResult is null
        /// </param>
        /// <param name="logMessage">The message to write to the log.  This will override the
        /// operation message, which is normally written</param>
        /// <param name="logResultData">The result data to write to the log.  This will override 
        /// operation result data, which is normally written</param>
        /// <returns>The result to return from the operation</returns>
        protected Task<OperationResult<TResultData>> EndOperation<TResultData>(
            OperationResult<TResultData> fromResult = null,
            JobResultStatusEnum? operationStatus = null,
            string operationMessage = null,
            string operationCode = null,
            TResultData resultData = null,
            string logMessage = null,
            object logResultData = null
        ) where TResultData : class {
            
            JobResultStatusEnum useStatus = 
                operationStatus ?? fromResult?.OperationStatus ??             
                    (!Monitor.IsInErrorState ? JobResultStatusEnum.Success :
                    Monitor.ErrorState == MonitorErrorState.RecoverableError ? 
                    JobResultStatusEnum.RecoverableError : 
                    JobResultStatusEnum.NonRecoverableError);
            string useMessage = 
                fromResult?.Message == null && operationMessage == null && Monitor.IsInErrorState ?
                    Monitor.ErrorMessage :
                    fromResult?.Message ?? operationMessage;
            object useCode = 
                fromResult?.Code ?? operationCode;
            TResultData useResultData = resultData ?? fromResult?.ResultData;

            if (isActive) {

                if (LogSession != null) {
                    
                    LogSession.EndLogBackground(
                        useStatus,
                        logMessage ?? fromResult?.LogMessage ?? useMessage,
                        logResultData ?? fromResult?.LogData ?? useResultData,
                        operationCode: useCode,
                        replayStrategy,
                        replayData
                    );

                    LogSession.Dispose();
                    LogSession = null;
                    Log = null;
                    Monitor = null;
                }

                isActive = false;

                return Task.FromResult(Result<TResultData>(
                    useStatus,
                    useResultData,
                    useMessage,
                    useCode
                ));
            }

            return Task.FromResult<OperationResult<TResultData>>(null);
        }

        /// <summary>
        /// Performs logic to end processing an operation.  The OperationResult from EndOperation is ready
        /// to be immediately returned.
        /// </summary>
        /// <param name="fromResult">A result from the operationAction code that is used as a 
        /// basis for the operation ending logic. </param>
        /// <param name="operationStatus">A status for the operation to be used if fromResult is null 
        /// </param>
        /// <param name="operationMessage">A message for the operation to be used if fromResult is null
        /// </param>
        /// <param name="resultData">Result data for the operation to be used if fromResult is null
        /// </param>
        /// <param name="logMessage">The message to write to the log.  This will override the
        /// operation message, which is normally written</param>
        /// <param name="logResultData">The result data to write to the log.  This will override 
        /// operation result data, which is normally written</param>
        /// <returns>The result to return from the operation</returns>
        protected Task<OperationResult> EndOperation(
            OperationResult fromResult = null,
            JobResultStatusEnum? operationStatus = null,
            string operationMessage = null,
            string operationCode = null,
            string logMessage = null
        ) {
            JobResultStatusEnum useStatus = 
                operationStatus ?? fromResult?.OperationStatus ??             
                    (!Monitor.IsInErrorState ? JobResultStatusEnum.Success :
                    Monitor.ErrorState == MonitorErrorState.RecoverableError ? 
                    JobResultStatusEnum.RecoverableError : 
                    JobResultStatusEnum.NonRecoverableError);
            string useMessage = 
                fromResult?.Message == null && operationMessage == null && Monitor.IsInErrorState ?
                    Monitor.ErrorMessage :
                    fromResult?.Message ?? operationMessage;
            object useCode = 
                fromResult?.Code ?? operationCode;

            if (isActive) {

                if (LogSession != null) {
                    LogSession.EndLogBackground(
                        useStatus,
                        logMessage ?? fromResult?.LogMessage ?? useMessage,
                        null,
                        useCode,
                        replayStrategy,
                        replayData
                    );

                    LogSession.Dispose();
                    LogSession = null;
                    Log = null;
                    Monitor = null;
                }

                isActive = false;

                return Task.FromResult(Result(
                    useStatus,
                    message: useMessage,
                    useCode
                ));
            }

            return Task.FromResult<OperationResult>(null);
        }

        /// <summary>
        /// Creates an operation result for return
        /// </summary>
        /// <param name="operationStatus">status to use</param>
        /// <param name="resultData">result data to use</param>
        /// <param name="operationMessage">result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult<TResultData> Result<TResultData>(
            JobResultStatusEnum? operationStatus,
            TResultData resultData,
            string message = null,
            object code = null,            
            string logMessage = null,
            TResultData logData = null
        ) where TResultData : class => new OperationResult<TResultData> {
            Message = message,
            OperationStatus = operationStatus,
            ResultData = resultData,
            LogMessage = logMessage,
            LogData = logData,
            Code = code
        };

        /// <summary>
        /// Creates an operation result for return
        /// </summary>
        /// <param name="operationStatus">status to use</param>
        /// <param name="resultData">result data to use</param>
        /// <param name="operationMessage">result message</param>
        /// <returns>Operation result for immediate return</returns>
        protected OperationResult Result(
            JobResultStatusEnum? operationStatus,
            string message = null,
            object code = null,            
            string logMessage = null
        ) => new OperationResult {
            Message = message,
            OperationStatus = operationStatus,
            LogMessage = logMessage,
            Code = code
        };

        // =====================================================================
        // Monitoring and Log
        // =====================================================================

        protected void NotReplayable() {
            this.replayStrategy = JobReplayStrategyEnum.NotReplayable;
            this.replayData = null;
        }

        protected void ReplayableFromOriginalRequest() {
            this.replayStrategy = JobReplayStrategyEnum.ReplayOriginalRequest;
            this.replayData = null;
        }

        protected void ReplayableFromModifiedRequest(object modifiedRequest) {
            this.replayStrategy = JobReplayStrategyEnum.ReplayModifiedRequest;
            this.replayData = modifiedRequest;
        }

        /// <summary>
        /// Log an erratum (bug, unexpected behaviour)
        /// </summary>
        protected void Erratum(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.ErratumAs(
                message,
                attributes,
                tags,
                callerFilePath,
                callerMemberName,
                callerLineNumber
            );

        /// <summary>
        /// Log an erratum (bug, unexpected behaviour)
        /// </summary>
        protected void Erratum(
            string message,
            object attributes,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.ErratumAs(
                message,
                attributes?.FieldsToDictionary(),
                tags,
                callerFilePath,
                callerMemberName,
                callerLineNumber
            );

        /// <summary>
        /// Log an error (a plausible error, as opposed to unexpected behaviour). 
        /// </summary>
        protected void Error(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            bool recoverable = false,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.ErrorAs(
                message,
                attributes,
                code,
                recoverable,
                tags,
                callerFilePath,
                callerMemberName,
                callerLineNumber
            );

        /// <summary>
        /// Log an error (a plausible error, as opposed to unexpected behaviour)
        /// </summary>
        protected void Error(
            string message,
            object attributes,
            object code = null,
            bool recoverable = false,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.ErrorAs(
                message,
                attributes?.FieldsToDictionary(),
                code,
                recoverable,
                tags,
                callerFilePath,
                callerMemberName,
                callerLineNumber
            );

        /// <summary>
        /// Log a warning 
        /// </summary>
        protected void Warning(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.WarningAs(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a warning 
        /// </summary>
        protected void Warning(
            string message,
            object attributes,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.WarningAs(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a status update 
        /// </summary>
        protected void Status(
            string message,
            IReadOnlyDictionary<string, object> attributes = null,
            object code = null,
            IEnumerable<string> tags = null
        ) => Monitor.Status(
            message,
            attributes,
            code,
            tags
        );
        
        /// <summary>
        /// Log a status update 
        /// </summary>
        protected void Status(
            string message,
            object attributes,
            object code = null,
            IEnumerable<string> tags = null
        ) => Monitor.Status(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags
        );

        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            string message,
            IReadOnlyDictionary<string, object> attributes,
            object code = null,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            message,
            attributes,
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );
                
        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            string message,
            object attributes,
            object code,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            message,
            attributes?.FieldsToDictionary(),
            code,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            string message,
            object attributes,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            message,
            attributes?.FieldsToDictionary(),
            null,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            object attributes,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            "",
            attributes?.FieldsToDictionary(),
            null,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            string message,
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            message,
            (Dictionary<string, object>)null,
            null,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log a trace message (for debugging and analysis) 
        /// </summary>
        protected void Trace(
            IEnumerable<string> tags = null,
			[CallerFilePath] string callerFilePath = "",
			[CallerMemberName] string callerMemberName = "",
			[CallerLineNumber] int callerLineNumber = 0
        ) => Monitor.TraceAs(
            "",
            (Dictionary<string, object>)null,
            null,
            tags,
            callerFilePath,
            callerMemberName,
            callerLineNumber
        );

        /// <summary>
        /// Log information that is part of an audit trail
        /// </summary>
        protected void Audit(
            string message, 
            object attributes = null, 
            object code = null, 
            IEnumerable<string> tags = null
        ) => Monitor.Audit(
            message,
            attributes,
            code,
            tags            
        );

        /// <summary>
        /// Log information that is part of an audit trail
        /// </summary>
        protected void Audit(
            string message, 
            IReadOnlyDictionary<string, object> attributes = null, 
            object code = null, 
            IEnumerable<string> tags = null
        ) => Monitor.Audit(
            message,
            attributes,
            code,
            tags            
        );

        /// <summary>
        /// Log info (generally for debugging and analysis.  If otherwise, consider Status instead) 
        /// </summary>
        protected void Info(
            string message = null,
            IReadOnlyDictionary<string, object> attributes = null,
            object data = null,
            object code = null, 
            IEnumerable<string> tags = null
        ) => Monitor.Info(
            message,
            attributes,
            data,
            code,
            tags
        );
        
        /// <summary>
        /// Log info (generally for debugging and analysis.  If otherwise, consider Status instead) 
        /// </summary>
        protected void Info(
            string message,
            object attributes,
            object data = null,
            object code = null, 
            IEnumerable<string> tags = null
        ) => Monitor.Info(
            message,
            attributes?.FieldsToDictionary(),
            data,
            code,
            tags
        );

        /// <summary>
        /// Log a snapshot of operation data.  This may be used by downstream processes analyzing the
        /// operation for specific purposes. 
        /// </summary>
        protected void Snapshot(
            IReadOnlyDictionary<string, object> attributes,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        ) => Log.Snapshot(
            attributes,
            data,
            code,
            tags
        );

        /// <summary>
        /// Log a snapshot of operation data.  This may be used by downstream processes analyzing the
        /// operation for specific purposes. 
        /// </summary>
        protected void Snapshot(
            object attributes,
            object data = null,
            object code = null,
            IEnumerable<string> tags = null
        ) => Log.Snapshot(
            attributes?.FieldsToDictionary(),
            data,
            code,
            tags
        );

        /// <summary>
        /// Automatically log a value on exit.
        /// </summary>
        protected void AutoLog(
            string name,
            object value
        ) => Log.AutoLog(name, value);

        /// <summary>
        /// Automatically log a value on exit.
        /// </summary>
        protected void AutoLog(
            object values
        ) => Log.AutoLog(values);
    }
}