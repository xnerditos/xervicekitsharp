using System;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Host.DefaultBaseClasses {

    public delegate void OnEnvironmentChangeDelegate(IRuntimeMonitor monitor);
    public delegate void OnServiceStartDelegate(IRuntimeMonitor monitor);
    public delegate void OnHostStartCompleteDelegate(IRuntimeMonitor monitor);
    public delegate void OnServiceStopDelegate(IRuntimeMonitor monitor);
    public delegate void OnServicePauseDelegate(IRuntimeMonitor monitor);
    public delegate void OnServiceResumeDelegate(IRuntimeMonitor monitor);
}
