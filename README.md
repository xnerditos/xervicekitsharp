# XKit.Lib

This repository contains projects aimed at the SOA clients and SOA servers (services).

## XKit.Lib.Server.Api
### implementation of the host meta API 
### implementation of the service meta API

## XKit.Lib.Server.Sync
### functionality and api for caching and syncing among multi-instance services

## XKit.Lib.Server.LocalLog
### functionality and api for pulling log entries

## XKit.Lib.Server.Storage
### functionality for storing data locally in a service

## XKit.Lib.Connector
### functionality for connecting to a XKit service in general 
### helper functionality for implementing specific service wrappers
### functionality for handling dependency discovery (connection to Registry service)
### functionality for connecting a host to the Registry

## XKit.Lib.Common
### common helper functionality


