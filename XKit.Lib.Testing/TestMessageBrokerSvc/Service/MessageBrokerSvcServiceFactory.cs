using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;

namespace XKit.Lib.Testing.MessageBrokerSvc.Service {

    public interface IMessageBrokerSvcServiceFactory : IServiceFactory {
		IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        );
    }

	public class MessageBrokerSvcServiceFactory : IMessageBrokerSvcServiceFactory
	{
		private static IMessageBrokerSvcServiceFactory factory = new MessageBrokerSvcServiceFactory();

		public static IMessageBrokerSvcServiceFactory Factory => factory;

        IReadOnlyDescriptor IServiceFactory.Descriptor => XKit.Lib.Common.Services.StandardConstants.Managed.StandardServices.MessageBroker.Descriptor;

        // =====================================================================
        // IRegistrySvcServiceFactory
        // =====================================================================

		IManagedService IMessageBrokerSvcServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new MessageBrokerSvcService(localEnvironment, dependencyConnector);
        } 

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => MessageBrokerSvcServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(IMessageBrokerSvcServiceFactory factory) =>
            MessageBrokerSvcServiceFactory.factory = factory; 
	}
}