using System.Collections.Generic;
using XKit.Lib.Host;
using XKit.Lib.LocalLog;
using XKit.Lib.Host.Config;
using XKit.Lib.Common.Log;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Linq;
using System;
using XKit.Lib.Common.Host;
using XKit.Lib.Connector.Protocols.Direct;
using XKit.Lib.Common.Registration;
using XKit.Lib.Testing.ConfigSvc.Service;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Threading;
using XKit.Lib.Testing.Mocking;
using Moq;
using XKit.Lib.Common.Utility.Threading;
using XKit.Lib.Testing.MessageBrokerSvc;
using XKit.Lib.Common.Utility.Extensions;
using System.IO;

namespace XKit.Lib.Testing {

    public static partial class TestHostHelper {

        private static SemaphoreSlim synchronizer = new SemaphoreSlim(1, 1);
        private static bool daemonDebugMode = true;
        public static string LocalAddress { get; private set; }
        public static string LocalDataPath { get; private set; }
        public static IHostManager Host { get; private set; }
        public static ILogManager LogManager => HostEnvironmentHelper.LogManager;
        public static IMessageBrokerSvcService TestMessageBrokerService { get; private set; }

        public static void Initialize(
            bool autoAddPlatformServices = true,
            bool loadMetaServices = false,
            bool useDaemonDebugMode = true
        ) {
            daemonDebugMode = useDaemonDebugMode;

            LocalAddress = "localhost";
            LocalDataPath = "./testdatatmp/" + DateTime.Now.ToString("yyyy-MM-dd.HH.mm.ss.fff");

            Host = HostEnvironmentHelper.CreateInitHost(
                instanceClientFactories: new[] { 
                    DirectLocalClientFactory.Factory
                },
                logManagerFactory: LogManagerFactory.Factory,
                localConfigSessionFactory: LocalConfigSessionFactory.Factory,
                hostAddress: LocalAddress,
                localDataStorageFolderPath: LocalDataPath,
                logEventMonitor: LogEntryDebugEmitter,
                logJobMonitor: LogJobDebugEmitter,
                capabilitiesToRegister: loadMetaServices ? null : new string[0] // empty array is "none"
            );

            DirectLocalClientFactory.SetLoadParameters( 
                autoDiscoverLocalServices: false
            );

            if (autoAddPlatformServices) {
                AddService(
                    XKit.Lib.Testing.RegistrySvc.Service.RegistrySvcServiceFactory.Create()
                );
                
                AddService(
                    XKit.Lib.Testing.ConfigSvc.Service.ConfigSvcServiceFactory.Create()
                );

                TestMessageBrokerService = (IMessageBrokerSvcService) AddService(
                    XKit.Lib.Testing.MessageBrokerSvc.Service.MessageBrokerSvcServiceFactory.Create()
                );
            }
        }

        /// <summary>
        /// Adds a mock service to the test environment
        /// </summary>
        /// <param name="descriptor">The Descriptor for the service beig mocked</param>
        /// <param name="createMockApiOperation">a delegate that returns a mock api operation</param>
        /// <typeparam name="TApiInterface">The operation interface for the mock operation</typeparam>
        /// <returns></returns>
        public static IMockService<TApiInterface> AddMockService<TApiInterface>(
            IReadOnlyDescriptor descriptor, 
            Mock<TApiInterface> apiMock
        ) where TApiInterface : class, IServiceApi {
            var service = new MockService<TApiInterface>(
                descriptor,
                apiMock
            );
            Host.AddManagedService(service);
            DirectLocalClientFactory.Factory.AddServiceForDirectAccess(
                service
            );
            return service;
        }

        /// <summary>
        /// Adds a mock service to the test environment
        /// </summary>
        /// <param name="descriptor">The Descriptor for the service beig mocked</param>
        /// <param name="createMockApiOperation">a delegate that returns a mock api operation</param>
        /// <typeparam name="TApiInterface">The operation interface for the mock operation</typeparam>
        /// <returns></returns>
        public static IMockService<TApiInterface> AddMockService<TApiInterface>(
            IReadOnlyDescriptor descriptor, 
            MockBehavior mockBehavior = MockBehavior.Loose
        ) where TApiInterface : class, IServiceCallable {
            var service = new MockService<TApiInterface>(
                descriptor,
                mockBehavior
            );
            Host.AddManagedService(service);
            DirectLocalClientFactory.Factory.AddServiceForDirectAccess(
                service
            );
            return service;
        }

        public static IManagedService AddService(
            IManagedService service
        ) {
            if (daemonDebugMode) {
                service.GetDaemons().ForEach(d => d.SetDebugMode(true));
            }
            Host.AddManagedService(
                service
            );
            DirectLocalClientFactory.Factory.AddServiceForDirectAccess(
                service
            );
            return service;
        }

        public static IManagedService AddCreateService(
            IReadOnlyDescriptor descriptor,
            Type apiOperationInterfaceType
        ) {

            var service = Host.AddCreateManagedService(
                descriptor, 
                apiOperationInterfaceType
            );
            if (daemonDebugMode) {
                service.GetDaemons().ForEach(d => d.SetDebugMode(true));
            }
            DirectLocalClientFactory.Factory.AddServiceForDirectAccess(
                service
            );
            return service;
        }

        public static void StartHost(
            Dictionary<string,object> hostStartupParams = null
        ) {

            Host.StartHost(
                initialRegistryHostAddresses: new[] { LocalAddress }, 
                monitor: null, 
                startupParameters: hostStartupParams, 
                failIfCannotRegister: false
            );
        }

        public static void DestroyHost(bool cleanUpData = true) {
            HostEnvironmentHelper.StopAndDestroyHost();
            Host = null;
            if (cleanUpData) {
                foreach(var f in Directory.EnumerateFiles(LocalDataPath, "*.*", new EnumerationOptions { RecurseSubdirectories = true })) {
                    try { File.Delete(f); } 
                    catch {}
                }
            }
        }

        public static void SetRuntimeConfiguration(
            HostConfigDocument hostConfig = null,
            IDictionary<IReadOnlyDescriptor, object> servicesConfig = null,
            bool clearAllExisting = true
        ) {
            if (clearAllExisting) { 
                ConfigSvcService.ClearAllExisting();
            }
            
            if (hostConfig != null) {
                ConfigSvcService.SetConfigForHost(hostConfig);
            }
            if (servicesConfig != null) {
                foreach(var kv in servicesConfig) {
                    ConfigSvcService.SetConfigForService(kv.Key, kv.Value);
                }
            }

            TaskUtil.RunSyncSafely(() => Host?.RefreshConfigurationFromSource((IRuntimeMonitor)null));
        }

        public async static Task RunTestAsync(
            Func<Task> action,
            [CallerMemberName] string testName = null
        ) {
            await synchronizer.WaitAsync();
            try {
                string separator = new String('=', testName.Length + 22);                
                WriteLineConsole("");
                WriteLineConsole(separator);
                WriteLineConsole($"========== {testName} ==========");
                WriteLineConsole(separator);
                await action();
                WriteLineConsole("---------------------------------------------");
                WriteLineConsole("");
            } catch(Exception ex) {
                WriteLineConsole("EXCEPTION!!! -->");
                WriteLineConsole(ex.Message);
                WriteLineConsole(ex.StackTrace);
                WriteLineConsole("<--");
                throw;
            } finally {
                synchronizer.Release();
            } 
        }

        public static void RunTest(
            Action action,
            [CallerMemberName] string testName = null
        ) {
            synchronizer.Wait();
            try {
                string separator = new String('=', testName.Length + 22);                
                WriteLineConsole("");
                WriteLineConsole(separator);
                WriteLineConsole($"========== {testName} ==========");
                WriteLineConsole(separator);
                action();
                WriteLineConsole("---------------------------------------------");
                WriteLineConsole("");
            } catch(Exception ex) {
                WriteLineConsole("EXCEPTION!!! -->");
                WriteLineConsole(ex.Message);
                WriteLineConsole(ex.StackTrace);
                WriteLineConsole("<--");
                throw;
            } finally {
                synchronizer.Release();
            } 
        }

        private static void LogJobDebugEmitter(
            IReadOnlyLogJobEntry job, 
            IEnumerable<IReadOnlyLogEntryData> data, 
            bool isEntry
        ) {

            if (!job.CompleteTimestamp.HasValue) {
                WriteLineConsole($"[[ Operation Started: {job.JobName} ]]");
            } else {
                WriteLineConsole($"[[] Operation Ended: {job.JobName} ]]");
            }
        }

        private static void LogEntryDebugEmitter(
            IReadOnlyLogJobEntry job, 
            IReadOnlyLogEventEntry entry, 
            IEnumerable<IReadOnlyLogEntryData> data
        ) {
            
            WriteLineConsole($">>>> {entry.EventTypeName} {entry.Timestamp}");
            WriteLineConsole($"Operation: {job?.JobName}");
            if (!string.IsNullOrEmpty(entry.Message)) { WriteLineConsole($"Message: {entry.Message}"); }
            //if (entry.Data != null) { LogObject("Data", entry.Data); }
            //if (entry.Attributes?.Any() == true) { LogDictionary("Attributes", entry.Attributes); }
            if (entry.Code != null) { WriteLineConsole($"code: {entry.Code}"); }
            if (entry.Tags?.Any() == true) { WriteLineConsole($"tags: {JsonConvert.SerializeObject(entry.Tags, Formatting.Indented)}"); }
            WriteLineConsole("<<<<");
        }

        private static void LogDictionary(string title, IDictionary<string, object> dict) {
            if (dict != null && dict.Any()) {
                WriteLineConsole($">> {title}");
                foreach(var kv in dict) {
                    LogObject(kv.Key, kv.Value);
                }
                WriteLineConsole($"<< {title}");
            }
        }
        
        private static void LogObject(string name, object obj) {
            WriteLineConsole($"-> {name}: {JsonConvert.SerializeObject(obj ?? new object(), Formatting.Indented)}");
            WriteLineConsole($"<- {name}");
        }

        public static void WriteLineConsole(string message) {
            Console.WriteLine(message);
            Debug.WriteLine(message);
        }
    }
}