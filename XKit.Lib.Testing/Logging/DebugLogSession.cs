using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Testing.Logging {

    public class DebugLogSession : ILogSession {

        private StreamWriter logFileWriter;
        private DebugLogManager DebugLogManager;
        private DebugLogWriter logWriter;

        public IDictionary<string, object> AutoLogValues { get; } = new Dictionary<string, object>();

        public DebugLogSession(DebugLogManager debugLogManager) {
            this.DebugLogManager = debugLogManager;
            logWriter = new DebugLogWriter(this);
        }

        public void WriteLine(string message) {
            WriteLineInternal(message);
            FlushLogFile();
        }

        public Task BeginLog(object workItem, IDictionary<string, object> parameters, LoggingOptions loggingOptions) {
            BeginLogBackground(workItem, parameters, loggingOptions);
            return Task.CompletedTask;
        }

        public Task PendingLog(
            IDictionary<string, object> pendingLogParameters,
            string statusMessage
        ) {
            PendingLogBackground(pendingLogParameters, statusMessage);
            return Task.CompletedTask;
        }

        public Task EndLog(
            JobResultStatusEnum? status,
            string statusMessage,
            object result,
            object code,
            JobReplayStrategyEnum replayStrategy,
            object replayData
        ) {
            WriteLineInternal($"^^^^^^^^^^ End Log for {DebugLogManager.TestName} ^^^^^^^^^^");
            LogDictionary("Auto Log Values", AutoLogValues);
            WriteLineInternal($"Result: {result }");
            WriteLineInternal($"Code: {code }");
            WriteLineInternal(DateTime.Now.ToString());
            WriteLineInternal("");
            FlushLogFile();
            logFileWriter.Dispose();
            logFileWriter = null;
            return Task.CompletedTask;
        }

        public ILogReader GetLogReader() {
            throw new NotImplementedException();
        }

        public ILogWriter GetLogWriter() => logWriter;

        public void BeginLogBackground(object workItem, IDictionary<string, object> parameters, LoggingOptions loggingOptions) {
            var loggerFactory = new LoggerFactory();
            var logFile = $"{AppContext.BaseDirectory}/{DebugLogManager.TestName}.test.log";
            if (File.Exists(logFile)) {
                using(FileStream fileStream = new FileStream(logFile, FileMode.Truncate)) {
                    fileStream.SetLength(0);
                    fileStream.Close();
                }
            }
            logFileWriter = File.AppendText(logFile);
            WriteLineInternal($"********** Begin Log for {DebugLogManager.TestName} ***********");
            WriteLineInternal(DateTime.Now.ToString());
            LogObject("Operation work item (request)", workItem);
            LogDictionary("Operation parameters", parameters);
            WriteLineInternal("");
            FlushLogFile();
        }

        public void PendingLogBackground(IDictionary<string, object> pendingLogParameters, string statusMessage) {
            WriteLineInternal(DateTime.Now.ToString());
            WriteLineInternal($"-- state transition for {DebugLogManager.TestName} ---");
            WriteLineInternal(DateTime.Now.ToString());
            LogDictionary("Operation parameters", pendingLogParameters);
            WriteLineInternal($"status message: {statusMessage}");
        }

        // =====================================================================
        // IDisposable
        // =====================================================================

        void IDisposable.Dispose() { }

        // =====================================================================
        // private utility
        // =====================================================================

        private void LogDictionary(string title, IDictionary<string, object> dict) {
            if (dict != null && dict.Any()) {
                WriteLineInternal($"==== {title} ==== ");
                foreach (var kv in dict) {
                    LogObject(kv.Key, kv.Value);
                }
            }
        }

        private void LogObject(string name, object obj) {
            WriteLineInternal($">> {name}: {JsonConvert.SerializeObject(obj ?? new object(), Formatting.Indented)}");
        }

        private void WriteLineInternal(string message) {
            Console.WriteLine(message);
            Debug.WriteLine(message);
            logFileWriter.WriteLine(message);
        }

        private void FlushLogFile() => logFileWriter?.Flush();

        public Task ExecuteStartupMaintenance() {
            return Task.CompletedTask;
        }

        public void EndLogBackground(JobResultStatusEnum? status = JobResultStatusEnum.Incomplete, string statusMessage = null, object operationResult = null, object operationCode = null, JobReplayStrategyEnum replayStrategy = JobReplayStrategyEnum.NotSpecifiedOrUnknown, object replayData = null) {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Guid>> ArchiveJobs(IEnumerable<Guid> jobIds = null, string matchingJobName = null, JobTypeEnum? matchingJobType = null, DateTime? startOnOrBeforeTimestamp = null, DateTime? startOnOrAfterTimestamp = null, bool? isComplete = null, DateTime? completeOnOrBeforeTimestamp = null, DateTime? completeOnOrAfterTimestamp = null, string matchingCorrelationId = null, string havingCorrelationTag = null, int limitCount = 1000, Func<IReadOnlyLogEventEntry, bool> eventSelector = null, string archiveDbFile = null, bool deleteArchivedJobsFromSource = false, int waitBetweenStepsMilliseconds = 0) {
            throw new NotImplementedException();
        }
    }
}