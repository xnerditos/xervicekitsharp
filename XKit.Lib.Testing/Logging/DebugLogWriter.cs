using System.Collections.Generic;
using System.Linq;
using XKit.Lib.Common.Log;
using XKit.Lib.Common.Utility.Extensions;
using Newtonsoft.Json;
using System.Threading.Tasks;
using XKit.Lib.LocalLog.Entities;
using System;

namespace XKit.Lib.Testing.Logging {
    public class DebugLogWriter : ILogWriter {
        private DebugLogSession logSession;

        public DebugLogWriter(DebugLogSession debugLogSession) {
            this.logSession = debugLogSession;
        }

        // =====================================================================
        // ILogWriter
        // =====================================================================

        ILogWriter ILogWriter.AutoLog(string key, object value) {
            logSession.AutoLogValues[key] = value;
            return this;
        }

        ILogWriter ILogWriter.AutoLog(IDictionary<string, object> values) {
            foreach(var kv in values) {
                logSession.AutoLogValues[kv.Key] = kv.Value;
            }
            return this;
        }

        ILogWriter ILogWriter.AutoLog(object anonObjectAsKeysAndValues) {
            foreach(var kv in anonObjectAsKeysAndValues.FieldsToDictionary()) {
                logSession.AutoLogValues[kv.Key] = kv.Value;
            }
            return this;
        }

        Task<IReadOnlyLogEventEntry> ILogWriter.Error(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Error, 
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        void ILogWriter.ErrorBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Error, 
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.ErrorAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        void ILogWriter.ErrorAsBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Erratum(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Erratum, 
                attributes: attributes,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        void ILogWriter.ErratumBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Erratum, 
                attributes: attributes,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.ErratumAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        void ILogWriter.ErratumAsBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Fatality(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Fatality,
                message: message,
                attributes: attributes,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        void ILogWriter.FatalityBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Fatality,
                message: message,
                attributes: attributes,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Info(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Info,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags            
        );

        void ILogWriter.InfoBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Info,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags            
        );

        Task<IReadOnlyLogEventEntry> ILogWriter.NewEvent(
            LogEventType eventType, 
            string message, 
            object data, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            bool isPrivate
        ) => WriteEntry(
                type: eventType,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        void ILogWriter.NewEventBackground(
            LogEventType eventType, 
            string message, 
            object data, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            bool isPrivate
        ) => WriteEntry(
                type: eventType,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.NewEvent(
            string eventTypeName, 
            string message, 
            object data, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            bool isPrivate
        ) => WriteEntry(
                typeAsString: eventTypeName,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        void ILogWriter.NewEventBackground(
            string eventTypeName, 
            string message, 
            object data, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            bool isPrivate
        ) => WriteEntry(
                typeAsString: eventTypeName,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Snapshot(
            IReadOnlyDictionary<string, object> attributes, 
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Snapshot,
                attributes: attributes,
                code: code,
                tags: tags
            );

        void ILogWriter.SnapshotBackground(
            IReadOnlyDictionary<string, object> attributes, 
            object data, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Snapshot,
                attributes: attributes,
                code: code,
                tags: tags
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Status(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Status,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        void ILogWriter.StatusBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Status,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Trace(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        void ILogWriter.TraceBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Error,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.TraceAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Trace,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        void ILogWriter.TraceAsBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code,
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Trace,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Warning(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Warning,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        void ILogWriter.WarningBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string callerFilePath, 
            string callerMemberName, 
            int callerLineNumber
        ) => WriteEntry(
                type: LogEventType.Warning,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: callerFilePath,
                callerMemberName: callerMemberName,
                callerLineNumber: callerLineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.WarningAs(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Warning,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        void ILogWriter.WarningAsBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags, 
            string filePath, 
            string memberName, 
            int lineNumber
        ) => WriteEntry(
                type: LogEventType.Warning,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags,
                callerFilePath: filePath,
                callerMemberName: memberName,
                callerLineNumber: lineNumber
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Audit(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Audit,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        void ILogWriter.AuditBackground(
            string message, 
            IReadOnlyDictionary<string, object> attributes, 
            object code, 
            IEnumerable<string> tags
        ) => WriteEntry(
                type: LogEventType.Audit,
                message: message,
                attributes: attributes,
                code: code,
                tags: tags
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.Begin(object workItem, IReadOnlyDictionary<string, object> parameters) 
            => WriteEntry(
                type: LogEventType.Enter,
                data: workItem,
                attributes: parameters
            );

        Task<IReadOnlyLogEventEntry> ILogWriter.End(
            JobResultStatusEnum status, 
            string message, 
            object resultData, 
            object resultCode,
            JobReplayStrategyEnum? replayStrategy,
            object replayData
        ) 
            => WriteEntry(
                type: LogEventType.Exit,
                message: message,
                code: resultCode
            );
        
        Task<IReadOnlyLogEventEntry> ILogWriter.ReplaceReplayHint(
            Guid forJobId, 
            JobReplayStrategyEnum replayStrategy, 
            object replayData
        ) => Task.FromResult<IReadOnlyLogEventEntry>(null);

        void ILogWriter.ReplaceReplayHintBackground(
            Guid forJobId, 
            JobReplayStrategyEnum replayStrategy, 
            object replayData
        ) {} 

        // =====================================================================
        // utility
        // =====================================================================

        private Task<IReadOnlyLogEventEntry> WriteEntry(
            string typeAsString = null, 
            LogEventType? type = null, 
            string message = null,
            object data = null, 
            IReadOnlyDictionary<string, object> attributes = null, 
            object code = null, 
            IEnumerable<string> tags = null, 
            string callerFilePath = null, 
            string callerMemberName = null, 
            int callerLineNumber = 0
        ) {
            
            logSession.WriteLine($"---- {typeAsString ?? type?.ToString()}");
            if (message != null) { 
                logSession.WriteLine($">> message: {message}"); 
            }
            if (data != null) { 
                logSession.WriteLine($">> data: {JsonConvert.SerializeObject(data, Formatting.Indented)}");
            }
            if (attributes?.Any() == true) { 
                logSession.WriteLine($">> attributes:");
                foreach(var kv in attributes) {
                    logSession.WriteLine($"- {kv.Key}={kv.Value}");
                }
            }
            if (code != null) { logSession.WriteLine($">> code: {code}"); }
            if (tags?.Any() == true) { logSession.WriteLine($"tags: {JsonConvert.SerializeObject(tags, Formatting.Indented)}"); }
            if (callerFilePath != null) {logSession.WriteLine($"location: {callerFilePath} - {callerMemberName} - {callerLineNumber}"); }
            logSession.WriteLine("");
            return Task.FromResult((IReadOnlyLogEventEntry) new LogEventEntry {
                Attributes = attributes?.Select(kv => new LogAttribute { Name = kv.Key, Value = kv.Value?.ToString() }).ToArray(),
                Code = code?.ToString(),
                EventTypeName = type?.ToString() ?? typeAsString ?? LogEventType.None.ToString(),
                Message = message,
                Tags = tags?.Select(s => new LogTag { Name = s }).ToArray()
            });
        }
    }
}