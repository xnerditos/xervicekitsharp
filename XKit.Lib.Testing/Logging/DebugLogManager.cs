using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using XKit.Lib.Common.Log;

namespace XKit.Lib.Testing.Logging {
    public class DebugLogManager : ILogManager {
                
        private ILogSession logSession;

        public string TestName { get; private set; }

        public DebugLogManager() {
            logSession = new DebugLogSession(this);
            TestName = "* initialization";
        }

        // =====================================================================
        // Public
        // =====================================================================
        
        public void SetTestName(string testName) {
            TestName = testName;
        }

        // =====================================================================
        // ILogManager
        // =====================================================================
        
        ILogSession ILogManager.CreateReadOnlySession() {
            return logSession;
        } 

        ILogSession ILogManager.CreateWriteableSession(
            JobTypeEnum jobType, 
            JobSystemEffect effect, 
            string originatorName, 
            int originatorVersion, 
            string name, 
            string hostId, 
            string instanceId, 
            Guid jobId, 
            string correlationId, 
            string requestorFabricId, 
            string requestorInstanceId, 
            IEnumerable<string> correlationTags
        ) { 
            return logSession;
        }

        public void Finish() { }
    }
}