using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;

namespace XKit.Lib.Testing.RegistrySvc.Service {

    public interface IRegistrySvcServiceFactory : IServiceFactory {
		IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        );
    }

	public class RegistrySvcServiceFactory : IRegistrySvcServiceFactory
	{
		private static IRegistrySvcServiceFactory factory = new RegistrySvcServiceFactory();

		public static IRegistrySvcServiceFactory Factory => factory;

        IReadOnlyDescriptor IServiceFactory.Descriptor => XKit.Lib.Common.Services.StandardConstants.Managed.StandardServices.Registry.Descriptor;

        // =====================================================================
        // IMockServiceFactory
        // =====================================================================

        IManagedService IRegistrySvcServiceFactory.Create(ILocalHostEnvironment localEnvironment, IDependencyConnector dependencyConnector) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new RegistrySvcService(localEnvironment, dependencyConnector);
        }

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => RegistrySvcServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(IRegistrySvcServiceFactory factory) =>
            RegistrySvcServiceFactory.factory = factory;
    }
}