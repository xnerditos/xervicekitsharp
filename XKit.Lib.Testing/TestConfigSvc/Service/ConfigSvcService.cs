using System.Collections.Generic;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Utility;
using XKit.Lib.Common.Utility.Extensions;
using XKit.Lib.Host.DefaultBaseClasses;

namespace XKit.Lib.Testing.ConfigSvc.Service {

    public class ConfigSvcService : ManagedService<ConfigSvcOperation>, IConfigSvcService {

        private static Dictionary<string, string> ConfigObjects = new Dictionary<string, string>();

        private static readonly IReadOnlyDescriptor descriptor = XKit.Lib.Common.Services.StandardConstants.Managed.StandardServices.Config.Descriptor;

        public static void ClearAllExisting() {
            ConfigObjects.Clear();
        }

        public static void SetConfigForService(IReadOnlyDescriptor descriptor, object configObject) {
            ConfigObjects[Common.Utility.Identifiers.GetServiceVersionLevelKey(descriptor)] = configObject.ToJson(pretty: true);
        }

        public static void SetConfigForHost(HostConfigDocument configObject) {
            ConfigObjects[string.Empty] = configObject.ToJson(pretty: true);
        }

        // =====================================================================
        // overrides
        // =====================================================================

        protected override IReadOnlyDescriptor Descriptor => descriptor;

        protected override IEnumerable<IReadOnlyDescriptor> Dependencies => new IReadOnlyDescriptor[0];

        protected override IReadOnlyServiceCallPolicy CallPolicy => DefaultPolicy;

        protected override string ConfigurationDocumentIdentifier => Identifiers.GetServiceVersionLevelKey(descriptor);

        // =====================================================================
        // construction
        // =====================================================================

        public ConfigSvcService(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) : base(localEnvironment, dependencyConnector) { }

        // =====================================================================
        // GetConfigForService
        // =====================================================================

        string IConfigSvcService.GetConfigJsonForService(string serviceKey) {
            string json;
            ConfigObjects.TryGetValue(serviceKey, out json);
            return json;
        }

        HostConfigDocument IConfigSvcService.GetConfigForHost() {
            string json;
            ConfigObjects.TryGetValue(string.Empty, out json);
            return json.FromJson<HostConfigDocument>() ?? new HostConfigDocument();
        }
    }
}