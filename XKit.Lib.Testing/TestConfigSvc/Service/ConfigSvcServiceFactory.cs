using System;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;
using XKit.Lib.Common.ObjectInstantiation;
using XKit.Lib.Common.Registration;
using XKit.Lib.Common.Services;
using XKit.Lib.Testing.RegistrySvc.Service;

namespace XKit.Lib.Testing.ConfigSvc.Service {

    public interface IConfigSvcServiceFactory : IServiceFactory {
		IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        );
    }

	public class ConfigSvcServiceFactory : IConfigSvcServiceFactory
	{
		private static IConfigSvcServiceFactory factory = new ConfigSvcServiceFactory();

		public static IConfigSvcServiceFactory Factory => factory;

        IReadOnlyDescriptor IServiceFactory.Descriptor => XKit.Lib.Common.Services.StandardConstants.Managed.StandardServices.Config.Descriptor;

        // =====================================================================
        // IRegistrySvcServiceFactory
        // =====================================================================

		IManagedService IConfigSvcServiceFactory.Create(
            ILocalHostEnvironment localEnvironment,
            IDependencyConnector dependencyConnector
        ) {
            localEnvironment = localEnvironment ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<ILocalHostEnvironment>(); 
            dependencyConnector = dependencyConnector ?? InprocessGlobalObjectRepositoryFactory.CreateSingleton().GetObject<IDependencyConnector>();
            if (localEnvironment == null) { throw new ArgumentNullException(nameof(localEnvironment)); }
            if (dependencyConnector == null) { throw new ArgumentNullException(nameof(dependencyConnector)); }
            return new ConfigSvcService(localEnvironment, dependencyConnector);
        } 

        // =====================================================================
        // Static methods
        // =====================================================================

        public static IManagedService Create(
            ILocalHostEnvironment localEnvironment = null,
            IDependencyConnector dependencyConnector = null
        ) => ConfigSvcServiceFactory.Factory.Create(localEnvironment, dependencyConnector);

        public static void InjectCustomFactory(IConfigSvcServiceFactory factory) =>
            ConfigSvcServiceFactory.factory = factory; 
	}
}