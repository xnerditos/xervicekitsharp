using XKit.Lib.Common.Host;

namespace XKit.Lib.Testing.ConfigSvc {
    public interface IConfigSvcService : IManagedService, IServiceBase {

        string GetConfigJsonForService(string serviceIdentifier);
        HostConfigDocument GetConfigForHost();
    }
}